# Runner setup

## Docker limitations
Cannot setup docker containers through github action because runners are on Windows.
won't make some investment on containerized runner before having this capability (linux runners available, or container supported for windows runners from github).

## Prerequisite for install
Some of these programs are not needed but currently installed

### Supported OS
Windows x64

### Mandatory
- Git + LFS
- Google drive sync
- Unity Hub
- Unity **version** (current 2019.3.0f6) installed with Unity Hub (because of installation path)
- Blender

### Usefull
- Remote Desktop
- Sublim Text 3
- Github desktop
- Visual Studio Community

- Gimp
- Affinity

### Steps to perform so that everything works

- Start git bash once to have value in the PATH (add git.exe location to the path)
- On Powershell : 'set-executionpolicy unrestricted' to allow script execution on runner

### Repository requirement

TryStartProcess to handle exit codes to make sure every error is stopping CI execution.

#Server requirement

IIS installed : https://www.itnota.com/install-iis-windows/

##Website access setup

## Internet access
	No-ip uses pixsaoul.ddns.net 