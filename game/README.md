# Chromaze.game

Latest build available in https://drive.google.com/drive/folders/1_t8cReUfPfu94hG9Z3QfsWS3VxXuESpO?usp=sharing
Discord of the project : https://discord.gg/RmYyWn Don't hesitate to come here for help about level creation, trying out some home made levels and share your own creations !

(temp solution for storing builds)
available in Android and windows standalone.

# How to install

## Windows
copy the build your want to try, for example 0.1.110564433 and paste it wherever you want. You can then start the game by launching Chromaze.exe.
There is currently no complete installer.

## Android
In the build folder, take the apk located in Android. COpy this onto your android device and from there launch the apk, this will start the installation.

# How to share levels
As there is currently no online cloud solution for levels, you can take them from the folder Chromaze_Data/StreamingAssets, and send it to your friend, they just have to paste it their too.

# issues

the best way to contact me is by creating an issue here. If somehow this is not really related issue material, contact
me at Chromaze@gmail.com

# contribution

Right now this project is only used for research purpose and training purpose, so I don't really need / want any particuliar help on how to solve some issues. However I would love some feedback on the current state of the game, issues with it and ideas you may have, so that I can focus on useful research ;)
