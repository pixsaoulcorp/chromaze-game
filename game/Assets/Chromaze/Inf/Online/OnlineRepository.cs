﻿using Chromaze.App.Online;
using Chromaze.Dom.Data.Payload;
using Pixsaoul.App.Persistency;
using Pixsaoul.Dom.Data;
using System;
using System.Collections.Generic;

namespace Chromaze.Inf.Online
{
    public class OnlineRepository : IOnlineRepository
    {
        [Inject] public IAsyncPersistencyService asyncPersistency { get; set; }

        public void Initialize()
        {
            _levelList = new EndPoint(new List<string>() { "api", "levels" });
            _downloadLevel = new EndPoint(new List<string>() { "api", "levels" });
            _uploadLevel = new EndPoint(new List<string>() { "api", "levels" }) ;
        }

        private EndPoint _levelList;
        private EndPoint _downloadLevel;
        private EndPoint _uploadLevel;

        public void TryGetLevelInfoList(Action<List<LevelForListOfLevel>> callback)
        {            
            asyncPersistency.RequestRead<List<Persistency.Payload.LevelForListsOfLevel>>(_levelList,
                (List<Persistency.Payload.LevelForListsOfLevel> lst) => PersistToDomCallback(lst, callback));
        }

        private void PersistToDomCallback(List<Persistency.Payload.LevelForListsOfLevel> persistList, Action<List<LevelForListOfLevel>> domCallback)
        {
            List<LevelForListOfLevel> infos = new List<LevelForListOfLevel>();
            for(int i = 0; i < persistList.Count; i++)
            {               
                infos.Add(persistList[i].ToDom());
            }
            domCallback(infos);
        }

        public void TrySetLevel(Persistency.Payload.LevelForCreation levelForCreation)
        {
            asyncPersistency.RequestWrite(levelForCreation, _uploadLevel);
        }

        public void TryGetLevel(LevelForListOfLevel levelInfo, Action<Dom.Data.Level> callback)
        {
            EndPoint levelpoint = EndPoint.Concat(_downloadLevel, new EndPoint(levelInfo.Id.ToString()));
            asyncPersistency.RequestRead<Persistency.Payload.LevelToStore>(levelpoint,
                (Persistency.Payload.LevelToStore p) => OnOnlineLevelReceived(p, callback));
        }

        private void OnOnlineLevelReceived(Persistency.Payload.LevelToStore onlineLevel, Action<Dom.Data.Level> callback)
        {
            Dom.Data.Level level = onlineLevel.ToDom();
            callback(level);
        }
    }
}
