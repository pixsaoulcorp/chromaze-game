using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.MaterialSwitch
{
    public class MaterialSwitch : MonoBehaviour
    {
        public List<Material> _materials;

        //Black, "Black" },
        //Blue, "Blue" },
        //Cyan, "Cyan" },
        //Green, "Green" },
        //Magenta, "Magenta" },
        //Red, "Red" },
        //White, "White" }
        //Yellow, "Yellow" },

        public void SetMaterial(int id, MeshRenderer meshRenderer)
        {
            meshRenderer.material = _materials[id];
        }

        private int GetColorId(Dom.Data.Color color)
        {
            if (color.Equals(Dom.Data.Color.BlackC))
                return 0;
            if (color.Equals(Dom.Data.Color.BlueC))
                return 1;
            if (color.Equals(Dom.Data.Color.CyanC))
                return 2;
            if (color.Equals(Dom.Data.Color.GreenC))
                return 3;
            if (color.Equals(Dom.Data.Color.MagentaC))
                return 4;
            if (color.Equals(Dom.Data.Color.RedC))
                return 5;
            if (color.Equals(Dom.Data.Color.WhiteC))
                return 6;
            else //(color.Equals(Dom.Data.Color.Yellow)
                return 7;
        }

        public Material GetMaterialFromColor(Dom.Data.Color color)
        {
            if (color.Equals(Dom.Data.Color.BlackC))
                return _materials[0];
            if (color.Equals(Dom.Data.Color.BlueC))
                return _materials[1];
            if (color.Equals(Dom.Data.Color.CyanC))
                return _materials[2];
            if (color.Equals(Dom.Data.Color.GreenC))
                return _materials[3];
            if (color.Equals(Dom.Data.Color.MagentaC))
                return _materials[4];
            if (color.Equals(Dom.Data.Color.RedC))
                return _materials[5];
            if (color.Equals(Dom.Data.Color.WhiteC))
                return _materials[6];
            else //(color.Equals(Dom.Data.Color.Yellow)
                return _materials[7];
        }

        public void SetMaterial(Dom.Data.Color color, MeshRenderer meshRenderer)
        {
            SetMaterial(GetColorId(color), meshRenderer);
        }
       
    }
}