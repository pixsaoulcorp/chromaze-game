﻿using Pixsaoul.Fou.Engine;
using System;
using UnityEngine;

namespace Chromaze.Inf.Player
{
    public class CameraViewPointDirectionProvider : BaseBehaviour, IDirectionProvider
    {
        private Camera _followingCamera;

        public void UpdateDirectionSource()
        {
            _followingCamera = this.GetComponentInChildren<Camera>();
            if(_followingCamera == null)
            {
                throw new Exception("No Camera available for input");
            }
        }

        public Vector3 GetForward()
        {
            return _followingCamera.transform.forward;
        }

        public Vector3 GetRight()
        {
            return _followingCamera.transform.right;
        }

        public Vector3 GetUp()
        {
            return _followingCamera.transform.up;
        }
    }
}
