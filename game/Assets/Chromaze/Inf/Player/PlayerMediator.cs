﻿
using Chromaze.App.Player;
using Chromaze.App.SceneWorkflow;
using Chromaze.Inf.Components.Start;
using Pixsaoul.App.Camera;
using Pixsaoul.App.UserInput;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using System;
using UnityEngine;

namespace Chromaze.Inf.Player
{
    public class PlayerMediator : Mediator
    {
        //expected workflow :
        /* Allow player movement when level is started
         * then, when we select an item, disable inputs on the player, and add it on the component.
         * when leaving the component control, disable control on it, and re enable it on player
         */
        [Inject] public NotifyMainAxisInputSignal NotifyMainAxisInputSignal { get; set; }
        [Inject] public NotifyMainButtonInputSignal NotifyMainButtonInputSignal { get; set; }
        [Inject] public NotifyLevelEndedSignal NotifyLevelEndedSignal { get; set; }

        [Inject] public EnablePlayerMovementSignal ControlPlayerSignal { get; set; }

        [Inject] public RequestSetPlayerLocationSignal ProvidePlayerLocationSignal { get; set; }

        [Inject] public SelectThirdPersonCameraSignal SelectThirdPersonCameraSignal { get; set; }

        protected override View GetView()
        {
            return view;
        }

        [Inject] public PlayerView view { get; set; }

        private Vector2 _stackedInputs;

        public override void OnRegister()
        {
            base.OnRegister();
            ProvidePlayerLocationSignal.AddListener(SetPlayerLocation);
            ControlPlayerSignal.AddListener(EnableControl);
            NotifyLevelEndedSignal.AddListener(DisableControl);
        }

        public override void OnRemove()
        {
            base.OnRemove();
            ProvidePlayerLocationSignal.RemoveListener(SetPlayerLocation);
            NotifyLevelEndedSignal.RemoveListener(DisableControl);
            ControlPlayerSignal.RemoveListener(EnableControl);

            // also remove those that can be listened at otherpoint in the use
            NotifyMainAxisInputSignal.RemoveListener(MovePlayer);
            NotifyMainButtonInputSignal.RemoveListener(TryComponentSelection);
        }
        
        public override void Initialize()
        {
            base.Initialize();
            _stackedInputs = Vector2.zero;
        }

        /// <summary>
        /// Called when [level loaded].
        /// </summary>
        /// <param name="param">The parameter.</param>
        private void SetPlayerLocation(Position position, Rotation rotation)
        {
             view.MoveToLocation(position, rotation);
        }

        private void EnableControl()
        {
            NotifyMainAxisInputSignal.AddListener(MovePlayer);
            SelectThirdPersonCameraSignal.Dispatch(view._thirdPersonAttachTarget.Result);
            NotifyMainButtonInputSignal.AddListener(TryComponentSelection);
            view.BeginControl();
        }

        private void DisableControl()
        {
            NotifyMainAxisInputSignal.RemoveListener(MovePlayer);
            NotifyMainButtonInputSignal.RemoveListener(TryComponentSelection);
            view.EndControl();
        }

        void MovePlayer(Vector2 direction)
        {
            _stackedInputs += direction;
        }

        void FixedUpdate()
        {
            view.Move(_stackedInputs.normalized);
            _stackedInputs = Vector2.zero;
        }

        void TryComponentSelection()
        {
            bool componentSelected = view.SelectTarget();
            if (componentSelected)
            { // we stop listening for input if we start controlling a component
                DisableControl();              
            }
        }
    }
}