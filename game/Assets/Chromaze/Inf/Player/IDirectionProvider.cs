﻿using UnityEngine;

namespace Chromaze.Inf.Player
{
    [System.Serializable]
    public class IDirectionProviderContainer : IUnifiedContainer<IDirectionProvider> { }
    public interface IDirectionProvider
    {
        void UpdateDirectionSource();

        Vector3 GetForward();
        Vector3 GetRight();
        Vector3 GetUp();
    }
}
