﻿
using Pixsaoul.App.Selection;
using Pixsaoul.App;
using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Inf.PhysicsEngine;
using Pixsaoul.Inf.Selection;

namespace Chromaze.Inf.Player
{
    public class PlayerView : View
    {
        [SerializeField] private Rigidbody _body;
        [SerializeField] private IColliderContainer _collider;
        [SerializeField] private Transform _playerLocation;
        [SerializeField] private ISelectorContainer _selector;
        [SerializeField] private PreselectionIndicator _selectorIndicator;

        [SerializeField] public IAttachTargetContainer _thirdPersonAttachTarget;

        [SerializeField] public IDirectionProviderContainer _directionProvider;

        [SerializeField] private float _speed;


        public override void Initialize()
        {
            base.Initialize();
            _selector.Result.Initialize();
            _selectorIndicator.Initialize();
        }

        /// <summary>
        /// Moves to transform.
        /// </summary>
        /// <param name="root">The root.</param>
        internal void MoveToLocation(Position position, Rotation rotation)
        {
            _body.transform.position = position;
            _body.transform.rotation = Quaternion.LookRotation(rotation.ToEuler()); //TODO TBU check if this work

            SyncLocation();
        }

        public void Move(Vector2 direction)
        {
            if (_collider.Result.IsColliding()) // is grounded
            {
                //Vector3 forward = this.transform.position - _pointOfView.transform.position;
                Vector3 forward = _directionProvider.Result.GetForward(); // TODO TBU find a way to follow camera anyway
                forward.y = 0;
                forward.Normalize();

                Vector3 right = Quaternion.AngleAxis(90, Vector3.up) * forward;
                //right.y = 0;
                right.Normalize();

                _body.AddForce(_speed * (right * direction.x + forward * direction.y), ForceMode.VelocityChange);
                Debug.DrawLine(this.transform.position, this.transform.position + _speed * (right * direction.x + forward * direction.y));
            }
        }

        public bool SelectTarget()
        {
            bool success = _selector.Result.TryApplySelection();
            return success;
        }

        private void LateUpdate()
        {
            SyncLocation();
        }

        /// <summary>
        /// Synchronize logic location and physical location.
        /// Used because the rigid body that moves in child of the logical view.
        /// </summary>
        private void SyncLocation()
        {
            _playerLocation.transform.position = _body.transform.position;
        }

        public void BeginControl()
        {
            _directionProvider.Result.UpdateDirectionSource();
        }

        public void EndControl()
        {

        }
    }
}