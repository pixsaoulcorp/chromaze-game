﻿using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Player
{
    public class RequestSetPlayerLocationSignal : Signal<Position,Rotation> { }
}
