﻿using System;
using Chromaze.App.Environment;
using Chromaze.App.Optics;
using Pixsaoul.App.Color;
using Pixsaoul.Fou.Engine;
using UnityEngine;
using System.Reactive.Subjects;

namespace Chromaze.Inf.Environment
{
    public class Door : BaseBehaviour, IDoor, IColorProvider
    {
        private bool _opened;

        [SerializeField]
        private GameObject _openablePart;

        [SerializeField] private IColorableContainer _colorablePart;

        [SerializeField] private IMonoReceiverContainer _receiver;



        private Chromaze.Dom.Data.Color _colorKey;

        public Dom.Data.Color ColorKey
        {
            get => _colorKey;
            set
            {
                _colorKey = value;
                _keyColorSubject.OnNext(value);
                _colorablePart.Result.Colorize(_colorKey.ToColor());
            }
        }

        private BehaviorSubject<Dom.Data.Color> _keyColorSubject = new BehaviorSubject<Dom.Data.Color>(Dom.Data.Color.BlackC);
        public IObservable<Dom.Data.Color> ObservableColor => _keyColorSubject;

        public void Initialize()
        {

        }

        public void SetData(Dom.Data.Door door)
        {
            _receiver.Result.ConfigureReception();
            ColorKey = door.Color;
            Close();
        }

        private void Update()
        {
            CheckReception();
        }

        private void CheckReception()
        {
            // based on Reception / reception color / previously opened close
            // analysis chat R C O (reception, color, opened)
            /*
             * R C O -> -
             * 0 0 0 -> -
             * 0 0 1 -> CLOSE
             * 0 1 0 -> -
             * 0 1 1 -> -
             * 1 0 0 -> -
             * 1 0 1 -> CLOSE
             * 1 1 0 -> OPEN
             * 1 1 1 -> -
             */
            IMonoReceiver receiver = _receiver.Result;
            if (receiver.IsReceiving == Dom.Data.ReceptionStatus.NotReceiving && _opened) // 0 0 1
            {
                Close();
            }
            else if (receiver.IsReceiving == Dom.Data.ReceptionStatus.Receiving) // 1 X X
            {
                if (!Dom.Data.Color.Equals(receiver.ReceivedRay?.Color, ColorKey) && _opened) // 1 0 1
                {
                    Close();
                }
                else if (Dom.Data.Color.Equals(receiver.ReceivedRay?.Color, ColorKey) && !_opened) // 1 1 0
                {
                    Open();
                }
            }
        }

        public void Open()
        {
            _opened = true;
            _openablePart.SetActive(false);
        }

        public void Close()
        {
            _opened = false;
            _openablePart.SetActive(true);
        }
    }
}
