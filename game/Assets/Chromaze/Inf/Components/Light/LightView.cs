
using Pixsaoul.Dom.Data.Engine;
using UnityEngine;

namespace Chromaze.Inf.Components.Light
{
    public class LightView : ComponentView
    {
        private Dom.Data.Color _color;
        private Intensity _intensity;
        [SerializeField] private UnityEngine.Light _light;
        [SerializeField] private MeshRenderer _flame;

        [Header("input between 0 and 100")]
        [SerializeField] private AnimationCurve _intensityScale;

        public Dom.Data.Color Color
        {
            get => _color;
            set
            {
                _color = value;                
                _light.color = Color.ToColor();
                _flame.material.color = Color.ToColor();
            }
        }

        public Intensity Intensity
        {
            get => _intensity;
            set
            {
                _intensity = value;
                _light.intensity = ScaleIntensity(Intensity);
            }
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Light light = new Dom.Data.Light(Id, (SnappedPosition)ComponentPosition, (SnappedRotation)ComponentOrientation, Color, Intensity);
            return light;
        }

        public override void SetComponentData(Dom.Data.Component lightData)
        {
            base.SetComponentData(lightData);
            if (lightData is Dom.Data.Light light)
            {
                Color = light.Color;
                Intensity = light.Intensity;
                //do specific light stuff
            }
        }

        private float ScaleIntensity(float inputIntensity)
        {
            return _intensityScale.Evaluate(inputIntensity/100f);
        }
    }
}