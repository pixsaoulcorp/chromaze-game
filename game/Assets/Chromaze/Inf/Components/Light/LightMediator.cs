using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Light
{
    public class LightMediator : ComponentMediator
    {
        [Inject] public LightView view { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        protected override View GetView()
        {
            return view;
        }
    }
}