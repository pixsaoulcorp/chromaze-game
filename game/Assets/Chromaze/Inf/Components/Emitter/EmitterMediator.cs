﻿using Pixsaoul.App.Camera;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Emitter
{
    public class EmitterMediator : ComponentMediator
    {              
        protected override View GetView()
        {
            return view;
        }

        [Inject] public EmitterView view { get; set; }
      
        public override void OnRegister()
        {
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();
        }
    }
}
