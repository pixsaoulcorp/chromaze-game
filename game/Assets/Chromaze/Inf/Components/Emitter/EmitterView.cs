﻿
using Assets.Pixsaoul.App.Engine;
using Chromaze.App.Optics;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Inf.Control;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Components.Emitter
{

    public class EmitterView : ComponentView
    {
        public Signal OnSelectedSignal = new Signal();
        [SerializeField] private IEmitterContainer _emitter;
        [SerializeField] private IOrientableContainer _orientable;
        [SerializeField] private GimbalCorrector _gimbalCorrector;
        [SerializeField] private List<IInitializableContainer> _subParts;


        public override void Initialize()
        {
            base.Initialize();
            foreach (IInitializableContainer initializableContainer in _subParts)
            {
                initializableContainer.Result.Initialize();
            }
        }

        public override void SetComponentData(Dom.Data.Component emitterData)
        {
            base.SetComponentData(emitterData);
            if (emitterData is Dom.Data.Emitter emitter)
            {
                _orientable.Result.SetDirection(emitter.EmissionOrientation);
                _emitter.Result.SetData(emitter.Status, emitter.Color);
                _emitter.Result.SetSourceId(emitter.Id);
            }
        }

        protected override void Move(Dom.Data.Component data)
        {
            base.Move(data);
            _gimbalCorrector.UpdateCorrection(_rotationRoot);
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Emitter emitter = new Dom.Data.Emitter(Id, (SnappedPosition) ComponentPosition, (SnappedRotation) ComponentOrientation, _emitter.Result.Status, _emitter.Result.Color, (Orientation) _orientable.Result.GetDirection());
            return emitter;
        }
    }
}