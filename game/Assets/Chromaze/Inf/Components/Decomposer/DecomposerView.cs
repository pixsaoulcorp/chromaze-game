﻿
using Assets.Pixsaoul.App.Engine;
using Chromaze.App.Optics;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Inf.Control;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Components.Decomposer
{
    public class DecomposerView : ComponentView
    {
        [SerializeField] private IDecomposerContainer _decomposer;
        [SerializeField] private IOrientableContainer _orientable;
        [SerializeField] private GimbalCorrector _gimbalCorrector;
        [SerializeField] private List<IInitializableContainer> _subParts;


        public override void Initialize()
        {
            base.Initialize();
            foreach (IInitializableContainer initializableContainer in _subParts)
            {
                initializableContainer.Result.Initialize();
            }
        }
        public override void SetComponentData(Dom.Data.Component componentData)
        {
            base.SetComponentData(componentData);
            if (componentData is Dom.Data.Decomposer decomposer)
            {
                _decomposer.Result.SetData(decomposer);
                _orientable.Result.SetDirection(decomposer.EmissionOrientation);
                //do specific Decomposer stuff
            }
        }

        protected override void Move(Dom.Data.Component data)
        {
            base.Move(data);
            _gimbalCorrector.UpdateCorrection(_rotationRoot);
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Decomposer decomposer = new Dom.Data.Decomposer(Id, (SnappedPosition) ComponentPosition, (SnappedRotation) ComponentOrientation, (Orientation) _orientable.Result.GetDirection());
            return decomposer;
        }

    }
}