﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Decomposer
{
    public class DecomposerMediator : ComponentMediator
    {
        [Inject]
        public DecomposerView View { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();

        }

    }
}
