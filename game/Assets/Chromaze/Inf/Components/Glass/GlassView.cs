using Pixsaoul.Dom.Data.Engine;

namespace Chromaze.Inf.Components.Glass
{
    public class GlassView : ComponentView
    {
        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Glass glass= new Dom.Data.Glass(Id, (SnappedPosition)ComponentPosition, (SnappedRotation)ComponentOrientation);
            return glass;
        }

        public override void Initialize()
        {
            base.Initialize();
            
        }
    }
}