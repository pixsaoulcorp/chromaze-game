﻿using Chromaze.App.Environment;
using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Components.Door
{
    public class DoorView : ComponentView
    {      

        [SerializeField] private IDoorContainer _door;

        [SerializeField] private List<IInitializableContainer> _subParts;
        
        public override void Initialize()
        {
            base.Initialize();
            foreach (IInitializableContainer initializableC in _subParts)
            {
                initializableC.Result.Initialize();
            }
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Door door = new Dom.Data.Door(Id, (SnappedPosition) ComponentPosition, (SnappedRotation) ComponentOrientation, _door.Result.ColorKey);
            return door;
        }

        public override void SetComponentData(Dom.Data.Component doorData)
        {
            base.SetComponentData(doorData);
            if (doorData is Dom.Data.Door door)
            {
                _door.Result.SetData(door);
            }
        }

    }
}
