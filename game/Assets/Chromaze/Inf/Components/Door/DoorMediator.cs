﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Door
{
    public class DoorMediator : ComponentMediator
    {

        [Inject] public DoorView View { get; set; }

        protected override View GetView()
        {
            return View;
        }

        //TODO register to events
    }
}
