﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.End
{
    public class OnReachedSignal : Signal { }
}
