﻿// * Copyright [2017] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Chromaze.App.SceneWorkflow;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.End
{
    public class EndMediator : Mediator
    {
        [Inject] public RequestEndLevelSignal RequestEndLevelSignal { get; set; }
        protected override View GetView()
        {
            return view;
        }

        [Inject]
        public EndView view { get; set; }

        public override void OnRegister()
        {
            base.OnRegister();
            view.OnReachedSignal.AddListener(RequestEndLevel);

        }

        public override void OnRemove()
        {
            view.OnReachedSignal.RemoveListener(RequestEndLevel);

            base.OnRemove();
        }

        private void RequestEndLevel()
        {
            RequestEndLevelSignal.Dispatch();
        }
    }
}