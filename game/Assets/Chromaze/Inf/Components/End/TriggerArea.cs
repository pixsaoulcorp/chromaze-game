﻿using Pixsaoul.Fou.Engine;
using System;
using UnityEngine;

namespace Chromaze.Inf.Components.End
{
    public class TriggerArea : BaseBehaviour //TODO TBU move this to a common place that can use it
    {

        [SerializeField] private Collider _collider;

        private Action _enterCallback;
        public void SetData(Action enterCallback)
        {
            _enterCallback = enterCallback;
        }

        /// <summary>
        /// Called when [trigger enter].
        /// </summary>
        /// <param name="other">The other.</param>
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
            {
                if (_enterCallback != null)
                {
                    _enterCallback.Invoke();
                }
            }
        }
    }
}
