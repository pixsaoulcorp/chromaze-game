﻿
using Pixsaoul.App.BoxelGeneration;
using Pixsaoul.App.Engine;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Inf.Components.Wall
{
    public class WallView : ComponentView
    {
        public Signal<SnappedDimension> RequestBoxelWallSignal = new Signal<SnappedDimension>();
        [SerializeField] private Transform _wallRoot;        
        [SerializeField] private ISizeableContainer _sizeableFocus; // used to resize the focus part.
        [SerializeField] private MaterialSwitch.MaterialSwitch _materialSwitch;
        private BoxelShapeObject _currentWall;
        private Dom.Data.Wall _wallData;

        public Dom.Data.Wall WallData { get => _wallData; }

        public override void SetComponentData(Dom.Data.Component wallData)
        {
            base.SetComponentData(wallData);
            if (wallData is Dom.Data.Wall wall)
            {
                _wallData = wall;
                RequestBoxelWallSignal.Dispatch(_wallData.Dimension);
                _sizeableFocus.Result.Resize(_wallData.Dimension);
            }
        }
                
        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Wall wall = new Dom.Data.Wall(Id, (SnappedPosition) ComponentPosition, (SnappedRotation) ComponentOrientation, WallData.Dimension, WallData.Color);
            return wall;
        }

        public void ReceiveMesh(BoxelShapeObject boxelWall)
        {
            if(_currentWall != null)
            {
                Destroy(_currentWall.gameObject);
                _currentWall = null;
            }
            boxelWall.transform.SetParent(_wallRoot, true);
            boxelWall.transform.localPosition = Vector3.zero;
            boxelWall.transform.localRotation = Quaternion.identity;
            boxelWall.SetMaterial(_materialSwitch.GetMaterialFromColor(_wallData.Color));
            _currentWall = boxelWall;
        }
    }
}