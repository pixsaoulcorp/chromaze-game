﻿
using Pixsaoul.App.BoxelGeneration;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Inf.BoxelGeneration;
using UnityEngine;

namespace Chromaze.Inf.Components.Wall
{
    public class WallMediator : ComponentMediator
    {

        [Inject] public RequestBoxelObjectSignal RequestBoxelObjectSignal { get; set; }
        [Inject] public WallView view { get; set; }
        
        protected override View GetView()
        {
            return view;
        }
        public override void Initialize()
        {
            base.Initialize();
            RequestWallObject(view.WallData.Dimension); // can be done twice, once on view request, or on mediator initialization
        }

        public override void OnRegister()
        {
            view.RequestBoxelWallSignal.AddListener(RequestWallObject);
            base.OnRegister();
        }


        public override void OnRemove()
        {
            view.RequestBoxelWallSignal.RemoveListener(RequestWallObject);
            base.OnRemove();
        }

        private void RequestWallObject(SnappedDimension dimensions)
        {
            BoxelShape wallShape = BoxelShapeExtensions.CreateSimpleShape(dimensions.Width, dimensions.Height, dimensions.Depth, SurfaceMode.Extern);          
            RequestBoxelObjectSignal.Dispatch(wallShape, OnWallBoxelReceived);
        }

        private void OnWallBoxelReceived(BoxelShapeObject wall)
        {
            view.ReceiveMesh(wall);
        }      
    }
}