﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Combiner
{
    public class CombinerMediator : ComponentMediator
    {
        [Inject]
        public CombinerView View { get; set; } //TODO Set view to real view type

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();

        }
    }
}
