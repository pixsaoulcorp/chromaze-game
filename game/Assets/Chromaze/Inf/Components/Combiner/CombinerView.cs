﻿using Assets.Pixsaoul.App.Engine;
using Chromaze.App.Optics;
using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Inf.Control;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Components.Combiner
{
    public class CombinerView : ComponentView
    {
        [SerializeField] public ICombinerContainer _combiner;
        [SerializeField] private IOrientableContainer _orientable;
        [SerializeField] private GimbalCorrector _gimbalCorrector;
        [SerializeField] private List<IInitializableContainer> _subParts; // TODO TBU move this to an Initializer component

        public override void Initialize()
        {
            base.Initialize();
            foreach (IInitializableContainer initializableContainer in _subParts)
            {
                initializableContainer.Result.Initialize();
            }
        }
        public override void SetComponentData(Dom.Data.Component data)
        {
            base.SetComponentData(data);
            if (data is Dom.Data.Combiner combiner)
            {
                _combiner.Result.SetData(combiner);
                _orientable.Result.SetDirection(combiner.EmissionOrientation);
                //do specific combiner stuff
            }
        }

        protected override void Move(Dom.Data.Component data)
        {
            base.Move(data);
            _gimbalCorrector.UpdateCorrection(_rotationRoot);
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Combiner combiner = new Dom.Data.Combiner(Id, (SnappedPosition)ComponentPosition, (SnappedRotation) ComponentOrientation, (Orientation) _orientable.Result.GetDirection());
            return combiner;
        }
    }
}