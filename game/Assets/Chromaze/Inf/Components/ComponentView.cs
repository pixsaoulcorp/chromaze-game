﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Chromaze.App.Components;
using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Reactive.Subjects;
using UnityEngine;
using Domain = Chromaze.Dom.Data;

namespace Chromaze.Inf.Components
{
    public abstract class ComponentView : View, IComponent
    {
        [SerializeField] protected GameObject _translationRoot;
        [SerializeField] protected GameObject _rotationRoot;

        private ComponentId _id;

        public IObservable<Dom.Data.Component> ObservableComponentData => _dataSubject;
        private ReplaySubject<Dom.Data.Component> _dataSubject = new ReplaySubject<Domain.Component>();
        protected SnappedPosition ComponentPosition { get => SnappedPosition.SnapPosition((Position)_translationRoot.transform.localPosition); }
        protected SnappedRotation ComponentOrientation
        {
            get {
                Vector3 localEulerAngle = _rotationRoot.transform.localEulerAngles; // pitch yaw roll on euler angle
                SnappedRotation snapped = new SnappedRotation(new SnappedAngle((Angle)localEulerAngle.y),//yaw
                    new SnappedAngle((Angle)localEulerAngle.x),//pitch
                    new SnappedAngle((Angle)localEulerAngle.z));//roll
                return snapped;
            }
        }
        protected ComponentId Id { get => _id; }

        public abstract Domain.Component GetComponentData();

        public virtual void SetComponentData(Domain.Component sourceData)
        {
            // base.Initialize(); DO NOT CALL BASE INITIALIZE HERE, the base initialize is the one called by the mvcs framework, here we just want to pass dom data
            _id = sourceData.Id;
            Move(sourceData);
            _dataSubject.OnNext(sourceData);
        }

        protected virtual void Move(Domain.Component data)
        {
            _translationRoot.transform.localPosition = data.Position;
            _rotationRoot.transform.localEulerAngles = data.Rotation.ToEulerAngle();
        }

    }
}