﻿using Chromaze.Inf.Components;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Relay
{
    public class RelayMediator : ComponentMediator
    {
        [Inject]
        public RelayView View { get; set; } //TODO Set view to real view type

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();

        }

    }
}
