﻿
using Assets.Pixsaoul.App.Engine;
using Chromaze.App.Optics;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Inf.Control;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Components.Relay
{
    public class RelayView : ComponentView
    {
        [SerializeField] private IOrientableContainer _orientable;
        [SerializeField] private GimbalCorrector _gimbalCorrector;
        [SerializeField] private IRelayContainer _relay;
        [SerializeField] private List<IInitializableContainer> _subParts;


        public override void Initialize()
        {
            base.Initialize();
            foreach (IInitializableContainer initializableContainer in _subParts)
            {
                initializableContainer.Result.Initialize();
            }
        }

        public override void SetComponentData(Dom.Data.Component componentData)
        {
            base.SetComponentData(componentData);
            if (componentData is Dom.Data.Relay relay)
            {
                _relay.Result.SetData(relay);
                _orientable.Result.SetDirection(relay.EmissionOrientation);
            }
        }

        protected override void Move(Dom.Data.Component data)
        {
            base.Move(data);
            _gimbalCorrector.UpdateCorrection(_rotationRoot);
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Relay relay = new Dom.Data.Relay(Id, (SnappedPosition) ComponentPosition, (SnappedRotation) ComponentOrientation, (Orientation) _orientable.Result.GetDirection());
            return relay;
        }
    }
}