﻿
using Assets.Pixsaoul.App.Engine;
using Chromaze.App.Optics;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Inf.Control;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Components.Divider
{
    public class DividerView : ComponentView
    {
        [SerializeField] private IOrientableContainer _orientable;
        [SerializeField] private GimbalCorrector _gimbalCorrector;
        [SerializeField] private IDividerContainer _divider;
        [SerializeField] private List<IInitializableContainer> _subParts;
        public override void Initialize()
        {
            base.Initialize();
            foreach (IInitializableContainer initializableContainer in _subParts)
            {
                initializableContainer.Result.Initialize();
            }
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Divider divider = new Dom.Data.Divider(Id, (SnappedPosition) ComponentPosition, (SnappedRotation) ComponentOrientation, (Orientation) _orientable.Result.GetDirection());
            return divider;
        }

        protected override void Move(Dom.Data.Component data)
        {
            base.Move(data);
            _gimbalCorrector.UpdateCorrection(_rotationRoot);
        }

        public override void SetComponentData(Dom.Data.Component componentData)
        {
            base.SetComponentData(componentData);
            if (componentData is Dom.Data.Divider divider)
            {
                _orientable.Result.SetDirection(divider.EmissionOrientation);
                _divider.Result.SetData(divider);
                //do specific Divider stuff
            }
        }
    }
}