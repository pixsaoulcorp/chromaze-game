﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Mirror
{
    public class MirrorMediator : ComponentMediator
    {
        [Inject]
        public MirrorView View { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();

        }

    }
}
