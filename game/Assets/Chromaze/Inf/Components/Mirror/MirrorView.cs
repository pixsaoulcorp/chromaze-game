﻿using Chromaze.Dom.Data;
using UnityEngine;
using Chromaze.App.Optics;
using Assets.Pixsaoul.App.Engine;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Inf.Control;

namespace Chromaze.Inf.Components.Mirror
{
    public class MirrorView : ComponentView
    {
        [SerializeField] private IMirrorContainer _mirror;

        [SerializeField] private IOrientableContainer _orientable;
        [SerializeField] private GimbalCorrector _gimbalCorrector;


        public override void SetComponentData(Dom.Data.Component mirrorData)
        {
            base.SetComponentData(mirrorData);
            if (mirrorData is Dom.Data.Mirror mirror)
            {
                _mirror.Result.SetData(mirror);
                _orientable.Result.SetDirection(mirror.EmissionOrientation);
            }
        }

        protected override void Move(Dom.Data.Component data)
        {
            base.Move(data);
            _gimbalCorrector.UpdateCorrection(_rotationRoot);
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Mirror mirror = new Dom.Data.Mirror(Id, (SnappedPosition) ComponentPosition, (SnappedRotation) ComponentOrientation, (Orientation)_orientable.Result.GetDirection());
            return mirror;
        }

    }
}
