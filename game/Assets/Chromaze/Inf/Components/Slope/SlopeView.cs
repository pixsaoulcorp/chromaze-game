﻿
using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using UnityEngine;

namespace Chromaze.Inf.Components.Slope
{
    public class SlopeView : ComponentView
    {
        [SerializeField] private MaterialSwitch.MaterialSwitch _materialSwitch;
        private Dom.Data.Color _color;
        [SerializeField] private MeshRenderer _cubeRenderer;

        public Dom.Data.Color Color
        {
            get => _color; set
            {
                _color = value;
                _materialSwitch.SetMaterial(_color, _cubeRenderer);
            }
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Slope Slope = new Dom.Data.Slope(Id, (SnappedPosition)ComponentPosition, (SnappedRotation)ComponentOrientation, Color);
            return Slope;
        }

        public override void SetComponentData(Dom.Data.Component SlopeData)
        {
            base.SetComponentData(SlopeData);
            if (SlopeData is Dom.Data.Slope slope)
            {
                Color = slope.Color;
                //do specific Slope stuff
            }
        }
    }
}