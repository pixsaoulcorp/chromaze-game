﻿
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Slope
{
    public class SlopeMediator : ComponentMediator
    {
        [Inject] public SlopeView view { get; set; }

        protected override View GetView()
        {
            return view;
        }
    }
}