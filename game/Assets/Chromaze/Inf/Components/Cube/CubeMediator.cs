﻿
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Cube
{
    public class CubeMediator : ComponentMediator
    {
        [Inject] public CubeView view { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        protected override View GetView()
        {
            return view;
        }
    }
}