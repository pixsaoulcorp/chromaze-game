﻿
using Pixsaoul.Dom.Data.Engine;
using UnityEngine;

namespace Chromaze.Inf.Components.Cube
{
    public class CubeView : ComponentView
    {
        [SerializeField] private MaterialSwitch.MaterialSwitch _materialSwitch;
        private Dom.Data.Color _color;
        [SerializeField] private MeshRenderer _cubeRenderer;

        public Dom.Data.Color Color
        {
            get => _color; set
            {
                _color = value;
                _materialSwitch.SetMaterial(_color, _cubeRenderer);
            }
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Cube cube = new Dom.Data.Cube(Id, (SnappedPosition)ComponentPosition, (SnappedRotation)ComponentOrientation, Color);
            return cube;
        }

        public override void SetComponentData(Dom.Data.Component cubeData)
        {
            base.SetComponentData(cubeData);
            if (cubeData is Dom.Data.Cube cube)
            {
                Color = cube.Color;

                //do specific cube stuff
            }
        }
    }
}