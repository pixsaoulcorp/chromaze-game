﻿using Pixsaoul.App.BoxelGeneration;
using Pixsaoul.App.Engine;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using UnityEngine;
using Chromaze.Inf.MaterialSwitch;

namespace Chromaze.Inf.Components.Boundaries
{
    public class BoundariesView : ComponentView, IMeshProviderProvider
    {
        public Signal<IBoxelShape> RequestBoxelBoundariesSignal = new Signal<IBoxelShape>();
        [SerializeField] private MaterialSwitch.MaterialSwitch _materialSwitch;
        [SerializeField] private Transform _wallRoot;
        [SerializeField] private List<IInitializableContainer> _subParts;

        private Dom.Data.Boundaries _boundariesData; // data used as reference
        private BoxelShapeObject _currentBoxelShapeObject; // the object we currently have

        private BehaviorSubject<IMeshProvider> _meshProviderSubject; // DO NOT USE THIS 
        private BehaviorSubject<IMeshProvider> MeshProviderSubject // USE THIS instead
        {
            get
            {
                if (_meshProviderSubject == null)
                {
                    _meshProviderSubject = new BehaviorSubject<IMeshProvider>(_currentBoxelShapeObject);
                }
                return _meshProviderSubject;
            }
        }

        public IObservable<IMeshProvider> ObservableMeshProvider { get => MeshProviderSubject; }        

        public Dom.Data.Boundaries BoundariesData { get => _boundariesData; }


        public Signal RequestBoxelObjectSignal = new Signal();
        public override void Initialize()
        {
            base.Initialize();
            foreach (IInitializableContainer initializableContainer in _subParts)
            {
                initializableContainer.Result.Initialize();
            }
        }

        public override void SetComponentData(Dom.Data.Component boundariesData)
        {
            base.SetComponentData(boundariesData);
            if (boundariesData is Dom.Data.Boundaries boundaries)
            {
                _boundariesData = boundaries;
                RequestBoxelBoundariesSignal.Dispatch(_boundariesData.Shape);
            }
        }

        public override Dom.Data.Component GetComponentData()
        {
            return GetData();
        }

        public Dom.Data.Boundaries GetData()
        {
            Dom.Data.Boundaries boundaries = new Dom.Data.Boundaries(Id, (SnappedPosition)ComponentPosition, (SnappedRotation)ComponentOrientation, BoundariesData.Shape, BoundariesData.Color);
            return boundaries;
        }

        public void ReceiveMesh(BoxelShapeObject boxelShape)
        {
            if (_currentBoxelShapeObject != null)
            {
                Destroy(_currentBoxelShapeObject.gameObject);
                _currentBoxelShapeObject = null;
            }
            _currentBoxelShapeObject = boxelShape;
            _currentBoxelShapeObject.transform.SetParent(_wallRoot, false);
            _currentBoxelShapeObject.transform.localPosition = Vector3.zero;
            _currentBoxelShapeObject.transform.localRotation = Quaternion.identity;

            _currentBoxelShapeObject.SetMaterial(_materialSwitch.GetMaterialFromColor(_boundariesData.Color));
            MeshProviderSubject.OnNext(_currentBoxelShapeObject);
            //TODO TBU update subscribed to mesh changes
        }
    }
}
