﻿using Pixsaoul.App.BoxelGeneration;
using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Inf.Components.Boundaries
{
    public class BoundariesMediator : ComponentMediator
    {
        [Inject] public RequestBoxelObjectSignal RequestBoxelObjectSignal { get; set; }
        [Inject] public BoundariesView view { get; set; }

        protected override View GetView()
        {
            return view;
        }
        public override void Initialize()
        {
            base.Initialize();
            RequestBoundaryBoxelObject(view.BoundariesData.Shape); // can be done twice, once on view request, or on mediator initialization
        }

        public override void OnRegister()
        {
            view.RequestBoxelBoundariesSignal.AddListener(RequestBoundaryBoxelObject);
            base.OnRegister();
        }


        public override void OnRemove()
        {
            view.RequestBoxelBoundariesSignal.RemoveListener(RequestBoundaryBoxelObject);
            base.OnRemove();
        }

        private void RequestBoundaryBoxelObject(IBoxelShape shape)
        {           
            RequestBoxelObjectSignal.Dispatch(shape, OnBoundaryBoxelReceived);
        }

        private void OnBoundaryBoxelReceived(BoxelShapeObject wall)
        {
            view.ReceiveMesh(wall);
        }
    }
}