﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Components.Start
{
    public class RequestMovePlayerToStartSignal : Signal { }
}
