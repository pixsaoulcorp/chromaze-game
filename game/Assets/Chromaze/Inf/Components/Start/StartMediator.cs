﻿// * Copyright [2017] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Chromaze.App.Components.Start;
using Chromaze.Inf.Player;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Start
{
    public class StartMediator : Mediator
    {
        protected override View GetView()
        {
            return view;
        }

        [Inject] public RequestSetPlayerLocationSignal SendPlayerPositionSignal { get; set; }

        [Inject] public OnStartViewPlacedSignal OnStartViewPlacedSignal { get; set; } //TODO TBU

        [Inject] public RequestMovePlayerToStartSignal RequestMovePlayerToStartSignal{get;set;}

        [Inject] public OnStartViewReadyForPlayerSignal OnStartViewReadyForPlayerSignal { get; set; }

        [Inject]
        public StartView view { get; set; }

        /// <summary>
        /// On Register
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            RequestMovePlayerToStartSignal.AddListener(MovePlayerToStart);
            view.ReadyForPlayerSignal.AddListener(CheckStartViewReadyForPlayer);

            // At this point View might already have been initialized, and data set, so we're checking if the view is ready for a player
            CheckStartViewReadyForPlayer();
        }

        /// <summary>
        /// On Remove
        /// </summary>
        public override void OnRemove()
        {
            base.OnRemove();
            RequestMovePlayerToStartSignal.RemoveListener(MovePlayerToStart);
            view.ReadyForPlayerSignal.RemoveListener(CheckStartViewReadyForPlayer);

        }

        private void MovePlayerToStart()
        {
            if (view.ReadyForPlayer)
            {
                SendPlayerPositionSignal.Dispatch(view.TargetPlayerPosition, view.TargetPlayerRotation);
            }
        }

        private void CheckStartViewReadyForPlayer()
        {
            if (view.ReadyForPlayer)
            {
                OnStartViewReadyForPlayerSignal.Dispatch();
            }
        }
    }
}