﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Start
{
    public class OnStartViewPlacedSignal : Signal { }
}