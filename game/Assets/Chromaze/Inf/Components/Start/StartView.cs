﻿// * Copyright [2017] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.

using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Inf.Components.Start
{
    public class StartView : ComponentView
    {
        [SerializeField] private GameObject _playerStartingPoint;

        private bool _readyForPlayer;
        public bool ReadyForPlayer
        {
            get
            {
                return _readyForPlayer;
            }
            private set
            {
                _readyForPlayer = value;
                if (value)
                {
                    ReadyForPlayerSignal.Dispatch(); // Try to dispatch the information asap, but mediator may not be available yet
                }
            }
        }

        public Signal ReadyForPlayerSignal = new Signal();

        public override void SetComponentData(Dom.Data.Component startData)
        {
            base.SetComponentData(startData);
            if (startData is Dom.Data.Start start)
            {
                ReadyForPlayer = true;
            }
        }

        public override Dom.Data.Component GetComponentData()
        {
            return GetData();
        }

        public Dom.Data.Start GetData()
        {
            Dom.Data.Start start = new Dom.Data.Start(Id, (SnappedPosition)ComponentPosition, (SnappedRotation)ComponentOrientation);
            return start;
        }

        public Position TargetPlayerPosition
        {
            get
            {
                return (Position)_playerStartingPoint.transform.position;
            }
        }

        public Rotation TargetPlayerRotation
        {
            get
            {
                var orientation = new Orientation(_playerStartingPoint.transform.eulerAngles);
                return new Rotation(orientation.yaw, orientation.pitch, (Angle)0);
            }
        }

    }
}