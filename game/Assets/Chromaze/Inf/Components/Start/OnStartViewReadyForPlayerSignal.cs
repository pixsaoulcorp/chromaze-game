﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Start
{
    public class OnStartViewReadyForPlayerSignal : Signal { }
}