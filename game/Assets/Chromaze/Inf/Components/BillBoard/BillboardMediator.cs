﻿using Pixsaoul.App.BoxelGeneration;
using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Inf.Components.BillBoards
{
    public class BillboardMediator : ComponentMediator
    {
        [Inject] public BillboardView view { get; set; }

        protected override View GetView()
        {
            return view;
        }
        public override void Initialize()
        {
            base.Initialize();
        }

        public override void OnRegister()
        {
            base.OnRegister();
        }


        public override void OnRemove()
        {
            base.OnRemove();
        }

    }
}