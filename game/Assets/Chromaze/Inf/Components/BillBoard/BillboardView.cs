﻿using Pixsaoul.Dom;
using Pixsaoul.Dom.Data.Engine;
using TMPro;
using UnityEngine;

namespace Chromaze.Inf.Components.BillBoards
{
    public class BillboardView : ComponentView
    {
        [SerializeField] TMP_Text _frontTextField;
        [SerializeField] TMP_Text _backTextField;

        private string _content;
        public override void Initialize()
        {
            base.Initialize();
         
        }

        public override void SetComponentData(Dom.Data.Component data)
        {
            base.SetComponentData(data);
            if (data is Dom.Data.Billboard billboard)
            {
                _content = billboard.Text;
                var content = ApplyTextTransforms(_content);
                _frontTextField.text = content;
                _backTextField.text = content;
                //do specific Billboard stuff
            }
        }

        public override Dom.Data.Component GetComponentData()
        {
            return GetData();
        }

        public Dom.Data.Billboard GetData()
        {
            Dom.Data.Billboard billboard= new Dom.Data.Billboard(Id, (SnappedPosition)ComponentPosition, (SnappedRotation)ComponentOrientation, (Text)_content);
            return billboard;
        }

        /// <summary>
        /// Structure for Multi OS support is "OSDEPENDENT//this is the windows text//this is the android one"
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string ApplyTextTransforms(string input)
        {
            if (input.StartsWith("OSDEPENDENT"))
            {
                string[] output = input.Split("//");

#if UNITY_ANDROID || (UNITY_EDITOR && ANDROID_EDITOR)
                return output[2];
#elif UNITY_STANDALONE_WIN || UNITY_EDITOR
                return output[1];
#endif
            }
            return input;
        }
    }
}
