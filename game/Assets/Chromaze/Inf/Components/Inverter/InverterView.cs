﻿
using Assets.Pixsaoul.App.Engine;
using Chromaze.App.Optics;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Inf.Control;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Components.Inverter
{
    public class InverterView : ComponentView
    {
        [SerializeField] private IOrientableContainer _orientable;
        [SerializeField] private GimbalCorrector _gimbalCorrector;
        [SerializeField] private IInverterContainer _inverter;
        [SerializeField] private List<IInitializableContainer> _subParts;


        public override void Initialize()
        {
            base.Initialize();
            foreach (IInitializableContainer initializableContainer in _subParts)
            {
                initializableContainer.Result.Initialize();
            }
        }
        public override void SetComponentData(Dom.Data.Component cubeData)
        {
            base.SetComponentData(cubeData);
            if (cubeData is Dom.Data.Inverter inverter)
            {
                _inverter.Result.SetData(inverter);
                _orientable.Result.SetDirection(inverter.EmissionOrientation);
                //do specific Inverter stuff
            }
        }

        protected override void Move(Dom.Data.Component data)
        {
            base.Move(data);
            _gimbalCorrector.UpdateCorrection(_rotationRoot);
        }

        public override Dom.Data.Component GetComponentData()
        {
            Dom.Data.Inverter inverter = new Dom.Data.Inverter(Id, (SnappedPosition) ComponentPosition, (SnappedRotation) ComponentOrientation, (Orientation) _orientable.Result.GetDirection());
            return inverter;
        }
    }
}