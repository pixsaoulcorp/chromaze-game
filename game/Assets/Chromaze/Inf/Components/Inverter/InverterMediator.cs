﻿using Chromaze.Inf.Components;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Components.Inverter
{
    public class InverterMediator : ComponentMediator
    {
        [Inject]
        public InverterView View { get; set; } //TODO Set view to real view type

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            //  AddContextListener(ExampleEvent, ExampleMethod);
            //  AddViewListener(ExampleEvent, ExampleMethod);
        }

        public override void OnRemove()
        {
            //  AddContextListener(ExampleEvent, ExampleMethod);
            //  AddViewListener(ExampleEvent, ExampleMethod);
            base.OnRemove();

        }

        //public void ExampleMethod(object data)
        //{
        //    if (data is int value)
        //    {
        //        Console.WriteLine(value)
        //    } else
        //    {
        //        throw Exception("error");
        //    }
        //}
    }
}
