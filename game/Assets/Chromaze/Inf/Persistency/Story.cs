﻿using Pixsaoul.Inf.Persistency;
using System.Collections.Generic;
using System;
using Pixsaoul.Fou.Markers;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Story : IPersistable, IPersister<Dom.Data.Story>
    {
        public List<string> LevelsName { get; set; }

        public Story()
        {

        }

        public Story(Dom.Data.Story dom)
        {
            LevelsName = new List<string>();
            foreach(var name in dom.LevelsName)
            {
                LevelsName.Add(name);
            }
        }

        public Dom.Data.Story ToDom()
        {
            var levelsName = new List<Pixsaoul.Dom.Data.Engine.Name>();
            foreach (var name in LevelsName)
            {
                levelsName.Add(name);
            }

            return new Dom.Data.Story(levelsName);
        }
    }
}
