﻿using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class End : Component
    {
        public End() : base()
        {

        }

        public End(Dom.Data.End end) : base(end)
        {
        }

        public Dom.Data.End ToDom()
        {
            return new Dom.Data.End(Id.ToDom(), Position.ToDom(), Rotation.ToDom());
        }

    }
}