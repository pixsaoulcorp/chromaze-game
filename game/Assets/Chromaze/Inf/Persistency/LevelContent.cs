﻿using Newtonsoft.Json;
using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency;
using System;
using System.Collections.Generic;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class LevelContent : IPersistable, IPersister<Dom.Data.LevelContent>
    {
        public Boundaries Boundaries { get; set; }
        public Start Start { get; set; }
        public End End { get; set; }

        public List<Billboard> Billboards { get; set; }
        public List<Cube> Cubes { get; set; }
        public List<Door> Doors { get; set; }
        public List<Wall> Walls { get; set; }
        public List<Emitter> Emitters { get; set; }
        public List<Mirror> Mirrors { get; set; }
        public List<Combiner> Combiners { get; set; }
        public List<Decomposer> Decomposers { get; set; }
        public List<Divider> Dividers { get; set; }
        public List<Inverter> Inverters { get; set; }
        public List<Relay> Relays { get; set; }
        public List<Slope> Slopes { get; set; }
        public List<Glass> Glasses { get; set; }
        public List<Light> Lights { get; set; }

        // add a new list for every kind of component here
        public LevelContent()
        {
            Boundaries = new Boundaries();
            Start = new Start();
            End = new End();

            Billboards = new List<Billboard>();
            Cubes = new List<Cube>();
            Doors = new List<Door>();
            Walls = new List<Wall>();
            Emitters = new List<Emitter>();
            Mirrors = new List<Mirror>();
            Combiners = new List<Combiner>();
            Decomposers = new List<Decomposer>();
            Dividers = new List<Divider>();
            Inverters = new List<Inverter>();
            Relays = new List<Relay>();
            Slopes = new List<Slope>();
            Glasses = new List<Glass>();
            Lights = new List<Light>();
        }

        public LevelContent(Dom.Data.LevelContent dom)
        {
            Boundaries = new Boundaries(dom.Boundaries);
            Start = new Start(dom.Start);
            End = new End(dom.End);

            Billboards = new List<Billboard>();
            Cubes = new List<Cube>();
            Doors = new List<Door>();
            Walls = new List<Wall>();
            Emitters = new List<Emitter>();
            Mirrors = new List<Mirror>();
            Combiners = new List<Combiner>();
            Decomposers = new List<Decomposer>();
            Dividers = new List<Divider>();
            Inverters = new List<Inverter>();
            Relays = new List<Relay>();
            Slopes = new List<Slope>();
            Glasses = new List<Glass>();
            Lights = new List<Light>();

            foreach (Dom.Data.Component cpt in dom.Components)
            {
                if (cpt is Dom.Data.Billboard billboard)
                {
                    Billboard newComponent = new Billboard(billboard);
                    Billboards.Add(newComponent);
                }
                else if (cpt is Dom.Data.Cube cube)
                {
                    Cube newComponent = new Cube(cube);
                    Cubes.Add(newComponent);
                }
                else if (cpt is Dom.Data.Door door)
                {
                    Door newComponent = new Door(door);
                    Doors.Add(newComponent);
                }
                else if (cpt is Dom.Data.Wall wall)
                {
                    Wall newComponent = new Wall(wall);
                    Walls.Add(newComponent);
                }
                else if (cpt is Dom.Data.Emitter emitter)
                {
                    Emitter newComponent = new Emitter(emitter);
                    Emitters.Add(newComponent);
                }
                else if (cpt is Dom.Data.Mirror mirror)
                {
                    Mirror newComponent = new Mirror(mirror);
                    Mirrors.Add(newComponent);
                }
                else if (cpt is Dom.Data.Combiner combiner)
                {
                    Combiner newComponent = new Combiner(combiner);
                    Combiners.Add(newComponent);
                }
                else if (cpt is Dom.Data.Decomposer decomposer)
                {
                    Decomposer newComponent = new Decomposer(decomposer);
                    Decomposers.Add(newComponent);
                }
                else if (cpt is Dom.Data.Divider divider)
                {
                    Divider newComponent = new Divider(divider);
                    Dividers.Add(newComponent);
                }
                else if (cpt is Dom.Data.Inverter inverter)
                {
                    Inverter newComponent = new Inverter(inverter);
                    Inverters.Add(newComponent);
                }
                else if (cpt is Dom.Data.Relay relay)
                {
                    Relay newComponent = new Relay(relay);
                    Relays.Add(newComponent);
                }
                else if (cpt is Dom.Data.Slope slope)
                {
                    Slope newComponent = new Slope(slope);
                    Slopes.Add(newComponent);
                }
                else if (cpt is Dom.Data.Glass glass)
                {
                    Glass newComponent = new Glass(glass);
                    Glasses.Add(newComponent);
                }
                else if (cpt is Dom.Data.Light light)
                {
                    Light newComponent = new Light(light);
                    Lights.Add(newComponent);
                }

            }
        }

        public Dom.Data.LevelContent ToDom()
        {
            Dom.Data.LevelContent dom = new Dom.Data.LevelContent();
            dom.Boundaries = this.Boundaries.ToDom();
            dom.Start = this.Start.ToDom();
            dom.End = this.End.ToDom();

            List<Dom.Data.Component> components = new List<Dom.Data.Component>();
            foreach (Billboard billboard in Billboards)
            {
                components.Add(billboard.ToDom());
            }
            foreach (Cube cube in Cubes)
            {
                components.Add(cube.ToDom());
            }
            foreach (Door door in Doors)
            {
                components.Add(door.ToDom());
            }
            foreach (Wall wall in Walls)
            {
                components.Add(wall.ToDom());
            }
            foreach (Emitter emitter in Emitters)
            {
                components.Add(emitter.ToDom());
            }
            foreach (Mirror mirror in Mirrors)
            {
                components.Add(mirror.ToDom());
            }
            foreach (Combiner combiner in Combiners)
            {
                components.Add(combiner.ToDom());
            }
            foreach (Decomposer decomposer in Decomposers)
            {
                components.Add(decomposer.ToDom());
            }
            foreach (Divider divider in Dividers)
            {
                components.Add(divider.ToDom());
            }
            foreach (Inverter inverter in Inverters)
            {
                components.Add(inverter.ToDom());
            }
            foreach (Relay relay in Relays)
            {
                components.Add(relay.ToDom());
            }
            foreach (Slope slope in Slopes)
            {
                components.Add(slope.ToDom());
            }
            foreach (Glass glass in Glasses)
            {
                components.Add(glass.ToDom());
            }
            foreach (Light light in Lights)
            {
                components.Add(light.ToDom());
            }

            dom.Components = components;
            return dom;
        }

        // TODO TBU find a clean way to maybe use the json service instead of static newtonsoft class
        public string ToStringBlob()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static LevelContent FromStringBlob(string stringBlob)
        {
            var output = JsonConvert.DeserializeObject<LevelContent>(stringBlob);
            return output;
        }
    }
}
