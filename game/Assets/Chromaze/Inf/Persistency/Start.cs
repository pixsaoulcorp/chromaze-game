﻿using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Start : Component, IPersister<Dom.Data.Start>
    {

        public Start() : base()
        {

        }

        public Start(Dom.Data.Start start) : base(start)
        {
        }

        public Dom.Data.Start ToDom()
        {
            return new Dom.Data.Start(Id.ToDom(), Position.ToDom(), Rotation.ToDom());
        }
    }
}