﻿using Pixsaoul.Dom;
using Pixsaoul.Fou.Markers;
using System;


namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Billboard : Component, IPersister<Dom.Data.Billboard>
    {
        public string Text { get; set; }

        public Billboard() : base()
        {

        }

        public Billboard(Dom.Data.Billboard billboard) : base(billboard)
        {
            Text = billboard.Text;
        }

        public Dom.Data.Billboard ToDom()
        {
            return new Dom.Data.Billboard(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), (Text)Text);
        }
    }
}