﻿using Pixsaoul.Inf.Persistency.Engine;
using System;
using Domain = Chromaze.Dom.Data;


namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Divider : Component
    {
        public Orientation EmissionOrientation { get; set; }

        public Divider() : base()
        {

        }


        public Divider(Dom.Data.Divider divider) : base(divider)
        {
            EmissionOrientation = new Orientation(divider.EmissionOrientation);
        }

        public Dom.Data.Divider ToDom()
        {
            return new Dom.Data.Divider(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), EmissionOrientation.ToDom());
        }

    }
}