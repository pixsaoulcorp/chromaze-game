﻿using Pixsaoul.Fou.Markers;
using System;


namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Light : Component , IPersister<Dom.Data.Light>
    {
        public Color Color { get; set; }

        public float Intensity { get; set; }

        public Light() : base()
        {
            Intensity = 1;
        }

        public Light(Dom.Data.Light light) : base(light)
        {
            Color = new Color(light.Color);
            Intensity = light.Intensity;
        }

        public Dom.Data.Light ToDom()
        {
            return new Dom.Data.Light(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), 
                Color.ToDom(), Intensity);
        }
    }
}