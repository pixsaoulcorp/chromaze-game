﻿using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Door : Component, IPersister<Dom.Data.Door>
    {
        public Color Color { get; set; }

        public Door() : base()
        {

        }

        public Door(Dom.Data.Door door) : base(door)
        {
            Color = new Color(door.Color);
        }

        public Dom.Data.Door ToDom()
        {
            return new Dom.Data.Door(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), Color.ToDom());
        }
    }
}
