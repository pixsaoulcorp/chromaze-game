﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency.Engine;
using System;


namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Combiner : Component, IPersister<Dom.Data.Combiner>
    {
        public Orientation EmissionOrientation { get; set; }

        public Combiner() : base()
        {

        }
        public Combiner(Dom.Data.Combiner combiner) : base(combiner)
        {
            EmissionOrientation = new Orientation(combiner.EmissionOrientation);
        }

        public Dom.Data.Combiner ToDom()
        {
            return new Dom.Data.Combiner(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), EmissionOrientation.ToDom());
        }
    }
}