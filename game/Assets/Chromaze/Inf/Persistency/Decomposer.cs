﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency.Engine;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Decomposer : Component, IPersister<Dom.Data.Decomposer>
    {
        public Orientation EmissionOrientation { get; set; }

        public Decomposer() : base()
        {

        }

        public Decomposer(Dom.Data.Decomposer decomposer) : base(decomposer)
        {
            EmissionOrientation = new Orientation(decomposer.EmissionOrientation);
        }

        public Dom.Data.Decomposer ToDom()
        {
            return new Dom.Data.Decomposer(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), EmissionOrientation.ToDom());
        }
    }
}