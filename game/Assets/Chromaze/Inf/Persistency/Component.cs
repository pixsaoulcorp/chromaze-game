﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency.Engine;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Component
    {
        public ComponentId Id { get; set; }
        public Pixsaoul.Inf.Persistency.Engine.SnappedPosition Position { get; set; }
        public Pixsaoul.Inf.Persistency.Engine.SnappedRotation Rotation { get; set; }

        public Component()
        {
            // Do not edit this constructor. Used for deserialization purpose
        }

        internal Component(Dom.Data.Component component)
        {
            Id= new ComponentId(component.Id);
            Position= new SnappedPosition(component.Position);
            Rotation= new SnappedRotation(component.Rotation);
        }

    }
}