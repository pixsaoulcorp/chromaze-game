﻿
using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency.Shape;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Boundaries : Component, IPersister<Dom.Data.Boundaries>
    {
        public BoxelShape Shape { get; set; }
        public Color Color { get; set; }

        public Boundaries() : base()
        {
        }

        public Boundaries(Dom.Data.Boundaries boundaries) : base(boundaries)
        {
            Shape = new BoxelShape(boundaries.Shape);
            Color = new Color(boundaries.Color);
        }

        public Dom.Data.Boundaries ToDom()
        {
            return new Dom.Data.Boundaries(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), Shape.ToDom(), Color.ToDom());
        }
    }
}
