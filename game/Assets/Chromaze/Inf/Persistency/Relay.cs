﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency.Engine;
using System;


namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Relay : Component, IPersister<Dom.Data.Relay>
    {
        public Orientation EmissionOrientation { get; set; }

        public Relay() : base()
        {

        }

        public Relay(Dom.Data.Relay relay) : base(relay)
        {
            EmissionOrientation = new Orientation(relay.EmissionOrientation);

        }

        public Dom.Data.Relay ToDom()
        {
            return new Dom.Data.Relay(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), EmissionOrientation.ToDom());
        }

    }
}