﻿using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public enum EmitterStatus
    {
        Enabled,
        Disabled
    }
}
