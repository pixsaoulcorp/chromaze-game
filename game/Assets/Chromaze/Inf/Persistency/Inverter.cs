﻿using Pixsaoul.Inf.Persistency.Engine;
using System;
using Domain = Chromaze.Dom.Data;


namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Inverter : Component
    {
        public Orientation EmissionOrientation { get; set; }

        public Inverter() : base()
        {

        }

        public Inverter(Dom.Data.Inverter inverter) : base(inverter)
        {
            EmissionOrientation = new Orientation(inverter.EmissionOrientation);
        }

        public Dom.Data.Inverter ToDom()
        {
            return new Dom.Data.Inverter(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), EmissionOrientation.ToDom());
        }
    }
}