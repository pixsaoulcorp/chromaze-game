﻿using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class ComponentId: IPersister<Dom.Data.ComponentId>
    {
        public Guid Id { get; set; }

        public ComponentId()
        {
            Id = Guid.NewGuid();
        }

        public Dom.Data.ComponentId ToDom()
        {
            return Id;
        }

        public ComponentId(Dom.Data.ComponentId componentId)
        {
            Id = componentId.Id;
        }
    }
}
