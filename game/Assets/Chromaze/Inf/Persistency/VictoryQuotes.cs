﻿using Pixsaoul.Fou.Markers;
using System;
using System.Collections.Generic;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class VictoryQuotes : IPersister<Dom.Data.VictoryQuotes>
    {
        public List<string> Quotes { get; set; }

        public VictoryQuotes()
        {

        }

        public VictoryQuotes(Dom.Data.VictoryQuotes dom)
        {
            Quotes = new List<string>();
            foreach(var domQuote in dom.Quotes)
            {
                Quotes.Add(domQuote);
            }
        }

        public Dom.Data.VictoryQuotes ToDom()
        {
            List<Pixsaoul.Dom.Text> domQuotes = new List<Pixsaoul.Dom.Text>();
            foreach(var quote in Quotes)
            {
                domQuotes.Add(quote);
            }
            return new Dom.Data.VictoryQuotes(domQuotes);
        }
    }
}
