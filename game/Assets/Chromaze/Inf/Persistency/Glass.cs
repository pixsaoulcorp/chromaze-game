﻿using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Glass : Component, IPersister<Dom.Data.Glass>
    {

        public Glass() : base()
        {

        }

        public Glass(Dom.Data.Glass glass) : base(glass)
        {

        }

        public Dom.Data.Glass ToDom()
        {
            return new Dom.Data.Glass(Id.ToDom(), Position.ToDom(), Rotation.ToDom());
        }
    }
}