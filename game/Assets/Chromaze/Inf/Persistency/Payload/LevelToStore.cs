﻿using Newtonsoft.Json;
using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Inf.Persistency.Payload
{
    [Serializable]
    public class LevelToStore : IPersister<Dom.Data.Level>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }

        public LevelToStore()
        {

        }

        public LevelToStore(Dom.Data.Level dom)
        {
            Id = dom.Id;
            Name = dom.Name;
            Content = new LevelContent(dom.Content).ToStringBlob();
        }

        public Dom.Data.Level ToDom()
        {
            return new Dom.Data.Level(Id, Name, LevelContent.FromStringBlob(Content).ToDom());
        }
    }
}
