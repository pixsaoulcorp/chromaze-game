﻿using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Inf.Persistency.Payload
{
    [Serializable]
    public class LevelForListsOfLevel : IPersister<Dom.Data.Payload.LevelForListOfLevel>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public LevelForListsOfLevel()
        {

        }

        public LevelForListsOfLevel(Dom.Data.Payload.LevelForListOfLevel dom)
        {
            Id = dom.Id;
            Name = dom.Name;
        }

        public Dom.Data.Payload.LevelForListOfLevel ToDom()
        {
            return new Dom.Data.Payload.LevelForListOfLevel(Id, Name);
        }
    }
}
