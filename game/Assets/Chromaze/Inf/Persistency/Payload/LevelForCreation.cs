﻿using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Inf.Persistency.Payload
{
    [Serializable]
    public class LevelForCreation : IPersister<Dom.Data.Level>
    {
        public string Name { get; set; }

        public string Content { get; set; }

        public LevelForCreation()
        {

        }

        public LevelForCreation(Dom.Data.Level dom)
        {
            Name = dom.Name;
            Content = new LevelContent(dom.Content).ToStringBlob();
        }

        public Dom.Data.Level ToDom()
        {
            return new Dom.Data.Level(-1,Name, LevelContent.FromStringBlob(Content).ToDom()); // TODO TBU MAPPER
        }
    }
}
