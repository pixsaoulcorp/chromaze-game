﻿using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Inf.Persistency.Payload
{
    [Serializable]
    public class LevelForDownload : IPersister<Dom.Data.Level>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }

        public LevelForDownload()
        {

        }

        public LevelForDownload(Dom.Data.Level dom)
        {
            Id = dom.Id;
            Name = dom.Name;
            Content = new LevelContent(dom.Content).ToStringBlob();
        }

        public Dom.Data.Level ToDom()
        {
            return new Dom.Data.Level(Id, Name, LevelContent.FromStringBlob(Content).ToDom()); // TODO TBU MAPPER
        }
    }
}
