﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency.Engine;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Mirror : Component, IPersister<Dom.Data.Mirror>
    {
        public Orientation EmissionOrientation { get; set; }

        public Mirror() : base()
        {

        }

        public Mirror(Dom.Data.Mirror mirror) : base(mirror)
        {
            EmissionOrientation = new Orientation(mirror.EmissionOrientation);
        }

        public Dom.Data.Mirror ToDom()
        {
            return new Dom.Data.Mirror(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), EmissionOrientation.ToDom());
        }

    }
}
