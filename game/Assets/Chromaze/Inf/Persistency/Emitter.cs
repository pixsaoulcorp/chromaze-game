﻿using Chromaze.Dom.Data;
using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency.Engine;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Emitter : Component, IPersister<Dom.Data.Emitter>
    {

        public EmitterStatus Status { get; set; }

        public Color Color { get; set; }

        public Orientation EmissionOrientation { get; set; }

        public Emitter() : base()
        {

        }
        public Emitter(Dom.Data.Emitter emitter) : base(emitter)
        {
            Status = (EmitterStatus)emitter.Status;
            Color = new Color(emitter.Color);
            EmissionOrientation = new Orientation(emitter.EmissionOrientation);
        }

        public Dom.Data.Emitter ToDom()
        {
            return new Dom.Data.Emitter(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), (Dom.Data.EmitterStatus)Status, Color.ToDom(), EmissionOrientation.ToDom()) ;
        }
    }
}
