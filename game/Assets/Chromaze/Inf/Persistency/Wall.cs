﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Inf.Persistency.Engine;
using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Wall : Component, IPersister<Dom.Data.Wall>
    {
        public SnappedDimension Dimension { get; set; }
        public Color Color { get; set; }

        public Wall() : base()
        {

        }

        public Wall(Dom.Data.Wall decomposer) : base(decomposer)
        {
            Dimension = new SnappedDimension(decomposer.Dimension);
            Color = new Color(decomposer.Color);
        }

        public Dom.Data.Wall ToDom()
        {
            return new Dom.Data.Wall(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), Dimension.ToDom(), Color.ToDom());
        }
    }
}
