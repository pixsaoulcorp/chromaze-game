﻿using Pixsaoul.Dom.Data.Engine;
using System;
using Domain = Chromaze.Dom.Data;


namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Cube : Component
    {
        public Color Color { get; set; }

        public Cube() : base()
        {

        }

        public Cube(Dom.Data.Cube cube) : base(cube)
        {
            Color = new Color(cube.Color);
        }

        public Dom.Data.Cube ToDom()
        {
            return new Dom.Data.Cube(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), Color.ToDom());
        }

    }
}