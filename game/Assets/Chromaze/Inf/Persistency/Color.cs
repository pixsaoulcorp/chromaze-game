﻿using System;

namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public struct Color
    {
        public bool Red { get; set; }
        public bool Green { get; set; }
        public bool Blue { get; set; }

        public Color(Dom.Data.Color color)
        {
            Red = color.Red;
            Green = color.Green;
            Blue = color.Blue;
        }

        public Dom.Data.Color ToDom()
        {
            return new Dom.Data.Color(Red, Green, Blue);
        }
    }
}
