﻿using Pixsaoul.Dom.Data.Engine;
using System;
using Domain = Chromaze.Dom.Data;


namespace Chromaze.Inf.Persistency
{
    [Serializable]
    public class Slope : Component
    {
        public Color Color { get; set; }

        public Slope() : base()
        {

        }

        public Slope(Dom.Data.Slope slope) : base(slope)
        {
            Color = new Color(slope.Color);
        }

        public Dom.Data.Slope ToDom()
        {
            return new Dom.Data.Slope(Id.ToDom(), Position.ToDom(), Rotation.ToDom(), Color.ToDom());
        }
    }
}