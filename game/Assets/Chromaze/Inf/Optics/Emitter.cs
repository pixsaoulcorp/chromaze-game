﻿using Chromaze.App.Environment;
using Chromaze.App.Optics;
using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using System;
using System.Reactive.Subjects;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    public class Emitter : BaseBehaviour, IEmitter, IColorProvider
    {
        private static float _maxRaycastDistance = 200f;
        [SerializeField] private Guid _sourceId;
        [SerializeField] private EmitterStatus _status;
        [SerializeField] private Dom.Data.Color _color;
        [SerializeField] private LayerMask _mask;

        [SerializeField] private GameObject _emissionRoot;
        [SerializeField] private IRayConsumerContainer _rayRenderer; // emission root is not emitter root, difference is emission is only the ray emitted
        // the emitter also have some other stuff like rotation etc...

        private IReceiver _lastKnownReceiver;

        public void SetSourceId(Guid sourceId)
        {
            if(_sourceId == null)
            {
                Debug.Log("An id was aready known for that emitter");
            } else
            {
                _sourceId = sourceId;
            }
        }
        public void SetData(EmitterStatus status, Dom.Data.Color color)
        {
            Status = status;
            Color = color;
        }

        public Guid Id
        {
            get => _sourceId;
        }

        public Position Position
        {
            get => (Position)this.transform.position;
            set
            {
                this.transform.position = value;
            }
        }

        //TODO TBU see if we need a Direction struct in Dom to avoid using V3 directly
        public Vector3 Direction
        {
            get => this.transform.forward;
            set
                {
                 this.transform.rotation = Quaternion.LookRotation(value);
                }
        }

        public EmitterStatus Status
        {
            get
            {
                return _status;
            }
            set
            {
                if(_status != value)
                {
                    _status = value;
                    if(_status == EmitterStatus.Enabled)
                    {
                        _rayRenderer.Result.Enable();
                    } else
                    {
                        _rayRenderer.Result.Disable();
                    }
                }
            }
        }

        public Dom.Data.Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                _emittedColorSubject.OnNext(_color);
                //_emissionRoot.Result.SetColor(_color);
            }
        }

        private BehaviorSubject<Dom.Data.Color> _emittedColorSubject = new BehaviorSubject<Dom.Data.Color>(Dom.Data.Color.BlackC);
        public IObservable<Dom.Data.Color> ObservableColor => _emittedColorSubject;

        void Update()
        {
            if (Status == EmitterStatus.Enabled)
            {
                Emit();
            } else
            {
                StopLastReceiver(_sourceId);
            }
        }

        private void Emit()
        {
            App.Optics.Ray ray = CreateRay(Color);
            DisplayRay(ray);
        }

        private App.Optics.Ray CreateRay(Dom.Data.Color color)
        {
            App.Optics.Ray ray;
            RaycastHit hit;
            //LayerMask mask;
            // TODO TBU use layers to interact with only what's needed
            if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, _maxRaycastDistance, _mask))
            {
                ray = new App.Optics.Ray(color, (Position)_emissionRoot.transform.position, (Position)hit.point);

                IReceiver receiver = hit.transform.GetComponent<IReceiver>();
                
                if (receiver != null)
                {
                    receiver.Receive(ray, _sourceId);
                }
                
                if ( receiver != _lastKnownReceiver)
                {
                    StopLastReceiver(_sourceId);
                }
                _lastKnownReceiver = receiver;
            }
            else
            {
                ray = new App.Optics.Ray(color, (Position)_emissionRoot.transform.position, (Position)(_emissionRoot.transform.position + _maxRaycastDistance * _emissionRoot.transform.forward));
                StopLastReceiver(_sourceId);

            }
            return ray;
        }

        public void DisplayRay(App.Optics.Ray ray)
        {
            UpdateLineRenderer(ray);
            Debug.DrawLine(ray.Source, ray.Target, ray.Color.ToColor());
        }

        private void UpdateLineRenderer(App.Optics.Ray ray)
        {
            _rayRenderer.Result.SetRay(ray);
        }

        private void StopLastReceiver(Guid emitterId)
        {
            if (_lastKnownReceiver != null)
            {
                _lastKnownReceiver.StopReceiving(emitterId);
                _lastKnownReceiver = null;
            }
        }
    }
}
