﻿using System;
using System.Collections.Generic;
using Chromaze.App.Environment;
using Chromaze.App.Optics;
using Pixsaoul.Fou.Engine;
using System.Reactive.Subjects;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    public class MultiReceiver : BaseBehaviour, IMultiReceiver, IColorProvider
    {
        private Dictionary<Guid, App.Optics.Ray> _receivedRays;

        public IReadOnlyDictionary<Guid, App.Optics.Ray> ReceivedRays => _receivedRays;
               
        public IObservable<Dom.Data.Color> ObservableColor { get => _colorObservable; }
        private BehaviorSubject<Dom.Data.Color> _colorObservable = new BehaviorSubject<Dom.Data.Color>(Dom.Data.Color.BlackC);

        public void ConfigureReception()
        {           
            _receivedRays = new Dictionary<Guid, App.Optics.Ray>();
        }


        public void Receive(App.Optics.Ray ray, Guid sourceId)
        {            
            if (_receivedRays.ContainsKey(sourceId))
            {
                _receivedRays[sourceId] = ray;
            }
            else
            {
                _receivedRays.Add(sourceId, ray);
            }
            UpdateReceivedColor();
        }
                
        public void StopReceiving(Guid sourceId)
        {
            _receivedRays.Remove(sourceId);
            UpdateReceivedColor();
        }

        private void UpdateReceivedColor()
        {
            Dom.Data.Color CombinedColor = Dom.Data.Color.BlackC;
            foreach (KeyValuePair<Guid, App.Optics.Ray> kvp in _receivedRays)
            {
                CombinedColor = Dom.Data.Color.Add(CombinedColor, kvp.Value.Color);
            }
            _colorObservable.OnNext(CombinedColor);
        }
    }
}
