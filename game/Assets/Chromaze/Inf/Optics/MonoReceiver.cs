﻿using Chromaze.App.Environment;
using Chromaze.App.Optics;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    /// <summary>
    /// First Priority color receiver => color received is the one from the first ray received in order.
    /// </summary>
    public class MonoReceiver : BaseBehaviour, IMonoReceiver, IColorProvider
    {
        private Dictionary<Guid, App.Optics.Ray> _receivedRays = new Dictionary<Guid, App.Optics.Ray>();

        public IReadOnlyDictionary<Guid, App.Optics.Ray> ReceivedRays => _receivedRays;

        public IObservable<Dom.Data.Color> ObservableColor { get => _colorObservable; }

        public ReceptionStatus IsReceiving
        {
            get
            {
                if (ReceivedRays.Count > 0)
                {
                    return ReceptionStatus.Receiving;
                }
                else
                {
                    return ReceptionStatus.NotReceiving;
                }
            }
        }

        public Guid? SourceId
        {
            get
            {
                if (ReceivedRays.Count > 0)
                {
                    return ReceivedRays.First().Key;
                }
                else
                {
                    return null;
                }
            }
        }

        public App.Optics.Ray? ReceivedRay
        {
            get
            {
                if (ReceivedRays.Count > 0)
                {
                    return ReceivedRays.First().Value;
                }
                else
                {
                    return null;
                }
            }
        }

        private BehaviorSubject<Dom.Data.Color> _colorObservable = new BehaviorSubject<Dom.Data.Color>(Dom.Data.Color.BlackC);

        public void ConfigureReception()
        {
            //_receivedRays = new Dictionary<Guid, App.Optics.Ray>();
        }


        public void Receive(App.Optics.Ray ray, Guid sourceId)
        {
            if (_receivedRays.ContainsKey(sourceId))
            {
                _receivedRays[sourceId] = ray;
            }
            else
            {
                _receivedRays.Add(sourceId, ray);
            }
            UpdateReceivedColor();
        }

        public void StopReceiving(Guid sourceId)
        {
            _receivedRays.Remove(sourceId);
            UpdateReceivedColor();
        }

        private void UpdateReceivedColor()
        {
            Dom.Data.Color outputColor = Dom.Data.Color.BlackC;
            if(ReceivedRays.Count > 0)
            {
                outputColor = ReceivedRays.First().Value.Color;
            }
            _colorObservable.OnNext(outputColor);
        }
    }
}
