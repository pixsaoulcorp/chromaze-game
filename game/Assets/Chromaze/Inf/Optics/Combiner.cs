﻿using Chromaze.App.Optics;
using Chromaze.App.Optics.Factories;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Engine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    public class Combiner : BaseBehaviour, ICombiner
    {
        [SerializeField] private IMultiReceiverContainer _receiver;
        [SerializeField] private IEmitterFactoryContainer _emitterFactory;

        [SerializeField] private GameObject _emitterRoot;

        private IEmitter _emitter;

        public void SetData(Dom.Data.Combiner combiner)
        {
            _receiver.Result.ConfigureReception();
            if(_emitter == null) // no need for another emitter if we have one
            {
                _emitter = _emitterFactory.Result.Create(Dom.Data.EmitterStatus.Disabled, Dom.Data.Color.BlackC, _emitterRoot.transform, combiner.Id);
            }
        }

        void Update() // TODO TBU a lot of stuff the be simplified here imo
        {
            IReadOnlyDictionary<Guid, App.Optics.Ray> receivedRays= _receiver.Result.ReceivedRays;
            int receivedCount = receivedRays.Count;
            if(receivedCount == 0)
            {
                _emitter.Status = Dom.Data.EmitterStatus.Disabled;
                _emitter.Color = Dom.Data.Color.BlackC;
            } else
            {
                Dom.Data.Color outputColor = Dom.Data.Color.BlackC;
                foreach(KeyValuePair<Guid, App.Optics.Ray> kvp in receivedRays)
                {                    
                    outputColor = Dom.Data.Color.Add(outputColor,kvp.Value.Color);
                    //TODO small opti, if we're white, stop iterating over rays
                }
                _emitter.Status = EmitterStatus.Enabled;
                _emitter.Color = outputColor;
            }
        }
    }
}
