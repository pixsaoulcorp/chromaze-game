﻿using Chromaze.App.Optics;
using Chromaze.App.Optics.Factories;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Engine;
using System;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    public class Divider : BaseBehaviour, IDivider
    {
        [SerializeField] private IMonoReceiverContainer _receiver;

        [SerializeField] private IEmitterContainer _leftEmitter;
        [SerializeField] private IEmitterContainer _rightEmitter;
        public void SetData(Dom.Data.Divider divider)
        {
            _receiver.Result.ConfigureReception();
            _leftEmitter.Result.SetSourceId(divider.Id);
            _rightEmitter.Result.SetSourceId(divider.Id);
        }
        
        void Update()
        {
            UpdateEmission();
            //TODO TBU get all received ray and set the color of emission
        }

        private void UpdateEmission()
        {
            IMonoReceiver receiver = _receiver.Result;
            if (receiver.IsReceiving == ReceptionStatus.Receiving)
            {
                //TODO TBU find best way to handle null situation in received ray
                Dom.Data.Color receivedColor = (Dom.Data.Color)receiver.ReceivedRay?.Color;
                _leftEmitter.Result.SetData(EmitterStatus.Enabled, receivedColor);
                _rightEmitter.Result.SetData(EmitterStatus.Enabled, receivedColor);                
            }
            else if (receiver.IsReceiving == ReceptionStatus.NotReceiving)
            {
                _leftEmitter.Result.SetData(EmitterStatus.Disabled, Dom.Data.Color.BlackC);
                _rightEmitter.Result.SetData(EmitterStatus.Disabled, Dom.Data.Color.BlackC);
            }
        }
    }
}
