﻿using Chromaze.App.Optics;
using Chromaze.App.Optics.Factories;
using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    public class Mirror : BaseBehaviour, IMirror
    {
        [SerializeField] private IEmitterFactoryContainer _emitterFactory; // TODO TBU pass factory through constructors of view => cascade of factories for cpt
        [SerializeField] private Transform _emitterRoot;
        [SerializeField] private IMultiReceiverContainer _receiver;
        [SerializeField] private Transform _reflectionSurfaceTransform;
        
        private Dictionary<Guid, IEmitter> _reflectionsMapping;

        private Vector3 NormalDirection { get => _reflectionSurfaceTransform.forward.normalized; }
        public void SetData(Dom.Data.Mirror mirror)
        {
           _reflectionsMapping = new Dictionary<Guid, IEmitter>();
           _receiver.Result.ConfigureReception();
        }

        void Update()
        {
            DisplayNormal();
            IReadOnlyDictionary<Guid, App.Optics.Ray> rays = _receiver.Result.ReceivedRays;
            foreach(KeyValuePair<Guid, App.Optics.Ray> kvp in rays)
            {
                if (!ContainsEmitter(_reflectionsMapping, kvp.Key))
                {
                    if (_reflectionsMapping.ContainsKey(kvp.Key))
                    {
                        UpdateReflection(_reflectionsMapping,kvp.Key, kvp.Value);                       
                    }
                    else
                    {
                        IEmitter newEmitter = _emitterFactory.Result.Create(EmitterStatus.Enabled, kvp.Value.Color, _emitterRoot.transform);
                        newEmitter.Direction = MirroredDirection(kvp.Value.Source, kvp.Value.Target, NormalDirection); ;
                        _reflectionsMapping.Add(kvp.Key, newEmitter);
                    }
                }
            }
            RemoveOldReflections(rays, _reflectionsMapping);
        }

        private void UpdateReflection(Dictionary<Guid, IEmitter> emitterMap,Guid rayId, App.Optics.Ray incomingRay)
        {
            Position inDirection =(Position)((Vector3)incomingRay.Target - (Vector3)incomingRay.Source);
            emitterMap[rayId].Status = EmitterStatus.Enabled;
            emitterMap[rayId].Position = incomingRay.Target;
            emitterMap[rayId].Direction = MirroredDirection(incomingRay.Source, incomingRay.Target, NormalDirection);
            emitterMap[rayId].Color = incomingRay.Color;
        }

        private void RemoveOldReflections(IReadOnlyDictionary<Guid, App.Optics.Ray> newrays, Dictionary<Guid, IEmitter> oldmap)
        {
            // disabled non used emitters
            foreach (KeyValuePair<Guid, IEmitter> kvp in oldmap)
            {
                if (!newrays.ContainsKey(kvp.Key))
                {// TODO TBU analyse performance issues with keeping old rays. limite per source
                    kvp.Value.Status = EmitterStatus.Disabled;
                }
            }
        }

        private bool ContainsEmitter(Dictionary<Guid, IEmitter> reflectionsMapping, Guid emitterGuid)
        {
            foreach(var kvp in reflectionsMapping)
            {
                if(kvp.Value.Id == emitterGuid)
                {
                    return true;
                }
            }
            return false;
        }

        private void DisplayNormal()
        {
            Debug.DrawRay(transform.position, NormalDirection, UnityEngine.Color.blue);
        }



        /// <summary>
        /// Compute direction of the reflected ray
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="normal"></param>
        /// <returns></returns>
        private Vector3 MirroredDirection(Position source, Position target, Vector3 normal)
        {
            Debug.DrawLine(source, target, UnityEngine.Color.green);
            Debug.DrawRay(target, normal, UnityEngine.Color.red);

            // r output
            // d input
            // n normal direction
            // r =d−2(d⋅n)n
            //Debug.Log($"Source : {source} / Target : {target} / Normal : {normal}");
            Vector3 direction = ((Vector3)target - (Vector3)source).normalized;
            Vector3 output = direction - 2 * Vector3.Dot(direction, normal.normalized) * normal.normalized;
            //Debug.Log($"Output : {output}");
            Debug.DrawRay(target, output, UnityEngine.Color.black);
            return output.normalized; //TODO TBU investigate good practice to avoid new
        }

    }
}