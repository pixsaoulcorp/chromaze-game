﻿using Chromaze.App.Optics;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    public class Inverter : BaseBehaviour, IInverter
    {
        [SerializeField] private IMonoReceiverContainer _receiver;
        [SerializeField] private IEmitterContainer _emitterContainer;

        public void SetData(Dom.Data.Inverter inverter)
        {
            _receiver.Result.ConfigureReception();
            _emitterContainer.Result.SetSourceId(inverter.Id);
        }

        void Update()
        {
            UpdateEmission();
        }

        private void UpdateEmission()
        {
            IMonoReceiver receiver = _receiver.Result;
            if (receiver.IsReceiving == ReceptionStatus.Receiving)
            {
                Dom.Data.Color receivedColor = Dom.Data.Color.Invert((Dom.Data.Color)receiver.ReceivedRay?.Color); // color is ensured because it IS receiving
                _emitterContainer.Result.Color = receivedColor;
                _emitterContainer.Result.Status = EmitterStatus.Enabled;
            }
            else if (receiver.IsReceiving == ReceptionStatus.NotReceiving)
            {
                _emitterContainer.Result.Status = EmitterStatus.Disabled;
            }
        }
    }
}
