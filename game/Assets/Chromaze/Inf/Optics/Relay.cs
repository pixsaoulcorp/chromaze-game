﻿using Chromaze.App.Optics;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    public class Relay : BaseBehaviour, IRelay
    {
        [SerializeField] private IMonoReceiverContainer _receiver;
        [SerializeField] private IEmitterContainer _emitter;

        public void SetData(Dom.Data.Relay relay)
        {
            _receiver.Result.ConfigureReception();
            _emitter.Result.SetSourceId(relay.Id);

        }

        void Update()
        {
            UpdateEmission();
        }

        private void UpdateEmission()
        {
            IMonoReceiver receiver = _receiver.Result;
            if (receiver.IsReceiving == ReceptionStatus.Receiving)
            {
                Dom.Data.Color receivedColor = (Dom.Data.Color)receiver.ReceivedRay?.Color; // color is ensured because it IS receiving
                _emitter.Result.Color = receivedColor;
                _emitter.Result.Status = EmitterStatus.Enabled;
            }
            else if (receiver.IsReceiving == ReceptionStatus.NotReceiving)
            {
                _emitter.Result.Status = EmitterStatus.Disabled;
            }
        }
    }
}
