﻿using Chromaze.App.Optics;
using Chromaze.App.Optics.Factories;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Chromaze.Inf.Optics
{
    public class Decomposer : BaseBehaviour, IDecomposer
    {
        [SerializeField] private IMonoReceiverContainer _receiver;
        [SerializeField] private IEmitterFactoryContainer _emitterFactory;

        [SerializeField] private Transform _redEmitterRoot;
        [SerializeField] private Transform _greenEmitterRoot;
        [SerializeField] private Transform _blueEmitterRoot;

        private IEmitter _redEmitter;
        private IEmitter _greenEmitter;
        private IEmitter _blueEmitter;

        public void SetData(Dom.Data.Decomposer decomposer)
        {
            _receiver.Result.ConfigureReception();
            _redEmitter = _emitterFactory.Result.Create(Dom.Data.EmitterStatus.Disabled, Dom.Data.Color.RedC , _redEmitterRoot, decomposer.Id);
            _greenEmitter = _emitterFactory.Result.Create(Dom.Data.EmitterStatus.Disabled, Dom.Data.Color.GreenC, _greenEmitterRoot, decomposer.Id);
            _blueEmitter = _emitterFactory.Result.Create(Dom.Data.EmitterStatus.Disabled, Dom.Data.Color.BlueC, _blueEmitterRoot, decomposer.Id);
        }

        void Update()
        {
            UpdateEmission();
        }

        private void UpdateEmission()
        {
            IMonoReceiver receiver = _receiver.Result;
            if (receiver.IsReceiving == ReceptionStatus.Receiving)
            {
                //TODO TBU find best way to handle null situation in received ray
                Dom.Data.Color receivedColor = (Dom.Data.Color)receiver.ReceivedRay?.Color;
                _redEmitter.Status = receivedColor.Red ? EmitterStatus.Enabled : EmitterStatus.Disabled;
                _greenEmitter.Status = receivedColor.Green ? EmitterStatus.Enabled : EmitterStatus.Disabled;
                _blueEmitter.Status = receivedColor.Blue ? EmitterStatus.Enabled : EmitterStatus.Disabled;
            }
            else if (receiver.IsReceiving == ReceptionStatus.NotReceiving)
            {
                _redEmitter.Status = EmitterStatus.Disabled;
                _greenEmitter.Status = EmitterStatus.Disabled;
                _blueEmitter.Status = EmitterStatus.Disabled;
            }
        }
    }
}
