﻿using Chromaze.App.Optics;
using Chromaze.App.Optics.Factories;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Engine;
using System;
using UnityEngine;

namespace Chromaze.Inf.Optics.Factories
{
    public class EmitterFactory : BaseBehaviour, IEmitterFactory
    {
        [SerializeField] private Emitter _emitterPrefab;

        //TODO TBU maybe implement a cache system if performance require it
        public IEmitter Create(EmitterStatus status, Dom.Data.Color color, Transform targetParent, Guid? sourceId = null)
        {
            Emitter newInstance = Instantiate<Emitter>(_emitterPrefab);
            newInstance.transform.SetParent(targetParent, false);
            newInstance.SetData(status, color);
            if(sourceId != null)
            {
                newInstance.SetSourceId(sourceId.Value);
            } 
            else
            {
                newInstance.SetSourceId(Guid.NewGuid());
            }

            return newInstance;
        }
    }
}
