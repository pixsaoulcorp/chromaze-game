﻿using Chromaze.App.Level;
using Chromaze.App.Level.Download;
using Pixsaoul.App.Persistency;
using System;
using System.Collections.Generic;
using System.IO;
using PDom = Pixsaoul.Dom.Data.Engine;


namespace Chromaze.Inf.Level
{
    public class DownloadRepository : IDownloadRepository
    {
        [Inject(PersistencyType.Local)] public IFilePersistencyService LocalPersistency { get; set; }

        [Inject] public OnLevelDownloadedSignal LevelDownloadedSignal { get; set; }
        [Inject] public OnDownloadedLevelDeletedSignal LevelDeletedSignal { get; set; }

        private string _endpoint;

        public void Initialize()
        {
            _endpoint = Path.Combine("levels", "download");
        }

        public Dom.Data.Level GetLevel(PDom.Name name)
        {
            Persistency.Payload.LevelForDownload persistLevel = LocalPersistency.Read<Inf.Persistency.Payload.LevelForDownload>(LevelNameToEndpoint(name));
            Dom.Data.Level level = persistLevel.ToDom();
            return level;
        }

        public void SetLevel(Dom.Data.Level level)
        {
            Persistency.Payload.LevelToStore persistlevel = new Persistency.Payload.LevelToStore(level);
            LocalPersistency.Write(persistlevel, LevelNameToEndpoint(level.Name));
            LevelDownloadedSignal.Dispatch(level);
        }
        
        public void RemoveLevel(Pixsaoul.Dom.Data.Engine.Name name)
        {
            LocalPersistency.Delete(LevelNameToEndpoint(name));
            LevelDeletedSignal.Dispatch(name);
        }

        public List<Pixsaoul.Dom.Data.Engine.Name> GetDownloadedLevels()
        {
            string[] fileList = LocalPersistency.ReadFolder(_endpoint);
            List<PDom.Name> levels = GetLevelsName(fileList);
            return levels;
        }

        private List<Pixsaoul.Dom.Data.Engine.Name> GetLevelsName(string[] levels)
        {
            List<Pixsaoul.Dom.Data.Engine.Name> levelNames = new List<Pixsaoul.Dom.Data.Engine.Name>();
            for (int i = 0; i < levels.Length; i++)
            {
                if (Path.GetExtension(levels[i]).ToLowerInvariant() == ".json")
                {
                    //TODO TBU use name only for last part of the name
                    levelNames.Add(new Pixsaoul.Dom.Data.Engine.Name(Path.GetFileNameWithoutExtension(levels[i])));
                }
            }
            return levelNames;
        }

        private string LevelNameToEndpoint(PDom.Name name)
        {
            return Path.Combine(_endpoint, name + ".json");
        }
    }
}
