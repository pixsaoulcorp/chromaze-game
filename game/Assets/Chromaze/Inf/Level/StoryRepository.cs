﻿using Chromaze.App.Level;
using Chromaze.Inf.Persistency;
using Pixsaoul.App.Persistency;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using System.IO;
using PDom = Pixsaoul.Dom.Data.Engine;

namespace Chromaze.Inf.Level {
    public class StoryRepository : IService, IStoryRepository
    { 

        [Inject(PersistencyType.Internal)] public IPersistencyService InternalPersistency { get; set; }



        private string _storyEndpoint;
        public void Initialize()
        {
            _storyEndpoint = Path.Combine("levels","story");
        }

        public List<PDom.Name> GetStory()
        {
            Story persistStory = InternalPersistency.Read<Story>(Path.Combine(_storyEndpoint, "_story.json"));
            var story = persistStory.ToDom();
            return story.LevelsName;
        }

        public Dom.Data.Level GetStoryLevel(PDom.Name name)
        {
            Persistency.Payload.LevelToStore persistLevel = InternalPersistency.Read<Persistency.Payload.LevelToStore>(Path.Combine(_storyEndpoint, name + ".json"));
            Dom.Data.Level level = persistLevel.ToDom();
            return level;
        }
    }
}
