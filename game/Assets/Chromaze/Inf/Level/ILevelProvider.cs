﻿
using Chromaze.App.Components;
using Chromaze.Dom.Data;
using Chromaze.Inf.Components.Boundaries;
using Chromaze.Inf.Components.End;
using Chromaze.Inf.Components.Start;
using Pixsaoul.Dom.Data.Engine;
using System;
using System.Collections.Generic;

namespace Chromaze.Inf.Level
{
    [Serializable]
    public class ILevelProviderContainer : IUnifiedContainer<ILevelProvider> { }

    public interface ILevelProvider
    {
        IDictionary<ComponentId, IComponent> Level { get; }
        BoundariesView BoundariesView { get; }
        StartView StartView { get; }
        EndView EndView { get; }
    }
}
