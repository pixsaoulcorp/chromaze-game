﻿
using Chromaze.App.SceneWorkflow;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Inf.Level.Use
{
    public class LevelUserService : ILevelUserService
    { //TODO TBU make this service non monobehavior anymore
        [Inject] public OnLevelStartedSignal OnLevelStartedSignal { get; set; }
        [Inject] public OnLevelEndedSignal OnLevelEndedSignal { get; set; }

        public void Initialize()
        {
        }

        public void Begin()
        {
            OnLevelStartedSignal.Dispatch();
        }

        public void Cancel()
        {
            Debug.Log("Level cancelled");
        }

        public void End()
        {
            Debug.Log("Level finished succesfully");
            OnLevelEndedSignal.Dispatch();
        }
    }
}