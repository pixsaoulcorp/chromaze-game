﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.

using Chromaze.App.Optics.Factories;
using Chromaze.Dom.Data;
using Chromaze.Inf.Components;
using Chromaze.Inf.Components.BillBoards;
using Chromaze.Inf.Components.Boundaries;
using Chromaze.Inf.Components.Combiner;
using Chromaze.Inf.Components.Cube;
using Chromaze.Inf.Components.Decomposer;
using Chromaze.Inf.Components.Divider;
using Chromaze.Inf.Components.Door;
using Chromaze.Inf.Components.Emitter;
using Chromaze.Inf.Components.End;
using Chromaze.Inf.Components.Glass;
using Chromaze.Inf.Components.Inverter;
using Chromaze.Inf.Components.Light;
using Chromaze.Inf.Components.Mirror;
using Chromaze.Inf.Components.Relay;
using Chromaze.Inf.Components.Slope;
using Chromaze.Inf.Components.Start; //TODO use something to remove needs to have Inf => Pre link
using Chromaze.Inf.Components.Wall;
using Pixsaoul.Fou.Engine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.LevelImport
{
    /// <summary>
    ///
    /// </summary>
    public class ComponentFactory : BaseBehaviour
    {
        //Component prefabs
        [SerializeField] private SlopeView _slopePrefab;
        [SerializeField] private BoundariesView _boundariesPrefab;
        [SerializeField] private BillboardView _billboardPrefab;
        [SerializeField] private StartView _startingPrefab;
        [SerializeField] private EndView _endPrefab;
        [SerializeField] private CubeView _cubePrefab;
        [SerializeField] private DoorView _doorPrefab;
        [SerializeField] private WallView _wallPrefab;
        [SerializeField] private EmitterView _emitterPrefab;
        [SerializeField] private MirrorView _mirrorPrefab;
        [SerializeField] private CombinerView _combinerPrefab;
        [SerializeField] private DecomposerView _decomposerPrefab;
        [SerializeField] private DividerView _dividerPrefab;
        [SerializeField] private InverterView _inverterPrefab;
        [SerializeField] private RelayView _relayPrefab;
        [SerializeField] private GlassView _glassPrefab;
        [SerializeField] private LightView _lightPrefab;
        //TODO TBU when adding component
        private Dictionary<Type, ComponentView> _prefabMap;

        public void Initialize()
        {
            FillPrefabMap();
        }

        /// <summary>
        /// Fills the prefab map.
        /// </summary>
        private void FillPrefabMap()
        {
            _prefabMap = new Dictionary<Type, ComponentView>();
            _prefabMap.Add(typeof(Slope), _slopePrefab);
            _prefabMap.Add(typeof(Boundaries), _boundariesPrefab);
            _prefabMap.Add(typeof(End), _endPrefab);
            _prefabMap.Add(typeof(Billboard), _billboardPrefab);
            _prefabMap.Add(typeof(Start), _startingPrefab);
            _prefabMap.Add(typeof(Cube), _cubePrefab);
            _prefabMap.Add(typeof(Door), _doorPrefab);
            _prefabMap.Add(typeof(Wall), _wallPrefab);
            _prefabMap.Add(typeof(Emitter), _emitterPrefab);
            _prefabMap.Add(typeof(Mirror), _mirrorPrefab);
            _prefabMap.Add(typeof(Combiner), _combinerPrefab);
            _prefabMap.Add(typeof(Decomposer), _decomposerPrefab);
            _prefabMap.Add(typeof(Divider), _dividerPrefab);
            _prefabMap.Add(typeof(Inverter), _inverterPrefab);
            _prefabMap.Add(typeof(Relay), _relayPrefab);
            _prefabMap.Add(typeof(Glass), _glassPrefab);
            _prefabMap.Add(typeof(Dom.Data.Light), _lightPrefab);
            //TODO TBU when adding component

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sourceData"></param>
        /// <returns></returns>
        public ComponentView InstantiateComponent(Dom.Data.Component sourceData, GameObject desiredParent)
        {
            ComponentView prefab = GetPrefab(sourceData.GetType());
            ComponentView newInstance = null;
            if (prefab != null)
            {
                newInstance = Instantiate<ComponentView>(prefab);
                newInstance.transform.SetParent(desiredParent.transform);
                newInstance.SetComponentData(sourceData);
            }
            return newInstance;
        }

        /// <summary>
        /// Gets the prefab.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <returns></returns>
        private ComponentView GetPrefab(Type dataType)
        {
            //Debug.Log(dataType.ToString());
            ComponentView prefab;
            _prefabMap.TryGetValue(dataType, out prefab);
            return prefab;
        }
    }
}