﻿
using Chromaze.App.Components;
using Chromaze.Dom.Data;
using Chromaze.Inf.Components;
using Chromaze.Inf.Components.Boundaries;
using Chromaze.Inf.Components.End;
using Chromaze.Inf.Components.Start;
using Chromaze.Inf.Level;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Extensions;
using Pixsaoul.Fou.Mvcs;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.LevelImport
{
    public class LevelImporterView : View, ILevelProvider
    {
        private IDictionary<ComponentId, IComponent> _currentComponents;

        public IDictionary<ComponentId, IComponent>  Level { get => _currentComponents;}

        public BoundariesView BoundariesView { get; private set; }
        public StartView StartView { get; private set; }
        public EndView EndView { get; private set; }

        [SerializeField]
        private ComponentFactory _componentFactory;

        [SerializeField]
        private GameObject _componentsRoot;

        public override void Initialize()
        {
            base.Initialize();
            _componentFactory.Initialize();
            _currentComponents = new Dictionary<ComponentId, IComponent>();
        }

        public void ImportLevel(Dom.Data.Level level)
        {
            BoundariesView = (BoundariesView)Create(level.Content.Boundaries);
            StartView = (StartView)Create(level.Content.Start);
            EndView = (EndView)Create(level.Content.End); //TODO TBU remove these casts
            foreach (Dom.Data.Component component in level.Content.Components)
            {
                IComponent cptView = CreateComponent(component);                
            }
        }

        public IComponent CreateComponent(Dom.Data.Component component)
        { // TODO TBU Test if component already exist. Should not happen often
            IComponent componentView = Create(component);
            _currentComponents.Add(component.Id, componentView);
            return componentView;
        }

        private IComponent Create(Dom.Data.Component component)
        {
            ComponentView componentView = _componentFactory.InstantiateComponent(component, _componentsRoot);
            return componentView;
        }

        public bool DeleteComponent(Dom.Data.Component component)
        {
            IComponent componentToDelete;
            _currentComponents.TryGetValue(component.Id, out componentToDelete);
            if(componentToDelete != null)
            {
                _currentComponents.Remove(component.Id);
                Destroy(((ComponentView)componentToDelete).gameObject); //TODO TBU avoid this cast
                return true;
            }
            return false; // we didn't delete anything
        }

        public void ClearLevel()
        {
            _currentComponents.Clear();
            _componentsRoot.DeleteChildren();
        }
    }
}