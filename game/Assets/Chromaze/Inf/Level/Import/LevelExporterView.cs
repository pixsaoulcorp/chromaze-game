﻿using Chromaze.App.Components;
using Chromaze.App.Level.Import;
using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Inf.Level.Import
{
    public class LevelExporterView : View, ILevelExporter
    {
        [SerializeField] private ILevelProviderContainer _levelProvider;
        
        public Signal<Dom.Data.Level> ExportLevelSignal = new Signal<Dom.Data.Level>();
        public override void Initialize()
        {
            base.Initialize();
        }

        public void ExportLevel(Pixsaoul.Dom.Data.Engine.Name name)
        {
            ILevelProvider provider = _levelProvider.Result;
            IDictionary<ComponentId, IComponent> levelViews = provider.Level;
            List<Dom.Data.Component> components = new List<Dom.Data.Component>();
            Boundaries boundaries = provider.BoundariesView.GetData();
            Start start = provider.StartView.GetData();
            End end = provider.EndView.GetData();
            foreach (KeyValuePair<ComponentId, IComponent> kvp in levelViews)
            {
                Dom.Data.Component cpt = kvp.Value.GetComponentData();               
                components.Add(cpt);
            }
            //TODO TBU maybe need the actual id here
            Dom.Data.Level level = new Dom.Data.Level(-1, name, new Dom.Data.LevelContent(boundaries, start, end, components));
            ExportLevelSignal.Dispatch(level);
        }
    }
}