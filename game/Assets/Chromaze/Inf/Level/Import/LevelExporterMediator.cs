﻿using Chromaze.App.Level.Import;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.Level.Import
{
    public class LevelExporterMediator : Mediator
    {
        [Inject] public LevelExporterView View { get; set; }
        protected override View GetView()
        {
            return View;
        }
        [Inject] public RequestLevelExportSignal RequestLevelExportSignal { get; set; }

        public override void OnRegister()
        {
            View.ExportLevelSignal.AddListener(RequestExportLevel);
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();
            View.ExportLevelSignal.RemoveListener(RequestExportLevel);
        }

        private void RequestExportLevel(Dom.Data.Level level)
        {
            RequestLevelExportSignal.Dispatch(level);
        }
    }
}
