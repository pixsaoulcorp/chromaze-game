﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.

using Pixsaoul.Fou.Mvcs;
using Pixsaoul.App.Persistency;
using System.Collections.Generic;
using UnityEngine;
using Chromaze.App.Level.Import;
using PDom = Pixsaoul.Dom.Data.Engine;
using System.IO;
using Pixsaoul.Inf.Persistency;

namespace Chromaze.Inf.Level.Import
{
    //TODO TBU maybe remove this service, persistency and everything are split
    public class LevelImporterService : Service, ILevelImporterService
    {
        [Inject(PersistencyType.Internal)] public IPersistencyService internalio {get;set;}
        [Inject(PersistencyType.Local)] public IFilePersistencyService localio { get; set; }

        [Inject] public OnLevelImportedSignal OnLevelImportedSignal { get; set; }


        public override void Initialize()
        {
            base.Initialize();
        }
        
        public void ImportLevel(Dom.Data.Level level)
        {
            Debug.Log("importing " + level.Name);
            StartCoroutine(TestDelayedImport(level));
        }

        private IEnumerator<WaitForSeconds> TestDelayedImport(Dom.Data.Level level)
        { //TODO TBU find a way to not need this delayed import.
          // guess : initialization of the levelloader mediator maybe.
            yield return new WaitForSeconds(1f);
            OnLevelImportedSignal.Dispatch(level);
        }

    }
}