﻿using Chromaze.App.Components;
using Chromaze.App.Edition;
using Chromaze.App.Level.Import;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Inf.LevelImport
{
    public class LevelImporterMediator : Mediator
    {
        [Inject]
        public LevelImporterView View { get; set; }

        [Inject] public OnLevelImportedSignal OnLevelImportedSignal { get; set; }

        [Inject] public RequestComponentCreationSignal RequestComponentCreationSignal { get; set; }

        [Inject] public OnComponentCreatedSignal OnComponentCreatedSignal { get; set; }

        [Inject] public RequestDeleteComponentSignal RequestDeleteComponentSignal { get; set; }

        [Inject] public NotifyComponentDeletedSignal NotifyComponentDeletedSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            OnLevelImportedSignal.AddListener(ImportLevel);
            RequestComponentCreationSignal.AddListener(AddComponent);
            RequestDeleteComponentSignal.AddListener(RemoveComponent);
        }

        public override void OnRemove()
        {
            OnLevelImportedSignal.RemoveListener(ImportLevel);
            RequestComponentCreationSignal.RemoveListener(AddComponent);
            RequestDeleteComponentSignal.RemoveListener(RemoveComponent);
            base.OnRemove();

        }

        public void ImportLevel(Dom.Data.Level level)
        {
            View.ImportLevel(level);
        }

        public void AddComponent(Dom.Data.Component component)
        {
            IComponent componentView = View.CreateComponent(component);
            OnComponentCreatedSignal.Dispatch(componentView);
        }

        public void RemoveComponent(Dom.Data.Component component)
        {
            if (View.DeleteComponent(component))
            {
                NotifyComponentDeletedSignal.Dispatch(component);
            }
        }
    }
}