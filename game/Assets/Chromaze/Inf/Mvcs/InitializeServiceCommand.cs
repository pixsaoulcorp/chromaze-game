﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Pixsaoul.App.SceneManagement;
using Pixsaoul.App.AugmentedReality;
using Pixsaoul.App.Persistency;
using Pixsaoul.App.Input;
using Pixsaoul.App.Advertising;
using Chromaze.App.Level.Import;
using Chromaze.App.SceneWorkflow;
using Chromaze.App.Level.Story;
using Chromaze.App.Level;
using Chromaze.App.Online;
using Pixsaoul.App.Serialization;

namespace Chromaze.Inf.Mvcs
{
    /// <summary>
    /// Command
    /// </summary>
    public class InitializeServiceCommand : Pixsaoul.Fou.Mvcs.InitializeServiceCommand
    {
        [Inject(SerializationType.Binary)] public ISerializerService _binarySerializer { get; set; }
        [Inject] public ISerializerService _defaultSerializer { get; set; }

        [Inject] public IAugmentedRealityService _arService { get; set; }
        [Inject(PersistencyType.Internal)] public IPersistencyService _internalIoService { get; set; }
        [Inject] public IAsyncPersistencyService _remoteIoService { get; set; }
        [Inject(PersistencyType.Local)] public IFilePersistencyService _localIoService { get; set; }
        [Inject] public IUserInputService _playerService { get; set; }
        [Inject] public ISceneManagerService _sceneManagerService { get; set; }
        [Inject] public IAdvertisingService _advertisingService { get; set; }
        [Inject] public ILevelImporterService _levelImporterService{ get; set; }
        [Inject] public ILevelUserService _levelUserService { get; set; }
        [Inject] public IStoryRepository _storyRepository { get; set; }
        [Inject] public ICreatorRepository _creatorRepository { get; set; }
        [Inject] public IDownloadRepository _downloadRepository { get; set; }
        [Inject] public IOnlineRepository _onlineRepository { get; set; }


        /// <summary>
        /// Execute method to override when creating a command
        /// call base.execute after your internal behaviour
        /// </summary>
        public override void Execute()
        {
            _binarySerializer.Initialize();
            _defaultSerializer.Initialize();

            _internalIoService.Initialize();
            _localIoService.Initialize();
            _remoteIoService.Initialize();
            _arService.Initialize();
            _playerService.Initialize();
            _sceneManagerService.Initialize();
            _advertisingService.Initialize();

            _levelImporterService.Initialize();
            _levelUserService.Initialize();

            // repositories
            _storyRepository.Initialize();
            _creatorRepository.Initialize();
            _downloadRepository.Initialize();
            _onlineRepository.Initialize();

            base.Execute();
        }
    }
}