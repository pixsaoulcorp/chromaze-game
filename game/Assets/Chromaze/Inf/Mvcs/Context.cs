﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Chromaze.App.Level.Import;
using Chromaze.App.Level.Story;
using Chromaze.App.SceneWorkflow;
using Chromaze.Inf.Components.Mirror;
using Chromaze.Inf.Components.Emitter;
using Chromaze.Inf.Components.Cube;
using Chromaze.Inf.Components.Door;
using Chromaze.Inf.Components.End;
using Chromaze.Inf.Components.Start;
using Chromaze.Pre.Level.AutoLoader;
using Chromaze.Inf.LevelImport;
using Chromaze.Pre.Menu;
using Chromaze.Inf.Player;
using Pixsaoul.App;
using Pixsaoul.App.SceneManagement;
using System;
using UnityEngine;
using Chromaze.Inf.Components.Combiner;
using Chromaze.Inf.Components.Decomposer;
using Chromaze.Inf.Components.Divider;
using Chromaze.Inf.Components.Inverter;
using Chromaze.Inf.Components.Relay;
using Pixsaoul.App.Camera;
using Chromaze.Pre.Level.Selection;
using Chromaze.Pre.PlayOption;
using Pixsaoul.App.UserInput;
using Pixsaoul.Inf.Camera;
using Chromaze.App.Player;
using Chromaze.Pre.Selection;
using Chromaze.App.Control;
using Chromaze.App.Selection;
using Pixsaoul.App.BoxelGeneration;
using Chromaze.Inf.Components.Wall;
using Chromaze.App.Level;
using Chromaze.Inf.Level;
using Pixsaoul.Inf.Control;
using Chromaze.Pre.Level.Creator;
using Chromaze.Pre.EndLevel;
using Chromaze.Inf.Level.Import;
using Chromaze.Pre.Level.EditableInfo;
using Chromaze.Inf.Components.Boundaries;
using Chromaze.Pre.Component.Creation;
using Chromaze.Pre.Component.Edition;
using Chromaze.Pre.EditorOption;
using Chromaze.App.Edition;
using Pixsaoul.Inf.Focus;
using Chromaze.Inf.Mock;
using Chromaze.Inf.Components.Slope;
using Chromaze.Pre.Settings;
using Chromaze.Pre.Level.Online;
using Chromaze.App.Level.Creator;
using Chromaze.App.Level.Download;
using Chromaze.App.Online;
using Chromaze.Inf.Online;
using Chromaze.Pre.Online;
using Chromaze.Pre.Gizmos;
using Chromaze.App.EndLevel;
using Chromaze.Inf.Components.BillBoards;
using Chromaze.Pre.Skybox;
using Chromaze.Inf.Components.Glass;
using Chromaze.Inf.Components.Light;
using Pixsaoul.Pre.SceneTransition;
using Pixsaoul.App.SceneTransition;
using Chromaze.App.Components.Start;

namespace Chromaze.Inf.Mvcs
{
    /// <summary>
    /// LB Context
    /// </summary>
    public class Context : Pixsaoul.Fou.Mvcs.Context // This requires link to app, inf and pre, so choice is not obvious "^^
    {
        //TODO TBU find the right layer of context
        public Context(MonoBehaviour view) : base(view)
        {
        }

        protected override void BindServices()
        {
            base.BindServices();

            // bind non monobehaviour services
            injectionBinder.Bind<IStoryRepository>().To<StoryRepository>().ToSingleton();
            injectionBinder.Bind<ICreatorRepository>().To<CreatorRepository>().ToSingleton();
            injectionBinder.Bind<IDownloadRepository>().To<DownloadRepository>().ToSingleton();

            injectionBinder.Bind<IOnlineRepository>().To<OnlineRepository>().ToSingleton();

            // bind anything that is not MVCS for test purpose for instance
            injectionBinder.Bind<ILevelMock>().To<LevelMock>().ToSingleton();
        }

        protected override void BindModels()
        {
            base.BindModels();
        }

        protected override void BindMediations()
        {
            base.BindMediations();

            // Main  Menu views
            mediationBinder.Bind<MainMenuView>().To<MainMenuMediator>();
            mediationBinder.Bind<StoryModeMenu>().To<StoryModeMenuMediator>();
            mediationBinder.Bind<CreatorModeMenu>().To<CreatorModeMenuMediator>();
            mediationBinder.Bind<DownloadModeMenu>().To<DownloadModeMenuMediator>();
            mediationBinder.Bind<OnlineMenu>().To<OnlineMenuMediator>();
            mediationBinder.Bind<SettingsMenu>().To<SettingsMenuMediator>();
            mediationBinder.Bind<SceneTransitionView>().To<SceneTransitionMediator>();

            //Play Menu
            mediationBinder.Bind<PlayOptionView>().To<PlayOptionMediator>();
            mediationBinder.Bind<VictoryPanelView>().To<VictoryPanelMediator>();

            //Create Menu
            mediationBinder.Bind<EditorOptionView>().To<EditorOptionMediator>();
            mediationBinder.Bind<LevelEditableInfoView>().To<LevelEditableInfoMediator>();
            mediationBinder.Bind<ComponentCreationView>().To<ComponentCreationMediator>();
            mediationBinder.Bind<ComponentEditionView>().To<ComponentEditionMediator>();

            //Player 
            mediationBinder.Bind<PlayerView>().To<PlayerMediator>();

            //Loading
            mediationBinder.Bind<LevelAutoLoaderView>().To<LevelAutoLoaderMediator>();
            mediationBinder.Bind<LevelImporterView>().To<LevelImporterMediator>();
            mediationBinder.Bind<LevelExporterView>().To<LevelExporterMediator>();

            //Camera
            mediationBinder.Bind<CameraView>().To<CameraMediator>();

            // control view
            mediationBinder.Bind<RotaterView>().To<RotaterMediator>();
            mediationBinder.Bind<SelectionView>().To<SelectionMediator>();

            mediationBinder.Bind<GizmoView>().To<GizmoMediator>();

            mediationBinder.Bind<SkyboxView>().To<SkyboxMediator>();

            // Focus
            mediationBinder.Bind<FocusView>().To<FocusMediator>();
            mediationBinder.Bind<TouchFocusView>().To<TouchFocusMediator>();

            // Components views
            mediationBinder.Bind<SlopeView>().To<SlopeMediator>();
            mediationBinder.Bind<BoundariesView>().To<BoundariesMediator>();
            mediationBinder.Bind<StartView>().To<StartMediator>();
            mediationBinder.Bind<BillboardView>().To<BillboardMediator>();
            mediationBinder.Bind<EndView>().To<EndMediator>();
            mediationBinder.Bind<CubeView>().To<CubeMediator>();
            mediationBinder.Bind<WallView>().To<WallMediator>();
            mediationBinder.Bind<DoorView>().To<DoorMediator>();
            mediationBinder.Bind<EmitterView>().To<EmitterMediator>();
            mediationBinder.Bind<MirrorView>().To<MirrorMediator>();
            mediationBinder.Bind<CombinerView>().To<CombinerMediator>();
            mediationBinder.Bind<DecomposerView>().To<DecomposerMediator>();
            mediationBinder.Bind<DividerView>().To<DividerMediator>();
            mediationBinder.Bind<InverterView>().To<InverterMediator>();
            mediationBinder.Bind<RelayView>().To<RelayMediator>();
            mediationBinder.Bind<GlassView>().To<GlassMediator>();
            mediationBinder.Bind<LightView>().To<LightMediator>();
        }

        /// <summary>
        ///
        /// </summary>
        protected override void BindCommands()
        {
            base.BindCommands();

            commandBinder.Bind<RequestExitSignal>().To<RequestExitCommand>();
            commandBinder.Bind<RequestChangeSceneSignal>().To<RequestLoadSceneCommand>();

            commandBinder.Bind<OpenMenuSignal>().To<OpenMenuCommand>();
            //Loading
            commandBinder.Bind<OnLevelImportedSignal>().To<OnLevelImportedCommand>();
            commandBinder.Bind<RequestLevelExportSignal>().To<ExportLevelCommand>();

            injectionBinder.Bind<RequestMovePlayerToStartSignal>().ToSingleton();
            injectionBinder.Bind<OnStartViewPlacedSignal>().ToSingleton();
            injectionBinder.Bind<OnStartViewReadyForPlayerSignal>().ToSingleton();



            // Transition fade
            injectionBinder.Bind<RequestFadeInSignal>().ToSingleton();
            injectionBinder.Bind<RequestFadeOutSignal>().ToSingleton();
            injectionBinder.Bind<OnFadedInSignal>().ToSingleton(); // maybeoverkill
            injectionBinder.Bind<OnFadedOutSignal>().ToSingleton(); // maybeoverkill


            commandBinder.Bind<OnLevelStartedSignal>().To<OnLevelStartedCommand>();
            commandBinder.Bind<RequestEndLevelSignal>().To<EndLevelCommand>();
            commandBinder.Bind<OnLevelEndedSignal>().To<OnLevelEndedCommand>(); // maybeoverkill
            injectionBinder.Bind<NotifyLevelEndedSignal>().ToSingleton(); // maybeoverkill
            commandBinder.Bind<RequestAutoloadLevelSignal>().To<LevelAutoloadCommand>();

            commandBinder.Bind<PlayStoryLevelSignal>().To<PlayStoryLevelCommand>();
            commandBinder.Bind<PlayCreatorLevelSignal>().To<PlayCreatorLevelCommand>();
            commandBinder.Bind<PlayDownloadedLevelSignal>().To<PlayDownloadedLevelCommand>();
            commandBinder.Bind<LoadPlayLevelSignal>().To<LoadPlayLevelCommand>();

            commandBinder.Bind<EditCreationLevelSignal>().To<EditCreationCommand>();
            commandBinder.Bind<RequestExitLevelSignal>().To<RequestExitLevelCommand>();

            // control signals
            injectionBinder.Bind<EnablePlayerMovementSignal>().ToSingleton();
            commandBinder.Bind<ReleaseComponentSelectionSignal>().To<StopComponentControlCommand>();

            // Component edition
            injectionBinder.Bind<RequestComponentCreationSignal>().ToSingleton();
            injectionBinder.Bind<OnComponentCreatedSignal>().ToSingleton();
            injectionBinder.Bind<NotifyComponentEditionBeginSignal>().ToSingleton();

            injectionBinder.Bind<NotifyComponentEditionEndedSignal>().ToSingleton();
            commandBinder.Bind<RequestEndComponentEditionSignal>().To<EndComponentEditionCommand>();

            commandBinder.Bind<RequestComponentSelectionSignal>().To<FocusComponentCommand>();
            injectionBinder.Bind<RequestOpenComponentCreationSignal>().ToSingleton();
            injectionBinder.Bind<RequestCloseComponentCreationSignal>().ToSingleton();
            injectionBinder.Bind<RequestDeleteComponentSignal>().ToSingleton();
            injectionBinder.Bind<NotifyComponentDeletedSignal>().ToSingleton();
            injectionBinder.Bind<RequestDeleteSelectedComponentSignal>().ToSingleton();

            // input command
            injectionBinder.Bind<NotifyMainAxisInputSignal>().ToSingleton();
            injectionBinder.Bind<NotifySecondaryAxisInputSignal>().ToSingleton();
            injectionBinder.Bind<NotifyMainButtonInputSignal>().ToSingleton();
            injectionBinder.Bind<NotifySecondaryButtonInputSignal>().ToSingleton();

            commandBinder.Bind<EnableInputSignal>().To<RequestEnableInputCommand>();
            commandBinder.Bind<MainAxisInputSignal>().To<OnMainAxisInputCommand>();
            commandBinder.Bind<SecondaryAxisInputSignal>().To<OnSecondaryAxisInputCommand>();
            commandBinder.Bind<MainButtonInputSignal>().To<OnMainButtonInputCommand>();
            commandBinder.Bind<SecondaryButtonInputSignal>().To<OnSecondaryButtonInputCommand>();

            //Level menu
            commandBinder.Bind<RequestStorySignal>().To<RequestStoryCommand>();
            commandBinder.Bind<RequestStoryLevelSignal>().To<RequestStoryLevelCommand>();
            commandBinder.Bind<RequestCreationsSignal>().To<RequestCreationsCommand>();
            commandBinder.Bind<RemoveCreatorLevelSignal>().To<RemoveCreatorLevelCommand>();

            //level download
            commandBinder.Bind<RequestDownloadedSignal>().To<RequestDownloadedCommand>();
            commandBinder.Bind<RequestDownloadedLevelSignal>().To<RequestDownloadedLevelCommand>();
            commandBinder.Bind<RemoveDownloadLevelSignal>().To<RemoveDownloadLevelCommand>();
            commandBinder.Bind<OnLevelDownloadedSignal>().To<OnLevelDownloadedCommand>();
            commandBinder.Bind<OnDownloadedLevelDeletedSignal>().To<OnDownloadedLevelDeletedCommand>();

            //online level menu
            commandBinder.Bind<RequestLevelListSignal>().To<RequestLevelListCommand>();
            commandBinder.Bind<RequestDownloadLevelSignal>().To<RequestDownloadLevelCommand>();
            commandBinder.Bind<UploadCreatorLevelSignal>().To<UploadCreatorLevelCommand>();

            injectionBinder.Bind<OpenSubMenuSignal>().ToSingleton();

            // Object generation
            commandBinder.Bind<RequestBoxelObjectSignal>().To<RequestBoxelObjectCommand>();

            //TODO TBU organize command binding per domain,
            //input management events

            //Camera signals
            injectionBinder.Bind<SelectFixedCameraSignal>().ToSingleton();
            injectionBinder.Bind<SelectThirdPersonCameraSignal>().ToSingleton();
            injectionBinder.Bind<SelectFirstPersonCameraSignal>().ToSingleton();
            injectionBinder.Bind<SelectFreeCameraSignal>().ToSingleton();
            injectionBinder.Bind<RequestCameraProviderSignal>().ToSingleton();
            injectionBinder.Bind<NotifyCameraProviderSignal>().ToSingleton();

            //Non command signals

            //Program workflow signals
            injectionBinder.Bind<OnSceneChangedSignal>().ToSingleton();
            injectionBinder.Bind<RequestSetPlayerLocationSignal>().ToSingleton();
            injectionBinder.Bind<NotifyLevelImportedSignal>().ToSingleton();

            //win panel
            commandBinder.Bind<RequestVictoryQuotesSignal>().To<LoadVictoryQuotesCommand>();

        }

        protected override Type GetInitializeServiceCommand()
        {
            return typeof(InitializeServiceCommand);
        }

        protected override Type GetInitializeModelCommand()
        {
            return typeof(InitializeModelCommand);
        }

        protected override Type GetOnInitializedCommand()
        {
            return typeof(OnInitializedCommand);
        }
    }
}