﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.App.SceneManagement;
using Chromaze.Inf.Mock;

namespace Chromaze.Inf.Mvcs
{
    /// <summary>
    /// Command
    /// </summary>
    public class OnInitializedCommand : Pixsaoul.Fou.Mvcs.OnInitializedCommand
    {
        [Inject]
        public ISceneManagerService _sceneManager { get; set; }

        [Inject] public ILevelMock LevelMock { get; set; }

        public override void Execute()
        {
            base.Execute();
#if UNITY_EDITOR
            LevelMock.Export();
#endif
            _sceneManager.LoadInitialScene();
        }
    }
}