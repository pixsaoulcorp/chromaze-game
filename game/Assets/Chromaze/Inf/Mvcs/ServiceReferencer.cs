﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.App.SceneManagement;
using System;
using Pixsaoul.App.AugmentedReality;
using Pixsaoul.App.Input;
using Chromaze.App.Level.Import;
using Pixsaoul.App.BoxelGeneration;
using Pixsaoul.App.Persistency;

namespace Chromaze.Inf.Mvcs
{
    /// <summary>
    /// Chromaze service referencer for monobehavior services
    /// </summary>
    public class ServiceReferencer : Pixsaoul.Fou.Mvcs.ServiceReferencer
    {
        public IAugmentedRealityServiceContainer AugmentedReality;        
        public IUserInputServiceContainer Input;
        public ISceneManagerServiceContainer SceneManager;
        public IBoxelGenerationServiceContainer BoxelGeneration;
        public ILevelImporterServiceContainer LevelImporter;
        public IAsyncPersistencyServiceContainer AsyncPersistency;

        /// <summary>
        /// Apply binding callback
        /// </summary>
        /// <param name="callback"></param>
        public override void ApplyBindingCallback(Action<Type, IService> callback)
        {
            base.ApplyBindingCallback(callback);
 
            callback(typeof(IAugmentedRealityService), AugmentedReality.Result);
            callback(typeof(IUserInputService), Input.Result);
            callback(typeof(ISceneManagerService), SceneManager.Result);
            callback(typeof(IBoxelGenerationService), BoxelGeneration.Result);

            callback(typeof(ILevelImporterService), LevelImporter.Result);
            callback(typeof(IAsyncPersistencyService), AsyncPersistency.Result);
        }

    }
}