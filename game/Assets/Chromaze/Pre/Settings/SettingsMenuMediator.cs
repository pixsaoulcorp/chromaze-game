﻿using Chromaze.Pre.Menu;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Settings
{

    public class SettingsMenuMediator : SubMenuMediator
    {
        [Inject]
        public SettingsMenu View { get; set; }

        protected override View GetView()
        {
            return View;
        }

        protected override SubMenuType ManagedType()
        {
            return SubMenuType.Settings;
        }
    }
}