﻿using Chromaze.Pre.Menu;
using TMPro;
using UnityEngine;

namespace Chromaze.Pre.Settings
{
    public class SettingsMenu : SubMenuView
    {
        [SerializeField] private TMP_Text _versionText;
        public override void Initialize()
        {
            base.Initialize();
            GetProjectVersion();
        }

        private void GetProjectVersion()
        {
            string version = Application.version;
            _versionText.text = version;
        }

        public void OpenDiscordInviteBrowser()
        {
            Application.OpenURL("https://discord.gg/FdArttMFXr");
        }
    }
}

