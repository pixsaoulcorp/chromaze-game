﻿
using Chromaze.Dom.Data.Payload;
using Chromaze.Pre.Menu;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Pre.Menu;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Online
{
    public class OnlineMenu : SubMenuView
    {

        [SerializeField] private Button _searchAllButton;
        [SerializeField] private Button _downloadLevel;
        [SerializeField] private Button _unselectLevel;
        [SerializeField] private LevelInfoList _levelInfoList;

        [SerializeField] private GameObject _levelDetailRoot;
        [SerializeField] private TMP_Text _levelName;
        [SerializeField] private TMP_Text _levelId;

        public Signal<LevelForListOfLevel> PreSelectSignal = new Signal<LevelForListOfLevel>();
        public Signal SearchAllLevelSignal = new Signal();
        public Signal DownloadPreselectedLevelSignal = new Signal();
        public Signal UnselectSignal = new Signal();

        public override void Initialize()
        {
            base.Initialize();
            _levelInfoList.Initialize(PreselectLevel);
            _searchAllButton.onClick.AddListener(GetLevelList);
            _downloadLevel.onClick.AddListener(RequestDownloadLevel);
            _unselectLevel.onClick.AddListener(UnselectLevel);
            CloseLevelDetail();
            GetLevelList();
        }

        private void GetLevelList()
        {
            SearchAllLevelSignal.Dispatch();
        }

        public void OnLevelListChanged(ICollection<LevelForListOfLevel> levelsInfo)
        {
            _levelInfoList.Set(levelsInfo);
        }

        private void PreselectLevel(LevelForListOfLevel levelInfo)
        {
            _levelName.text = levelInfo.Name;
            _levelId.text = levelInfo.Id.ToString();
            PreSelectSignal.Dispatch(levelInfo);
        }

        private void UnselectLevel()
        {
            UnselectSignal.Dispatch();
        }

        private void RequestDownloadLevel()
        {
            DownloadPreselectedLevelSignal.Dispatch();
        }

        public void OpenLevelDetail(LevelForListOfLevel levelInfo)
        {
            _levelDetailRoot.SetActive(true);
        }

        public void CloseLevelDetail()
        {
            _levelDetailRoot.SetActive(false);
        }
    }
}
