﻿using Chromaze.Dom.Data.Payload;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Fou.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Pre.Online
{
    public class LevelInfoList : BaseBehaviour
    { //TODO TBU use DataElementList

        [SerializeField] private LevelInfoButton _buttonPrefab;
        [SerializeField] private GameObject _listRoot;

        private Action<LevelForListOfLevel> _callback;
        public void Initialize(Action<LevelForListOfLevel> callback)
        {
            _callback = callback;
        }

        public void Set(ICollection<LevelForListOfLevel> levelsInfo)
        {
            Clear();
            foreach(LevelForListOfLevel info in levelsInfo)
            {
                Create(info, _callback);
            }
        }

        private  LevelInfoButton Create(LevelForListOfLevel info, Action<LevelForListOfLevel> callback)
        {
            LevelInfoButton button = Instantiate<LevelInfoButton>(_buttonPrefab, _listRoot.transform, false);
            button.Initiaze(callback, info);
            return button;
        }

        private void Clear()
        {
            _listRoot.DeleteChildren();
        }

    }
}
