﻿using Chromaze.App.Online;
using Chromaze.Dom.Data.Payload;
using Chromaze.Pre.Menu;
using Pixsaoul.Fou.Mvcs;
using System.Collections.Generic;

namespace Chromaze.Pre.Online
{
    public class OnlineMenuMediator : SubMenuMediator
    {
        private LevelForListOfLevel _preselectedLevel;
        [Inject] public RequestLevelListSignal RequestLevelListSignal{ get; set; }
        [Inject] public RequestDownloadLevelSignal DownloadLevelSignal { get; set; }


        [Inject] public OnlineMenu View { get; set; }
        protected override View GetView()
        {
            return View;
        }

        protected override SubMenuType ManagedType()
        {
            return SubMenuType.Online;
        }
        public override void OnRegister()
        {
            base.OnRegister();
            View.SearchAllLevelSignal.AddListener(RequestLevelList);
            View.PreSelectSignal.AddListener(RequestLevelPreselection);
            View.DownloadPreselectedLevelSignal.AddListener(DownloadLevel);
            View.UnselectSignal.AddListener(UnselectLevel);
        }

        public override void OnRemove()
        {
            View.SearchAllLevelSignal.RemoveListener(RequestLevelList);
            View.PreSelectSignal.RemoveListener(RequestLevelPreselection);
            View.DownloadPreselectedLevelSignal.RemoveListener(DownloadLevel);
            View.UnselectSignal.RemoveListener(UnselectLevel);

            base.OnRemove();
        }

        private void RequestLevelList()
        {
            RequestLevelListSignal.Dispatch(OnLevelInfoListReceived);
        }

        private void OnLevelInfoListReceived(ICollection<LevelForListOfLevel> levels)
        {
            View.OnLevelListChanged(levels);
        }

        private void DownloadLevel()
        {
            DownloadLevelSignal.Dispatch(_preselectedLevel);
        }

        private void RequestLevelPreselection(LevelForListOfLevel levelInfo)
        {
            _preselectedLevel = levelInfo;
            View.OpenLevelDetail(_preselectedLevel);
        }

        private void UnselectLevel()
        {
            _preselectedLevel = null;
            View.CloseLevelDetail();
        }
    }
}
