﻿using Chromaze.Dom.Data.Payload;
using Pixsaoul.Fou.Engine;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Online
{
    public class LevelInfoButton : BaseBehaviour
    {
        private LevelForListOfLevel _info;
        [SerializeField] private Button _selectInfoButton;
        [SerializeField] private TMP_Text _nameField;

        private Action<LevelForListOfLevel> _callback;

        public void Initiaze(Action<LevelForListOfLevel> levelInfo, LevelForListOfLevel info)
        {
            _callback = levelInfo;
            Set(info);
            _selectInfoButton.onClick.AddListener(SelectInfo);
        }

        private void Set(LevelForListOfLevel info)
        {
            _info = info;
            _nameField.text = info.Name;
        }

        private void SelectInfo()
        {
            if(_info != null)
            {
                _callback?.Invoke(_info);
            }
        }
    }
}
