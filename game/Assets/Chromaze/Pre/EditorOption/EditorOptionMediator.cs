﻿using Chromaze.App.Edition;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.EditorOption
{
    public class EditorOptionMediator : Mediator
    {
        [Inject] public EditorOptionView View { get; set; } 
        [Inject] public RequestOpenComponentCreationSignal RequestOpenComponentCreationSignal { get; set; }
        [Inject] public RequestDeleteSelectedComponentSignal RequestDeleteSelectedComponentSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            View.OpenComponentCreationSignal.AddListener(RequestOpenComponentCreation);
            View.DeleteComponentSignal.AddListener(RequestDeleteComponent);
        }

        public override void OnRemove()
        {
            View.OpenComponentCreationSignal.RemoveListener(RequestOpenComponentCreation);
            View.DeleteComponentSignal.RemoveListener(RequestDeleteComponent);
            base.OnRemove();
        }

        private void RequestOpenComponentCreation()
        {
            RequestOpenComponentCreationSignal.Dispatch();
        }
        private void RequestDeleteComponent()
        {
            RequestDeleteSelectedComponentSignal.Dispatch();
        }
    }
}
