﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.EditorOption
{
    public class EditorOptionView : View
    {
        [SerializeField] private Button _openComponentCreationButton;
        [SerializeField] private Button _editComponentButton;
        [SerializeField] private Button _deleteComponentButton;

        public Signal OpenComponentCreationSignal = new Signal();
        public Signal EditComponentSignal = new Signal();
        public Signal DeleteComponentSignal = new Signal();

        public override void Initialize()
        {
            base.Initialize();
            _openComponentCreationButton.onClick.AddListener(RequestCreateComponent);
            _editComponentButton.onClick.AddListener(RequestEditComponent);
            _deleteComponentButton.onClick.AddListener(RequestDeleteComponent);
        }

        private void RequestCreateComponent()
        {
            OpenComponentCreationSignal.Dispatch();
        }

        private void RequestEditComponent()
        {
            EditComponentSignal.Dispatch();
        }

        private void RequestDeleteComponent()
        {
            DeleteComponentSignal.Dispatch();
        }

    }
}
