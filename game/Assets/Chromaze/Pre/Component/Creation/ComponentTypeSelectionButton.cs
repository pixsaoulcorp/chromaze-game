﻿using Pixsaoul.Fou.Engine;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Component.Creation
{
    public class ComponentTypeSelectionButton : BaseBehaviour
    {
        [SerializeField] private TMP_Text _text;
        [SerializeField] private Button _selectComponentTypeButton;

        private Type _type;

        public void Initialize(Pixsaoul.Dom.Data.Engine.Name name, Action typeCallback, Transform parent)
        {
            transform.SetParent(parent, false);
            _text.text = name;
            _selectComponentTypeButton.onClick.AddListener(()=>typeCallback.Invoke());
        }
    }
}
