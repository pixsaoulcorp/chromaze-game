﻿using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Inf.Camera;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Component.Creation
{
    public class ComponentCreationView : View
    {
        [SerializeField] private GameObject _root;
        [SerializeField] private ComponentTypeSelectionButton _buttonPrefab;
        [SerializeField] private Transform _buttonsRoot;
        [SerializeField] private Button _closeButton;

        public Dictionary<Type, Action> _creatableTypes;

        private SnappedPosition _defaultPosition = new SnappedPosition(2, 3, 2);

        [SerializeField] private CameraView CameraView;
        [SerializeField] private LayerMask LayerMask;
        
        public override void Initialize()
        {
            base.Initialize();
            _creatableTypes = new Dictionary<Type, Action>()
            {              
                //TODO TBU when adding new Component
                 { typeof(Dom.Data.Slope), () => RequestComponentCreation(new Dom.Data.Slope()) },               
                 { typeof(Dom.Data.Billboard), ()=> RequestComponentCreation(new Dom.Data.Billboard()) },
                 { typeof(Dom.Data.Combiner),() => RequestComponentCreation(new Dom.Data.Combiner()) },
                 { typeof(Dom.Data.Cube),() => RequestComponentCreation(new Dom.Data.Cube()) },
                 { typeof(Dom.Data.Decomposer),() =>RequestComponentCreation( new Dom.Data.Decomposer()) },
                 { typeof(Dom.Data.Divider),() => RequestComponentCreation(new Dom.Data.Divider()) },
                 { typeof(Dom.Data.Door),() => RequestComponentCreation(new Dom.Data.Door() )},
                 { typeof(Dom.Data.Emitter),() => RequestComponentCreation(new Dom.Data.Emitter()) },
                 { typeof(Dom.Data.Inverter),() => RequestComponentCreation(new Dom.Data.Inverter()) },
                 { typeof(Dom.Data.Mirror),() => RequestComponentCreation(new Dom.Data.Mirror()) },
                 { typeof(Dom.Data.Relay),() => RequestComponentCreation(new Dom.Data.Relay()) },
                 { typeof(Dom.Data.Glass),() => RequestComponentCreation(new Dom.Data.Glass()) },
                 { typeof(Dom.Data.Light),() => RequestComponentCreation(new Dom.Data.Light()) },
                 { typeof(Dom.Data.Wall),() => RequestComponentCreation(new Dom.Data.Wall()) }
            };

            foreach (KeyValuePair<Type, Action> kvp in _creatableTypes)
            {
                ComponentTypeSelectionButton button = Instantiate<ComponentTypeSelectionButton>(_buttonPrefab);
                button.Initialize((Pixsaoul.Dom.Data.Engine.Name)kvp.Key.Name, kvp.Value, _buttonsRoot);
            }
            _closeButton.onClick.AddListener(Close);
            Close();
        }


        private void RequestComponentCreation(Dom.Data.Component component)
        {
            SnappedPosition position = _defaultPosition;
            if(CameraView != null)
            {
               Debug.Log(CameraView.CurrentCameraManager.ManagedCamera.Camera.gameObject.name);
                var camera = CameraView.CurrentCameraManager.ManagedCamera.Camera;
                var ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;
                if(Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask))
                {
                    var hitPos = hit.point;

                    Vector3 direction = Vector3.Normalize(hitPos  - camera.transform.position);
                    Vector3 cptPos = hitPos - 0.3f*direction;

                    position = new SnappedPosition((int)cptPos.x, (int)cptPos.y, (int)cptPos.z);
                }
            }
            component.Position = position;
            RequestComponentCreationSignal.Dispatch(component);
        }

        public Signal<Dom.Data.Component> RequestComponentCreationSignal = new Signal<Dom.Data.Component>();

        public void Open()
        {
            _root.SetActive(true);
        }

        public void Close()
        {
            _root.SetActive(false);

        }
    }
}