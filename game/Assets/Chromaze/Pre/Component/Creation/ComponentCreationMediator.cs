﻿using Chromaze.App.Level.Creator;
using Chromaze.App.Edition;
using Chromaze.App.Level.Import;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Component.Creation {
    public class ComponentCreationMediator : Mediator
    {
        [Inject] public ComponentCreationView View { get; set; }

        [Inject] public RequestComponentCreationSignal RequestComponentCreationSignal { get; set; }
        [Inject] public RequestOpenComponentCreationSignal RequestOpen { get; set; }
        [Inject] public RequestCloseComponentCreationSignal RequestClose{ get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            View.RequestComponentCreationSignal.AddListener(RequestComponentCreation);
            RequestOpen.AddListener(Open);
            RequestClose.AddListener(Close);
            base.OnRegister();
        }

        public override void OnRemove()
        {

            View.RequestComponentCreationSignal.RemoveListener(RequestComponentCreation);
            RequestOpen.RemoveListener(Open);
            RequestClose.RemoveListener(Close);
            base.OnRemove();
        }

        public void Open()
        {
            View.Open();
        }

        public void Close()
        {
            View.Close();
        }

        public void RequestComponentCreation(Dom.Data.Component component)
        {
            RequestComponentCreationSignal.Dispatch(component);
        }
    }
}