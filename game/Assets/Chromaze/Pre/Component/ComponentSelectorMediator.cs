﻿using Pixsaoul.Fou.Mvcs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Pre.Component.Creation
{
    public class ComponentSelectorMediator : Mediator
    {
        [Inject] public ComponentSelectorView View { get; set; }

        protected override View GetView()
        {
            return View;
        }
    }
}