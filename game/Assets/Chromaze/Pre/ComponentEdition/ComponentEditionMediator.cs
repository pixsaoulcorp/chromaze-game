﻿using Chromaze.App.Components;
using Chromaze.App.Edition;
using Chromaze.App.Level.Import;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Component.Edition
{
    public class ComponentEditionMediator : Mediator
    {
        [Inject] public ComponentEditionView View { get; set; }
        [Inject] public OnComponentCreatedSignal ComponentCreatedSignal{ get; set; }
        [Inject] public NotifyComponentEditionBeginSignal ComponentSelectedSignal { get; set; }

        [Inject] public RequestDeleteSelectedComponentSignal RequestDeleteSelectedComponentSignal { get; set; }
        [Inject] public RequestDeleteComponentSignal RequestDeleteComponentSignal { get; set; }
        [Inject] public NotifyComponentDeletedSignal NotifyComponentDeletedSignal { get; set; }

        [Inject] public RequestEndComponentEditionSignal RequestEndComponentEditionSignal { get; set; }
        [Inject] public NotifyComponentEditionEndedSignal NotifyComponentEditionEndedSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            ComponentCreatedSignal.AddListener(BeginEdition);
            ComponentSelectedSignal.AddListener(BeginEdition);
            RequestDeleteSelectedComponentSignal.AddListener(DeleteSelectedComponent);
            NotifyComponentDeletedSignal.AddListener(RequestEndEdition);

            View.RequestEndEditionSignal.AddListener(RequestEndEdition);
            NotifyComponentEditionEndedSignal.AddListener(EndEdition);

            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();
            ComponentSelectedSignal.RemoveListener(BeginEdition);
            ComponentCreatedSignal.RemoveListener(BeginEdition);
            NotifyComponentDeletedSignal.RemoveListener(RequestEndEdition);
            RequestDeleteSelectedComponentSignal.RemoveListener(DeleteSelectedComponent);

            View.RequestEndEditionSignal.RemoveListener(RequestEndEdition);
            NotifyComponentEditionEndedSignal.RemoveListener(EndEdition);


        }

        public void BeginEdition(IComponent component)
        {
            View.StartEdition(component);
        }

        public void RequestEndEdition(Dom.Data.Component component)
        {
            RequestEndEdition();
        }

        public void RequestEndEdition()
        {
            RequestEndComponentEditionSignal.Dispatch();
        }

        public void EndEdition()
        {
            View.EndEdition();
        }

        private void DeleteSelectedComponent()
        {
            Dom.Data.Component componentData = View.CurrentEditor?.GetData();
            RequestDeleteComponentSignal.Dispatch(componentData);
        }
    }
}
