using System;
using System.Reactive.Subjects;
using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Pre.DataEditor;
using UnityEngine;
namespace Chromaze.Pre.Component.Edition.Light
{
    public class LightEditor : BaseBehaviour, IComponentEditor
    {
        [SerializeField] private GameObject _root;
        public ComponentId _refId;


        [SerializeField] SnappedPositionEditor _snappedPositionEditor;
        [SerializeField] SnappedRotationEditor _snappedRotationEditor;
        [SerializeField] ColorEditor _colorEditor;
        [SerializeField] IntensityEditor _intensityEditor;

        private BehaviorSubject<Dom.Data.Component> _dataSubject; // Don't use field
        private BehaviorSubject<Dom.Data.Component> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Dom.Data.Component>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Dom.Data.Component> ObservableData => DataSubject;

        public void Initialize()
        {
            _snappedPositionEditor.Initialize();
            _snappedRotationEditor.Initialize();
            _colorEditor.Initialize();
            _intensityEditor.Initialize();

            _snappedPositionEditor.ObservableData.Subscribe((SnappedPosition o) => OnChanged());
            _snappedRotationEditor.ObservableData.Subscribe((SnappedRotation o) => OnChanged());
            _colorEditor.ObservableData.Subscribe((Dom.Data.Color o) => OnChanged());
            _intensityEditor.ObservableData.Subscribe((Intensity i) => OnChanged());

            Close();
        }

        public void SetData(Dom.Data.Component initialComponent)
        {
            Dom.Data.Light Light = initialComponent as Dom.Data.Light;
            _refId = initialComponent.Id;
            _snappedPositionEditor.SetData(Light.Position);
            _snappedRotationEditor.SetData(Light.Rotation);
            _colorEditor.SetData(Light.Color);
            _intensityEditor.SetData(Light.Intensity);
        }

        public Dom.Data.Component GetData()
        {
            return new Dom.Data.Light(_refId, _snappedPositionEditor.GetData(), _snappedRotationEditor.GetData(), _colorEditor.GetData(), _intensityEditor.GetData());
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        public void Open()
        {
            _root.SetActive(true);
        }

        public void Close()
        {
            _root.SetActive(false);
        }
    }
}