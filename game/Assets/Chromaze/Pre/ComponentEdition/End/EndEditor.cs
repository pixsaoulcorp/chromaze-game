﻿using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Pre.DataEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reactive.Subjects;
using UnityEngine;
namespace Chromaze.Pre.Component.Edition.End
{
    public class EndEditor : BaseBehaviour, IComponentEditor
    {
        [SerializeField] private GameObject _root;
        public ComponentId _refId;

        [SerializeField] SnappedPositionEditor _snappedPositionEditor;
        [SerializeField] SnappedRotationEditor _snappedRotationEditor;

        private BehaviorSubject<Dom.Data.Component> _dataSubject; // Don't use field
        private BehaviorSubject<Dom.Data.Component> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Dom.Data.Component>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Dom.Data.Component> ObservableData => DataSubject;

        public void Initialize()
        {
            _snappedPositionEditor.Initialize();
            _snappedRotationEditor.Initialize();

            _snappedPositionEditor.ObservableData.Subscribe((SnappedPosition o) => OnChanged());
           _snappedRotationEditor.ObservableData.Subscribe((SnappedRotation o) => OnChanged());
            Close();
        }

        public void SetData(Dom.Data.Component initialComponent)
        {
            _refId = initialComponent.Id;

            _snappedPositionEditor.SetData(initialComponent.Position);
            _snappedRotationEditor.SetData(initialComponent.Rotation);
        }

        public Dom.Data.Component GetData()
        {
            return new Dom.Data.End(_refId, _snappedPositionEditor.GetData(), _snappedRotationEditor.GetData());
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }
        
        public void Open()
        {
            _root.SetActive(true);
        }

        public void Close()
        {
            _root.SetActive(false);
        }
    }
}