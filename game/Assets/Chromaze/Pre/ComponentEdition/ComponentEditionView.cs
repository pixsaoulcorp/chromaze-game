﻿using Chromaze.App.Components;
using Chromaze.Inf.Components.BillBoards;
using Chromaze.Inf.Components.Boundaries;
using Chromaze.Inf.Components.Combiner;
using Chromaze.Inf.Components.Cube;
using Chromaze.Inf.Components.Decomposer;
using Chromaze.Inf.Components.Divider;
using Chromaze.Inf.Components.Door;
using Chromaze.Inf.Components.Emitter;
using Chromaze.Inf.Components.End;
using Chromaze.Inf.Components.Glass;
using Chromaze.Inf.Components.Inverter;
using Chromaze.Inf.Components.Light;
using Chromaze.Inf.Components.Mirror;
using Chromaze.Inf.Components.Relay;
using Chromaze.Inf.Components.Slope;
using Chromaze.Inf.Components.Start;
using Chromaze.Inf.Components.Wall;
using Chromaze.Pre.Component.Edition.Billboard;
using Chromaze.Pre.Component.Edition.Boundaries;
using Chromaze.Pre.Component.Edition.Combiner;
using Chromaze.Pre.Component.Edition.Cube;
using Chromaze.Pre.Component.Edition.Decomposer;
using Chromaze.Pre.Component.Edition.Divider;
using Chromaze.Pre.Component.Edition.Door;
using Chromaze.Pre.Component.Edition.Emitter;
using Chromaze.Pre.Component.Edition.End;
using Chromaze.Pre.Component.Edition.Glass;
using Chromaze.Pre.Component.Edition.Inverter;
using Chromaze.Pre.Component.Edition.Light;
using Chromaze.Pre.Component.Edition.Mirror;
using Chromaze.Pre.Component.Edition.Relay;
using Chromaze.Pre.Component.Edition.Slope;
using Chromaze.Pre.Component.Edition.Start;
using Chromaze.Pre.Component.Edition.Wall;
using EnsureThat;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Reactive;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Component.Edition
{
    public class ComponentEditionView : View
    {
        [SerializeField] private GameObject _root;
        [SerializeField] private Button _endEditionButton;

        [Header("Editors")]
        [SerializeField] private DefaultEditor _defaultEditor;

        [SerializeField] private SlopeEditor _slopeEditor;
        [SerializeField] private BoundariesEditor _boundariesEditor;
        [SerializeField] private StartEditor _startEditor;
        [SerializeField] private EndEditor _endEditor;
        [SerializeField] private BillboardEditor _billboardEditor;
        [SerializeField] private CubeEditor _cubeEditor;
        [SerializeField] private DoorEditor _doorEditor;
        [SerializeField] private WallEditor _wallEditor;
        [SerializeField] private EmitterEditor _emitterEditor;
        [SerializeField] private MirrorEditor _mirrorEditor;
        [SerializeField] private CombinerEditor _combinerEditor;
        [SerializeField] private DecomposerEditor _decomposerEditor;
        [SerializeField] private DividerEditor _dividerEditor;
        [SerializeField] private InverterEditor _inverterEditor;
        [SerializeField] private RelayEditor _relayEditor;
        [SerializeField] private GlassEditor _glassEditor;
        [SerializeField] private LightEditor _lightEditor;
        //TODO TBU when adding component

        private IComponentEditor _currentEditor;
        private IDisposable _currentSubscription;
        public IComponentEditor CurrentEditor { get => _currentEditor; }

        public Signal RequestEndEditionSignal = new Signal();

        public override void Initialize()
        {
            
            base.Initialize();

            EnsureArg.IsNotNull(_slopeEditor);
            EnsureArg.IsNotNull(_boundariesEditor);
            EnsureArg.IsNotNull(_startEditor);
            EnsureArg.IsNotNull(_endEditor);
            EnsureArg.IsNotNull(_billboardEditor);
            EnsureArg.IsNotNull(_cubeEditor);
            EnsureArg.IsNotNull(_doorEditor);
            EnsureArg.IsNotNull(_wallEditor);
            EnsureArg.IsNotNull(_emitterEditor);
            EnsureArg.IsNotNull(_mirrorEditor);
            EnsureArg.IsNotNull(_combinerEditor);
            EnsureArg.IsNotNull(_decomposerEditor);
            EnsureArg.IsNotNull(_dividerEditor);
            EnsureArg.IsNotNull(_inverterEditor);
            EnsureArg.IsNotNull(_relayEditor);
            EnsureArg.IsNotNull(_glassEditor);
            EnsureArg.IsNotNull(_lightEditor);
            //TODO TBU when adding component

            _defaultEditor.Initialize();
            _slopeEditor.Initialize();
            _boundariesEditor.Initialize();
            _billboardEditor.Initialize();
            _startEditor.Initialize();
            _endEditor.Initialize();
            _cubeEditor.Initialize();
            _doorEditor.Initialize();
            _wallEditor.Initialize();
            _emitterEditor.Initialize();
            _mirrorEditor.Initialize();
            _combinerEditor.Initialize();
            _decomposerEditor.Initialize();
            _dividerEditor.Initialize();
            _inverterEditor.Initialize();
            _relayEditor.Initialize();
            _glassEditor.Initialize();
            _lightEditor.Initialize();
            //TODO TBU when adding component

            _endEditionButton.onClick.AddListener(RequestEndEdition);
            RequestEndEdition();
        }

        public void StartEdition(IComponent view)
        {
            RequestEndEdition(); //This might not be needed
            _currentEditor = GetEditor(view);
            _currentEditor.SetData(view.GetComponentData());
            IObserver<Dom.Data.Component> observer = Observer.Create<Dom.Data.Component>((Dom.Data.Component cpt) => OnEdited(view, cpt));
            _currentSubscription = _currentEditor.ObservableData.Subscribe(observer);
            _currentEditor.Open();
            Open();
        }

        private void RequestEndEdition()
        {
            RequestEndEditionSignal.Dispatch();
        }

        public void EndEdition()
        {
            _currentSubscription?.Dispose();
            _currentEditor?.Close();
            _currentEditor = null;

            Close();
        }

        private void OnEdited(IComponent view, Dom.Data.Component component)
        {
            if(component != null)
            {
                view.SetComponentData(_currentEditor?.GetData());
            }
        }

        private IComponentEditor GetEditor(IComponent component)
        {
            if (component is SlopeView) //TODO TBU find a way to avoid this
            {
                return _slopeEditor;
            }
            else if (component is BoundariesView) //TODO TBU find a way to avoid this
            {
                return _boundariesEditor;
            }
            else if (component is BillboardView) //TODO TBU find a way to avoid this
            {
                return _billboardEditor;
            }
            else if(component is StartView) //TODO TBU find a way to avoid this
            {
                return _startEditor;
            }
            else if (component is EndView) //TODO TBU find a way to avoid this
            {
                return _endEditor;
            }
            else if (component is CubeView) //TODO TBU find a way to avoid this
            {
                return _cubeEditor;
            }
            else if (component is DoorView) //TODO TBU find a way to avoid this
            {
                return _doorEditor;
            }
            else if (component is WallView) //TODO TBU find a way to avoid this
            {
                return _wallEditor;
            }
            else if (component is EmitterView) //TODO TBU find a way to avoid this
            {
                return _emitterEditor;
            }
            else if (component is MirrorView) //TODO TBU find a way to avoid this
            {
                return _mirrorEditor;
            }
            else if (component is CombinerView) //TODO TBU find a way to avoid this
            {
                return _combinerEditor;
            }
            else if (component is DecomposerView) //TODO TBU find a way to avoid this
            {
                return _decomposerEditor;
            }
            else if (component is DividerView) //TODO TBU find a way to avoid this
            {
                return _dividerEditor;
            }
            else if (component is InverterView) //TODO TBU find a way to avoid this
            {
                return _inverterEditor;
            }
            else if (component is RelayView) //TODO TBU find a way to avoid this
            {
                return _relayEditor;
            }
            else if (component is GlassView) //TODO TBU find a way to avoid this
            {
                return _glassEditor;
            }
            else if (component is LightView) //TODO TBU find a way to avoid this
            {
                return _lightEditor;
            }
            //TODO TBU when adding component

            else
            {
                Debug.LogWarning("Not implemented component selected");
                return _defaultEditor; // TODO TBU have default editor
            }
        }

        private void Open()
        {
            _root.SetActive(true);
        }

        private void Close()
        {
            _root.SetActive(false);
        }

    }
}
