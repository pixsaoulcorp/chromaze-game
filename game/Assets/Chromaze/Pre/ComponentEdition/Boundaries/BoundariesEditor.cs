﻿using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Inf.BoxelGeneration;
using Pixsaoul.Pre.DataEditor;
using System;
using System.Reactive.Subjects;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Component.Edition.Boundaries
{
    public class BoundariesEditor : BaseBehaviour, IComponentEditor
    {
        [SerializeField] private GameObject _root;
        //public ComponentId _refId;
        private Dom.Data.Boundaries _currentBoundaries;

        [SerializeField] SnappedDimensionEditor _snappedDimensionEditor;
        [SerializeField] Button _generateNewBoundaryButton;
        [SerializeField] ColorEditor _colorEditor;

        private BehaviorSubject<Dom.Data.Component> _dataSubject; // Don't use field
        private BehaviorSubject<Dom.Data.Component> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Dom.Data.Component>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Dom.Data.Component> ObservableData => DataSubject;

        public void Initialize()
        {
            _snappedDimensionEditor.Initialize();
            _colorEditor.Initialize();
            _generateNewBoundaryButton.onClick.AddListener(CreateSimpleBoundary);
            Close();


        }

        public void SetData(Dom.Data.Component initialComponent)
        {
            _currentBoundaries = (Dom.Data.Boundaries)initialComponent;
            //_refId = _currentBoundaries.Id;          
        }

        public Dom.Data.Component GetData()
        {
            return _currentBoundaries;                   
        }

        private void CreateSimpleBoundary()
        {
            SnappedDimension dimensions = _snappedDimensionEditor.GetData();
            _currentBoundaries =  new Dom.Data.Boundaries(_currentBoundaries.Id, new SnappedPosition(), new SnappedRotation(), BoxelShapeExtensions.CreateSimpleShape(dimensions.Width, dimensions.Height, dimensions.Depth, SurfaceMode.Intern), _colorEditor.GetData());
            DataSubject.OnNext(GetData());
        }
        
        public void Open()
        {
            _root.SetActive(true);
        }

        public void Close()
        {
            _root.SetActive(false);
        }
    }
}