﻿using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Pre.DataEditor;
using System;
using System.Reactive.Subjects;
using UnityEngine;
namespace Chromaze.Pre.Component.Edition.Door
{
    public class DoorEditor : BaseBehaviour, IComponentEditor
    {
        [SerializeField] private GameObject _root;
        public ComponentId _refId;

        [SerializeField] SnappedPositionEditor _snappedPositionEditor;
        [SerializeField] SnappedRotationEditor _snappedRotationEditor;
        [SerializeField] ColorEditor _colorEditor;

        private BehaviorSubject<Dom.Data.Component> _dataSubject; // Don't use field
        private BehaviorSubject<Dom.Data.Component> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Dom.Data.Component>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Dom.Data.Component> ObservableData => DataSubject;

        public void Initialize()
        {
            _snappedPositionEditor.Initialize();
            _snappedRotationEditor.Initialize();
            _colorEditor.Initialize();

            _snappedPositionEditor.ObservableData.Subscribe((SnappedPosition o) => OnChanged());
            _snappedRotationEditor.ObservableData.Subscribe((SnappedRotation o) => OnChanged());
            _colorEditor.ObservableData.Subscribe((Dom.Data.Color o) => OnChanged());
            Close();
        }

        public void SetData(Dom.Data.Component initialComponent)
        {
            Dom.Data.Door doorComponent = (Dom.Data.Door) initialComponent;
            _refId = doorComponent.Id;

            _snappedPositionEditor.SetData(doorComponent.Position);
            _snappedRotationEditor.SetData(doorComponent.Rotation);
            _colorEditor.SetData(doorComponent.Color);
        }

        public Dom.Data.Component GetData()
        {
            return new Dom.Data.Door(_refId, _snappedPositionEditor.GetData(), _snappedRotationEditor.GetData(), _colorEditor.GetData());
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        public void Open()
        {
            _root.SetActive(true);
        }

        public void Close()
        {
            _root.SetActive(false);
        }
    }
}