﻿using System;
using System.Reactive.Subjects;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Chromaze.Pre.Component.Edition.Emitter
{
    public class DefaultEditor : BaseBehaviour, IComponentEditor
    {
        [SerializeField] private GameObject _root;

        private BehaviorSubject<Dom.Data.Component> _dataSubject; // Don't use field
        private BehaviorSubject<Dom.Data.Component> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Dom.Data.Component>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Dom.Data.Component> ObservableData => DataSubject;

        public void Initialize()
        {
            Close();

        }

        public void SetData(Dom.Data.Component initialComponent)
        {
        }

        public Dom.Data.Component GetData()
        {
            throw new NotImplementedException();
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        public void Open()
        {
            _root.SetActive(true);
        }

        public void Close()
        {
            _root.SetActive(false);
        }
    }
}
