﻿using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Engine;

namespace Chromaze.Pre.Component.Edition
{
    public class IComponentEditorContainer : IUnifiedContainer<IComponentEditor> { }

    public interface IComponentEditor : IOpenable, IInitializable, IProvider<Dom.Data.Component>
    {
        void SetData(Dom.Data.Component component);
        Dom.Data.Component GetData();
    }
}
