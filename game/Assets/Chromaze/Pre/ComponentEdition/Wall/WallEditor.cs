﻿using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Pre.DataEditor;
using System;
using System.Reactive.Subjects;
using UnityEngine;

namespace Chromaze.Pre.Component.Edition.Wall
{
    public class WallEditor : BaseBehaviour, IComponentEditor
    {
        [SerializeField] private GameObject _root;
        public ComponentId _refId;

        [SerializeField] SnappedPositionEditor _snappedPositionEditor;
        [SerializeField] SnappedRotationEditor _snappedRotationEditor;
        [SerializeField] SnappedDimensionEditor _snappedDimensionEditor;
        [SerializeField] ColorEditor _colorEditor;


        private BehaviorSubject<Dom.Data.Component> _dataSubject; // Don't use field
        private BehaviorSubject<Dom.Data.Component> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Dom.Data.Component>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Dom.Data.Component> ObservableData => DataSubject;

        public void Initialize()
        {
            _snappedPositionEditor.Initialize();
            _snappedRotationEditor.Initialize();
            _snappedDimensionEditor.Initialize();
            _colorEditor.Initialize();

            _snappedPositionEditor.ObservableData.Subscribe((SnappedPosition o) => OnChanged());
            _snappedRotationEditor.ObservableData.Subscribe((SnappedRotation o) => OnChanged());
            _snappedDimensionEditor.ObservableData.Subscribe((SnappedDimension o) => OnChanged());
            _colorEditor.ObservableData.Subscribe((Dom.Data.Color o) => OnChanged());

            Close();
        }

        public void SetData(Dom.Data.Component initialComponent)
        {
            Dom.Data.Wall wall = (Dom.Data.Wall)initialComponent;
            _refId = wall.Id;
            _snappedPositionEditor.SetData(wall.Position);
            _snappedRotationEditor.SetData(wall.Rotation);
            _snappedDimensionEditor.SetData(wall.Dimension);
            _colorEditor.SetData(wall.Color);

        }

        public Dom.Data.Component GetData()
        {
            return new Dom.Data.Wall(_refId, _snappedPositionEditor.GetData(), _snappedRotationEditor.GetData(), _snappedDimensionEditor.GetData(), _colorEditor.GetData()) ;
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        public void Open()
        {
            _root.SetActive(true);
        }

        public void Close()
        {
            _root.SetActive(false);
        }
    }
}