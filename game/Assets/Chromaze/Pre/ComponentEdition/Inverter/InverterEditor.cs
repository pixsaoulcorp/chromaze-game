﻿using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Pre.DataEditor;
using System;
using System.Reactive.Subjects;
using UnityEngine;
namespace Chromaze.Pre.Component.Edition.Inverter
{
    public class InverterEditor : BaseBehaviour, IComponentEditor
    {
        [SerializeField] private GameObject _root;
        public ComponentId _refId;

        [SerializeField] SnappedPositionEditor _snappedPositionEditor;
        [SerializeField] SnappedRotationEditor _snappedRotationEditor;
        [SerializeField] OrientationEditor _orientationEditor;

        private BehaviorSubject<Dom.Data.Component> _dataSubject; // Don't use field
        private BehaviorSubject<Dom.Data.Component> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Dom.Data.Component>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Dom.Data.Component> ObservableData => DataSubject;


        public void Initialize()
        {
            _snappedPositionEditor.Initialize();
            _snappedRotationEditor.Initialize();
            _orientationEditor.Initialize();

            _snappedPositionEditor.ObservableData.Subscribe((SnappedPosition o) => OnChanged());
           _snappedRotationEditor.ObservableData.Subscribe((SnappedRotation o) => OnChanged());
            _orientationEditor.ObservableData.Subscribe((Orientation o) => OnChanged());

            Close();
        }

        public void SetData(Dom.Data.Component initialComponent)
        {
            Dom.Data.Inverter inverter = initialComponent as Dom.Data.Inverter;
            _refId = initialComponent.Id;

            _snappedPositionEditor.SetData(initialComponent.Position);
            _snappedRotationEditor.SetData(initialComponent.Rotation);
            _orientationEditor.SetData(inverter.EmissionOrientation);

        }

        public Dom.Data.Component GetData()
        {
            return new Dom.Data.Inverter(_refId, _snappedPositionEditor.GetData(), _snappedRotationEditor.GetData(), _orientationEditor.GetData());
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }
        
        public void Open()
        {
            _root.SetActive(true);
        }

        public void Close()
        {
            _root.SetActive(false);
        }
    }
}