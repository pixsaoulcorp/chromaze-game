﻿using Chromaze.App.Components;
using Chromaze.App.Edition;
using Chromaze.App.Edition;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Pre.Gizmos
{
    public class GizmoMediator : Mediator
    {
        [Inject] public NotifyComponentEditionEndedSignal EndEditionSignal { get; set; }
        [Inject] public NotifyComponentEditionBeginSignal BeginEditionSignal { get; set; }

        [Inject] public GizmoView View { get; set; }


        public override void OnRegister()
        {
            EndEditionSignal.AddListener(HideGizmo);
            BeginEditionSignal.AddListener(BindGizmo);
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();
            EndEditionSignal.RemoveListener(HideGizmo);
            BeginEditionSignal.RemoveListener(BindGizmo);

        }

        public void BindGizmo(IComponent component)
        {
            View.Bind(component);
        }

        private void HideGizmo()
        {
            View.Hide();
        }

        protected override View GetView()
        {
            return View;
        }
    }
}