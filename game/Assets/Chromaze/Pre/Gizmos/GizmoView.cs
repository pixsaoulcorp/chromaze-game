﻿using Chromaze.App.Components;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Reactive;
using UnityEngine;

namespace Chromaze.Pre.Gizmos
{
    public class GizmoView : View
    {
        [SerializeField] private Gizmo _currentGizmo;

        private IDisposable _latestBinding;

        public override void Initialize()
        {
            base.Initialize();
            Hide();
        }

        public void Bind(IComponent component)
        {
            Hide();
            _latestBinding = component.ObservableComponentData.Subscribe(Observer.Create<Dom.Data.Component>(OnBindedComponentChanged));
            _currentGizmo.gameObject.SetActive(true);
        }

        public void OnBindedComponentChanged(Dom.Data.Component componentData)
        {
            _currentGizmo.transform.position = componentData.Position;
        }

        public void Hide()
        {
            _latestBinding?.Dispose();
            _currentGizmo.gameObject.SetActive(false);
        }
    }
}
