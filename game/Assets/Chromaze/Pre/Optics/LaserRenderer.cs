﻿using Chromaze.App.Optics;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Chromaze.Pre.Optics
{
    public class LaserRenderer : BaseBehaviour, IRayConsumer
    {
        [SerializeField] private LineRenderer _line;
        [SerializeField] private GameObject _effectsRoot;
        [SerializeField] private GameObject _hitEffectsRoot;
        [SerializeField] private List<ParticleSystem> _effects;
        [SerializeField] private List<ParticleSystem> _hitEffects;

        [SerializeField] private float _hitMarge;

        //Internal EGA stuff

        [SerializeField] private float MainTextureLength = 1f;
        [SerializeField] private float NoiseTextureLength = 1f;
        private Vector4 Length = new Vector4(1, 1, 1, 1);

        public void Enable()
        {
            _line.enabled = true;
            _effectsRoot.SetActive(true);
            _hitEffectsRoot.SetActive(true);
        }

        public void Disable()
        {
            _line.enabled = false;
            _effectsRoot.SetActive(false);
            _hitEffectsRoot.SetActive(false);
        }

        public void SetRay(App.Optics.Ray ray)
        { //Update laser Color
            SetColor(ray.Color);
        //Update position
            SetPositions(ray.Source, ray.Target);
        }

        private void SetColor(Dom.Data.Color color)
        {
            Color unityColor = color.ToColor();
            _line.startColor = unityColor;
            _line.endColor = unityColor;

            foreach(var particleSystem in _effects)
            {
                var main = particleSystem.main;
                main.startColor = unityColor;
            }

            foreach (var particleSystem in _hitEffects)
            {
                if (particleSystem.colorOverLifetime.enabled)
                {
                    var lifeTimeColor = particleSystem.colorOverLifetime;
                    Gradient grad = new Gradient();
                    grad.SetKeys(new GradientColorKey[] { new GradientColorKey(unityColor, 0.0f), new GradientColorKey(Color.white, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) });
                    lifeTimeColor.color = grad;
                } else
                {
                    var main = particleSystem.main;
                    main.startColor = unityColor;
                }                           
            }

        }

        private void SetPositions(Position source, Position target)
        {
            // Update line position
            _line.SetPosition(0, source);
            _line.SetPosition(1, target);

            // Update effect position
            _hitEffectsRoot.transform.position = target - Vector3.Normalize((Vector3)target-source)*_hitMarge;

            //set EGA internal stuff, should be splitted into multiple methods
            Length[0] = MainTextureLength * (Vector3.Distance(source, target));
            Length[2] = NoiseTextureLength * (Vector3.Distance(source, target));

            _line.material.SetTextureScale("_MainTex", new Vector2(Length[0], Length[1]));
            _line.material.SetTextureScale("_Noise", new Vector2(Length[2], Length[3]));
        }
    }
}