﻿using Chromaze.App.Environment;
using Pixsaoul.Fou.Engine;
using System.Reactive;
using UnityEngine;

namespace Chromaze.Pre.Optics.Reception
{
    public class Colorable : BaseBehaviour, IInitializable
    {
        [SerializeField] private Renderer _renderer;
        [SerializeField] private IColorProviderContainer _colorProvider;
        [Header("Warning, color of material will be overriden")]
        [SerializeField] private float _alphaOverride;
        private MaterialPropertyBlock block;
        private System.IObserver<Dom.Data.Color> _colorObserver;

        public void Initialize()
        {
            block = new MaterialPropertyBlock();

            _colorObserver = Observer.Create<Dom.Data.Color>(SetColor);
            _colorProvider.Result.ObservableColor.Subscribe(_colorObserver);
        }

        public void SetColor(Dom.Data.Color color)
        {
            var previousColor = block.GetColor("_BaseColor");
            block.SetColor("_BaseColor", color.ToColor(_alphaOverride));
            _renderer.SetPropertyBlock(block);
        }
    }

}