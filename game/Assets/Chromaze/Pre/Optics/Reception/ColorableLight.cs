﻿using Chromaze.App.Environment;
using Pixsaoul.Fou.Engine;
using System.Reactive;
using UnityEngine;

namespace Chromaze.Pre.Optics.Reception
{
    public class ColorableLight : BaseBehaviour, IInitializable
    {
        [SerializeField] private Light _light;
        [SerializeField] private IColorProviderContainer _colorProvider;
        private MaterialPropertyBlock block;
        private System.IObserver<Dom.Data.Color> _colorObserver;

        public void Initialize()
        {
            _colorObserver = Observer.Create<Dom.Data.Color>(SetColor);
            _colorProvider.Result?.ObservableColor.Subscribe(_colorObserver);
        }

        public void SetColor(Dom.Data.Color color)
        {
            _light.color = color.ToColor();
        }
    }

}