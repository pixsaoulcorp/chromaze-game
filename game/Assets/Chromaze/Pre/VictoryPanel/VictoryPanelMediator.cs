﻿using Chromaze.App.EndLevel;
using Chromaze.App.Level.Import;
using Chromaze.App.SceneWorkflow;
using Chromaze.Dom.Data;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.EndLevel
{
    public class VictoryPanelMediator : Mediator
    {
        //TODO TBU get level info from opening
        //TODO TBU get level ending messages from opening
        //TODO TBU 
        //TODO TBU

        private Dom.Data.Level _currentlyPlayedLevel;

        private VictoryQuotes _victoryQuotes;
        [Inject] public NotifyLevelImportedSignal NotifyLevelImportedSignal { get; set; }
        [Inject] public RequestExitLevelSignal RequestExitLevelSignal { get; set; }
        [Inject] public NotifyLevelEndedSignal NotifyLevelEndedSignal { get; set; }

        [Inject] public RequestVictoryQuotesSignal RequestQuotesSignal { get; set; }
        [Inject] public VictoryPanelView View { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            NotifyLevelImportedSignal.AddListener(OnLevelOpened);
            View.RequestLoadMenuSignal.AddListener(RequestLoadMenu);
            NotifyLevelEndedSignal.AddListener(OnLevelWon);
        }

        public override void OnRemove()
        {
            NotifyLevelImportedSignal.RemoveListener(OnLevelOpened);
            View.RequestLoadMenuSignal.RemoveListener(RequestLoadMenu);
            NotifyLevelEndedSignal.RemoveListener(OnLevelWon);
            base.OnRemove();
        }

        private void RequestLoadMenu()
        {
            RequestExitLevelSignal.Dispatch();
        }

        private void OnLevelWon()
        {
            View.Open(_currentlyPlayedLevel, _victoryQuotes.GetRandom());
        }

        private void OnLevelOpened(Dom.Data.Level level)
        {
            _currentlyPlayedLevel = level;
            RequestVictoryQuotes();
        }

        private void RequestVictoryQuotes()
        {
            RequestQuotesSignal.Dispatch(OnVictoryQuotesReceived);
        }

        private void OnVictoryQuotesReceived(VictoryQuotes quotes)
        {
            _victoryQuotes = quotes;
        }
    }
}
