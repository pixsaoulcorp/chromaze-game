﻿using Pixsaoul.Fou.Mvcs;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.EndLevel
{
    public class VictoryPanelView : View
    {
        [SerializeField] private GameObject _panelRoot;

        [SerializeField] private Button _backToMenuButton;

        [SerializeField] private TMP_Text _victoryQuote;
        [SerializeField] private TMP_Text _levelName;
        [SerializeField] private TMP_Text _levelId;

        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private AnimationCurve _fadeCurve;
        [SerializeField] private float _fadeDuration;
            


        public Signal RequestLoadMenuSignal = new Signal();
        private float _lastKnownFadeProgress = 0f;

        public override void Initialize()
        {
            base.Initialize();
            _backToMenuButton.onClick.AddListener(RequestLoadMenu);
            ForceClose();
        }

        public void Open(Dom.Data.Level level, Pixsaoul.Dom.Text victoryQuote)
        {
            _victoryQuote.text = victoryQuote;
            _levelName.text = "You just completed " + level.Name + ".";
            _levelId.text = level.Id.ToString();
            _panelRoot.SetActive(true);
            StopAllCoroutines();
            StartCoroutine(FadeInC(true));
        }

        public void ForceClose()
        {
            StopAllCoroutines();
            SetAlpha(0);
            FinalizeFade(false);
        }

        private void RequestLoadMenu()
        {
            RequestLoadMenuSignal.Dispatch();
        }

        private IEnumerator FadeInC(bool fadeIn)
        {
            _canvasGroup.interactable = false;
            float startTime = Time.time;
            float initialFade = _lastKnownFadeProgress;
            while (fadeIn ? (_lastKnownFadeProgress <= 1f) : (_lastKnownFadeProgress >= 0f))
            {
                SetAlpha(initialFade + (fadeIn ? 1 : -1) * (Time.time - startTime)/_fadeDuration);
                yield return null;
            }
            FinalizeFade(fadeIn);
        }
        private void SetAlpha(float value)
        {
            _canvasGroup.alpha = _fadeCurve.Evaluate(value);
            _lastKnownFadeProgress = value;

        }
        private void FinalizeFade(bool fadeIn)
        {
            if (fadeIn)
            {
                _canvasGroup.interactable = true;
            }
            else
            {
                _canvasGroup.interactable = false;                
            }
        }
    }
}