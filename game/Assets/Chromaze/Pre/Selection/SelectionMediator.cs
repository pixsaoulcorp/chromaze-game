﻿using Chromaze.App.Selection;
using Pixsaoul.App.UserInput;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Selection
{
    public class SelectionMediator : Mediator
    {
        // selection comes from the selectable part in view
        // deselection comes from main button to go back to player selection
        [Inject]
        public SelectionView View { get; set; } //TODO Set view to real view type

        [Inject] public NotifyMainButtonInputSignal NotifyMainButtonInputSignal { get; set; }

        [Inject] public ReleaseComponentSelectionSignal ReleaseComponentSelectionSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            View.OnSelectedSignal.AddListener(OnSelected);
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();
            View.OnSelectedSignal.RemoveListener(OnSelected);
            NotifyMainButtonInputSignal.RemoveListener(OnDeselected);
        }

        private void OnSelected()
        {
            View.ApplySelection();
            NotifyMainButtonInputSignal.AddListener(OnDeselected);
        }

        private void OnDeselected()
        {
            View.ApplyDeselection();
            ReleaseComponentSelectionSignal.Dispatch();
            NotifyMainButtonInputSignal.RemoveListener(OnDeselected);
        }
    }
}
