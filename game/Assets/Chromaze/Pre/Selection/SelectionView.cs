﻿using Chromaze.App;
using Chromaze.Pre.Gizmos;
using Pixsaoul.App.Selection;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Pre.Selection
{
    /// <summary>
    /// View handling the selection of an object. Used to handle the selection
    /// of a component by the player to take control of it.
    /// </summary>
    public class SelectionView : View
    {
        [SerializeField] private ISelectableContainer _selectable;
        [SerializeField] private IEnablableContainer _enablable; // Should be enabled when selected, disabled otherwise


        public Signal OnSelectedSignal = new Signal();
        public override void Initialize()
        {
            _selectable.Result.SetSelectionCallback(OnSelected);
            base.Initialize();
        }

        private void OnSelected()
        { // called only from the selectable part.
            OnSelectedSignal.Dispatch();
        }

        public void ApplySelection()
        {
            _enablable?.Result.Enable();
        }

        public void ApplyDeselection()
        {
            _enablable?.Result.Disable();
        }
    }
}
