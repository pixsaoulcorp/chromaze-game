﻿using Chromaze.Pre.Menu;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Pre.Menu;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Level.Creator
{
    public class CreatorModeMenu : SubMenuView
    {
        [SerializeField] private NamedButtonList _levelButtons;
        [Header("Global actions")]
        [SerializeField] private Button _createButton;
        [SerializeField] private Button _refreshButton;

        [Header("Level specific actions")]
        [SerializeField] private Button _editButton;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _uploadButton;
        [SerializeField] private Button _deleteButton;

        [SerializeField] private GameObject _levelDetailRoot;
        [SerializeField] private TMP_Text _levelName;
        [SerializeField] private Button _unselectLevelButton;


        //TODO TBU do not use specific signal type for signals between view and mediator
        public Signal<Pixsaoul.Dom.Data.Engine.Name> PreSelectSignal = new Signal<Pixsaoul.Dom.Data.Engine.Name>();
        public Signal RequestCreatedLevels = new Signal();
        public Signal EditLevelSignal = new Signal();
        public Signal PlayLevelSignal = new Signal();
        public Signal UploadLevelSignal = new Signal();
        public Signal DeleteLevelSignal = new Signal();
        public Signal UnselectSignal = new Signal();
        public Signal CreateLevelSignal = new Signal();

        public override void Initialize()
        {
            base.Initialize();
            _levelButtons.Initialize();
            _levelButtons.SetButtonCallback(PreselectLevel);
            _refreshButton.onClick.AddListener(RefreshLevelList);
            _editButton.onClick.AddListener(EditLevel);
            _playButton.onClick.AddListener(PlayLevel);
            _uploadButton.onClick.AddListener(UploadLevel);
            _deleteButton.onClick.AddListener(DeleteLevel);
            _unselectLevelButton.onClick.AddListener(UnselectLevel);
            _createButton.onClick.AddListener(EditNewLevel);
            CloseLevelDetail();
        }

        public override void Open()
        {
            base.Open();
            RefreshLevelList();
        }

        public void OnLevelListChanged(ICollection<Pixsaoul.Dom.Data.Engine.Name> levelNames)
        {
            _levelButtons.UpdateElements(levelNames);
        }

        private void PreselectLevel(Pixsaoul.Dom.Data.Engine.Name levelName)
        {
            _levelName.text = levelName;
            PreSelectSignal.Dispatch(levelName);
        }

        private void UnselectLevel()
        {
            UnselectSignal.Dispatch();
        }

        private void RefreshLevelList()
        {
            RequestCreatedLevels.Dispatch();
        }

        public void OpenLevelDetail(Pixsaoul.Dom.Data.Engine.Name name)
        {
            _levelDetailRoot.SetActive(true);
        }

        public void CloseLevelDetail()
        {
            _levelDetailRoot.SetActive(false);
        }

        private void EditLevel()
        {
            EditLevelSignal.Dispatch();
        }

        private void PlayLevel()
        {
            PlayLevelSignal.Dispatch();
        }


        private void UploadLevel()
        {
            UploadLevelSignal.Dispatch();
        }

        private void DeleteLevel()
        {
            DeleteLevelSignal.Dispatch();
        }

        private void EditNewLevel()
        {
            CreateLevelSignal.Dispatch();
        }
    }
}
