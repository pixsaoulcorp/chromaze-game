﻿using Chromaze.Pre.Menu;
using Chromaze.App.SceneWorkflow;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using Chromaze.App.Level.Creator;
using Chromaze.App.Online;

namespace Chromaze.Pre.Level.Creator
{
    public class CreatorModeMenuMediator : SubMenuMediator
    {
        private Pixsaoul.Dom.Data.Engine.Name _preselectedLevelName;

        [Inject] public RequestCreationsSignal LevelListSignal { get; set; }
        [Inject] public EditCreationLevelSignal EnterCreateModeSignal { get; set; }
        [Inject] public PlayCreatorLevelSignal PlayCreatorLevelSignal { get; set; }
        [Inject] public UploadCreatorLevelSignal UploadCreatorLevelSignal { get; set; }
        [Inject] public RemoveCreatorLevelSignal RemoveLevelSignal{ get; set; }

        protected override SubMenuType ManagedType()
        {
            return SubMenuType.Creator;
        }

        [Inject]
        public CreatorModeMenu View { get; set; }

        protected override View GetView()
        {
            return View;
        }
        public override void OnRegister()
        {
            base.OnRegister();
            View.RequestCreatedLevels.AddListener(RequestLevelList);
            View.PreSelectSignal.AddListener(RequestLevelPreselection);
            View.UnselectSignal.AddListener(UnselectLevel);
            View.PlayLevelSignal.AddListener(PlayLevel);
            View.UploadLevelSignal.AddListener(UploadLevel);
            View.DeleteLevelSignal.AddListener(DeleteLevel);
            View.EditLevelSignal.AddListener(EditLevel);
            View.CreateLevelSignal.AddListener(CreateLevel);
        }

        public override void OnRemove()
        {
            View.RequestCreatedLevels.RemoveListener(RequestLevelList);
            View.PreSelectSignal.RemoveListener(RequestLevelPreselection);
            View.UnselectSignal.RemoveListener(UnselectLevel);

            View.PlayLevelSignal.RemoveListener(PlayLevel);
            View.UploadLevelSignal.RemoveListener(UploadLevel);
            View.DeleteLevelSignal.RemoveListener(DeleteLevel);

            View.EditLevelSignal.RemoveListener(EditLevel);
            View.CreateLevelSignal.RemoveListener(CreateLevel);
            base.OnRemove();
        }


        private void RequestLevelPreselection(Pixsaoul.Dom.Data.Engine.Name name)
        {
            _preselectedLevelName = name;
            View.OpenLevelDetail(_preselectedLevelName);
        }
        private void UnselectLevel()
        {
            _preselectedLevelName = Pixsaoul.Dom.Data.Engine.Name.Empty;
            View.CloseLevelDetail();
        }

        private void RequestLevelList()
        {
            LevelListSignal.Dispatch(View.OnLevelListChanged);
        }

        private void PlayLevel()
        {            
            PlayCreatorLevelSignal.Dispatch(_preselectedLevelName);
        }

        private void UploadLevel()
        {
            UploadCreatorLevelSignal.Dispatch(_preselectedLevelName);
        }

        private void DeleteLevel()
        {
            RemoveLevelSignal.Dispatch(_preselectedLevelName);
            RequestLevelList();
            UnselectLevel();
        }

        private void EditLevel()
        {
            EnterCreateModeSignal.Dispatch(_preselectedLevelName);
        }

        private void CreateLevel()
        {
            EnterCreateModeSignal.Dispatch(Pixsaoul.Dom.Data.Engine.Name.Empty);
        }

        protected override void Close()
        {
            base.Close();
            UnselectLevel();
        }
    }
}