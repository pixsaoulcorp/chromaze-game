﻿using Pixsaoul.Fou.Mvcs;
using Chromaze.App.Level.Import;

namespace Chromaze.Pre.Level.AutoLoader
{
    class LevelAutoLoaderMediator : Mediator
    {
        [Inject]
        public LevelAutoLoaderView View { get; set; }
        
        protected override View GetView()
        {
            return View;
        }

        [Inject] public RequestAutoloadLevelSignal RequestAutoloadLevelSignal { get; set; }

        public override void OnRegister()
        {
            base.OnRegister();
            View.RequestAutoLoadSignal.AddListener(RequestAutoLoad);
        }

        /// <summary>
        /// On Remove
        /// </summary>
        public override void OnRemove()
        {
            View.RequestAutoLoadSignal.RemoveListener(RequestAutoLoad);
            base.OnRemove();
        }

        private void RequestAutoLoad()
        {
            RequestAutoloadLevelSignal.Dispatch();
        }
    }
}
