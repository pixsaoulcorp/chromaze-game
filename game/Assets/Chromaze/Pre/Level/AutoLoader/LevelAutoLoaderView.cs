﻿
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Level.AutoLoader
{
    public class LevelAutoLoaderView : View
    {
        public RequestAutoLoadSignal RequestAutoLoadSignal = new RequestAutoLoadSignal();

        public enum LocalLevelAutoLoaderEvent
        {
            RequestAutoLoad
        }

        public override void Initialize()
        {
            base.Initialize();
            RequestAutoLoadSignal.Dispatch();
            BindButtons();
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindButtons()
        {
        }
    }
}
