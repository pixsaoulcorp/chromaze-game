﻿using Chromaze.Pre.Menu;
using Chromaze.App.Level.Story;
using Chromaze.App.SceneWorkflow;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;

namespace Chromaze.Pre.Level.Selection
{
    public class StoryModeMenuMediator : SubMenuMediator
    {
        private Pixsaoul.Dom.Data.Engine.Name _preselectedLevelName;

        [Inject] public RequestStorySignal RequestLevelListSignal { get; set; }
        [Inject] public PlayStoryLevelSignal EnterPlayModeSignal { get; set; }

        protected override SubMenuType ManagedType()
        {
            return SubMenuType.Story;
        }

        [Inject]
        public StoryModeMenu View { get; set; }

        protected override View GetView()
        {
            return View;
        }
        public override void OnRegister()
        {
            base.OnRegister();
            View.RequestLevelListSignal.AddListener(RequestLevelList);
            View.PreSelectSignal.AddListener(PreselectLevel);
            View.UnselectSignal.AddListener(UnselectLevel);
            View.RequestPlayLevelSignal.AddListener(RequestLoadPlayMode);
        }

        public override void OnRemove()
        {
            View.RequestLevelListSignal.RemoveListener(RequestLevelList);
            View.PreSelectSignal.RemoveListener(PreselectLevel);
            View.UnselectSignal.RemoveListener(UnselectLevel);

            View.RequestPlayLevelSignal.RemoveListener(RequestLoadPlayMode);
            base.OnRemove();
        }

        private void PreselectLevel(Pixsaoul.Dom.Data.Engine.Name name)
        {
            _preselectedLevelName = name;
            View.OpenLevelDetail(_preselectedLevelName);
        }

        private void UnselectLevel()
        {
            _preselectedLevelName = Pixsaoul.Dom.Data.Engine.Name.Empty;
            View.CloseLevelDetail();
        }

        private void RequestLevelList(Action<ICollection<Pixsaoul.Dom.Data.Engine.Name>> callback)
        {
            RequestLevelListSignal.Dispatch(callback);
        }

        private void RequestLoadPlayMode()
        {
            if(_preselectedLevelName != Pixsaoul.Dom.Data.Engine.Name.Empty)
            {
                EnterPlayModeSignal.Dispatch(_preselectedLevelName);
            }
        }

        protected override void Close()
        {
            base.Close();
            UnselectLevel();
        }
    }
}