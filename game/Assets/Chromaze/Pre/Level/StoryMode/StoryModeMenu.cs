﻿using Chromaze.Pre.Menu;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Pre.Menu;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Level.Selection
{
    public class StoryModeMenu : SubMenuView
    {
        [SerializeField] private NamedButtonList _levelButtons;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _unselectButton;
        [SerializeField] private GameObject _levelDetailRoot;
        [SerializeField] private TMP_Text _detailName;

        public Signal<Pixsaoul.Dom.Data.Engine.Name> PreSelectSignal = new Signal<Pixsaoul.Dom.Data.Engine.Name>();
        public Signal UnselectSignal = new Signal();
        public Signal<Action<ICollection<Pixsaoul.Dom.Data.Engine.Name>>> RequestLevelListSignal = new Signal<Action<ICollection<Pixsaoul.Dom.Data.Engine.Name>>>();
        public Signal RequestPlayLevelSignal = new Signal();

        public override void Initialize()
        {
            base.Initialize();
            _levelButtons.Initialize();
            _levelButtons.SetButtonCallback(PreselectLevel);
            _unselectButton.onClick.AddListener(UnselectLevel);
            _playButton.onClick.AddListener(PlayLevel);
            CloseLevelDetail();            
        }

        public override void Open()
        {
            base.Open();
            GetLevelList();
        }

        public void OnLevelListChanged(ICollection<Pixsaoul.Dom.Data.Engine.Name> levelNames)
        {
            _levelButtons.UpdateElements(levelNames);
        }

        private void PreselectLevel(Pixsaoul.Dom.Data.Engine.Name levelName)
        {
            PreSelectSignal.Dispatch(levelName);
        }

        private void UnselectLevel()
        {
            UnselectSignal.Dispatch();
        }

        private void GetLevelList()
        {
            RequestLevelListSignal.Dispatch(OnLevelListChanged);            
        }

        public void OpenLevelDetail(Pixsaoul.Dom.Data.Engine.Name name)
        {
            _detailName.text = name;
            _levelDetailRoot.SetActive(true);
        }

        public void CloseLevelDetail()
        {
            _levelDetailRoot.SetActive(false);
        }

        private void PlayLevel()
        {
            RequestPlayLevelSignal.Dispatch();
        }
    }
}
