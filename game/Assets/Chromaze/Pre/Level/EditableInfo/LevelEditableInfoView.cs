﻿using Chromaze.App.Level.Import;
using Pixsaoul.Fou.Mvcs;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Level.EditableInfo
{
    public class LevelEditableInfoView : View
    {
        [SerializeField] private TMP_InputField _levelNameField;
        [SerializeField] private Button _requestExportButton;
        [SerializeField] private ILevelExporterContainer _levelExporter;

        public override void Initialize()
        {
            base.Initialize();
            _requestExportButton.onClick.AddListener(RequestExport);
        }

        public void RequestExport()
        {
            _levelExporter.Result.ExportLevel((Pixsaoul.Dom.Data.Engine.Name)_levelNameField.text);
        }

        public void SetLevelName(Pixsaoul.Dom.Data.Engine.Name name)
        {
            _levelNameField.text = name.Value;
        }
    }
}