﻿using Chromaze.App.Level.Import;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Level.EditableInfo
{
    public class LevelEditableInfoMediator : Mediator
    {
        [Inject]
        public LevelEditableInfoView View { get; set; }

        [Inject] public OnLevelImportedSignal OnLevelImportedSignal { get; set; }


        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            OnLevelImportedSignal.AddListener(OnLevelImported);
        }

        public override void OnRemove()
        {
            base.OnRemove();
            OnLevelImportedSignal.RemoveListener(OnLevelImported);
        }

        private void OnLevelImported(Dom.Data.Level level)
        {
            View.SetLevelName(level.Name);
        }
    }
}
