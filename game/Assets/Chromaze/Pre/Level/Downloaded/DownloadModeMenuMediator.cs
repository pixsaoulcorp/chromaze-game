﻿using Chromaze.App.Level.Download;
using Chromaze.App.SceneWorkflow;
using Chromaze.Pre.Menu;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;

namespace Chromaze.Pre.Level.Online
{
    public class DownloadModeMenuMediator : SubMenuMediator
    {
        private Pixsaoul.Dom.Data.Engine.Name _preselectedLevelName;

        [Inject] public RequestDownloadedSignal RequestLevelListSignal { get; set; }
        [Inject] public PlayDownloadedLevelSignal PlayOnlineModeSignal { get; set; }

        [Inject] public RemoveDownloadLevelSignal RemoveDownloadLevelSignal { get; set; }

        protected override SubMenuType ManagedType()
        {
            return SubMenuType.Download;
        }

        [Inject]
        public DownloadModeMenu View { get; set; }

        protected override View GetView()
        {
            return View;
        }
        public override void OnRegister()
        {
            base.OnRegister();
            View.RequestLevelListSignal.AddListener(RequestLevelList);
            View.PreSelectSignal.AddListener(RequestLevelPreselection);
            View.RequestPlayLevelSignal.AddListener(RequestLoadPlayMode);
            View.DeleteSignal.AddListener(RequestDeleteLevel);
            View.UnselectSignal.AddListener(UnselectLevel);
        }

        public override void OnRemove()
        {
            View.RequestLevelListSignal.RemoveListener(RequestLevelList);
            View.PreSelectSignal.RemoveListener(RequestLevelPreselection);
            View.RequestPlayLevelSignal.RemoveListener(RequestLoadPlayMode);
            View.DeleteSignal.RemoveListener(RequestDeleteLevel);
            View.UnselectSignal.RemoveListener(UnselectLevel);
            base.OnRemove();
        }

        private void RequestLevelPreselection(Pixsaoul.Dom.Data.Engine.Name name)
        {
            _preselectedLevelName = name;
            View.OpenLevelDetail(_preselectedLevelName);
        }

        private void UnselectLevel()
        {
            _preselectedLevelName = Pixsaoul.Dom.Data.Engine.Name.Empty;
            View.CloseLevelDetail();
        }

        private void RequestDeleteLevel()
        {
            RemoveDownloadLevelSignal.Dispatch(_preselectedLevelName);
            RequestLevelList();
            UnselectLevel();
        }

        private void RequestLevelList()
        {
            RequestLevelListSignal.Dispatch(View.OnLevelListChanged);
        }

        private void RequestLoadPlayMode()
        {
            PlayOnlineModeSignal.Dispatch(_preselectedLevelName);
        }

        protected override void Close()
        {
            base.Close();
            UnselectLevel();
        }
    }
}