﻿using Chromaze.Pre.Menu;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Pre.Menu;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Level.Online
{
    public class DownloadModeMenu : SubMenuView
    {
        [SerializeField] private NamedButtonList _levelButtons;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _unselectLevel;
        [SerializeField] private Button _deleteLevel;
        [SerializeField] private GameObject _levelDetailRoot;
        [SerializeField] private TMP_Text _levelName;

        //TODO TBU do not use specific signal type for signals between view and mediator
        public Signal<Pixsaoul.Dom.Data.Engine.Name> PreSelectSignal = new Signal<Pixsaoul.Dom.Data.Engine.Name>();
        public Signal RequestLevelListSignal = new Signal();
        public Signal RequestPlayLevelSignal = new Signal();
        public Signal UnselectSignal = new Signal();
        public Signal DeleteSignal = new Signal();

        public override void Initialize()
        {
            base.Initialize();
            _levelButtons.Initialize();
            _levelButtons.SetButtonCallback(PreselectLevel);
            _playButton.onClick.AddListener(PlayLevel);
            _unselectLevel.onClick.AddListener(UnselectLevel);
            _deleteLevel.onClick.AddListener(DeleteLevel);
            CloseLevelDetail();
        }

        public override void Open()
        {
            base.Open();
            GetLevelList();
        }

        public void OnLevelListChanged(ICollection<Pixsaoul.Dom.Data.Engine.Name> levelNames)
        {
            _levelButtons.UpdateElements(levelNames);
        }

        private void PreselectLevel(Pixsaoul.Dom.Data.Engine.Name levelName)
        {
            _levelName.text = levelName;
            PreSelectSignal.Dispatch(levelName);
        }

        private void UnselectLevel()
        {
            UnselectSignal.Dispatch();
        }

        private void DeleteLevel()
        {
            DeleteSignal.Dispatch();
        }

        private void GetLevelList()
        {
            RequestLevelListSignal.Dispatch();
        }

        public void OpenLevelDetail(Pixsaoul.Dom.Data.Engine.Name name)
        {
            _levelDetailRoot.SetActive(true);
        }

        public void CloseLevelDetail()
        {
            _levelDetailRoot.SetActive(false);
        }

        private void PlayLevel()
        {
            RequestPlayLevelSignal.Dispatch();
        }
    }
}
