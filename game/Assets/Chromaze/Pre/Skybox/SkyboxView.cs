using Pixsaoul.Fou.Engine;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Pre.Skybox
{
    public class SkyboxView : View
    {
        [SerializeField] private Material _editorSky;
        [SerializeField] private Material _windowsSky;
        [SerializeField] private Material _androidSky; // TO generate simple skybox, use Tool/Bake material into texture with the complex skybox as input


        public override void Initialize()
        {
            base.Initialize();
            SetSkybox();
        }

        private void SetSkybox()
        {
#if UNITY_EDITOR
            RenderSettings.skybox = _editorSky;
#elif UNITY_ANDROID
            RenderSettings.skybox = _androidSky;
#elif UNITY_STANDALONE
            RenderSettings.skybox = _windowsSky;
#else
            RenderSettings.skybox = _androidSky;            
#endif
        }
    }
}