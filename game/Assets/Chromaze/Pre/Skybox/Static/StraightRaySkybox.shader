Shader "Skybox/StraightRaysBox"
{

	// inspired by https://www.shadertoy.com/view/4ddSDS Fur Space 3
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Speed("Speed", float) = 0.02
		_T("T", float) = 0.06
		_Thickness("Thickness", float) = 0.02
		_GlowReduction("GlowReduction", float) = 1.
		_Progress("Progress", float)= 0.
		_MaxLayers("MaxLayers", float) = 15
		_MarchingStep("MarchingStep", float)=0.002
	}
		SubShader
		{
			Tags { "RenderType" = "Background" }
			LOD 100

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog

				#include "UnityCG.cginc"
				#define mod(x,y) (x-y*floor(x/y))
				#define PI 3.141592653589793

				struct appdata
				{
					half4 vertex : POSITION;
					half2 uv : TEXCOORD0;
				};

				struct v2f
				{
					half2 uv : TEXCOORD0;
					UNITY_FOG_COORDS(1)
					half4 vertex : SV_POSITION;
				};

				sampler2D _MainTex;
				half4 _MainTex_ST;
				half _Speed;
				half _T;
				half _Thickness;
				half _GlowReduction;
				half  _Progress;
				half _MaxLayers;
				half _MarchingStep;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);


					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}
				//Beginning of shader toy


				half3x3 getRotZMat(half a) { return half3x3(cos(a), -sin(a), 0., sin(a), cos(a), 0., 0., 0., 1.); }

				half dstepf = 0.0;

				half map(half3 p)
				{
					//p.x += sin(p.z * 1.8);
					//p.y += cos(p.z * .2) * sin(p.x * .8);
					//p = mul(getRotZMat(p.z * 0.8 + sin(p.x) + cos(p.y)), p);
					p.xy = mod(p.xy, 0.3) - 0.15;
					return length(p.xy);
				}

				fixed4 frag(v2f i) : SV_Target
				{
					half2 uv = 2. * i.uv;
					half progress = (_Time.y + _Progress) * _Speed;
					half3 rd = normalize(half3(uv, (1. - dot(uv, uv) * .5) * .5));
					half3 ro = half3(0, 0, progress * 1.26 / 2.);
					half3 col = 0.0;
					half3 sp;
					half cs = cos(progress * 0.375);
					half si = sin(progress * 0.375);
					rd.xz = mul(rd.xz, half2x2(cs, si, -si, cs));
					half t = _T, layers = 0., d = 0., aD;
					for (half i = 0.; i < 150.; i++)
					{
						if (layers <= _MaxLayers && col.x <= 1. && t <= 8)
						{
							sp = ro + rd * t;
							d = map(sp);
							aD = (_Thickness - abs(d) * _GlowReduction) / _Thickness;
							if (aD > 0.)
							{
								col += aD * aD * (3. - 2. * aD) / (1. + t * t * 0.25) *.2;
								layers++;
							}
							t += max(d , _Thickness*2.0) * i * _MarchingStep;
						}
					}
				col = max(col, 0.);
				col = clamp(col, 0., 1.);
				half4 pixel = tex2D(_MainTex,uv);
				return pixel * float4(col, 1.);
			}
			ENDCG
		}
		}
}