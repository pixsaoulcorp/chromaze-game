using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Chromaze.Pre.Skybox
{
    public class StaticRotater : BaseBehaviour
    {
        [SerializeField] Material _skybox;

        [SerializeField] public float _speed;

        // Update is called once per frame
        void Update()
        {
            float offset = Time.time * _speed;
            _skybox.SetTextureOffset("_MainTex", new Vector2(offset, 0));
        }
    }
}