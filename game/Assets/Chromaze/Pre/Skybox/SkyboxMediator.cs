﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Skybox
{
    public class SkyboxMediator : Mediator
    {
        [Inject] public SkyboxView View { get; set; }
        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
           // EndEditionSignal.AddListener(HideGizmo);
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();
            //EndEditionSignal.RemoveListener(HideGizmo);

        }

    }
}