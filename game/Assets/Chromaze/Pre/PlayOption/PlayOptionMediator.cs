﻿using Chromaze.App.SceneWorkflow;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.PlayOption
{
    public class PlayOptionMediator : Mediator
    {
        [Inject]
        public PlayOptionView View { get; set; } //TODO Set view to real view type

        [Inject] public RequestExitLevelSignal RequestExitLevelSignal { get; set; }
        
        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            View.ExitLevelSignal.AddListener(RequestExitLevel);
        }

        public override void OnRemove()
        {
            View.ExitLevelSignal.RemoveListener(RequestExitLevel);
            base.OnRemove();
        }

        private void RequestExitLevel()
        {
            RequestExitLevelSignal.Dispatch();
        }
    }
}
