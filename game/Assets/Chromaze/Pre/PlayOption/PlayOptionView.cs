﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.PlayOption
{
    public class PlayOptionView : View
    {
        // TODO TBU add Settings => Exit
        // restart
        [SerializeField] private Button _exitPlayLevelButton;

        public Signal ExitLevelSignal = new Signal();

        public override void Initialize()
        {
            base.Initialize();
            _exitPlayLevelButton.onClick.AddListener(RequestExitLevel);
        }

        private void RequestExitLevel()
        {
            ExitLevelSignal.Dispatch();
        }

    }
}
