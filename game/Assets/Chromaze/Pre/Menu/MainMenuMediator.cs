﻿using Pixsaoul.App;
using Pixsaoul.App.Camera;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Pre.Menu
{
    public class MainMenuMediator : Mediator
    {
        [Inject]
        public MainMenuView View { get; set; }

        [Inject] public OpenSubMenuSignal OpenSubMenuSignal { get; set; }
        [Inject] public RequestExitSignal RequestExitSignal { get; set; }

        //[Inject] public SelectFixedCameraSignal RequestFixedCameraSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            View.OpenSubmenuSignal.AddListener(RequestOpenSubmenu);
            View.RequestExitApplicationSignal.AddListener(RequestExit);

        }

        public override void OnRemove()
        {
            base.OnRemove();
            View.OpenSubmenuSignal.RemoveListener(RequestOpenSubmenu);
            View.RequestExitApplicationSignal.RemoveListener(RequestExit);
        }

        private void RequestOpenSubmenu(SubMenuType type)
        {
            OpenSubMenuSignal.Dispatch(type, true);
        }

        private void RequestOpenSettings()
        {
            if (Debug.isDebugBuild)
            {
                Debug.Log("Opening settings");
            }
        }


        private void RequestExit()
        {
            RequestExitSignal.Dispatch();
        }
    }
}