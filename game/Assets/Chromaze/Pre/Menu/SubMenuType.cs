﻿namespace Chromaze.Pre.Menu
{
    // these menu are disjointed, one opening closes the others
    // see SubMenuMediator for implementation
    public enum SubMenuType
    {
        Story,
        Creator,
        Online,
        Settings,
        Download
    }
}
