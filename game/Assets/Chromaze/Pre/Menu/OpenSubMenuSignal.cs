﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Menu
{
    public class OpenSubMenuSignal : Signal<SubMenuType, bool> { }
}
