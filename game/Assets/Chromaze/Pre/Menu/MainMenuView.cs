﻿using Chromaze.Pre.Menu;
using Pixsaoul.Fou.Mvcs;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Chromaze.Pre.Menu
{
    /// <summary>
    /// Main menu view
    /// </summary>
    public class MainMenuView : View
    {
        [Header("UI components")]
        [SerializeField]
        private Button _storyMode;

        [SerializeField]
        private Button _creatorMode;

        [SerializeField]
        private Button _onlineMode;

        [SerializeField]
        private Button _downloadMode;

        [SerializeField]
        private Button _settings;

        [SerializeField]
        private Button _exitButton;

        public Signal RequestExitApplicationSignal = new Signal(); // TODO TBU see this, it may be in pixsaoul more than in Chromaze as it is to quit the game
        public Signal<SubMenuType> OpenSubmenuSignal = new Signal<SubMenuType>();


        public override void Initialize()
        {
            base.Initialize();
            _storyMode.onClick.AddListener(OpenStory);
            _creatorMode.onClick.AddListener(OpenCreator);
            _downloadMode.onClick.AddListener(OpenDownload);
            _onlineMode.onClick.AddListener(OpenOnline);
            _settings.onClick.AddListener(OpenSettings);
            _exitButton.onClick.AddListener(ExitApp);
        }

        public void OpenStory()
        {
            OpenSubmenuSignal.Dispatch(SubMenuType.Story);
        }

        public void OpenCreator()
        {
            OpenSubmenuSignal.Dispatch(SubMenuType.Creator);
        }

        public void OpenDownload()
        {
            OpenSubmenuSignal.Dispatch(SubMenuType.Download);
        }

        public void OpenOnline()
        {
            OpenSubmenuSignal.Dispatch(SubMenuType.Online);
        }

        public void OpenSettings()
        {
            OpenSubmenuSignal.Dispatch(SubMenuType.Settings);
        }

        public void ExitApp()
        {
            RequestExitApplicationSignal.Dispatch();
        }
    }
}