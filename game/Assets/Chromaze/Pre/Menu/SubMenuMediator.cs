﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.Pre.Menu
{
    public abstract class SubMenuMediator : Mediator
    {
        [Inject] public OpenSubMenuSignal OpenSubMenuSignal { get; set; }

        protected abstract SubMenuType ManagedType();
        
        public override void OnRegister()
        {
            base.OnRegister();
            OpenSubMenuSignal.AddListener(OnSubMenuOpen);
        }

        public override void OnRemove()
        {
            OpenSubMenuSignal.RemoveListener(OnSubMenuOpen);
            base.OnRemove();
        }

        private void OnSubMenuOpen(SubMenuType type, bool open)
        {
            if(type == ManagedType() && open)
            {
                Open();
            } else
            {
                Close();
            }
        }

        protected virtual void Open()
        {
            ((SubMenuView)GetView()).Open();
        }

        protected virtual void Close()
        {
            ((SubMenuView)GetView()).Close();
        }
    }
}
