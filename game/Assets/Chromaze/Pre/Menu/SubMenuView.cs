﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.Pre.Menu
{
    public abstract class SubMenuView : View
    {
        [SerializeField] private GameObject _root;

        public override void Initialize()
        {
            base.Initialize();
            Close(); // submenu are closed by default.
        }
        public virtual void Open()
        {
            _root.SetActive(true);
        }

        public virtual void Close()
        {
            _root.SetActive(false);
        }
    }
}
