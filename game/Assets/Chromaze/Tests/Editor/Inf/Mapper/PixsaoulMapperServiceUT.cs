using NUnit.Framework;
using Pixsaoul.Dom.Data.Engine;
using PersistEngine = Pixsaoul.Inf.Persistency.Engine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Chromaze.Tests.Editor.Inf.Mapper
{
    [TestFixture]
    public class PixsaoulMapperServiceUT
    {

        #region Engine tests
        [Test]
        public void AngleDomInf(
            [NUnit.Framework.Random(-359f, 359.9f, 3)] float value)
        {
            Angle angle = new Angle(value);

            var persistAngle = (float)(angle);

            var outDom = (Angle)(persistAngle);
            Assert.True(outDom.Value == angle.Value);
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "IdGenerator")]

        public void IdDomInf(int value)
        {

            Id id = new Id(value);
            var persistId = (int)(id);
            var outDom = (Id)(persistId);
            Assert.True(outDom.Value == id.Value);
        }

        [Test]
        public void LengthDomInf(
    [NUnit.Framework.Random(-1000f, 1000f, 3)] float value)
        {
          
            
            
            Length length = new Length(value);

            var persistLength = (float)(length);

            var outDom = (Length)(persistLength);
            Assert.True(outDom.Value == length.Value);
        }

        [Test]
        public void OffsetDomInf(
[NUnit.Framework.Random(-1000f, 1000f, 3)] float value)
        {
            
            
            
            Offset offset = new Offset(value);
            var persist = (float)(offset);
            var outDom = (Length)(persist);
            Assert.True(outDom.Value == offset.Value);
        }

        [Test]
        public void OrientationDomInf(
[NUnit.Framework.Random(-359f, 359f, 3)] float yaw,
[NUnit.Framework.Random(-359f, 359f, 3)] float pitch)
        {        
            
            
            Orientation inputDom = new Orientation(yaw, pitch);

            var persistOrientation = new PersistEngine.Orientation(inputDom);

            var outDom = persistOrientation.ToDom();
            Assert.True(outDom.Equals(inputDom));
        }

        [Test]
        public void PositionDomInf(
[NUnit.Framework.Random(-359f, 359f, 3)] float x,
[NUnit.Framework.Random(-359f, 359f, 3)] float y,
[NUnit.Framework.Random(-359f, 359f, 3)] float z)
        {
            Position inputDom = new Position(x, y, z);

            var persistPosition = new PersistEngine.Position(inputDom);

            var outDom = persistPosition.ToDom();
            Assert.True(outDom.Equals(inputDom));
        }

        [Test]
        public void RotationDomInf(
[NUnit.Framework.Random(-359f, 359f, 3)] float yaw,
[NUnit.Framework.Random(-359f, 359f, 3)] float pitch,
[NUnit.Framework.Random(-359f, 359f, 3)] float roll)
        {
            
            Rotation inputDom = new Rotation(yaw, pitch, roll);

            var persistRotation = new PersistEngine.Rotation(inputDom);
            Assert.True(yaw == persistRotation.yaw
                && pitch == persistRotation.pitch
                && roll== persistRotation.roll);

            var outDom = persistRotation.ToDom();
            Assert.True(outDom.Equals(inputDom));
        }

        [Test]
        public void SnappedAngleDomInf(
[NUnit.Framework.Random(-359, 359, 3)] int angle)
        {
            
            
            
            SnappedAngle inputDom = new SnappedAngle(angle);

            var persisted = (int)(inputDom);
            //Assert.True(persisted.Equals(angle));

            var outDom = (SnappedAngle)(persisted);
            Assert.True(outDom.Equals(inputDom));
        }

        [Test]
        public void SnappedDimensionDomInf(
[NUnit.Framework.Random(-359, 359, 3)] int width,
[NUnit.Framework.Random(-359, 359, 3)] int height,
[NUnit.Framework.Random(-359, 359, 3)] int depth)
        {
            SnappedDimension inputDom = new SnappedDimension(width, height, depth);

            var persisted = new PersistEngine.SnappedDimension(inputDom);

            var outDom = persisted.ToDom();
            Assert.True(outDom.Equals(inputDom));
        }

        [Test]
        public void SnappedLengthDomInf(
[NUnit.Framework.Random(-359, 359, 3)] int length)
        {
            SnappedLength inputDom = new SnappedLength(length);

            var persisted = (int)(inputDom);
            //Assert.True(persisted.Equals(angle));

            var outDom = (SnappedLength)(persisted);
            Assert.True(outDom.Equals(inputDom));
        }

        [Test]
        public void SnappeOffsetDomInf(
[NUnit.Framework.Random(-359, 359, 3)] int offset)
        {
            SnappedOffset inputDom = new SnappedOffset(offset);

            var persisted = (int)(inputDom);
            //Assert.True(persisted.Equals(angle));

            var outDom = (SnappedOffset)(persisted);
            Assert.True(outDom.Equals(inputDom));
        }

        [Test]
        public void SnappedPositionDomInf(
[NUnit.Framework.Random(-359, 359, 3)] int x,
[NUnit.Framework.Random(-359, 359, 3)] int y,
[NUnit.Framework.Random(-359, 359, 3)] int z)
        {
            SnappedPosition inputDom = new SnappedPosition(x, y, z);

            var persisted = new PersistEngine.SnappedPosition(inputDom);
            //Assert.True(persisted.Equals(angle));

            var outDom = persisted.ToDom();
            Assert.True(outDom.Equals(inputDom));
        }

        [Test]
        public void SnappedRotationDomInf(
[NUnit.Framework.Random(-359, 359, 3)] int yaw,
[NUnit.Framework.Random(-359, 359, 3)] int pitch,
[NUnit.Framework.Random(-359, 359, 3)] int roll)
        {
            SnappedRotation inputDom = new SnappedRotation(yaw, pitch, roll);

            var persistRotation = new PersistEngine.SnappedRotation(inputDom);

            var outDom = persistRotation.ToDom();
            Assert.True(outDom.Equals(inputDom));
        }

        #endregion

        #region shapes

        [Test]
        [TestCaseSource(typeof(DomGenerator), nameof(DomGenerator.TotoGenerator))]
        public void BoxelDomInf(Pixsaoul.Dom.Shape.Boxel inputDom)
        {
            var persisted = new Pixsaoul.Inf.Persistency.Shape.Boxel(inputDom);
            var outpurDom = persisted.ToDom();
            Assert.True(outpurDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), nameof(DomGenerator.BoxelShapeGenerator))]

        public void BoxelShapeDomInf(Pixsaoul.Dom.Shape.BoxelShape inputDom)
        {
            var persisted = new Pixsaoul.Inf.Persistency.Shape.BoxelShape(inputDom);
            var outpurDom = persisted.ToDom();
            Assert.True(outpurDom.Equals(inputDom));
        }
        #endregion

        public class DomGenerator
        {
            public static IEnumerable IdGenerator
            {
                get
                {
                    yield return new TestCaseData(UnityEngine.Random.Range(0,110000));
                }
            }

            public static IEnumerable StringGenerator
            {
                get
                {
                    yield return new TestCaseData("This is an example of string");
                }
            }

            public static IEnumerable TotoGenerator
            {
                get
                {
                    HashSet<Pixsaoul.Dom.Shape.Surface> directions = new HashSet<Pixsaoul.Dom.Shape.Surface>();
                    int size = UnityEngine.Random.Range(1, 7);
                    for (int i = 0; i < size; i++)
                    {
                        directions.Add((Pixsaoul.Dom.Shape.Surface)UnityEngine.Random.Range(0, 6));
                    }
                    Pixsaoul.Dom.Shape.Boxel randomBoxel = new Pixsaoul.Dom.Shape.Boxel(directions);
                    yield return new TestCaseData(randomBoxel);
                }
            }

            public static IEnumerable BoxelShapeGenerator
            {
                get
                {
                    int size = UnityEngine.Random.Range(1, 20);
                    Pixsaoul.Dom.Shape.BoxelShape shape = new Pixsaoul.Dom.Shape.BoxelShape();
                    for (int i = 0; i < size; i++)
                    {
                        shape.SetBoxel(UnityEngine.Random.Range(-100, 100), UnityEngine.Random.Range(-100, 100), UnityEngine.Random.Range(-100, 100), CreateRandomBoxel());
                    }
                    yield return new TestCaseData(shape);
                }
            }

            private static Pixsaoul.Dom.Shape.Boxel CreateRandomBoxel()
            {
                HashSet<Pixsaoul.Dom.Shape.Surface> directions = new HashSet<Pixsaoul.Dom.Shape.Surface>();
                int size = UnityEngine.Random.Range(1, 6);
                for (int i = 0; i < size; i++)
                {
                    directions.Add((Pixsaoul.Dom.Shape.Surface)UnityEngine.Random.Range(0, 6));
                }
                Pixsaoul.Dom.Shape.Boxel randomBoxel = new Pixsaoul.Dom.Shape.Boxel(directions);
                return randomBoxel;
            }
        }
    }
}