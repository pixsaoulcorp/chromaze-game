using Chromaze.Dom.Data;
using NUnit.Framework;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Inf.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.Tests.Editor.Inf.Mapper
{
    [TestFixture]
    public class ChromazeMapperServiceUT
    {

        [Test]
        [TestCaseSource(typeof(DomGenerator), "BillboardGenerator")]

        public void BillboardDomInf(
    Billboard inputDom)
        {

            var persisted = new Chromaze.Inf.Persistency.Billboard(inputDom);
            var outputDom = persisted.ToDom() ;
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "BoundariesGenerator")]

        public void BoundariesDomInf(
Boundaries inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Boundaries(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "ColorGenerator")]

        public void ColorDomInf(
          Chromaze.Dom.Data.Color colorDom)
        {

            var persistColor = new Chromaze.Inf.Persistency.Color(colorDom);
            Assert.True(colorDom.Blue == persistColor.Blue);
            Assert.True(colorDom.Green == persistColor.Green);
            Assert.True(colorDom.Red == persistColor.Red);

            var outDom = persistColor.ToDom();
            Assert.True(outDom.Equals(colorDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "CombinerGenerator")]

        public void CombinerDomInf(
Combiner inputDom)
        {
            
            var persisted = new Chromaze.Inf.Persistency.Combiner(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "CubeGenerator")]

        public void CubeDomInf(
            Cube inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Cube(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "DecomposerGenerator")]
        public void DecomposerDomInf(Decomposer inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Decomposer(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "DividerGenerator")]
        public void DividerDomInf(Divider inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Divider(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "DoorGenerator")]
        public void DoorDomInf(Door inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Door(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "EmitterGenerator")]
        public void EmitterDomInf(Emitter inputDom)
        {      
            var persisted = new Chromaze.Inf.Persistency.Emitter(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "EndGenerator")]
        public void EndDomInf(End inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.End(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "GlassGenerator")]
        public void GlassDomInf(Glass inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Glass(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "InverterGenerator")]
        public void InverterDomInf(Inverter inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Inverter(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }


        [Test]
        [TestCaseSource(typeof(DomGenerator), "LevelGenerator")]
        public void LevelDomInf(Level inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Payload.LevelForDownload(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }


        [Test]
        [TestCaseSource(typeof(DomGenerator), "LevelContentGenerator")]
        public void LevelContentDomInf(LevelContent inputDom)
        {

            var persisted = new Chromaze.Inf.Persistency.LevelContent(inputDom);
            var content = persisted.ToDom();
            var outPersisted = new Chromaze.Inf.Persistency.LevelContent(content);
            var outputDom = outPersisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "LightGenerator")]
        public void LightDomInf(Chromaze.Dom.Data.Light inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Light(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "MirrorGenerator")]
        public void MirrorDomInf(Mirror inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Mirror(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "RelayGenerator")]
        public void RelayDomInf(Relay inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Relay(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "SlopeGenerator")]
        public void SlopeDomInf(Slope inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Slope(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "StartGenerator")]
        public void StartDomInf(Start inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Start(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "StoryGenerator")]
        public void StoryDomInf(Story inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Story(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }

        [Test]
        [TestCaseSource(typeof(DomGenerator), "VictoryQuotesGenerator")]
        public void VictoryQuotesDomInf(VictoryQuotes inputDom)
        {           
            var persisted = new Chromaze.Inf.Persistency.VictoryQuotes(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }


        [Test]
        [TestCaseSource(typeof(DomGenerator), "WallGenerator")]
        public void WallDomInf(Wall inputDom)
        {
            var persisted = new Chromaze.Inf.Persistency.Wall(inputDom);
            var outputDom = persisted.ToDom();
            Assert.True(outputDom.Equals(inputDom));
        }
        //TODO TBU when adding component

        public class DomGenerator
        {

            private static Pixsaoul.Dom.Shape.BoxelShape CreateRandomShape()
            {
                int size = Random.Range(1, 20);
                Pixsaoul.Dom.Shape.BoxelShape shape = new Pixsaoul.Dom.Shape.BoxelShape();
                for (int i = 0; i < size; i++)
                {
                    shape.SetBoxel(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100), CreateRandomBoxel());
                }
                return shape;
            }

            private static Pixsaoul.Dom.Shape.Boxel CreateRandomBoxel()
            {
                HashSet<Pixsaoul.Dom.Shape.Surface> directions = new HashSet<Pixsaoul.Dom.Shape.Surface>();
                int size = Random.Range(1, 7);
                for (int i = 0; i < size; i++)
                {
                    directions.Add((Pixsaoul.Dom.Shape.Surface)Random.Range(0, 6));
                }
                Pixsaoul.Dom.Shape.Boxel randomBoxel = new Pixsaoul.Dom.Shape.Boxel(directions);
                return randomBoxel;
            }



            public static IEnumerable BillboardGenerator
            {
                get
                {
                    yield return new Billboard(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), " tutu toto");
                    yield return new Billboard(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), "never gonna give you up/");
                }
            }
            public static IEnumerable BoundariesGenerator
            {
                get
                {
                    yield return new Boundaries(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), CreateRandomShape(), Chromaze.Dom.Data.Color.CyanC);
                    yield return new Boundaries(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), CreateRandomShape(), Chromaze.Dom.Data.Color.BlueC);
                }
            }

            public static IEnumerable ColorGenerator
            {
                get
                {
                    yield return Chromaze.Dom.Data.Color.GreenC;
                    yield return Chromaze.Dom.Data.Color.RedC;
                    yield return Chromaze.Dom.Data.Color.BlueC;
                    yield return Chromaze.Dom.Data.Color.YellowC;
                    yield return Chromaze.Dom.Data.Color.MagentaC;
                    yield return Chromaze.Dom.Data.Color.CyanC;
                    yield return Chromaze.Dom.Data.Color.BlackC;
                    yield return Chromaze.Dom.Data.Color.WhiteC;
                }
            }

            public static IEnumerable CombinerGenerator
            {
                get
                {
                    yield return new Combiner(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26));
                    yield return new Combiner(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), new Orientation(0, -5));
                }
            }


            public static IEnumerable CubeGenerator
            {
                get
                {
                    yield return new Cube(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC);
                    yield return new Cube(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), Chromaze.Dom.Data.Color.BlueC);
                }
            }

            public static IEnumerable DecomposerGenerator
            {
                get
                {
                    yield return new Decomposer(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26));
                    yield return new Decomposer(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), new Orientation(0, -5));
                }
            }
            public static IEnumerable DividerGenerator
            {
                get
                {
                    yield return new Divider(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26));
                    yield return new Divider(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), new Orientation(0, -5));
                }
            }

            public static IEnumerable DoorGenerator
            {
                get
                {
                    yield return new Door(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC);
                    yield return new Door(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), Chromaze.Dom.Data.Color.BlueC);
                }
            }

            public static IEnumerable EmitterGenerator
            {
                get
                {
                    yield return new Emitter(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), EmitterStatus.Enabled, Chromaze.Dom.Data.Color.CyanC, new Orientation(12, 26));
                    yield return new Emitter(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), EmitterStatus.Disabled, Chromaze.Dom.Data.Color.WhiteC, new Orientation(0, -5));
                }
            }

            public static IEnumerable EndGenerator
            {
                get
                {
                    yield return new End(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0));
                    yield return new End(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5));
                }
            }

            public static IEnumerable GlassGenerator
            {
                get
                {
                    yield return new Glass(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0));
                    yield return new Glass(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5));
                }
            }

            public static IEnumerable InverterGenerator
            {
                get
                {
                    yield return new Inverter(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26));
                    yield return new Inverter(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), new Orientation(0, -5));
                }
            }

            public static IEnumerable LevelGenerator
            {
                get
                {
                    yield return new Chromaze.Dom.Data.Level
                        (
                        1,
                        "TestLevelName",
                        new LevelContent(
                            new Boundaries(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), CreateRandomShape(), Chromaze.Dom.Data.Color.CyanC),
                            new Start(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0)),
                            new End(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0)),
                            new List<Chromaze.Dom.Data.Component>()
                            {
                                new Billboard(ComponentId.Create(), new SnappedPosition(1, 1, 2), new SnappedRotation(0, 0, 0)," tutu toto"),
                                new Combiner(ComponentId.Create(), new SnappedPosition(1, 11, 1), new SnappedRotation(0, 0, 0), new Orientation(12,26)),
                                new Cube(ComponentId.Create(), new SnappedPosition(1, 1, 12), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC),
                                new Decomposer(ComponentId.Create(), new SnappedPosition(16, 1, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Divider(ComponentId.Create(), new SnappedPosition(1, 0, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Door(ComponentId.Create(), new SnappedPosition(1, 5, 11), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC),
                                new Emitter(ComponentId.Create(), new SnappedPosition(3, 3, 1), new SnappedRotation(0, 0, 0), EmitterStatus.Enabled, Chromaze.Dom.Data.Color.CyanC, new Orientation(12, 26)),
                                new Glass(ComponentId.Create(), new SnappedPosition(4, 7, 6), new SnappedRotation(0, 0, 0)),
                                new Inverter(ComponentId.Create(), new SnappedPosition(5, 5, 5), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Chromaze.Dom.Data.Light(ComponentId.Create(), new SnappedPosition(10, 5, 10), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC, 1f),
                                new Mirror(ComponentId.Create(), new SnappedPosition(1, 1, 0), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Relay(ComponentId.Create(), new SnappedPosition(1, 0, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Slope(ComponentId.Create(), new SnappedPosition(0, 1, 1), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC),
                                new Wall(ComponentId.Create(), new SnappedPosition(10, 10, 10), new SnappedRotation(0, 0, 0), new SnappedDimension(1,10,20), Chromaze.Dom.Data.Color.WhiteC)
                            }
                            )
                        );
                }
            }

            public static IEnumerable LevelContentGenerator
            {
                get
                {
                    yield return new LevelContent(
                            new Boundaries(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), CreateRandomShape(), Chromaze.Dom.Data.Color.CyanC),
                            new Start(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0)),
                            new End(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0)),
                            new List<Chromaze.Dom.Data.Component>()
                            {
                                new Billboard(ComponentId.Create(), new SnappedPosition(1, 1, 2), new SnappedRotation(0, 0, 0)," tutu toto"),
                                new Combiner(ComponentId.Create(), new SnappedPosition(1, 11, 1), new SnappedRotation(0, 0, 0), new Orientation(12,26)),
                                new Cube(ComponentId.Create(), new SnappedPosition(1, 1, 12), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC),
                                new Decomposer(ComponentId.Create(), new SnappedPosition(16, 1, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Divider(ComponentId.Create(), new SnappedPosition(1, 0, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Door(ComponentId.Create(), new SnappedPosition(1, 5, 11), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC),
                                new Emitter(ComponentId.Create(), new SnappedPosition(3, 3, 1), new SnappedRotation(0, 0, 0), EmitterStatus.Enabled, Chromaze.Dom.Data.Color.CyanC, new Orientation(12, 26)),
                                new Glass(ComponentId.Create(), new SnappedPosition(4, 7, 6), new SnappedRotation(0, 0, 0)),
                                new Inverter(ComponentId.Create(), new SnappedPosition(5, 5, 5), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Chromaze.Dom.Data.Light(ComponentId.Create(), new SnappedPosition(10, 5, 10), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC, 25f),
                                new Mirror(ComponentId.Create(), new SnappedPosition(1, 1, 0), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Relay(ComponentId.Create(), new SnappedPosition(1, 0, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26)),
                                new Slope(ComponentId.Create(), new SnappedPosition(0, 1, 1), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC),
                                new Wall(ComponentId.Create(), new SnappedPosition(10, 10, 10), new SnappedRotation(0, 0, 0), new SnappedDimension(1,10,20), Chromaze.Dom.Data.Color.WhiteC)
                            }
                            );
                }
            }

            public static IEnumerable LightGenerator
            {
                get
                {
                    yield return new Chromaze.Dom.Data.Light(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC,1f);
                    yield return new Chromaze.Dom.Data.Light(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), Chromaze.Dom.Data.Color.BlueC,0.23f);
                }
            }

            public static IEnumerable MirrorGenerator
            {
                get
                {
                    yield return new Mirror(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26));
                    yield return new Mirror(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), new Orientation(0, -5));
                }
            }

            public static IEnumerable RelayGenerator
            {
                get
                {
                    yield return new Relay(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), new Orientation(12, 26));
                    yield return new Relay(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), new Orientation(0, -5));
                }
            }

            public static IEnumerable SlopeGenerator
            {
                get
                {
                    yield return new Slope(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), Chromaze.Dom.Data.Color.CyanC);
                    yield return new Slope(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), Chromaze.Dom.Data.Color.BlueC);
                }
            }

            public static IEnumerable StartGenerator
            {
                get
                {
                    yield return new Start(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0));
                    yield return new Start(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5));
                }
            }

            public static IEnumerable StoryGenerator
            {
                get
                {
                    yield return new Story(new System.Collections.Generic.List<Pixsaoul.Dom.Data.Engine.Name>() { "toto", "tutu", "titi" });
                    yield return new Story(new System.Collections.Generic.List<Pixsaoul.Dom.Data.Engine.Name>() { "toto" });
                }
            }

            //public static IEnumerable VictoryQuotesGenerator
            //{
            //    get
            //    {
            //        yield return new VictoryQuotes(new System.Collections.Generic.List<Text>() { "toto", "tutu", "titi" });
            //    }
            //}

            public static IEnumerable WallGenerator
            {
                get
                {
                    yield return new Wall(ComponentId.Create(), new SnappedPosition(1, 1, 1), new SnappedRotation(0, 0, 0), new SnappedDimension(1, 10, 20), Chromaze.Dom.Data.Color.CyanC);
                    yield return new Wall(ComponentId.Create(), new SnappedPosition(0, 5, 8), new SnappedRotation(12, 9, -5), new SnappedDimension(1, 15, 0), Chromaze.Dom.Data.Color.BlueC);
                }
            }

            //TODO TBU when adding component
        }
    }
}