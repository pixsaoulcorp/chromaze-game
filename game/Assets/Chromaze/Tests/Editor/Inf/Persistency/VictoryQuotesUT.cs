﻿using Chromaze.Inf.Persistency;
using NUnit.Framework;
using Pixsaoul.Inf.Serialization;
using System.Collections.Generic;

namespace Chromaze.Tests.Editor.Inf.Persistency
{
    public class VictoryQuotesUT
    {
        [Test]

        public void PersistBackAndForth()
        {
            VictoryQuotes persistQuotes = new VictoryQuotes();
            persistQuotes.Quotes = new List<string>() 
            { 
                "All our base are belong to you." ,
                "You win... this time." ,
                "The cake is a... in another game." ,
                "Your princess is in another level." 
            };

            JsonSerializerService serializer = new JsonSerializerService();
            string seri = serializer.Serialize(persistQuotes);
            VictoryQuotes outputPersistId = serializer.Deserialize<VictoryQuotes>(seri);

            Assert.IsTrue(persistQuotes.Quotes.Count == 4);
        }
    }
}
