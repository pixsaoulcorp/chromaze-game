﻿using Chromaze.Dom.Data;
using Persist = Chromaze.Inf.Persistency;
using NUnit.Framework;
using Pixsaoul.Inf.Serialization;
using System.Collections;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Dom.Shape;
using Pixsaoul.Inf.BoxelGeneration;
using System.Collections.Generic;

namespace Chromaze.Tests.Editor.Inf.Persistency
{
    public class LevelUT
    {
        [Test]
        [TestCaseSource(typeof(LevelCases), "ToPersist")]
        public void PersistBackAndForth(LevelContent level)
        {            
            var persistLevel = new Persist.LevelContent(level);
            //Persist.Level persistLevel = new Persist.Level(level);
            BinarySerializer serializer = new BinarySerializer();
            serializer.Initialize();
            string serializedData = serializer.Serialize(persistLevel);
            Persist.LevelContent outputPersist = serializer.Deserialize<Persist.LevelContent>(serializedData);

            LevelContent outputDom =outputPersist.ToDom();
            //Level outputDom = outputPersist.ToDom();
            bool equals = level.Equals(outputDom);
            Assert.IsTrue(equals);            
        }
    }

    public class LevelCases
    {
        public static IEnumerable ToPersist
        {
            get
            {
                yield return new TestCaseData(GetMockLevel("test"));
            }
        }

        public static Chromaze.Dom.Data.LevelContent GetMockLevel(string name)
        {
            BoxelShape shape = BoxelShapeExtensions.CreateSimpleShape(5, 2, 3, SurfaceMode.Intern);

            List<Component> components = new List<Chromaze.Dom.Data.Component>();
            Boundaries boundaries = new Boundaries(ComponentId.Create(), new SnappedPosition((SnappedOffset)(-20), (SnappedOffset)(0), (SnappedOffset)(-20)), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), shape, Color.BlackC);
            Start start = new Start(ComponentId.Create(), new SnappedPosition((SnappedOffset)0, (SnappedOffset)0, (SnappedOffset)0), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0));
            End end = new End(ComponentId.Create(), new SnappedPosition((SnappedOffset)4, (SnappedOffset)0, (SnappedOffset)8), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0));

            components.Add(new Door(ComponentId.Create(), new SnappedPosition((SnappedOffset)8, (SnappedOffset)0, (SnappedOffset)8), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), Chromaze.Dom.Data.Color.BlueC));
            components.Add(new Door(ComponentId.Create(), new SnappedPosition((SnappedOffset)8, (SnappedOffset)0, (SnappedOffset)10), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), Chromaze.Dom.Data.Color.RedC));
            components.Add(new Door(ComponentId.Create(), new SnappedPosition((SnappedOffset)8, (SnappedOffset)0, (SnappedOffset)12), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), Chromaze.Dom.Data.Color.MagentaC));

            components.Add(new Emitter(ComponentId.Create(), new SnappedPosition((SnappedOffset)3, (SnappedOffset)1, (SnappedOffset)5), new SnappedRotation((SnappedAngle)1, (SnappedAngle)1, (SnappedAngle)0), EmitterStatus.Enabled, Chromaze.Dom.Data.Color.RedC, new Orientation((Angle)45f, (Angle)90f)));
            components.Add(new Emitter(ComponentId.Create(), new SnappedPosition((SnappedOffset)6, (SnappedOffset)1, (SnappedOffset)8), new SnappedRotation((SnappedAngle)2, (SnappedAngle)2, (SnappedAngle)0), EmitterStatus.Enabled, Chromaze.Dom.Data.Color.BlueC, new Orientation((Angle)0f, (Angle)0f)));
            components.Add(new Emitter(ComponentId.Create(), new SnappedPosition((SnappedOffset)1, (SnappedOffset)1, (SnappedOffset)8), new SnappedRotation((SnappedAngle)1, (SnappedAngle)3, (SnappedAngle)0), EmitterStatus.Enabled, Chromaze.Dom.Data.Color.BlueC, new Orientation((Angle)0f, (Angle)0f)));

            components.Add(new Mirror(ComponentId.Create(), new SnappedPosition((SnappedOffset)3, (SnappedOffset)1, (SnappedOffset)(-5)), new SnappedRotation((SnappedAngle)1, (SnappedAngle)2, (SnappedAngle)1), new Orientation((Angle)90f, (Angle)45f)));
            components.Add(new Mirror(ComponentId.Create(), new SnappedPosition((SnappedOffset)(-4), (SnappedOffset)1, (SnappedOffset)(-2)), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), new Orientation((Angle)0f, (Angle)0f)));

            components.Add(new Cube(ComponentId.Create(), new SnappedPosition((SnappedOffset)(-5), (SnappedOffset)1, (SnappedOffset)1), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), Chromaze.Dom.Data.Color.BlueC));
            components.Add(new Cube(ComponentId.Create(), new SnappedPosition((SnappedOffset)10, (SnappedOffset)1, (SnappedOffset)3), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), Chromaze.Dom.Data.Color.BlueC));
            components.Add(new Cube(ComponentId.Create(), new SnappedPosition((SnappedOffset)2, (SnappedOffset)1, (SnappedOffset)1), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), Chromaze.Dom.Data.Color.BlueC));

            components.Add(new Slope(ComponentId.Create(), new SnappedPosition((SnappedOffset)3, (SnappedOffset)0, (SnappedOffset)5), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), Chromaze.Dom.Data.Color.BlueC));
            components.Add(new Slope(ComponentId.Create(), new SnappedPosition((SnappedOffset)(-1), (SnappedOffset)0, (SnappedOffset)(-2)), new SnappedRotation((SnappedAngle)0, (SnappedAngle)1, (SnappedAngle)0), Chromaze.Dom.Data.Color.BlueC));
            components.Add(new Slope(ComponentId.Create(), new SnappedPosition((SnappedOffset)(-3), (SnappedOffset)1, (SnappedOffset)3), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), Chromaze.Dom.Data.Color.BlueC));


            components.Add(new Wall(ComponentId.Create(), new SnappedPosition((SnappedOffset)10, (SnappedOffset)1, (SnappedOffset)3), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), new SnappedDimension((SnappedLength)5, (SnappedLength)8, (SnappedLength)2), Chromaze.Dom.Data.Color.BlueC));

            components.Add(new Combiner(ComponentId.Create(), new SnappedPosition((SnappedOffset)2, (SnappedOffset)1, (SnappedOffset)3), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), new Orientation((Angle)0f, (Angle)0f)));
            components.Add(new Decomposer(ComponentId.Create(), new SnappedPosition((SnappedOffset)3, (SnappedOffset)1, (SnappedOffset)3), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), new Orientation((Angle)0f, (Angle)0f)));
            components.Add(new Divider(ComponentId.Create(), new SnappedPosition((SnappedOffset)4, (SnappedOffset)1, (SnappedOffset)3), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), new Orientation((Angle)0f, (Angle)0f)));
            components.Add(new Inverter(ComponentId.Create(), new SnappedPosition((SnappedOffset)5, (SnappedOffset)1, (SnappedOffset)3), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), new Orientation((Angle)0f, (Angle)0f)));
            components.Add(new Relay(ComponentId.Create(), new SnappedPosition((SnappedOffset)6, (SnappedOffset)1, (SnappedOffset)3), new SnappedRotation((SnappedAngle)0, (SnappedAngle)0, (SnappedAngle)0), new Orientation((Angle)0f, (Angle)0f)));

            Chromaze.Dom.Data.LevelContent mock = new LevelContent(boundaries, start, end, components);
            //no Id.Create here to avoid creating a new GUID every time for nothing (conflicts)
            return mock;

        }

    }
}
