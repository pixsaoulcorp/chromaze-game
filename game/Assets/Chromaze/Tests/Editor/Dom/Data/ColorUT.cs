﻿using Chromaze.Dom.Data;
using NUnit.Framework;
using System.Collections;

namespace Chromaze.Tests.Dom.Data
{

    [TestFixture]
    public class ColorUT
    {
        [Test]
        [TestCaseSource(typeof(ColorCaseFactory), "InvertCases")]        
        public Color Invert(Color color)
        {
            return Color.Invert(color);
        }

        [Test]
        [TestCaseSource(typeof(ColorCaseFactory), "AddCases")]
        public Color Add(Color a, Color b)
        {
            return Color.Add(a,b);
        }
    }

    public class ColorCaseFactory
    {
        public static IEnumerable InvertCases
        {
            get
            {
                yield return new TestCaseData(Color.WhiteC).Returns(Color.BlackC);
                yield return new TestCaseData(Color.RedC).Returns(Color.CyanC);
                yield return new TestCaseData(Color.BlueC).Returns(Color.YellowC);
                yield return new TestCaseData(Color.GreenC).Returns(Color.MagentaC);
                yield return new TestCaseData(Color.CyanC).Returns(Color.RedC);
                yield return new TestCaseData(Color.YellowC).Returns(Color.BlueC);
                yield return new TestCaseData(Color.MagentaC).Returns(Color.GreenC);
                yield return new TestCaseData(Color.BlackC).Returns(Color.WhiteC);
            }
        }

        public static IEnumerable AddCases
        {
            get
            {
                yield return new TestCaseData(Color.BlackC, Color.WhiteC).Returns(Color.WhiteC);
                yield return new TestCaseData(Color.BlackC, Color.RedC).Returns(Color.RedC);
                yield return new TestCaseData(Color.BlackC, Color.BlueC).Returns(Color.BlueC);
                yield return new TestCaseData(Color.BlackC, Color.GreenC).Returns(Color.GreenC);
                yield return new TestCaseData(Color.BlackC, Color.CyanC).Returns(Color.CyanC);
                yield return new TestCaseData(Color.BlackC, Color.YellowC).Returns(Color.YellowC);
                yield return new TestCaseData(Color.BlackC, Color.MagentaC).Returns(Color.MagentaC);
                yield return new TestCaseData(Color.BlackC, Color.BlackC).Returns(Color.BlackC);

                yield return new TestCaseData(Color.RedC, Color.WhiteC).Returns(Color.WhiteC);
                yield return new TestCaseData(Color.RedC, Color.RedC).Returns(Color.RedC);
                yield return new TestCaseData(Color.RedC, Color.BlueC).Returns(Color.MagentaC);
                yield return new TestCaseData(Color.RedC, Color.GreenC).Returns(Color.YellowC);
                yield return new TestCaseData(Color.RedC, Color.CyanC).Returns(Color.WhiteC);
                yield return new TestCaseData(Color.RedC, Color.YellowC).Returns(Color.YellowC);
                yield return new TestCaseData(Color.RedC, Color.MagentaC).Returns(Color.MagentaC);
                yield return new TestCaseData(Color.RedC, Color.BlackC).Returns(Color.RedC);
            }
        }
    }
}
