﻿using Chromaze.Dom.Data;
using Pixsaoul.Fou.Mvcs;
using System;

namespace Chromaze.App.EndLevel
{
    public class RequestVictoryQuotesSignal : Signal<Action<VictoryQuotes>>
    {
    }
}
