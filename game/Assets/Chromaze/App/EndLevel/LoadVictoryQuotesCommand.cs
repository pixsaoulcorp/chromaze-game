﻿using Chromaze.Dom.Data;
using Pixsaoul.App.Persistency;
using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;

namespace Chromaze.App.EndLevel
{
    public class LoadVictoryQuotesCommand : Command
    {
        [Inject] public Action<VictoryQuotes> OnQuotesReceived { get; set; }
        [Inject(PersistencyType.Internal)] public IPersistencyService PersistencyService { get; set; }
        [Inject(PersistencyType.Local)] public IFilePersistencyService FilePersistencyService { get; set; }
                
        public override void Execute()
        {
            EndPoint _endpoint = new EndPoint(new List<string>() {"utils","victoryQuotes.json" });
            Inf.Persistency.VictoryQuotes internalVictoryQuotesPersist = PersistencyService.Read<Inf.Persistency.VictoryQuotes>(_endpoint.ToString());

            Dom.Data.VictoryQuotes domVictoryQuotes = internalVictoryQuotesPersist.ToDom();
            //TODO TBU MAPPER
            // disabled right now because if the file on player side does not exist => crash
            ////Customizable quotes from player
            //Inf.Persistency.VictoryQuotes customPersistQuotes = FilePersistencyService.Read<Inf.Persistency.VictoryQuotes>(_endpoint.ToString());
            //VictoryQuotes customQuotes = customPersistQuotes.ToDom();

            //internalQuotes.Combine(customQuotes);
            OnQuotesReceived(domVictoryQuotes);
        }
    }
}
