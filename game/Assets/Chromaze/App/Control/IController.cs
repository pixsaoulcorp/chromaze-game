﻿using System;

namespace Chromaze.App.Control
{
    [Serializable]
    public class IControllerContainer : IUnifiedContainer<IController> { }

    public interface IController : IEnablable
    {

    }
}
