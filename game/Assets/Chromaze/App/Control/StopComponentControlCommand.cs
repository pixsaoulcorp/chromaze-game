﻿using Chromaze.App.Player;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Control
{
    public class StopComponentControlCommand : Command
    {
        [Inject] public EnablePlayerMovementSignal AllowPlayerMovementSignal { get; set; }

        public override void Execute()
        {
            AllowPlayerMovementSignal.Dispatch();
        }
    }
}