﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Edition
{
    /// <summary>
    /// Request the deletion of a component, to be used when knowing what you want to delete.
    /// </summary>
    public class RequestDeleteComponentSignal : Signal<Dom.Data.Component> { }
}
