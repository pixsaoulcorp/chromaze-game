﻿
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Edition
{
    public class EndComponentEditionCommand : Command
    {
        [Inject] public NotifyComponentEditionEndedSignal NotifyComponentEditionEndedSignal { get; set; }
        public override void Execute()
        {
            NotifyComponentEditionEndedSignal.Dispatch();
        }
    }
}
