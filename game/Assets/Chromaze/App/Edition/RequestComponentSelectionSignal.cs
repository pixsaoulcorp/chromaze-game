﻿using Chromaze.App.Components;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Edition
{
    public class RequestComponentSelectionSignal : Signal<IComponent> { }
}
