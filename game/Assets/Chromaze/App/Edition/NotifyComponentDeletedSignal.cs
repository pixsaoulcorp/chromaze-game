﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Edition
{
    public class NotifyComponentDeletedSignal : Signal<Dom.Data.Component> { }
}
