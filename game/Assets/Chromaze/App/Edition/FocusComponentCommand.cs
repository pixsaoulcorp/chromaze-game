﻿using Chromaze.App.Components;
using Chromaze.App.Edition;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Edition
{
    public class FocusComponentCommand : Command
    {
        [Inject] public NotifyComponentEditionBeginSignal BeginEditionSignal { get; set; }

        [Inject] public IComponent Component { get; set; }

        public override void Execute()
        {
            BeginEditionSignal.Dispatch(Component);
        }
    }
}
