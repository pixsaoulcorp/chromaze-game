﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Edition
{
    /// <summary>
    /// Generic request to delete the currently selected signal, without knowing it.
    /// </summary>
    public class RequestDeleteSelectedComponentSignal : Signal { }
}
