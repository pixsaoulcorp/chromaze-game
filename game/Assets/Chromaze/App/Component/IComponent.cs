﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.App.Components
{
    [Serializable]
    public class IComponentContainer : IUnifiedContainer<IComponent> { }

    public interface IComponent
    {
        Chromaze.Dom.Data.Component GetComponentData();
        void SetComponentData(Chromaze.Dom.Data.Component componentData);

        IObservable<Dom.Data.Component> ObservableComponentData { get; }

    }
}