﻿using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using PDom = Pixsaoul.Dom.Data.Engine;

namespace Chromaze.App.Level.Creator
{
    public class RequestCreationsSignal : Signal<Action<List<PDom.Name>>> { }
}
