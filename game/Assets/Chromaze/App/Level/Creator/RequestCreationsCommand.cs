﻿using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using PDom = Pixsaoul.Dom.Data.Engine;

namespace Chromaze.App.Level.Creator
{
    public class RequestCreationsCommand : Command
    {
        [Inject] public ICreatorRepository Repository { get; set; }
        [Inject] public Action<List<PDom.Name>> Callback { get; set; }

        public override void Execute()
        {
            List<PDom.Name> creations = Repository.GetList();
            Callback.Invoke(creations);
        }
    }
}
