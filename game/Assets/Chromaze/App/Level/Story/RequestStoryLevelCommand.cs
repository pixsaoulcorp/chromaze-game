﻿using Pixsaoul.Fou.Mvcs;
using System;

namespace Chromaze.App.Level.Story
{
    public class RequestStoryLevelCommand : Command
    {
        [Inject] public Pixsaoul.Dom.Data.Engine.Name Name { get; set; }

        [Inject] public IStoryRepository LevelSelectionService { get; set; }
        [Inject] public Action<Dom.Data.Level> Callback { get; set; }

        public override void Execute()
        {
            Dom.Data.Level level = LevelSelectionService.GetStoryLevel(Name);
            Callback.Invoke(level);
        }
    }
}
