﻿using Pixsaoul.Fou.Mvcs;
using System;

namespace Chromaze.App.Level.Story
{
    public class RequestStoryLevelSignal : Signal<Pixsaoul.Dom.Data.Engine.Name, Action<Dom.Data.LevelContent>> { }
}
