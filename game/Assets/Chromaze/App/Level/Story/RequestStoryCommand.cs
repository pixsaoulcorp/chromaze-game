﻿using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using PDom = Pixsaoul.Dom.Data.Engine;


namespace Chromaze.App.Level.Story
{
    public class RequestStoryCommand : Command
    {
        [Inject] public IStoryRepository LevelSelectionService { get; set; }
        [Inject] public Action<List<PDom.Name>> Callback { get; set; }

        public override void Execute()
        {
            List<PDom.Name> story = LevelSelectionService.GetStory();
            Callback.Invoke(story);
        }
    }
}
