﻿using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using PDom = Pixsaoul.Dom.Data.Engine;

namespace Chromaze.App.Level.Story
{
    public class RequestStorySignal : Signal<Action<List<PDom.Name>>> { }
}
