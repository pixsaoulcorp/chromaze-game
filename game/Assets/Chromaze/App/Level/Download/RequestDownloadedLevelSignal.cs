﻿using Pixsaoul.Fou.Mvcs;
using System;

namespace Chromaze.App.Level.Download
{
    public class RequestDownloadedLevelSignal : Signal<Pixsaoul.Dom.Data.Engine.Name, Action<Dom.Data.LevelContent>> { }
}
