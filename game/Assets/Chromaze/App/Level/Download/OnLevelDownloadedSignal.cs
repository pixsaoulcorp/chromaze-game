﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Download
{
    public class OnLevelDownloadedSignal : Signal<Dom.Data.Level>
    {
    }
}
