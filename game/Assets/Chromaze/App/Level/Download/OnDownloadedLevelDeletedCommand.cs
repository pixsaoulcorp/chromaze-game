﻿using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Download
{
    public class OnDownloadedLevelDeletedCommand : Command
    {
        [Inject] public CreateLogSignal CreateSignal { get; set; }

        [Inject] public Pixsaoul.Dom.Data.Engine.Name levelName { get; set; }

        public override void Execute()
        {
            CreateSignal.Dispatch(new Log("Download", $"Level {levelName} has been removed."));
        }
    }
}
