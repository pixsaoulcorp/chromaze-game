﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Download
{
    public class OnDownloadedLevelDeletedSignal : Signal<Pixsaoul.Dom.Data.Engine.Name>
    {
    }
}
