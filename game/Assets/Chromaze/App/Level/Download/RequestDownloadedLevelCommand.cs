﻿using Pixsaoul.Fou.Mvcs;
using System;
using PDom = Pixsaoul.Dom.Data.Engine;

namespace Chromaze.App.Level.Download
{
    public class RequestDownloadedLevelCommand : Command
    {
        [Inject] public IDownloadRepository Repository { get; set; }
        [Inject] public Action<Dom.Data.Level> Callback { get; set; }
        [Inject] public PDom.Name Name { get; set; }

        public override void Execute()
        {
            Dom.Data.Level level = Repository.GetLevel(Name);
            Callback.Invoke(level);
        }
    }
}
