﻿using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using PDom = Pixsaoul.Dom.Data.Engine;

namespace Chromaze.App.Level.Download
{
    public class RequestDownloadedCommand : Command
    {
        [Inject] public IDownloadRepository Repository { get; set; }
        [Inject] public Action<List<PDom.Name>> Callback { get; set; }

        public override void Execute()
        {
            List<PDom.Name> levelsName = Repository.GetDownloadedLevels();
            Callback.Invoke(levelsName);
        }
    }
}
