﻿using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Download
{
    public class OnLevelDownloadedCommand : Command
    {
        [Inject] public CreateLogSignal Log { get; set; }
        [Inject] public Dom.Data.Level Level { get; set; }
        public override void Execute()
        {
            Log.Dispatch(new Log("Download", $"Level {Level.Name} has been downloaded"));
        }
    }
}
