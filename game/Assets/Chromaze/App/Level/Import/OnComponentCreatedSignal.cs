﻿using Chromaze.App.Components;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Import
{
    public class OnComponentCreatedSignal : Signal<IComponent> { }
}
