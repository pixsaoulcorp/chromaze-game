﻿using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Import
{
    public class ExportLevelCommand : Command
    {
        [Inject] public Dom.Data.Level Level { get; set; }
        [Inject] public ICreatorRepository Repository { get; set; }
        [Inject] public CreateLogSignal logSignal { get; set; }

        public override void Execute()
        {
            Repository.SetLevel(Level);
            logSignal.Dispatch(new Log($"Export", $"Created level {Level.Name.ToString()} saved"));

        }
    }
}
