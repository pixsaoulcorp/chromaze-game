﻿using Chromaze.App.SceneWorkflow;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Import
{
    public class OnLevelImportedCommand : Command
    {
        [Inject] public ILevelUserService LevelUse{get;set;}
        [Inject] public NotifyLevelImportedSignal NotifyLevelImportedSignal { get; set; }
        [Inject] public Dom.Data.Level ImportedLevel { get; set; }
        /// <summary>
        /// Notify the views about
        /// </summary>
        /// <param name="data"></param>
        public override void Execute()
        {
            // Once the level has been started, the user can start moving
            NotifyLevelImportedSignal.Dispatch(ImportedLevel);
           // LevelUse.Begin();
        }
    }
}