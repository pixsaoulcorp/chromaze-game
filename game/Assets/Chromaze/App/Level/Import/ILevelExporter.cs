﻿using System;

namespace Chromaze.App.Level.Import
{
    [Serializable]
    public class ILevelExporterContainer : IUnifiedContainer<ILevelExporter> { }

    public interface ILevelExporter
    {
        void ExportLevel(Pixsaoul.Dom.Data.Engine.Name name);
    }
}
