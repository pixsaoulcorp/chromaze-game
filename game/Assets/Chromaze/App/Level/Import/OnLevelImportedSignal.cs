﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Import
{
    public class OnLevelImportedSignal : Signal<Dom.Data.Level> { }
}
