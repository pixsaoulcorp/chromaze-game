﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.

using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Import
{

    [System.Serializable]
    public class ILevelImporterServiceContainer : IUnifiedContainer<ILevelImporterService> { }
    /// <summary>
    /// Level manager service interface. Responsable for anything level representation management
    /// including loading storing level. For now also manage in game lifecycle of level.
    /// </summary>
    public interface ILevelImporterService : IService
    {
        void ImportLevel(Dom.Data.Level level);        
    }
}