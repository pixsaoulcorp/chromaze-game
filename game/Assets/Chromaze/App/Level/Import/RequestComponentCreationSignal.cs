﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Import
{
    public class RequestComponentCreationSignal : Signal<Dom.Data.Component>{ }
}
