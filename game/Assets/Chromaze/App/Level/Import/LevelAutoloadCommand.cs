﻿using Chromaze.App.Level.Story;
using Chromaze.App.SceneWorkflow;
using Pixsaoul.Fou.Mvcs;
using System.Collections;
using System.Collections.Generic;

namespace Chromaze.App.Level.Import
{
    public class LevelAutoloadCommand : Command
    {
        [Inject] public ILevelImporterService levelImporterService { get; set; }
        [Inject] public IStoryRepository levelSelectionService{ get; set; }

        [Inject] public PlayStoryLevelSignal EnterPlayModeSignal { get; set; }

        /// <summary>
        /// Execute method to override when creating a command
        /// call base.Execute() after your specific behaviour
        /// </summary>
        public override void Execute()
        {
            ICollection<Pixsaoul.Dom.Data.Engine.Name> levels = levelSelectionService.GetStory();
            if(levels.Count > 0)
            {
                IEnumerator en = levels.GetEnumerator();
                en.MoveNext();
                EnterPlayModeSignal.Dispatch((Pixsaoul.Dom.Data.Engine.Name)en.Current);
            }
        }
    }
}
