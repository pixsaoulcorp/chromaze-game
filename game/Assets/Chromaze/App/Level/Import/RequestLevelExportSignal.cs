﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Import
{
    public class RequestLevelExportSignal : Signal<Dom.Data.Level> { }
}
