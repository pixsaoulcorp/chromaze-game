﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Level.Import
{
    public class NotifyLevelImportedSignal : Signal<Dom.Data.Level> { }
}
