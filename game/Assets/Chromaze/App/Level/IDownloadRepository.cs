﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Fou.Mvcs;
using System.Collections.Generic;

namespace Chromaze.App.Level
{
    public interface IDownloadRepository : IRepository, IService
    {
        List<Pixsaoul.Dom.Data.Engine.Name> GetDownloadedLevels();

        Dom.Data.Level GetLevel(Pixsaoul.Dom.Data.Engine.Name name);

        void SetLevel(Dom.Data.Level level);

        void RemoveLevel(Pixsaoul.Dom.Data.Engine.Name level);
    }
}
