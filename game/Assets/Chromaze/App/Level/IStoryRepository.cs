﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Fou.Mvcs;
using System.Collections.Generic;
using PDom = Pixsaoul.Dom.Data.Engine;


namespace Chromaze.App.Level
{
    [System.Serializable]
    public class IStoryRepositoryContainer : IUnifiedContainer<IStoryRepository> { }

    public interface IStoryRepository : IService, IRepository
    {
        List<PDom.Name> GetStory();

        Dom.Data.Level GetStoryLevel(Pixsaoul.Dom.Data.Engine.Name name);
    }
}