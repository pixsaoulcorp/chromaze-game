﻿using Pixsaoul.Fou.Markers;
using Pixsaoul.Fou.Mvcs;
using System.Collections.Generic;

namespace Chromaze.App.Level
{
    public interface ICreatorRepository : IRepository, IService
    {
        List<Pixsaoul.Dom.Data.Engine.Name> GetList();

        Dom.Data.Level GetLevel(Pixsaoul.Dom.Data.Engine.Name name);

        void SetLevel(Dom.Data.Level level);

        void RemoveLevel(Pixsaoul.Dom.Data.Engine.Name level);
    }
}
