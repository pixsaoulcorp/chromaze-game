﻿
namespace Chromaze.App
{
    public interface ITriggerable
    {
        void Trigger();
    }
}