﻿
using System;

namespace Chromaze.App.Optics
{
    [System.Serializable]
    public class IReceiverContainer : IUnifiedContainer<IReceiver> { }

    public interface IReceiver
    {
        void ConfigureReception();
        void Receive(Ray ray, Guid sourceId);
        void StopReceiving(Guid rayId);
    }
}
