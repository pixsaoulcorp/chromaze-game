﻿using UnityEngine;

namespace Chromaze.App.Optics
{
    [System.Serializable]
    public class IRayConsumerContainer : IUnifiedContainer<IRayConsumer> { }

    public interface IRayConsumer : IEnablable
    {
        void SetRay(Ray ray);

    }
}
