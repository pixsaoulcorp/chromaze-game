﻿using System;

namespace Chromaze.App.Optics
{
    [Serializable]
    public class IInverterContainer : IUnifiedContainer<IInverter> { }

    public interface IInverter
    {
        void SetData(Dom.Data.Inverter inverter);
    }
}
