﻿using Chromaze.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using System;
using UnityEngine;

namespace Chromaze.App.Optics
{
    [System.Serializable]
    public class IEmitterContainer : IUnifiedContainer<IEmitter> { }

    public interface IEmitter
    {
        Guid Id { get; }
        Position Position { get; set; }
        Vector3 Direction { get; set; }

        EmitterStatus Status { get; set; }
        Dom.Data.Color Color { get; set; }
        void SetSourceId(Guid sourceId);

        void SetData(EmitterStatus status, Dom.Data.Color color);
    }
}