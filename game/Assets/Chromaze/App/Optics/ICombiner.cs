﻿using Chromaze.App.Optics.Factories;

namespace Chromaze.App.Optics
{
    [System.Serializable]
    public class ICombinerContainer : IUnifiedContainer<ICombiner> { }

    public interface ICombiner
    {
        void SetData(Dom.Data.Combiner combiner);
    }
}
