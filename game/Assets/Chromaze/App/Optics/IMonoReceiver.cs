﻿using Chromaze.Dom.Data;
using System;

namespace Chromaze.App.Optics
{
    [System.Serializable]
    public class IMonoReceiverContainer : IUnifiedContainer<IMonoReceiver> { }

    public interface IMonoReceiver : IReceiver
    {
        ReceptionStatus IsReceiving { get; }
        //any access should first test IsReceiving
        Guid? SourceId { get; }
        Ray? ReceivedRay { get; }
    }
}