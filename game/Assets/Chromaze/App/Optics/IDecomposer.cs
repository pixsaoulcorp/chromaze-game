﻿using System;

namespace Chromaze.App.Optics
{
    [Serializable]
    public class IDecomposerContainer : IUnifiedContainer<IDecomposer> { }

    public interface IDecomposer
    {
        void SetData(Dom.Data.Decomposer decomposer);
    }
}
