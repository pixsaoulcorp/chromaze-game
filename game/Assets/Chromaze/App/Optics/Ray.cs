﻿

using Pixsaoul.Dom.Data.Engine;

namespace Chromaze.App.Optics
{
    public struct Ray
    {
        private Position _source;
        private Position _target;
        private Dom.Data.Color _color;

        public Ray(Dom.Data.Color color, Position source, Position target)
        {
            _source = source;
            _color = color;
            _target = target;
        }

        public Position Source { get => _source; set => _source = value; }
        public Position Target { get => _target; set => _target = value; }
        public Dom.Data.Color Color { get => _color; set => _color = value; }
    }
}
