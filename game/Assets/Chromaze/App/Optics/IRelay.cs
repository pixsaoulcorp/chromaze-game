﻿using System;

namespace Chromaze.App.Optics
{
    [Serializable]
    public class IRelayContainer : IUnifiedContainer<IRelay> { }

    public interface IRelay
    {
        void SetData(Dom.Data.Relay relay);
    }
}
