﻿using Chromaze.Dom.Data;
using System;
using UnityEngine;

namespace Chromaze.App.Optics.Factories
{
    [System.Serializable]
    public class IEmitterFactoryContainer : IUnifiedContainer<IEmitterFactory> { }

    public interface IEmitterFactory
    {
        IEmitter Create(EmitterStatus status, Dom.Data.Color color, Transform targetParent, Guid? sourceId = null);
    }
}
