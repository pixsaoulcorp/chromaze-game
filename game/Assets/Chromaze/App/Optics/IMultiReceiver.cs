﻿using System;
using System.Collections.Generic;

namespace Chromaze.App.Optics
{
    [System.Serializable]
    public class IMultiReceiverContainer : IUnifiedContainer<IMultiReceiver> { }

    public interface IMultiReceiver : IReceiver
    {
        IReadOnlyDictionary<Guid,Ray> ReceivedRays { get; }
    }
}