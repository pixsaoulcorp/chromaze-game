﻿
using UnityEngine;

namespace Chromaze.App.Optics
{
    [System.Serializable]
    public class IMirrorContainer : IUnifiedContainer<IMirror> { }

    public interface IMirror
    {
        void SetData(Dom.Data.Mirror mirror);

    }
}