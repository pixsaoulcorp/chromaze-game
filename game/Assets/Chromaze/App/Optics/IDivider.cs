﻿using System;

namespace Chromaze.App.Optics
{
    [Serializable]
    public class IDividerContainer : IUnifiedContainer<IDivider> { }

    public interface IDivider
    {
        void SetData(Dom.Data.Divider divider);
    }
}
