﻿namespace Chromaze.App.Environment
{

    [System.Serializable]
    public class IDoorContainer : IUnifiedContainer<IDoor> { }
    public interface IDoor
    {
        Dom.Data.Color ColorKey { get; }
        void SetData(Dom.Data.Door door);
    }
}
