﻿
using System;
using Chromaze.Dom.Data;

namespace Chromaze.App.Environment
{

    [System.Serializable]
    public class IColorProviderContainer : IUnifiedContainer<IColorProvider> { }

    public interface IColorProvider
    { 
        IObservable<Color> ObservableColor { get; }
    }

}
