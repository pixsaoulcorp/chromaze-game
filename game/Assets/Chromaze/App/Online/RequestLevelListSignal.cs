﻿using Chromaze.Dom.Data.Payload;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;

namespace Chromaze.App.Online
{
    public class RequestLevelListSignal : Signal<Action<List<LevelForListOfLevel>>>{}
}