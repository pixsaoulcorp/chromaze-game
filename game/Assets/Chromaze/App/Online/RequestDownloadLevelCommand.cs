﻿
using Chromaze.App.Level;
using Chromaze.Dom.Data.Payload;
using Pixsaoul.App.Serialization;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Chromaze.App.Online
{
    public class RequestDownloadLevelCommand : Command
    {
        [Inject] public LevelForListOfLevel LevelInfo { get; set; }

        [Inject] public IOnlineRepository OnlineRepository { get; set; }

        [Inject] public IDownloadRepository DownloadsRepository { get; set; }

        [Inject(SerializationType.Binary)] public ISerializerService _binarySerializer { get; set; }

        public override void Execute()
        {
            OnlineRepository.TryGetLevel(LevelInfo, SaveDownloadedLevel);
            Debug.Log("Reques download level : " + LevelInfo.ToString());
        }

        private void SaveDownloadedLevel(Dom.Data.Level level)
        {
            //TODO TBU this should be done in the repository
            //Inf.Persistency.Level persistLevel = _binarySerializer.Deserialize<Inf.Persistency.Level>(level.Content);
            //Dom.Data.LevelContent level = MapperService.Mapper.Map<Dom.Data.LevelContent>(persistLevel);
            DownloadsRepository.SetLevel(level);
        }
    }
}
