﻿using Chromaze.Dom.Data.Payload;
using Pixsaoul.Fou.Markers;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;

namespace Chromaze.App.Online
{
    public interface IOnlineRepository : IService, IRepository
    {
        void TryGetLevelInfoList(Action<List<LevelForListOfLevel>> callback);

        void TrySetLevel(Inf.Persistency.Payload.LevelForCreation levelForCreation);

        void TryGetLevel(LevelForListOfLevel levelInfo, Action<Dom.Data.Level> callback);
    }
}
