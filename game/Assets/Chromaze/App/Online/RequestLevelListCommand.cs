﻿using Chromaze.Dom.Data.Payload;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chromaze.App.Online
{
    public class RequestLevelListCommand : Command
    {
        [Inject] public IOnlineRepository OnlineRepository { get; set; }
        [Inject] public Action<List<LevelForListOfLevel>> Callback { get; set; }

        public override void Execute()
        {
            OnlineRepository.TryGetLevelInfoList(Callback);
        }
    }
}