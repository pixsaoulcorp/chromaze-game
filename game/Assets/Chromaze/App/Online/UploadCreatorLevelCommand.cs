﻿using Chromaze.App.Level;
using Chromaze.Inf.Persistency.Payload;
using Pixsaoul.App.Serialization;
using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Online
{
    public class UploadCreatorLevelCommand : Command
    {
        [Inject] public Pixsaoul.Dom.Data.Engine.Name Name { get; set; }

        [Inject] public ICreatorRepository CreatorRepo { get; set; }

        [Inject(SerializationType.Binary)] public ISerializerService BinarySerializer { get; set; }

        [Inject] public IOnlineRepository OnlineRepository { get; set; }

        [Inject] public CreateLogSignal logSignal { get; set; }

        public override void Execute()
        {
            Dom.Data.Level initialLevel = CreatorRepo.GetLevel(Name);
            Dom.Data.Level level = new Dom.Data.Level(-1, Name, initialLevel.Content);

            //TODO TBU Create accurate mapping to have a LevelForCreation correct
            LevelForCreation persistPayload = new LevelForCreation(level);

            //Content binaryLevel = new Content(BinarySerializer.Serialize(persistPayload));
            OnlineRepository.TrySetLevel(persistPayload);

            // Update this because it might not be needed. Online repository should take Level as input maybe, or we should create LevelForCreation payload

            logSignal.Dispatch(new Log($"Upload", $"Level {Name.ToString()} has been uploaded"));

        }
    }
}
