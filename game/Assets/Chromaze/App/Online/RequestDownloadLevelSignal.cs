﻿
using Chromaze.Dom.Data.Payload;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Online
{
    public class RequestDownloadLevelSignal : Signal<LevelForListOfLevel>
    {
    }
}
