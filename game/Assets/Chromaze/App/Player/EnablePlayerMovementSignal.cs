﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.Player
{
    public class EnablePlayerMovementSignal : Signal { }
}
