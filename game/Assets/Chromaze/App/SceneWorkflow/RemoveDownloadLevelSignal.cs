﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class RemoveDownloadLevelSignal : Signal<Pixsaoul.Dom.Data.Engine.Name>{}
}
