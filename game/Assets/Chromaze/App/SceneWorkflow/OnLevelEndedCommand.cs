﻿
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class OnLevelEndedCommand : Command
    {
      [Inject] public NotifyLevelEndedSignal NotifyLevelEndedSignal { get; set; }
        /// <summary>
        /// Notify the views about
        /// </summary>
        /// <param name="data"></param>
        public override void Execute()
        {
            NotifyLevelEndedSignal.Dispatch();
        }
    }
}