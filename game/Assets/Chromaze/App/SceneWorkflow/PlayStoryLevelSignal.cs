﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class PlayStoryLevelSignal : Signal<Pixsaoul.Dom.Data.Engine.Name> { }
}
