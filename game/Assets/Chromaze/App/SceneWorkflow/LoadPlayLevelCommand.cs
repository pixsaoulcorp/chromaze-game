﻿using Chromaze.App.Components.Start;
using Chromaze.App.Level.Import;
using Chromaze.Inf.Components.Start;
using Pixsaoul.App.SceneManagement;
using Pixsaoul.App.SceneTransition;
using Pixsaoul.App.UserInput;
using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class LoadPlayLevelCommand : Command
    {
        [Inject] public Dom.Data.Level LevelDom { get; set; }

        //Fade In
        [Inject] public RequestFadeInSignal RequestFadeInSignal { get; set; }
        [Inject] public OnFadedInSignal OnFadedInSignal { get; set; }

        // Change Scene
        [Inject] public RequestChangeSceneSignal RequestChangeSceneSignal { get; set; }
        [Inject] public OnSceneChangedSignal OnSceneChangedSignal { get; set; }

        // Import level
        [Inject] public ILevelImporterService levelImporter { get; set; }
        [Inject] public OnLevelImportedSignal OnLevelImportedSignal { get; set; }

        // Player start view teleportation
        [Inject] public OnStartViewReadyForPlayerSignal OnStartViewReadyForPlayerSignal { get; set; }
        [Inject] public RequestMovePlayerToStartSignal RequestMovePlayerToStartSignal { get; set; }

        // Trigger beginning of level play
        [Inject] public ILevelUserService LevelUserService { get; set; }

        // Fade Out
        [Inject] public RequestFadeOutSignal RequestFadeOutSignal { get; set; }
        [Inject] public OnFadedOutSignal OnFadedOutSignal { get; set; }


        [Inject] public EnableInputSignal enableInputSignal { get; set; }

        private bool _scenechanged = false;
        private bool _startViewReady = false;
        private bool _levelImported = false;
        private bool _fadedIn = false;
        /// <summary>
        /// Execute method to override when creating a command
        /// call base.Execute() after your specific behaviour
        /// </summary>
        public override void Execute()
        {
            OnFadedInSignal.AddOnce(OnOverlayFadedIn);
            RequestFadeInSignal.Dispatch();
        }

        private void OnOverlayFadedIn()
        {
            _fadedIn = true;
            CheckLoadingProcessCompletion();
            OnSceneChangedSignal.AddOnce(OnSceneChanged);
            RequestChangeSceneSignal.Dispatch(SceneId.Play);
        }

        private void OnSceneChanged(SceneId sceneId)
        {
            _scenechanged = true;
            CheckLoadingProcessCompletion();

            if (sceneId == SceneId.Play)
            {                
                OnLevelImportedSignal.AddOnce(OnLevelImported);
                OnStartViewReadyForPlayerSignal.AddOnce(OnStartViewReady); // Scenario where we can here signal from the mediator here.
                levelImporter.ImportLevel(LevelDom);
            }
            else
            {
                throw new System.Exception("Error in scene loading, did you spam the UI ?");
            }
        }

        private void OnLevelImported(Dom.Data.Level level)
        {
            _levelImported = true;
            CheckLoadingProcessCompletion();
        }

        private void OnStartViewReady()
        {
            // We have no idea what comes first : the view initialization, the mediator registering, or this command listening
            // so we'll try to have this triggered regardless the scenario
            _startViewReady = true;
            CheckLoadingProcessCompletion();
        }

        private void OnFadedOut()
        {
            enableInputSignal.Dispatch(true);
        }

        private void CheckLoadingProcessCompletion()
        {
            if (_fadedIn && _scenechanged && _startViewReady && _levelImported)
            {
                LevelUserService.Begin();
                RequestMovePlayerToStartSignal.Dispatch();
                RequestFadeOutSignal.Dispatch();

                enableInputSignal.Dispatch(true); // TODO TBU allow inputs only after fade out
            }
        }
    }
}
