﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class EndLevelCommand : Command
    {
        [Inject]
        public ILevelUserService levelManagerService { get; set; }

        /// <summary>
        /// Execute method to override when creating a command
        /// call base.Execute() after your specific behaviour
        /// </summary>
        public override void Execute()
        {
            levelManagerService.End();
        }
    }
}