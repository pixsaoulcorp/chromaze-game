﻿using Pixsaoul.Fou.Mvcs;
using PDom = Pixsaoul.Dom.Data.Engine;
using Chromaze.App.Level;

namespace Chromaze.App.SceneWorkflow
{
    public class PlayStoryLevelCommand : Command
    {
        [Inject] public IStoryRepository Repository { get; set; }
        [Inject] public PDom.Name LevelName { get; set; }

        [Inject] public LoadPlayLevelSignal LoadPlayLevelSignal { get; set; }

      
        /// <summary>
        /// Execute method to override when creating a command
        /// call base.Execute() after your specific behaviour
        /// </summary>
        public override void Execute()
        {
            Dom.Data.Level level = Repository.GetStoryLevel(LevelName);
            LoadPlayLevelSignal.Dispatch(level);
        }
    }
}
