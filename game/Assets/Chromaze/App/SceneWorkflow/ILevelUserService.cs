﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    /// <summary>
    /// For any access to the level model during the playing of it. 
    /// include starting the level, finishing it, stuff like that.
    /// Most of the other stuff should happen between views during this.
    /// </summary>
    public interface ILevelUserService : IService
    {
        void Begin();
        void End();
        void Cancel();
    }
}