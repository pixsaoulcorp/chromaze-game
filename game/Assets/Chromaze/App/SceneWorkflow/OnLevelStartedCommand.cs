﻿using Pixsaoul.Fou.Mvcs;
using Chromaze.App.Player;

namespace Chromaze.App.SceneWorkflow
{
    /// <summary>
    /// On level started, transmit to the view
    /// </summary>
    public class OnLevelStartedCommand : Command
    {      
        [Inject] public EnablePlayerMovementSignal AllowPlayerMovementSignal { get; set; }

        /// <summary>
        /// Notify the views about
        /// </summary>
        /// <param name="data"></param>
        public override void Execute()
        {
            // Once the level has been started, the user can start moving
            AllowPlayerMovementSignal.Dispatch();
        }
    }
}