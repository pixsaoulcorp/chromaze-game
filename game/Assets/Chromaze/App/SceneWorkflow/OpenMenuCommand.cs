﻿using Pixsaoul.App.SceneManagement;
using Pixsaoul.App.UserInput;
using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class OpenMenuCommand : Command
    {
        [Inject] public RequestChangeSceneSignal RequestChangeSceneSignal { get; set; }
        [Inject] public EnableInputSignal enableInputSignal { get; set; }

        public override void Execute()
        {
            RequestChangeSceneSignal.Dispatch(SceneId.Menu);
            enableInputSignal.Dispatch(false);
        }
    }
}
