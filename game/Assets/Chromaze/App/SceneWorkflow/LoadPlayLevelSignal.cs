﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class LoadPlayLevelSignal: Signal<Dom.Data.Level> { }
}
