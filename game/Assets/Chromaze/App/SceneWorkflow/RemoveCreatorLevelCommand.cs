﻿using Chromaze.App.Level;
using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class RemoveCreatorLevelCommand : Command
    {
        [Inject] public ICreatorRepository Repository { get; set; }

        [Inject] public Pixsaoul.Dom.Data.Engine.Name Name { get; set; }
        [Inject] public CreateLogSignal logSignal { get; set; }


        public override void Execute()
        {
            Repository.RemoveLevel(Name);
            logSignal.Dispatch(new Log($"Removed", $"Level {Name.ToString()} has been removed"));
        }
    }
}
