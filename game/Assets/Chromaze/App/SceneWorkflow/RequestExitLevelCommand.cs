﻿using Pixsaoul.App.SceneManagement;
using Pixsaoul.App.SceneTransition;
using Pixsaoul.App.UserInput;
using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class RequestExitLevelCommand : Command
    {
        //Fade In
        [Inject] public RequestFadeInSignal RequestFadeInSignal { get; set; }
        [Inject] public OnFadedInSignal OnFadedInSignal { get; set; }

        // Fade Out
        [Inject] public RequestFadeOutSignal RequestFadeOutSignal { get; set; }
        [Inject] public OnFadedOutSignal OnFadedOutSignal { get; set; }

        // Scene changed
        [Inject] public RequestChangeSceneSignal RequestChangeSceneSignal { get; set; }
        [Inject] public OnSceneChangedSignal OnSceneChangedSignal { get; set; }


        [Inject] public EnableInputSignal enableInputSignal { get; set; }


        public override void Execute()
        {
            OnFadedInSignal.AddOnce(OnOverlayFadedIn);
            RequestFadeInSignal.Dispatch();
        }

        private void OnOverlayFadedIn()
        {
            OnSceneChangedSignal.AddOnce(OnSceneChanged);
            RequestChangeSceneSignal.Dispatch(SceneId.Menu);
            enableInputSignal.Dispatch(false);
        }

        private void OnSceneChanged(SceneId sceneId)
        {
            if (sceneId == SceneId.Menu)
            {
                OnFadedOutSignal.AddOnce(OnFadedOut);
                RequestFadeOutSignal.Dispatch();
            }
            else
            {
                throw new System.Exception("Error in scene loading, did you spam the UI ?");
            }
        }

        private void OnFadedOut()
        {

        }
    }
}
