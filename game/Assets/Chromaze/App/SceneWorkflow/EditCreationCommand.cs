﻿using Chromaze.App.Level;
using Chromaze.App.Level.Import;
using Chromaze.Dom.Data;
using Pixsaoul.App.SceneManagement;
using Pixsaoul.App.SceneTransition;
using Pixsaoul.App.UserInput;
using Pixsaoul.Dom.Data;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Inf.BoxelGeneration;
using PDom = Pixsaoul.Dom.Data.Engine;

namespace Chromaze.App.SceneWorkflow
{
    public class EditCreationCommand : Command
    {
        [Inject] public ICreatorRepository CreatorRepository { get; set; }

        //Fade In
        [Inject] public RequestFadeInSignal RequestFadeInSignal { get; set; }
        [Inject] public OnFadedInSignal OnFadedInSignal { get; set; }

        // Scene change
        [Inject] public RequestChangeSceneSignal RequestChangeSceneSignal { get; set; }
        [Inject] public OnSceneChangedSignal OnSceneChangedSignal { get; set; }

        // Fade Out
        [Inject] public RequestFadeOutSignal RequestFadeOutSignal { get; set; }
        [Inject] public OnFadedOutSignal OnFadedOutSignal { get; set; }

        // Level import
        [Inject] public ILevelImporterService levelImporter { get; set; }
        [Inject] public OnLevelImportedSignal OnLevelImportedSignal { get; set; }


        [Inject] public PDom.Name LevelName { get; set; }
        [Inject] public EnableInputSignal enableInputSignal { get; set; }

        /// <summary>
        /// Execute method to override when creating a command
        /// call base.Execute() after your specific behaviour
        /// </summary>
        public override void Execute()
        {
            OnFadedInSignal.AddOnce(OnOverlayFadedIn);
            RequestFadeInSignal.Dispatch();

        }

        private void OnOverlayFadedIn()
        {
            OnSceneChangedSignal.AddOnce(OnSceneChanged);
            RequestChangeSceneSignal.Dispatch(SceneId.Create);           
        }

        private void OnSceneChanged(SceneId sceneId)
        {           
            if (sceneId == SceneId.Create)
            {
                Dom.Data.Level level;
                if (!LevelName.IsEmpty())
                {
                    level = CreatorRepository.GetLevel(LevelName);
                }
                else
                {
                    level = GetDefaultLevel();
                }

                OnLevelImportedSignal.AddOnce(OnLevelImported);
                levelImporter.ImportLevel(level);
            }
            else
            {
                throw new System.Exception("Error in scene loading, did you spam the UI ?");
            }
        }

        private void OnLevelImported(Dom.Data.Level level)
        {
            OnFadedOutSignal.AddOnce(OnFadedOut);
            RequestFadeOutSignal.Dispatch();
        }

        private void OnFadedOut()
        {
            enableInputSignal.Dispatch(true);
        }

        private Dom.Data.Level GetDefaultLevel()
        {
            Dom.Data.Boundaries boundaries = new Dom.Data.Boundaries(
                    ComponentId.Create(),
                    new PDom.SnappedPosition(),
                    new PDom.SnappedRotation(),
                    BoxelShapeExtensions.CreateSimpleShape(5, 5, 5, Pixsaoul.Dom.Shape.SurfaceMode.Intern),
                    Color.WhiteC);
            Dom.Data.Start start = new Dom.Data.Start(ComponentId.Create(), new PDom.SnappedPosition((PDom.SnappedOffset)2, (PDom.SnappedOffset)0, (PDom.SnappedOffset)0), new PDom.SnappedRotation());
            Dom.Data.End end = new Dom.Data.End(ComponentId.Create(), new PDom.SnappedPosition((PDom.SnappedOffset)2, (PDom.SnappedOffset)0, (PDom.SnappedOffset)4), new PDom.SnappedRotation());

            var level = new Dom.Data.Level(
                -1,
                (PDom.Name)"newlevel",
                new LevelContent(
                    boundaries,
                    start,
                    end,
                    new System.Collections.Generic.List<Dom.Data.Component>())
                );

            return level;
        }
    }
}
