﻿using Chromaze.App.Level;
using Pixsaoul.Fou.Mvcs;
using PDom = Pixsaoul.Dom.Data.Engine;


namespace Chromaze.App.SceneWorkflow
{
    public class PlayCreatorLevelCommand : Command
    {
        [Inject] public PDom.Name LevelName { get; set; }

        [Inject] public ICreatorRepository Repository { get; set; }

        [Inject] public LoadPlayLevelSignal LoadPlayLevelSignal { get; set; }

        /// <summary>
        /// Execute method to override when creating a command
        /// call base.Execute() after your specific behaviour
        /// </summary>
        public override void Execute()
        {
            Dom.Data.Level level = Repository.GetLevel(LevelName);
            LoadPlayLevelSignal.Dispatch(level);
        }
    }
}
