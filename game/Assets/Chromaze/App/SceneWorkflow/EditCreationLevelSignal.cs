﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class EditCreationLevelSignal : Signal<Pixsaoul.Dom.Data.Engine.Name> { }
}
