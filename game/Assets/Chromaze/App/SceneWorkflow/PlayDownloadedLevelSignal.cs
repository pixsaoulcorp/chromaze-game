﻿using Pixsaoul.Fou.Mvcs;

namespace Chromaze.App.SceneWorkflow
{
    public class PlayDownloadedLevelSignal : Signal<Pixsaoul.Dom.Data.Engine.Name> { }
}
