﻿
namespace Chromaze.App
{
    [System.Serializable]
    public class IEnablableContainer : IUnifiedContainer<IEnablable> { }

    public interface IEnablable
    {
        void Enable();
        void Disable();
    }
}