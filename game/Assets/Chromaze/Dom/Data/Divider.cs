﻿using Pixsaoul.Dom.Data.Engine;
using System;
using UnityEngine;

namespace Chromaze.Dom.Data
{
    public class Divider : Component, IEquatable<Divider>
    {
        public Orientation EmissionOrientation { get; protected set; }

        public Divider() : base()
        {
            EmissionOrientation = new Orientation();
        }

        public Divider(ComponentId id, SnappedPosition position, SnappedRotation rotation, Orientation emissionOrientation) : base(id, position, rotation)
        {
            EmissionOrientation = emissionOrientation;
        }

        public bool Equals(Divider other)
        {
            return base.Equals(other) && EmissionOrientation.Equals(other.EmissionOrientation);
        }
    }
}