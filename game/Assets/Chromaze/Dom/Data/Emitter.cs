﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Emitter : Component, IEquatable<Emitter>
    {
        public EmitterStatus Status { get; protected set; }
        public Color Color { get; protected set; }
        public Orientation EmissionOrientation { get; protected set; }

        public Emitter() : base()
        {
            EmissionOrientation = new Orientation();
        }

        public Emitter(ComponentId id, SnappedPosition position, SnappedRotation rotation, EmitterStatus status, Color color, Orientation emissionOrientation) : base(id, position, rotation)
        {
            Status = status;
            Color = color;
            EmissionOrientation = emissionOrientation;
        }

        public bool Equals(Emitter other)
        {
            return base.Equals(other) &&
                Status.Equals(other.Status) &&
                Color.Equals(other.Color) &&
                EmissionOrientation.Equals(other.EmissionOrientation);
        }
    }
}
