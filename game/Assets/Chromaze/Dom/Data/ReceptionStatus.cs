﻿namespace Chromaze.Dom.Data
{
    public enum ReceptionStatus
    {
        NotReceiving,
        Receiving
    }
}
