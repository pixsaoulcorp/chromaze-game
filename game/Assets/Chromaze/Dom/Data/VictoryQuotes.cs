﻿using EnsureThat;
using Pixsaoul.Dom;
using System.Collections.Generic;

namespace Chromaze.Dom.Data
{
    public class VictoryQuotes
    {
        private List<Text> _quotes;

        public VictoryQuotes(List<Text> quotes)
        {
            Ensure.That(quotes).IsNotNull();
            _quotes = quotes;
        }

        public IList<Text> Quotes { get => _quotes; }

        public void Add(Text text)
        {
            _quotes.Add(text);
        }

        public void Combine(VictoryQuotes quotes)
        {
            IList<Text> texts = quotes.Quotes;
            foreach(Text t in texts)
            {
                Add(t);
            }
        }

        public Text GetRandom()
        {
            int maxSpot = _quotes.Count-1;
            int randomSpot = UnityEngine.Random.Range(0, maxSpot);
            return _quotes[randomSpot];
        }
    }
}
