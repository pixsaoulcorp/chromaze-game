﻿
using System;
using System.Collections.Generic;

namespace Chromaze.Dom.Data
{
    public struct Color : IEquatable<Color>
    {
        public bool Red;
        public bool Green;
        public bool Blue;

        public Color(bool r, bool g, bool b)
        {
            Red = r;
            Green = g;
            Blue = b;
        }

        // all usable setup
        public static readonly Color BlackC = new Color(false, false, false);
        public static readonly Color RedC = new Color(true, false, false);
        public static readonly Color GreenC = new Color(false, true, false);
        public static readonly Color BlueC = new Color(false, false, true);
        public static readonly Color YellowC = new Color(true, true, false);
        public static readonly Color MagentaC = new Color(true, false, true);
        public static readonly Color CyanC = new Color(false, true, true);
        public static readonly Color WhiteC = new Color(true, true, true);

        public bool Equals(Color other)
        {
            return Red == other.Red &&
                   Green == other.Green &&
                   Blue == other.Blue;
        }

        public UnityEngine.Color ToColor(float alpha = 1)
        {
            return new UnityEngine.Color(Red ? 1 : 0, Green ? 1 : 0, Blue ? 1 : 0, alpha);
        }

        public static Color Add(Color a, Color b)
        {
            return new Color(a.Red || b.Red, a.Green || b.Green, a.Blue || b.Blue);
        }

        public static Color Substract(Color a, Color b)
        {
            return new Color(a.Red && !b.Red, a.Green && !b.Green, a.Blue && !b.Blue);
        }

        public static Color Invert(Color a)
        {
            return new Color(!a.Red, !a.Green, !a.Blue);
        }

        private static Dictionary<Color, string> _toStringMap = new Dictionary<Color, string>()
        {
            {Color.BlackC, "Black" },
            {Color.RedC, "Red" },
            {Color.GreenC, "Green" },
            {Color.BlueC, "Blue" },
            {Color.YellowC, "Yellow" },
            {Color.MagentaC, "Magenta" },
            {Color.CyanC, "Cyan" },
            {Color.WhiteC, "White" }
        };

        public override string ToString()
        {
            string res;
            _toStringMap.TryGetValue(this, out res);
            return res;
        }
    }    
}
