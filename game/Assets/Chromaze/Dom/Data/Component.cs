﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public abstract class Component : IEquatable<Component>
    {
        public ComponentId Id { get; set; }
        public SnappedPosition Position { get; set; }
        public SnappedRotation Rotation { get; set; }

        public Component(ComponentId id, SnappedPosition position, SnappedRotation rotation)
        {
            Id = id;
            Position = position;
            Rotation = rotation;
        }

        public Component()
        {
            Id = ComponentId.Create();
            Position = new SnappedPosition();
            Rotation = new SnappedRotation();
        }

        public bool Equals(Component other)
        {
            return Id.Equals(other.Id) &&
                Position.Equals(other.Position) &&
                Rotation.Equals(other.Rotation);
        }
    }
}