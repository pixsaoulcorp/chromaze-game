﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Chromaze.Dom.Data
{
    public class LevelContent : IEquatable<LevelContent>
    {
        public Boundaries Boundaries { get; set; }
        public Start Start { get; set; }
        public End End { get; set; }
        public List<Component> Components { get; set; }

        public LevelContent()
        {
            //Do not use, made for Automapper purposes
            Components = new List<Component>();
        }

        public LevelContent(Boundaries boundaries, Start start, End end, List<Component> cpt)
        {
            Boundaries = boundaries;
            Start = start;
            End = end;
            Components = cpt;
        }

        public bool Equals(LevelContent other)
        {
            bool sameComponent = (Components.Count == other.Components.Count);
            for(int i = 0; i < Components.Count; i++)
            {
                bool contains = other.Components.Where(cpt=> cpt.Equals(Components[i])).Any();
                sameComponent &= contains;
                if (!sameComponent)
                    return false;
            }
            return
                Boundaries.Equals(other.Boundaries) &&
                Start.Equals(other.Start) &&
                End.Equals(other.End) &&
                sameComponent; //TODO check equality of all components
        }
    }
}