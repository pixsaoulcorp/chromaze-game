﻿using Pixsaoul.Dom;
using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Billboard : Component, IEquatable<Billboard>
    {
        /// <summary>
        /// Billboard structure Supports Multi OS, see BillboardView
        /// </summary>
        public Text Text { get; protected set; }
        public Billboard() : base()
        {
            Text = new Text("");
        }
        public Billboard(ComponentId id, SnappedPosition position, SnappedRotation rotation, Text text) : base(id, position, rotation)
        {
            Text = text;
        }

        public bool Equals(Billboard other)
        {
            return base.Equals(other) && Text.Equals(other.Text);
        }
    }
}
