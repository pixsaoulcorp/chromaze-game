﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Cube : Component, IEquatable<Cube>
    {
        public Color Color { get; protected set; }

        public Cube() : base()
        {
            Color = Color.WhiteC;
        }

        public Cube(ComponentId id, SnappedPosition position, SnappedRotation rotation, Color color) : base(id, position, rotation) 
        {
            Color = color;
        }

        public bool Equals(Cube other)
        {
            return Color.Equals(other.Color) && base.Equals(other);
        }
    }
}