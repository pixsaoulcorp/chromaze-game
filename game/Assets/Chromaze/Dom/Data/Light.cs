﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Light : Component, IEquatable<Light>
    {

        public Color Color { get; protected set; }
        public Intensity Intensity { get; protected set; }

        public Light() : base()
        {
            Color = Color.WhiteC;
            Intensity = 1f;
        }

        public Light(ComponentId id, SnappedPosition position, SnappedRotation rotation, Color color, Intensity intensity) : base(id, position, rotation)
        {
            Color = color;
            Intensity = intensity;
        }

        public bool Equals(Light other)
        {
            return Color.Equals(other.Color) && Intensity.Equals(other.Intensity) && base.Equals(other);
        }
    }
}