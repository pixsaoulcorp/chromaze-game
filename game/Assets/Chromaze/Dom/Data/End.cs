﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class End : Component, IEquatable<End>
    {
        public End() : base()
        {

        }

        public End(ComponentId id, SnappedPosition position, SnappedRotation rotation) : base(id, position, rotation) { }

        public bool Equals(End other)
        {
            return base.Equals(other);
        }
    }
}