﻿using Pixsaoul.Dom.Data.Engine;
using System;
using UnityEngine;

namespace Chromaze.Dom.Data
{
    public class Door : Component, IEquatable<Door>
    {
        private Color _acceptedColor;
        public Door() : base()
        {

        }
        public Door(ComponentId id, SnappedPosition position, SnappedRotation rotation, Color color) : base(id, position, rotation)
        {
            _acceptedColor = color;
        }

        public Color Color { get => _acceptedColor; set => _acceptedColor = value; }

        public bool Equals(Door other)
        {
            return base.Equals(other) && Color.Equals(other.Color);
        }


    }
}
