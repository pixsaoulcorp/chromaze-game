﻿using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Dom.Shape;
using System;

namespace Chromaze.Dom.Data
{
    public class Boundaries : Component , IEquatable<Boundaries>
    {
        private BoxelShape _shape;
        public Color Color { get; protected set; }


        public Boundaries() : base()
        {
            _shape = new BoxelShape(); //TODO handle boxel shape creation
            Color = Color.WhiteC;

        }

        public Boundaries(ComponentId id, SnappedPosition position, SnappedRotation rotation, BoxelShape shape, Color color) : base(id, position, rotation)
        {
            _shape = shape;
            Color = color;
        }

        public BoxelShape Shape { get => _shape; set => _shape = value; }

        public bool Equals(Boundaries other)
        {
            return base.Equals(other) && Color.Equals(other.Color) && Shape.Equals(other.Shape);
        }
    }
}