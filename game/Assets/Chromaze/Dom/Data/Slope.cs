﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Slope : Component, IEquatable<Slope>
    {
        public Color Color { get; protected set; }

        public Slope() : base()
        {
            Color = Color.WhiteC;
        }
        public Slope(ComponentId id, SnappedPosition position, SnappedRotation rotation, Color color) : base(id, position, rotation)
        {
            Color = color;
        }

        public bool Equals(Slope other)
        {
            return Color.Equals(other.Color) && base.Equals(other);
        }
    }
}