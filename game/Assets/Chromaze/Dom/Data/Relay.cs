﻿using Pixsaoul.Dom.Data.Engine;
using System;
using UnityEngine;

namespace Chromaze.Dom.Data
{

    public class Relay : Component, IEquatable<Relay>
    {
        public Orientation EmissionOrientation { get; protected set; }

        public Relay() : base()
        {
            EmissionOrientation = new Orientation();
        }

        public Relay(ComponentId id, SnappedPosition position, SnappedRotation rotation, Orientation emissionOrientation) : base(id, position, rotation)
        {
            EmissionOrientation = emissionOrientation;
        }

        public bool Equals(Relay other)
        {
            return base.Equals(other) && EmissionOrientation.Equals(other.EmissionOrientation);
        }
    }
}