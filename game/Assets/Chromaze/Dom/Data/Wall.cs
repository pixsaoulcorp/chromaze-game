﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    [Serializable]
    public class Wall : Component, IEquatable<Wall>
    {
        public SnappedDimension Dimension { get; set; }
        public Color Color { get; protected set; }


        public Wall() : base() 
        {
            Dimension = new SnappedDimension((SnappedLength)1, (SnappedLength)1, (SnappedLength)1);
            Color = Color.WhiteC;
        }

        public Wall(ComponentId id, SnappedPosition position, SnappedRotation rotation, SnappedDimension dimensions, Color color) : base(id, position, rotation)
        {
            Dimension = dimensions;
            Color = color;
        }


        public bool Equals(Wall other)
        {
            return base.Equals(other) && Color.Equals(other.Color) && Dimension.Equals(other.Dimension);
        }
    }
}