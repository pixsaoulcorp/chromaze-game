
using Pixsaoul.Fou.Markers;
using System;

namespace Chromaze.Dom.Data
{
    public struct ComponentId: IValueObject, IEquatable<ComponentId>
    {
        #region DATA

        public Guid Id { get; set; }

        #endregion

        #region CTORS

        public ComponentId(Guid value)
        {
            Id = value;
        }

        public static ComponentId Create()
        {
            return new ComponentId(Guid.NewGuid());
        }

        #endregion


        public bool SameValueAs(ComponentId other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator Guid(ComponentId type)
        {
            return type.Id;
        }

        public static implicit operator ComponentId(Guid value)
        {
            return new ComponentId(value);
        }

        public static bool operator ==(ComponentId left, ComponentId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ComponentId left, ComponentId right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(ComponentId other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is ComponentId other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        #endregion

        public override string ToString()
        {
            return Id.ToString();
        }
    }
}

