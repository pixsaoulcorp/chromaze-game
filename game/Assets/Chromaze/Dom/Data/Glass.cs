using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Glass : Component, IEquatable<Glass>
    {
        public Glass(): base()
        {

        }

        public Glass(ComponentId id, SnappedPosition position, SnappedRotation rotation) : base(id, position, rotation)
        {
           
        }

        public bool Equals(Glass other)
        {
            return base.Equals(other);
        }
    }
}