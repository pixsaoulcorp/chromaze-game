﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Combiner : Component, IEquatable<Combiner>
    {
        public Orientation EmissionOrientation { get; protected set; }
        public Combiner() : base()
        {
            EmissionOrientation = new Orientation();
        }
        public Combiner(ComponentId id, SnappedPosition position, SnappedRotation rotation, Orientation emissionOrientation) : base(id, position, rotation)
        {
            EmissionOrientation = emissionOrientation;
        }

        public bool Equals(Combiner other)
        {
            return base.Equals(other) && EmissionOrientation.Equals(other.EmissionOrientation);
        }
    }
}
