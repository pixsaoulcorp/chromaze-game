﻿using Pixsaoul.Dom.Data.Engine;
using System;
using UnityEngine;

namespace Chromaze.Dom.Data
{
    public class Mirror : Component, IEquatable<Mirror>
    {
        public Orientation EmissionOrientation { get; protected set; }

        public Mirror() : base()
        {
            EmissionOrientation = new Orientation();

        }
        public Mirror(ComponentId id, SnappedPosition position, SnappedRotation rotation, Orientation emissionOrientation) : base(id, position, rotation)
        {
            EmissionOrientation = emissionOrientation;
        }

        public bool Equals(Mirror other)
        {
            return base.Equals(other) && EmissionOrientation.Equals(other.EmissionOrientation);
        }
    }
}
