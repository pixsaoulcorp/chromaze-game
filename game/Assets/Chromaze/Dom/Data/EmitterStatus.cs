﻿namespace Chromaze.Dom.Data
{
    public enum EmitterStatus
    {
        Enabled,
        Disabled
    }
}
