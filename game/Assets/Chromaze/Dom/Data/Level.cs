﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Level : IEquatable<Level>
    {
        public Id Id { get; set; }
        public Pixsaoul.Dom.Data.Engine.Name Name { get; set; }
        public LevelContent Content { get; set; }

        public Level()
        {
            //Do not use, made for Automapper purposes
        }

        public Level(Id id, Pixsaoul.Dom.Data.Engine.Name name, LevelContent content)
        {
            Id = id;
            Name = name;
            Content = content;
        }

        public bool Equals(Level other)
        {
            return Id.Equals(other.Id) &&
                Name.Equals(other.Name) &&
                Content.Equals(other.Content);
        }
    }
}
