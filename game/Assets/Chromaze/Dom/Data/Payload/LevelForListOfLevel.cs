﻿

using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data.Payload
{
    // Payload to get list of levels without their data
    public class LevelForListOfLevel : IEquatable<LevelForListOfLevel>
    {
        public Id Id { get; private set; }
        public Pixsaoul.Dom.Data.Engine.Name Name { get; private set; }
        
        public LevelForListOfLevel(Id id, Pixsaoul.Dom.Data.Engine.Name name)
        {
            Id = id;
            Name = name;
        }

        public bool Equals(LevelForListOfLevel other)
        {
            return Id.Equals(other.Id) && Name.Equals(other.Name);
        }
    }
}
