﻿using Pixsaoul.Dom.Data.Engine;
using System;

namespace Chromaze.Dom.Data
{
    public class Start : Component, IEquatable<Start>
    {
        public Start() : base()
        {

        }
        public Start(ComponentId id, SnappedPosition position, SnappedRotation rotation) : base(id, position, rotation) { }

        public bool Equals(Start other)
        {
            return base.Equals(other);
        }
    }
}