﻿using Pixsaoul.Dom.Data.Engine;
using System;
using UnityEngine;

namespace Chromaze.Dom.Data
{

    public class Decomposer : Component, IEquatable<Decomposer>
    {
        public Orientation EmissionOrientation { get; protected set; }

        public Decomposer() : base()
        {
            EmissionOrientation = new Orientation();
        }
        public Decomposer(ComponentId id, SnappedPosition position, SnappedRotation rotation, Orientation emissionOrientation) : base(id, position, rotation)
        {
            EmissionOrientation = emissionOrientation;
        }

        public bool Equals(Decomposer other)
        {
            return base.Equals(other) && EmissionOrientation.Equals(other.EmissionOrientation);
        }
    }
}