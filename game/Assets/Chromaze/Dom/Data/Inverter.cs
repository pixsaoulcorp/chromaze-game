﻿using Pixsaoul.Dom.Data.Engine;
using System;
using UnityEngine;

namespace Chromaze.Dom.Data
{

    public class Inverter : Component, IEquatable<Inverter>
    {
        public Orientation EmissionOrientation { get; protected set; }

        public Inverter() : base()
        {
            EmissionOrientation = new Orientation();

        }
        public Inverter(ComponentId id, SnappedPosition position, SnappedRotation rotation, Orientation emissionOrientation) : base(id, position, rotation)
        {
            EmissionOrientation = emissionOrientation;
        }

        public bool Equals(Inverter other)
        {
            return base.Equals(other) && EmissionOrientation.Equals(other.EmissionOrientation);
        }
    }
}