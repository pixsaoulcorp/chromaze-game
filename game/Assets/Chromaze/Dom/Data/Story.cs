﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomEngine = Pixsaoul.Dom.Data.Engine;

namespace Chromaze.Dom.Data
{
    public class Story : IEquatable<Story>
    {
        public List<DomEngine.Name> LevelsName { get; private set; }

        public Story(List<DomEngine.Name> levelsName)
        {
            LevelsName = levelsName;
        }

        public bool Equals(Story other)
        {
            return LevelsName.SequenceEqual(other.LevelsName);
        }
    }
}

