How to use

To test, just play in unity with only Main scene opened.


# Scene management

Each scene has only one child object with is a prefab of the same name, to ease manipulation of it, never use the scene, but the prefab, this avoid loading and unloading everything
## Main
    Contain all the MVCS context, all services and model, this scene is never unloaded.

## Play
    Contains a level loader, a player and all the process to play the game.

## Menu
    Responsible for all menu prior to creation or play.
## Creation
    Level editor part.

# Prefab management
    
# Component