﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pixsaoul.Ci
{

    public class Builder
    {

        #region public editor methods
        public const string continuousIntegrationFolder = "Continuous Integration/";


        public static void BuildAll()
        {
            BuildAndroid();
            BuildWindows();
        }

        public static void BuildAndroid()
        {
            string[] args = GetExecutionMethodParameters(2); // first version, second publishing password
            BuildPlayerOptions options = GetCommonOptions(BuildTarget.Android, args[0], "Chromaze.apk");
            SetupPublishing(args[1]);
            BuildPipeline.BuildPlayer(options);
        }

        public static void BuildWindows()
        {
            string[] version = GetExecutionMethodParameters(1);            
            BuildPlayerOptions options = GetCommonOptions(BuildTarget.StandaloneWindows, version[0], "Chromaze.exe");
            BuildPipeline.BuildPlayer(options);
        }      

        #endregion

        private static void SetupPublishing(string password)
        {
            //Should already be setup from project
            //PlayerSettings.Android.keystoreName = keystoreName;
            //PlayerSettings.Android.keyaliasName = keyAliasName;
            PlayerSettings.Android.keystorePass = password;
            PlayerSettings.Android.keyaliasPass = password;
        }

        private static BuildPlayerOptions GetCommonOptions(BuildTarget target, string version, string installName)
        {
            BuildPlayerOptions commonOptions = new BuildPlayerOptions();
            commonOptions.target = target;
            commonOptions.locationPathName = $"bin/{target.ToString()}/{installName}";
            commonOptions.scenes = GetProjectScenes();
            commonOptions.options = BuildOptions.None;
            SetVersion(version);
            return commonOptions;
        }

        private static string[] GetProjectScenes()
        {
            EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;

            string[] stringScenes = new string[scenes.Length];
            for (int i = 0; i < scenes.Length; i++)
            {
                stringScenes[i] = scenes[i].path;
            }
            return stringScenes;
        }

        static string GetProjectName()
        {
            string[] s = Application.dataPath.Split('/');
            return s[s.Length - 2];
        }

        static string GetProjectFolderPath()
        {
            var s = Application.dataPath;
            s = s.Substring(s.Length - 7, 7); // remove "Assets/"
            return s;
        }

        // copies over files from somewhere in my project folder to my standalone build's path
        // do not put a "/" at beginning of assetsFolderName
        static void CopyFromProjectAssets(string fullDataPath, string assetsFolderPath, bool deleteMetaFiles = true)
        {
            Debug.Log("CopyFromProjectAssets: copying over " + assetsFolderPath);
            FileUtil.ReplaceDirectory(Application.dataPath + "/" + assetsFolderPath, fullDataPath + assetsFolderPath); // copy over languages

            // delete all meta files
            if (deleteMetaFiles)
            {
                var metaFiles = Directory.GetFiles(fullDataPath + assetsFolderPath, "*.meta", SearchOption.AllDirectories);
                foreach (var meta in metaFiles)
                {
                    FileUtil.DeleteFileOrDirectory(meta);
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="expectedArgumentCount"></param>
        /// <returns></returns>
        public static string[] GetExecutionMethodParameters(int expectedArgumentCount)
        {
            string[] args = System.Environment.GetCommandLineArgs();
            return GetExecutionMethodParameters(args, expectedArgumentCount);
        }

        public static string[] GetExecutionMethodParameters(string[] arguments, int expectedParametersCount)
        {
            int executeMethodLocation = -1;
            for (int i = 0; i < arguments.Length; i++) // find the -executeMethod argument location in parameters
            {
                if (arguments[i] == "-executeMethod")
                {
                    executeMethodLocation = i;
                }
            }
            // If we didn't find any -executeMethod, there can be no parameter, and this method should not be called
            if (executeMethodLocation < 0)
            {
                throw new System.Exception("No -executeMethod found on command line, could not gather parameters");
            }

            int foundOptionCount = 0;
            bool keepSearching = true;
            while (keepSearching)
            {
                if (!((executeMethodLocation + foundOptionCount + 1) >= arguments.Length))
                {
                    if (arguments[executeMethodLocation + foundOptionCount + 1].StartsWith("-"))
                    {
                        keepSearching = false;
                    }
                    else
                    {
                        foundOptionCount++;
                    }
                }
                else
                {
                    keepSearching = false;
                }
            }

            if (foundOptionCount - 1 != expectedParametersCount)
            {
                throw new Exception($"Argument count does not match expected {expectedParametersCount} arguments");
            }
            else
            {
                string[] output = new string[expectedParametersCount];
                Array.Copy(arguments, executeMethodLocation + 2, output, 0, expectedParametersCount);
                return output;
            }
        }

        #region editor test
        [MenuItem(continuousIntegrationFolder + "Test Build Windows")]
        public static void TestBuildWindows()
        {
            Path.GetDirectoryName(".");
            BuildPlayerOptions options = GetCommonOptions(BuildTarget.StandaloneWindows, "5.55.55555", "Chromaze.exe");

            BuildPipeline.BuildPlayer(options);
        }

        [MenuItem(continuousIntegrationFolder + "Test Build Android")]
        public static void TestBuildAndroid()
        {
            Path.GetDirectoryName(".");
            BuildPlayerOptions options = GetCommonOptions(BuildTarget.Android, "5.55.55555", "Chromaze.apk");
            BuildPipeline.BuildPlayer(options);
        }
        #endregion

        public static void SetVersion(string version)
        {
            PlayerSettings.bundleVersion = version;
        }
       
    }
}
