﻿using Assets.Pixsaoul.App.Engine;
using Chromaze.App.Control;
using Pixsaoul.App.Engine;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.Inf.Control
{
    public class RotaterView : View, IController, IOrientable
    {
        [SerializeField] private IAttachTargetContainer _viewPoint;
        [SerializeField] private Transform _yawRoot;
        [SerializeField] private Transform _pitchRoot;

        [SerializeField] private float _sensitivity;// = 50f;

        private float _pitchLimit = 10f; // limit angle up and down for the pitch. to avoid neck breaking ;)

        public Signal EnableControlSignal = new Signal();
        public Signal DisableControlSignal = new Signal();

        public IAttachTarget AttachTarget
        {
            get => _viewPoint.Result;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Rotate(Orientation orientation)
        {

            //_yawRoot.Rotate(Vector3.up, orientation.yaw, Space.Self); // Left Right axis
            //_pitchRoot.Rotate(-Vector3.right, orientation.pitch, Space.Self); // Up down axis
            // Cannot use Rotate around a specific axis because sometime it applies small changes to other axis
            // Which induce when forcing limits=> snapping of the roll value, which prevent any proper clamp
            // by setting manually the local euler value, we ensure that we are on pure one axis rotation for each node.
            _yawRoot.localEulerAngles = new Vector3(0, _yawRoot.localEulerAngles.y +  orientation.yaw * _sensitivity, 0);
            _pitchRoot.localEulerAngles = new Vector3(_pitchRoot.localEulerAngles.x -  orientation.pitch * _sensitivity, 0 , 0);

            ClampPitch();
        }

        private void ClampPitch()
        { //TODO TBU find a more elegant way to clamp rotation
            Vector3 localEulerAngle = _pitchRoot.transform.localEulerAngles;
            float angle = localEulerAngle.x % 360;

            _pitchRoot.transform.localEulerAngles = new Vector3(angle, localEulerAngle.y, localEulerAngle.z);
            if (angle <=180 && angle >= 90 - _pitchLimit)
            {
                _pitchRoot.transform.localEulerAngles = new Vector3(90 - _pitchLimit, localEulerAngle.y, localEulerAngle.z);
            }
            else if (angle >= 180 && angle <= 270 + _pitchLimit) // 270 == 360 - 90
            {
                _pitchRoot.transform.localEulerAngles = new Vector3(270 + _pitchLimit, localEulerAngle.y, localEulerAngle.z);
            }
        }

        public void Enable()
        {
            EnableControlSignal.Dispatch();
        }

        public void Disable()
        {
            DisableControlSignal.Dispatch();
        }

        // This set and get Should be the only way to change this to ensure there is no roll.
        public Orientation GetDirection()
        {//TODO TBU test if there is no roll implied in this split.
            return new Orientation((Angle)_yawRoot.localEulerAngles.y, (Angle)_pitchRoot.localEulerAngles.x);
        }

        public void SetDirection(Orientation direction)
        {
           _yawRoot.localEulerAngles = new Vector3(0, direction.yaw, 0);      // change this to have direction using only 2 values => same input and rotation       
           _pitchRoot.localEulerAngles = new Vector3(direction.pitch, 0 , 0 );      // change this to have direction using only 2 values => same input and rotation       
        }
    }
}
