﻿using Chromaze.Inf.Components;
using Pixsaoul.App.Camera;
using Pixsaoul.App.UserInput;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using System;
using UnityEngine;

namespace Pixsaoul.Inf.Control
{
    public class RotaterMediator : Mediator
    {
        [Inject]
        public RotaterView View { get; set; } //TODO Set view to real view type

        [Inject] public NotifySecondaryAxisInputSignal NotifySecondaryAxisInputSignal { get; set; }

        [Inject] public SelectFirstPersonCameraSignal SelectFirstPersonCameraSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void OnRegister()
        {
            base.OnRegister();
            View.EnableControlSignal.AddListener(EnableControl);
            View.DisableControlSignal.AddListener(DisableControl);
        }

        public override void OnRemove()
        {
            View.EnableControlSignal.RemoveListener(EnableControl);
            View.DisableControlSignal.RemoveListener(DisableControl);
            NotifySecondaryAxisInputSignal.RemoveListener(ApplyRotation);
            base.OnRemove();
        }

        // Currently control is accessible through IControllable interface from view
        // this is not perfect, should be investigated.

        private void EnableControl()
        {
            NotifySecondaryAxisInputSignal.AddListener(ApplyRotation);
            SelectFirstPersonCameraSignal.Dispatch(View.AttachTarget);
        }

        private void DisableControl()
        {
            NotifySecondaryAxisInputSignal.RemoveListener(ApplyRotation);
        }

        private void ApplyRotation(Vector2 data)
        {
            Orientation direction = new Orientation((Angle)data.x,(Angle)data.y);
            View.Rotate(direction);
        }
    }
}
