﻿
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Control
{
    //TODO TBU find a more accurate name
   public class GimbalCorrector : BaseBehaviour
    {
        //This class purpose is to provide a root for orientable part of component with more user friendly transform (because of rotation of component)
        // intended behaviour:
        /*
         * If up vector of the referenced parent, we do nothing, everything is fine
         * if up vector is down => we want to control everything as if default direction is down, to ease manipulating between -90 and 90 (ceiling)
         * if up vector is in a side direction => we want to have the parent up to our forward, and the world up to our up, regardless of the roll
         */

            // This should be a direct child of the referenced parent, or at least, intermediate parent should have no rotation

        [SerializeField] private Transform _corrector;
        //TODO TBU currently not working
        public void UpdateCorrection(GameObject referencedParent)
        {
            Vector3 parentUp = referencedParent.transform.up;
            Vector3 worldUp = Vector3.up;
            float dotProduct = Vector3.Dot(parentUp,worldUp);

            if(dotProduct > 0.75)
            {// we're most likely already with upward direction => nothing to do really.
                _corrector.transform.LookAt(this.transform.position + referencedParent.transform.forward, worldUp); //Should not be moving anything, but just in case
            }
                else if(dotProduct < -0.75)
            {// we're facing downward
                _corrector.transform.LookAt(this.transform.position + parentUp, referencedParent.transform.forward); // we face downward (with is parent up), up can be forward, just changes orientation
                //TODO TBU see if we really can/need up direction
            } else
            { // we're on a side, with up horizontal
                _corrector.transform.LookAt(this.transform.position + parentUp, worldUp);
                // here we set up to the world up because of the roll object can have, we want the orientable child to be always straight like a head.
            }
        }
    }
}
