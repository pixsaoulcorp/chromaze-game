﻿using Pixsaoul.App.Camera;
using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.Inf.Focus
{
    public class TouchFocusMediator : Mediator
    {
        [Inject]
        public TouchFocusView View { get; set; }
        
        [Inject] public RequestCameraProviderSignal RequestCameraProviderSignal { get; set; }
        [Inject] public NotifyCameraProviderSignal NotifyCameraProviderSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            View.RequestCameraProviderSignal.AddListener(RequestCameraProvider);
            NotifyCameraProviderSignal.AddListener(RegisterCameraProvider);

        }

        public override void OnRemove()
        {
            base.OnRemove();
            View.RequestCameraProviderSignal.RemoveListener(RequestCameraProvider);
            NotifyCameraProviderSignal.RemoveListener(RegisterCameraProvider);


        }

        private void RequestCameraProvider()
        {
            RequestCameraProviderSignal.Dispatch();
        }

        private void RegisterCameraProvider(CameraDelegate.GetCameraDelegate CameraProvider)
        {
            View.SetCameraProvider(CameraProvider);
        }
    }
}
