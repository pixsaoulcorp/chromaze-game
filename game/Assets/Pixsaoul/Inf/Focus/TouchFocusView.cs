﻿using Pixsaoul.App.Focus;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;
using UnityEngine.EventSystems;
using static Pixsaoul.App.Camera.CameraDelegate;

namespace Pixsaoul.Inf.Focus
{
    public class TouchFocusView : View
    {
        //TODO TBU add Selectable information on each component to trace back to the component and request selection.
        //Care about the Selectable not to be mixed up with component selection for control.
        [SerializeField] private LayerMask _selectionLayer;

        private GetCameraDelegate _cameraProvider;

        public Signal RequestCameraProviderSignal = new Signal();
        void Update()
        {
            CheckTouchesInput();
        }


        public void SetCameraProvider(GetCameraDelegate GetCamera)
        {
            _cameraProvider = GetCamera;
        }

        private void CheckTouchesInput()
        { //Input.touches is NOT compatible with standalone so we need to have two implementation of this to be clean.
#if UNITY_EDITOR || UNITY_STANDALONE
            if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                Vector3 position = Input.mousePosition;
                TryGetTarget(position);
            }
#elif UNITY_ANDROID
            for (int i = 0; i < Input.touchCount; i++)
            {
                //TODO TBU check is each touch in on UI or not
                // TODO TBU get the currently used camera properly.
                Touch touchInput = Input.touches[i];
                if (touchInput.phase == TouchPhase.Ended)
                {
                    Vector3 touchPosition = UnityEngine.Camera.main.ScreenToWorldPoint(Input.touches[i].position);
                    TryGetTarget(touchPosition);
                }
            }
#endif
        }

        private void TryGetTarget(Vector3 screenTouchPosition)
        {   
            if(_cameraProvider == null)
            {
                RequestCameraProvider();
            }

            if(_cameraProvider != null)
            {
                UnityEngine.Camera camera = _cameraProvider.Invoke();           
                Ray ray = camera.ScreenPointToRay(screenTouchPosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 200, _selectionLayer)) //TODO TBU have a max distance that is actually handled.
                {
                    //Debug.Log("Touched : " + hit.transform.gameObject.name);
                    IFocusable focusable = hit.transform.gameObject.GetComponent<IFocusable>();
                    if (focusable != null)
                    {
                        focusable.Focus();
                    }
                }
            }
        }

        private void RequestCameraProvider()
        {
            RequestCameraProviderSignal.Dispatch();
        }
    }
}