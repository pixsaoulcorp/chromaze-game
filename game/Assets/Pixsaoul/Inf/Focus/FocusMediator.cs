﻿using Chromaze.App.Components;
using Chromaze.App.Edition;
using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.Inf.Focus
{
    public class FocusMediator : Mediator
    {
        [Inject]
        public FocusView View { get; set; }        
        [Inject] public RequestComponentSelectionSignal RequestComponentSelectionSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            View.FocusSignal.AddListener(Focus);
        }

        public override void OnRemove()
        {
            base.OnRemove();
            View.FocusSignal.RemoveListener(Focus);

        }

        private void Focus(IComponent focusComponent)
        {
            RequestComponentSelectionSignal.Dispatch(focusComponent);
        }
    }
}