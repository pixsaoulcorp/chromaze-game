﻿using Chromaze.App.Components;
using Pixsaoul.App.Engine;
using Pixsaoul.App.Focus;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.Inf.Focus
{
    public class FocusView : View, IFocusable, ISizeable
    {
        public Signal<IComponent> FocusSignal = new Signal<IComponent>();
        [SerializeField] private Transform _resizableArea;
        [SerializeField] private IComponentContainer _focusTarget;

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Focus()
        {
            if (_focusTarget != null)
            {
                FocusSignal.Dispatch(_focusTarget.Result);
            }
        }

        public void Resize(SnappedDimension dimensions)
        {
            _resizableArea.localScale = (Vector3)dimensions;
            _resizableArea.localPosition = new Vector3(-0.5f,-0.5f,-0.5f) + (Vector3)dimensions / 2;
        }
    }
}
