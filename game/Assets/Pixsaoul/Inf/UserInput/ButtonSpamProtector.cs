using Pixsaoul.Fou.Engine;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSpamProtector : BaseBehaviour
{
    [SerializeField] Button _targetButton;
    [SerializeField] float _delay = 0.3f;

    private void Awake()
    {
        _targetButton.onClick.AddListener(OnButtonClicked);
    }
    public void OnButtonClicked()
    {
        StopAllCoroutines();
        StartCoroutine(DelayedEnabled());
    }

    private IEnumerator DelayedEnabled()
    {
        _targetButton.interactable = false;
        yield return new WaitForSeconds(_delay);
        _targetButton.interactable = true;

    }
}
