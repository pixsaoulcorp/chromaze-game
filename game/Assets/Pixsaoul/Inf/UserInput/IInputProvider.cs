﻿using Pixsaoul.Fou.Engine;
using System;
using UnityEngine;

namespace Pixsaoul.Inf.UserInput
{
    [Serializable]
    public class IInputProviderContainer : IUnifiedContainer<IInputProvider> { }

    public interface IInputProvider: IInitializable, IEnablable
    {
        IObservable<Vector2> MainAxis { get; }
        IObservable<Vector2> SecondaryAxis { get; }

        IObservable<int> MainAction { get; } // int not used yet
        IObservable<int> SecondaryAction { get; }
    }
}
