﻿using Pixsaoul.Fou.Engine;
using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using UnityEngine;
using UnityEngine.InputSystem;
using static Pixsaoul.Inf.UserInput.MinimalistMapAsset;

namespace Pixsaoul.Inf.UserInput
{
    public class InputListener : BaseBehaviour, IInputProvider, IMinimalistMapActions
    {
        [Header("Customization")]
        [SerializeField] private float _mainAxisSensitivity;
        [SerializeField] private float _secondaryAxisSensitivity;

        [SerializeField] private MinimalistMapAsset _mapAsset;

        private BehaviorSubject<Vector2> _mainAxisSubject;
        private BehaviorSubject<Vector2> _secondaryAxisSubject;
        private BehaviorSubject<int> _mainActionSubject;
        private BehaviorSubject<int> _secondaryActionSubject;

        public IObservable<Vector2> MainAxis => _mainAxisSubject;
        public IObservable<Vector2> SecondaryAxis => _secondaryAxisSubject;
        public IObservable<int> MainAction => _mainActionSubject;
        public IObservable<int> SecondaryAction => _secondaryActionSubject;


        public void Initialize()
        {
            _mainAxisSubject = new BehaviorSubject<Vector2>(Vector2.zero);
            _secondaryAxisSubject = new BehaviorSubject<Vector2>(Vector2.zero);

            _mainActionSubject = new BehaviorSubject<int>(-1);
            _secondaryActionSubject = new BehaviorSubject<int>(-1);

            _mapAsset = new MinimalistMapAsset();
            _mapAsset.Enable();
            
            _mapAsset.MinimalistMap.SetCallbacks(this);

            StartCoroutine(ListenMainAxis());
            StartCoroutine(ListenSecondaryAxis());
        }

        private bool _secondaryAxisEnabled = false; 

        public void OnEnableSecondaryAxis(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _secondaryAxisEnabled = true;
            } else if  (context.canceled)
            {
                _secondaryAxisEnabled = false;
            }
        }

        public void OnSecondaryAction(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                _mainActionSubject.OnNext(0);
            }
        }

        public void OnMainAction(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                _mainActionSubject.OnNext(0);
            }
        }

        #region axis
        public void OnMainAxis(InputAction.CallbackContext context){}

        public void OnSecondaryAxis(InputAction.CallbackContext context){}

        private IEnumerator<WaitForEndOfFrame> ListenMainAxis()
        {
            WaitForEndOfFrame fixedUpdate = new WaitForEndOfFrame();
            while (true)
            {
                Vector2 mainAxis = _mapAsset.MinimalistMap.MainAxis.ReadValue<Vector2>();
                if(mainAxis != Vector2.zero)
                {
                    _mainAxisSubject.OnNext(_mainAxisSensitivity * mainAxis);
                }
                yield return fixedUpdate;
            }
        }

        private IEnumerator<WaitForEndOfFrame> ListenSecondaryAxis()
        {
            WaitForEndOfFrame fixedUpdate = new WaitForEndOfFrame();
            while (true)
            {
                if (_secondaryAxisEnabled)
                {                    
                   Vector2 secondaryAxis =  _mapAsset.MinimalistMap.SecondaryAxis.ReadValue<Vector2>();
                    if(secondaryAxis != Vector2.zero)
                    {
                        _secondaryAxisSubject.OnNext(_secondaryAxisSensitivity * secondaryAxis);
                    }
                }
                yield return fixedUpdate;
            }
        }

        public void Enable()
        {
            _mapAsset.Enable();
        }

        public void Disable()
        {
            _mapAsset.Disable();
        }
        #endregion

    }
}
