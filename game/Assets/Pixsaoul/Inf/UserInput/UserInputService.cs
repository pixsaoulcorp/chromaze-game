﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.

using Pixsaoul.App.Input;
using Pixsaoul.App.UserInput;
using Pixsaoul.Fou.Mvcs;
using System.Collections.Generic;
using System.Reactive;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Pixsaoul.Inf.UserInput
{
    /// <summary>
    /// AR service interface
    /// </summary>
    public class UserInputService : Service, IUserInputService
    {      

        [Inject] public MainAxisInputSignal MainAxisInputSignal { get; set; }
        [Inject] public SecondaryAxisInputSignal SecondaryAxisInputSignal { get; set; }
        [Inject] public MainButtonInputSignal MainButtonInputSignal { get; set; }
        [Inject] public SecondaryButtonInputSignal SecondaryButtonInputSignal { get; set; }

        [SerializeField] private IInputProviderContainer _windowsProvider;
        [SerializeField] private IInputProviderContainer _androidProvider;

        public override void Initialize()
        {
            base.Initialize();
#if UNITY_STANDALONE || UNITY_EDITOR
            IInputProvider windowsProvider = _windowsProvider.Result;
            InitializeInputProvider(windowsProvider);
#endif

#if UNITY_ANDROID || UNITY_EDITOR
            IInputProvider androidProvider = _androidProvider.Result;
            InitializeInputProvider(androidProvider);
#endif
        }

        private void InitializeInputProvider(IInputProvider inputProvider)
        {
            inputProvider.Initialize();

            inputProvider.MainAction.Subscribe(Observer.Create<int>((int v) => MainButtonInputSignal.Dispatch()));
            inputProvider.SecondaryAction.Subscribe(Observer.Create<int>((int v) => SecondaryButtonInputSignal.Dispatch()));

            inputProvider.MainAxis.Subscribe(Observer.Create<Vector2>((Vector2 v) => MainAxisInputSignal.Dispatch(v)));
            inputProvider.SecondaryAxis.Subscribe(Observer.Create<Vector2>((Vector2 v) => SecondaryAxisInputSignal.Dispatch(v)));
        }

        public void Enable()
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            _windowsProvider.Result.Enable();
#endif

#if UNITY_ANDROID || UNITY_EDITOR
            _androidProvider.Result.Enable();
#endif
        }

        public void Disable()
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            _windowsProvider.Result.Disable();
#endif

#if UNITY_ANDROID || UNITY_EDITOR
            _androidProvider.Result.Disable();
#endif
        }
    }
}