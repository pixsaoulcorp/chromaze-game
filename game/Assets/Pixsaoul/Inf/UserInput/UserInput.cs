// GENERATED AUTOMATICALLY FROM 'Assets/Pixsaoul/Inf/UserInput/UserInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @UserInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @UserInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""UserInput"",
    ""maps"": [
        {
            ""name"": ""MinimalistMap"",
            ""id"": ""53491bbb-a01d-48e2-b439-707c189fa674"",
            ""actions"": [
                {
                    ""name"": ""MainAction"",
                    ""type"": ""Button"",
                    ""id"": ""ca9bc529-41e8-4c98-941d-c06b38cf977c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SecondaryAction"",
                    ""type"": ""Button"",
                    ""id"": ""7bae859e-a412-4e2d-94ec-ee5d72e67619"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Axis"",
                    ""type"": ""Button"",
                    ""id"": ""e6adcba3-d2b9-4bf7-89e5-e5ba5f894a35"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Axis1"",
                    ""type"": ""Button"",
                    ""id"": ""7327d1e9-b0a7-4d61-b2e2-306f907cd0f3"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""1a2d48c1-ec90-4838-89b7-54c0b60ac3d5"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MainAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c518840a-6e9c-4058-afdf-2aa7be60f8cf"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SecondaryAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""LeftHand"",
                    ""id"": ""545e3d11-c2eb-4f1d-9604-ef60382b6583"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""4d065792-1482-4622-a417-7885daf894b0"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""eb88c7de-2c36-49b0-b548-58a63643a66b"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""c01e3dba-79ee-45bd-8c13-6f6b67fec33a"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""f2c3cb51-8d3d-4f00-a675-95a7cb00f5d4"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""RightHand"",
                    ""id"": ""8bb2db2d-4933-4306-b673-2b7d1b9f9c96"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""421c1598-2dbb-45d9-9875-fc618731c099"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""201687c4-0a60-433c-8092-0dba6deaed65"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""61b56f88-1e3f-4056-a57b-d0bab7f53042"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""b50ff8d6-c2db-4e89-9431-c940dbfc20b1"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""LeftHand"",
                    ""id"": ""37865005-fef6-4579-9cd9-b7ecb55a42ba"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis1"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""09aecfc8-25be-4fea-9a92-02df634b53f6"",
                    ""path"": ""<Pointer>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""15e85ac6-73a1-4d94-b52b-f92115f5e4a2"",
                    ""path"": ""<Pointer>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""f5c887bb-6d11-483e-a778-60402e2c7cc4"",
                    ""path"": ""<Pointer>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""78dbf68a-643c-43d8-8ff8-1a3886f5cb52"",
                    ""path"": ""<Pointer>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""e11c9389-1375-4e25-9a12-a9c9a8f71d56"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // MinimalistMap
        m_MinimalistMap = asset.FindActionMap("MinimalistMap", throwIfNotFound: true);
        m_MinimalistMap_MainAction = m_MinimalistMap.FindAction("MainAction", throwIfNotFound: true);
        m_MinimalistMap_SecondaryAction = m_MinimalistMap.FindAction("SecondaryAction", throwIfNotFound: true);
        m_MinimalistMap_Axis = m_MinimalistMap.FindAction("Axis", throwIfNotFound: true);
        m_MinimalistMap_Axis1 = m_MinimalistMap.FindAction("Axis1", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // MinimalistMap
    private readonly InputActionMap m_MinimalistMap;
    private IMinimalistMapActions m_MinimalistMapActionsCallbackInterface;
    private readonly InputAction m_MinimalistMap_MainAction;
    private readonly InputAction m_MinimalistMap_SecondaryAction;
    private readonly InputAction m_MinimalistMap_Axis;
    private readonly InputAction m_MinimalistMap_Axis1;
    public struct MinimalistMapActions
    {
        private @UserInput m_Wrapper;
        public MinimalistMapActions(@UserInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @MainAction => m_Wrapper.m_MinimalistMap_MainAction;
        public InputAction @SecondaryAction => m_Wrapper.m_MinimalistMap_SecondaryAction;
        public InputAction @Axis => m_Wrapper.m_MinimalistMap_Axis;
        public InputAction @Axis1 => m_Wrapper.m_MinimalistMap_Axis1;
        public InputActionMap Get() { return m_Wrapper.m_MinimalistMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MinimalistMapActions set) { return set.Get(); }
        public void SetCallbacks(IMinimalistMapActions instance)
        {
            if (m_Wrapper.m_MinimalistMapActionsCallbackInterface != null)
            {
                @MainAction.started -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnMainAction;
                @MainAction.performed -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnMainAction;
                @MainAction.canceled -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnMainAction;
                @SecondaryAction.started -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnSecondaryAction;
                @SecondaryAction.performed -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnSecondaryAction;
                @SecondaryAction.canceled -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnSecondaryAction;
                @Axis.started -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnAxis;
                @Axis.performed -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnAxis;
                @Axis.canceled -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnAxis;
                @Axis1.started -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnAxis1;
                @Axis1.performed -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnAxis1;
                @Axis1.canceled -= m_Wrapper.m_MinimalistMapActionsCallbackInterface.OnAxis1;
            }
            m_Wrapper.m_MinimalistMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MainAction.started += instance.OnMainAction;
                @MainAction.performed += instance.OnMainAction;
                @MainAction.canceled += instance.OnMainAction;
                @SecondaryAction.started += instance.OnSecondaryAction;
                @SecondaryAction.performed += instance.OnSecondaryAction;
                @SecondaryAction.canceled += instance.OnSecondaryAction;
                @Axis.started += instance.OnAxis;
                @Axis.performed += instance.OnAxis;
                @Axis.canceled += instance.OnAxis;
                @Axis1.started += instance.OnAxis1;
                @Axis1.performed += instance.OnAxis1;
                @Axis1.canceled += instance.OnAxis1;
            }
        }
    }
    public MinimalistMapActions @MinimalistMap => new MinimalistMapActions(this);
    public interface IMinimalistMapActions
    {
        void OnMainAction(InputAction.CallbackContext context);
        void OnSecondaryAction(InputAction.CallbackContext context);
        void OnAxis(InputAction.CallbackContext context);
        void OnAxis1(InputAction.CallbackContext context);
    }
}
