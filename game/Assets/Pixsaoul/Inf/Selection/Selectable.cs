﻿using Pixsaoul.App.Selection;
using Pixsaoul.Fou.Engine;
using System;
using UnityEngine;

namespace Pixsaoul.Inf.Selection
{
    public class Selectable : BaseBehaviour, ISelectable
    {
        private bool _isSelected;
        Action _selectCallback;
        [SerializeField] private Transform Center;

        public Transform SelectionLocation => Center;

        public void SetSelectionCallback(Action selectCallback)
        {
            _isSelected = false;
            _selectCallback = selectCallback;
        }

        public void Select()
        {
            _isSelected = true;
            _selectCallback.Invoke();
        }
    }
}