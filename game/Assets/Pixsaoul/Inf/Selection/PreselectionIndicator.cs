﻿using Pixsaoul.App.Selection;
using Pixsaoul.Fou.Engine;
using System;
using System.Reactive;
using UnityEngine;

namespace Pixsaoul.Inf.Selection
{
    public class PreselectionIndicator : BaseBehaviour
    {
        //TODO move this in Pre
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private IObservableSelectorContainer _selector;
        [SerializeField] private Transform _selectionWatcher;

        private Transform _selectionTarget;
        
        public void Initialize()
        {
            IObserver<Transform> selectionObserver = Observer.Create<Transform>((Transform go) => OnTargetChanged(go));
            _selector.Result.SubscribeTarget(selectionObserver);            
        }

        private void Update()
        {
            DebugDisplay();
            UpdateDirection();

        }
        private void DebugDisplay()
        {
            if (_selectionTarget != null)
            {
                Debug.DrawLine(this.transform.position, _selectionTarget.transform.position);
            }
        }

        private void UpdateDirection()
        {
            if (_selectionTarget)
            {
                _selectionWatcher.LookAt(_selectionTarget.transform);
            }
        }

        private void OnTargetChanged(Transform target)
        {
            _selectionTarget = target;
            //TODO TBU 
            // disable particle system if no target
            UpdateDirection();
            UpdateParticle();
            
        }

        private void UpdateParticle()
        {
            if (_selectionTarget != null)
            {
                _particleSystem.Play();
            } else
            {
                _particleSystem.Stop();
            }
        }
    }
}