﻿using Pixsaoul.App.Selection;
using Pixsaoul.Fou.Engine;
using System;
using System.Collections.ObjectModel;
using System.Reactive.Subjects;
using UnityEngine;

namespace Pixsaoul.Inf.Selection
{
    public class ClosestSelector : BaseBehaviour, ISelector, IObservableSelector
    {
        private ISelectable _currentSelectable;
        private Collection<Collider> _accessibleColliders;

        public BehaviorSubject<Transform> TargetSelection = new BehaviorSubject<Transform>(null);

        public void Initialize()
        {
            _accessibleColliders = new Collection<Collider>();
        }       
        
        public void SubscribeTarget(IObserver<Transform> observer)
        {
            TargetSelection.Subscribe(observer);
        }

        public bool TryApplySelection()
        {
            if(_currentSelectable != null)
            {
                _currentSelectable.Select();
                return true;
            }
            return false;
        }

        private void OnTriggerEnter(UnityEngine.Collider other)
        {
            _accessibleColliders.Add(other);
        }

        private void OnTriggerExit(UnityEngine.Collider other)
        {
            _accessibleColliders.Remove(other);
        }

        void Update()
        {
            float minDistance = 100f;
            GameObject targetSelectable = null;
            foreach(Collider collider in _accessibleColliders)
            {            
                if(collider == null) // just cos sometime object get destroyed while being available
                {
                    _accessibleColliders.Remove(collider);
                }
                float distance = Vector3.Distance(collider.transform.position, this.transform.position);
                if (distance <= minDistance)
                {
                    ISelectable selectable = collider.GetComponent<ISelectable>();
                    if (selectable != null)
                    {
                        targetSelectable = collider.gameObject;
                        minDistance = distance;
                    }
                }                
            }
            _currentSelectable = targetSelectable != null ? targetSelectable.GetComponent<ISelectable>() : null;
            TargetSelection.OnNext(_currentSelectable?.SelectionLocation); //TODO change.transform to get the desired center of selectable location
                //TODO TBU use this information at some point to display pre selection
        }
    }
}
