﻿using Pixsaoul.App.Persistency;
using Pixsaoul.App.Serialization;
using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Pixsaoul.Inf.Persistency.Remote
{
    public class RemotePersistencyService : Service, IAsyncPersistencyService
    {
        [Inject] public ISerializerService SerializerService { get; set; }

        private EndPoint _rootEndpoint;

        private EndPoint _pingEndpoint;
        public override void Initialize()
        {

#if DEBUGGING_LOCAL
            Debug.Log("Running as self hosted");
            _rootEndpoint = new EndPoint(new List<string>() { "http://localhost:9917" });
#else
            _rootEndpoint = new EndPoint(new List<string>() { "http://pixsaoul.ddns.net:15432" });
#endif
            //Debug only or ping process
            _pingEndpoint = new EndPoint(new List<string>() { "version" });
            RequestReadRaw(_pingEndpoint, (string v) => Debug.Log("online version : " + v));
        }
        
        public void RequestRead<T>(EndPoint relativeEndPoint, Action<T> callback) where T : class, new()
        {            
            EndPoint fullEndPoint = EndPoint.Concat(_rootEndpoint, relativeEndPoint);

            Get(fullEndPoint, (string serializedData) => {
                T output = new T();
                output = SerializerService.Deserialize<T>(serializedData);
                callback(output);
                });
        }

        public void RequestReadRaw(EndPoint relativeEndPoint, Action<string> callback)
        {
            EndPoint fullEndPoint = EndPoint.Concat(_rootEndpoint, relativeEndPoint);

            Get(fullEndPoint, (string serializedData) => {
                string output = serializedData;               
                callback(output);
            });
        }

        public void RequestWrite(object data, EndPoint relativeEndPoint)
        {
            string serializedData = SerializerService.Serialize(data);
            EndPoint fullEndpoint = EndPoint.Concat(_rootEndpoint, relativeEndPoint);
            Post(fullEndpoint, serializedData);
        }


        //TODO TBU move GET POST PUT DELETE ETC TO ANOTHER CLASS WITH EVERYTHING NEEDED
        private void Get(EndPoint endpoint, Action<string> callback)
        {
            StartCoroutine(GetC(endpoint, callback));
        }

        private IEnumerator<UnityWebRequestAsyncOperation> GetC(EndPoint endpoint, Action<string> jsonCallback)
        {
#if UNITY_EDITOR
            Debug.Log(endpoint.ToString());
#endif
            UnityWebRequest request = UnityWebRequest.Get(endpoint.ToString());
            request.SetRequestHeader("Content-Type", "application/json");
            request.timeout = 5;
            yield return request.SendWebRequest();
            if(request.responseCode == 200)
            {
                byte[] bytesOutput = request.downloadHandler.data;
                string serializedJsonData = Encoding.UTF8.GetString(bytesOutput);
                jsonCallback(serializedJsonData);
            } else
            {
                Debug.LogWarning("Error code : " + request.responseCode); // TODO Handle errors properly
            }
        }

        private void Post(EndPoint endpoint, string jsonBody)
        {
            StartCoroutine(PostC(endpoint, jsonBody));
        }

        private IEnumerator<UnityWebRequestAsyncOperation> PostC(EndPoint endpoint, string jsonBody)
        {
#if UNITY_EDITOR
            Debug.Log(endpoint.ToString());
            Debug.Log(jsonBody);
#endif
            UnityWebRequest request = new UnityWebRequest(endpoint.ToString(), UnityWebRequest.kHttpVerbPOST);
            byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonBody);
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();
            Debug.Log("Status Code: " + request.responseCode);
        }

    }
}
