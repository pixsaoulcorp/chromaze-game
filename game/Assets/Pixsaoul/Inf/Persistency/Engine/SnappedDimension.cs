﻿using Pixsaoul.Fou.Markers;
using System;

namespace Pixsaoul.Inf.Persistency.Engine
{
    [Serializable]
    public class SnappedDimension : IPersister<Dom.Data.Engine.SnappedDimension>
    {
        public int width { get; set; }
        public int height { get; set; }
        public int depth { get; set; }

        public SnappedDimension()
        {

        }

        public SnappedDimension(Dom.Data.Engine.SnappedDimension dom)
        {
            width = dom.Width;
            height = dom.Height;
            depth = dom.Depth;
        }

        public Dom.Data.Engine.SnappedDimension ToDom()
        {
            return new Dom.Data.Engine.SnappedDimension(width, height, depth);
        }
    }
}