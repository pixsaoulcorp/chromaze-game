﻿using Pixsaoul.Fou.Markers;
using System;

namespace Pixsaoul.Inf.Persistency.Engine
{
    [Serializable]
    public class SnappedPosition : IPersister<Pixsaoul.Dom.Data.Engine.SnappedPosition>
    {
        public int x { get; set; }

        public int y { get; set; }

        public int z { get; set; }

        public SnappedPosition()
        {

        }

        public SnappedPosition(Dom.Data.Engine.SnappedPosition dom)
        {
            x = dom.X;
            y = dom.Y;
            z = dom.Z;
        }

        public Dom.Data.Engine.SnappedPosition ToDom()
        {
            return new Dom.Data.Engine.SnappedPosition(x, y, z);
        }
    }
}
