﻿using Pixsaoul.Fou.Markers;
using System;

namespace Pixsaoul.Inf.Persistency.Engine
{
    [Serializable]
    public class Orientation : IPersister<Dom.Data.Engine.Orientation>
    {
        public Orientation()
        {
            
        }

        public float yaw { get; set; }
        public float pitch { get; set; }

        public Orientation(Dom.Data.Engine.Orientation dom)
        {
            yaw = dom.yaw;
            pitch = dom.pitch;
        }

        public Dom.Data.Engine.Orientation ToDom()
        {
           return new Dom.Data.Engine.Orientation(yaw, pitch);
        }
    }
}
