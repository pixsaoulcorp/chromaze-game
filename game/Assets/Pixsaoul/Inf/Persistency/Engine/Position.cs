﻿using Pixsaoul.Fou.Markers;
using System;

namespace Pixsaoul.Inf.Persistency.Engine
{
    [Serializable]
    public class Position: IPersister<Dom.Data.Engine.Position>
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }

        public Position()
        {

        }
        public Position(Dom.Data.Engine.Position dom)
        {
            x = dom.x;
            y = dom.y;
            z = dom.z;
        }

        public Dom.Data.Engine.Position ToDom()
        {
            return new Dom.Data.Engine.Position(x, y, z);
        }

    }
}
