﻿using Pixsaoul.Fou.Markers;
using System;

namespace Pixsaoul.Inf.Persistency.Engine
{
    [Serializable]
    public class SnappedRotation : IPersister<Dom.Data.Engine.SnappedRotation>
    {
        public int yaw { get; set; }

        public int pitch { get; set; }

        public int roll { get; set; }

        public SnappedRotation()
        {

        }
        public SnappedRotation(Dom.Data.Engine.SnappedRotation dom)
        {
            yaw = dom.yaw;
            pitch = dom.pitch;
            roll = dom.roll;
        }


        public Dom.Data.Engine.SnappedRotation ToDom()
        {
            return new Dom.Data.Engine.SnappedRotation(yaw,pitch,roll);

        }
    }
}
