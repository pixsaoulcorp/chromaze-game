﻿using Pixsaoul.Fou.Markers;
using System;
using DomEngine = Pixsaoul.Dom.Data.Engine;

namespace Pixsaoul.Inf.Persistency.Engine
{
    [Serializable]
    public class Rotation : IPersister<Dom.Data.Engine.Rotation>
    {
        public float yaw { get; set; }
        public float pitch { get; set; }
        public float roll { get; set; }

        public Rotation()
        {

        }
        public Rotation(DomEngine.Rotation dom)
        {
            yaw = dom.yaw;
            pitch = dom.pitch;
            roll = dom.roll;
        }

        public DomEngine.Rotation ToDom()
        {
            return new DomEngine.Rotation(yaw, pitch, roll);
        }
    }
}
