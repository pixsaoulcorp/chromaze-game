﻿using System;

namespace Pixsaoul.Inf.Persistency
{
    [Serializable]
    public class Version : IEquatable<Version>
    {
        private string _version;

        public Version()
        {
            _version = CurrentVersion;
        }

        public Version(string version)
        {
            _version = version;
        }


        public override string ToString()
        {
            return _version;
        }

        public bool Equals(Version other)
        {
            return _version.Equals(other._version);
        }

        public static string CurrentVersion = "0.5.0";
    }
}