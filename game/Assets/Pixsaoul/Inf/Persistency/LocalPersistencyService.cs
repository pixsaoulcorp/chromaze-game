﻿using Pixsaoul.App.Persistency;
using Pixsaoul.App.Serialization;
using System.IO;
using UnityEngine;

namespace Pixsaoul.Inf.Persistency
{
    public class LocalPersistencyService : IFilePersistencyService
    {
        [Inject] public ISerializerService SerializerService { get; set; }

        private string _rootEndPoint;

        public void Initialize()
        {            
            _rootEndPoint = Application.persistentDataPath;
        }

        public string[] ReadFolder(string endpoint)
        { //TODO TBU have better stuff to get level list
            string fullPath = GetFullPath(endpoint);
            string[] res;

            DirectoryInfo directoryInfo = new DirectoryInfo(fullPath);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            res = Directory.GetFiles(fullPath);            
            return res;
        }

        public T Read<T>(string endpoint) where T : class, new()
        {
            T res = new T();
            string fullPath = GetFullPath(endpoint);
            string serializedData = File.ReadAllText(fullPath);
            res = SerializerService.Deserialize<T>(serializedData);
            return res;        
        }

        public void Write(object data, string endpoint)
        {
            string serializedData = SerializerService.Serialize(data);
            string fullPath = GetFullPath(endpoint);

            System.IO.FileInfo file = new System.IO.FileInfo(fullPath);
            file.Directory.Create(); // If the directory already exists, this method does nothing.

            File.WriteAllText(fullPath, serializedData);
        }

        public void Delete(string endpoint)
        {
            string fullPath = GetFullPath(endpoint);
            File.Delete(fullPath);
        }

        private string GetFullPath(string endpoint)
        {
            return Path.Combine(_rootEndPoint, endpoint);
        }
    }
}
