﻿// * Copyright [2020] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Pixsaoul.App.Persistency;
using Pixsaoul.App.Serialization;
using Pixsaoul.Fou.Mvcs;
using System;
using System.IO;
using UnityEngine;

namespace Pixsaoul.Inf.Persistency
{
    public class InternalPersistencyService : IService, IPersistencyService
    {      
        [Inject] public ISerializerService SerializerService { get; set; }

        private string _rootEndPoint;
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            _rootEndPoint = Application.streamingAssetsPath;
        }

        public T Read<T>(string relativePath) where T : class, new()
        {
            T res = new T();
            string fullPath = Path.Combine(_rootEndPoint, relativePath);
            UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get(fullPath);
            www.SendWebRequest();
            while (!www.isDone){}
            string serializedData = www.downloadHandler.text;
            res = SerializerService.Deserialize<T>(serializedData);
            return res;
        }

        public void Write(object data, string filePath)
        {
            throw new NotImplementedException();
        }
    }
}