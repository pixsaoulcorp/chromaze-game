﻿using System;

namespace Pixsaoul.Inf.Persistency.Shape
{
    [Serializable]
    public class BoxelLocation : IEquatable<BoxelLocation>
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public Boxel Boxel { get; set; }

        public BoxelLocation()
        {
            Boxel = new Boxel();
        }

        public BoxelLocation(int x, int y, int z, Boxel boxel)
        {
            X = x;
            Y = y;
            Z = z;
            Boxel = boxel;
        }

        public bool Equals(BoxelLocation other)
        {
            return (other.X == X) && (other.Y == Y) && (other.Z == Z) && (other.Boxel.Equals(Boxel));
        }
    }
}
