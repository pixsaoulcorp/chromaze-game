﻿using Pixsaoul.Fou.Markers;
using System;
using System.Collections.Generic;

namespace Pixsaoul.Inf.Persistency.Shape
{
    [Serializable]
    public class BoxelShape : IPersister<Dom.Shape.BoxelShape>
    {
        public List<BoxelLocation> Boxels { get; set; }
        public SurfaceMode SurfaceMode { get; set; }
      
        public BoxelShape()
        {
            Boxels = new List<BoxelLocation>();
            SurfaceMode = SurfaceMode.Extern;
        }

        public BoxelShape(Dom.Shape.BoxelShape dom)
        {
            SurfaceMode = (SurfaceMode)dom.SurfaceMode;
            Boxels = new List<BoxelLocation>();
            foreach(var boxel in dom.Boxels)
            {
                Boxels.Add(new BoxelLocation(boxel.Key.x, boxel.Key.y, boxel.Key.z, new Boxel(boxel.Value)));
            } // TODO TBU MAPPER double check this
        }

        public Dom.Shape.BoxelShape ToDom()
        {
            var output = new Dom.Shape.BoxelShape((Dom.Shape.SurfaceMode)SurfaceMode);
            foreach(var boxel in Boxels)
            {
                output.SetBoxel(boxel.X, boxel.Y, boxel.Z, boxel.Boxel.ToDom());
            }
            return output;
        }
    }
}