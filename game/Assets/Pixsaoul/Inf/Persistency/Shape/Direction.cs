﻿namespace Pixsaoul.Inf.Persistency.Shape
{
    public enum Direction
    {
        // see dom for explanation of order
        Up = 0,
        Down,
        Left,
        Right,
        Back,
        Front
    }
}
