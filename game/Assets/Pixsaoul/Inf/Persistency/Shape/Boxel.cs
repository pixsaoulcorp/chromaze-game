﻿using Pixsaoul.Fou.Markers;
using System;
using System.Collections.Generic;

namespace Pixsaoul.Inf.Persistency.Shape
{
    [Serializable]
    public class Boxel : IEquatable<Boxel>, IPersister<Dom.Shape.Boxel>
    {
        public List<Direction> Directions { get; set; }

        public Boxel()
        {
            Directions = new List<Direction>();
        }

        public bool Equals(Boxel other)
        {
            if(other.Directions.Count != Directions.Count)
            {
                return false;
            }
            foreach(Direction dir in Directions)
            {
                if (!other.Directions.Contains(dir))
                {
                    return false;
                }
            }
            return true;
        }

        public Boxel(Dom.Shape.Boxel boxel)
        {
            Directions = new List<Direction>(); //TODO TBU MAPPER
            foreach(var dir in boxel.Directions)
            {
                Directions.Add((Direction)dir);
            }
        }

        public Dom.Shape.Boxel ToDom()
        {
            HashSet<Pixsaoul.Dom.Shape.Surface> surfaces = new HashSet<Pixsaoul.Dom.Shape.Surface>();
            foreach(var surface in Directions)
            {
                surfaces.Add((Pixsaoul.Dom.Shape.Surface)surface);
            }
            return new Dom.Shape.Boxel(surfaces);
        }
    }
}
