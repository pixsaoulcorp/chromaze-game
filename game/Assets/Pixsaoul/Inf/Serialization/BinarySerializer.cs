﻿using Pixsaoul.App.Serialization;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Pixsaoul.Inf.Serialization
{
    public class BinarySerializer : ISerializerService
    {
        public void Initialize()
        {
            
        }

        public string Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            byte[] bytes = stream.GetBuffer();
            string encodedData = Convert.ToBase64String(bytes);
            return encodedData;
        }

        public T Deserialize<T>(string serializedObject) where T : class, new()
        {
            byte[] b = Convert.FromBase64String(serializedObject);
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream(b);
            T obj = (T)formatter.Deserialize(stream);
            return obj;
        }
    }
}
