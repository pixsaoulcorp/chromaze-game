﻿using Newtonsoft.Json;
using Pixsaoul.App.Serialization;

namespace Pixsaoul.Inf.Serialization
{
    public class JsonSerializerService : ISerializerService
    {
        /// <summary>
        /// Settings used force to serialize the type of data in the json to ensure flawless deserialization
        /// </summary>
        //private JsonSerializerSettings _settings = new JsonSerializerSettings()
        //{
        //    TypeNameHandling = TypeNameHandling.
        //};

        public void Initialize()
        {
            
        }

        // json.net version
        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public T Deserialize<T>(string serializedObject) where T : class, new()
        {
            T deserialized = new T();
            deserialized = JsonConvert.DeserializeObject<T>(serializedObject);
            return deserialized;
        }
    }
}
