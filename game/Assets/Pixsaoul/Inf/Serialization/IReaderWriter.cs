﻿namespace Pixsaoul.Inf.Persistency
{
    public interface IReaderWriter
    {
        void Write(object obj, string path);
        T Read<T>(string path) where T : class, new();
    }
}