﻿using Pixsaoul.App.Camera;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.Free
{
    public class FreeCameraManager : BaseBehaviour, IFreeCameraManager
    {
        [SerializeField] FreeCamera _freeCamera;

        [Header("Rotation sensitivity")]
        [SerializeField] private float _rotationSensitivity;
        [SerializeField] private AnimationCurve _rotationSmoothness;

        [Header("Translation sensitivity")]
        [SerializeField] private float _translationSensitivity;
        [SerializeField] private AnimationCurve _translationSmoothness;
        public ICamera ManagedCamera => _freeCamera;


        public void Disable()
        {
            _freeCamera.Disable();
        }

        public void Enable()
        {
            _freeCamera.Enable();
        }

        public void Initialize()
        {
            _freeCamera.Initialize();
        }

        public void Rotate(Vector2 rotation)
        {
            float smoothedX = (rotation.x >= 0 ? 1 : -1) * _rotationSmoothness.Evaluate(Mathf.Abs(rotation.x));
            float smoothedY = (rotation.y >= 0 ? 1 : -1) * _rotationSmoothness.Evaluate(Mathf.Abs(rotation.y));
            _freeCamera.Rotate(_rotationSensitivity * new Vector2(smoothedX, smoothedY));
        }

        public void SetLocation(Vector3 position, Quaternion orientation)
        {
            _freeCamera.SetLocation(position, orientation);
        }

        public void Translate(Vector2 translation)
        {

            float smoothedX = (translation.x >= 0 ? 1 : -1) * _translationSmoothness.Evaluate(Mathf.Abs(translation.x));
            float smoothedY = (translation.y >= 0 ? 1 : -1) * _translationSmoothness.Evaluate(Mathf.Abs(translation.y));
            _freeCamera.Translate(_translationSensitivity * new Vector2(smoothedX, smoothedY));
        }
    }
}
