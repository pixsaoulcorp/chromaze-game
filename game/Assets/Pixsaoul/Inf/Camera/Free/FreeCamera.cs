﻿using Pixsaoul.App.Camera;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.Free
{
    public class FreeCamera : BaseBehaviour, ICamera
    {
        [SerializeField] private UnityEngine.Camera _camera;
        [SerializeField] private GameObject _cameraRoot; // camera holder, camera should ALWAYS be under this

        public UnityEngine.Camera Camera => _camera;

        public void Initialize()
        {
            _camera.transform.SetParent(_cameraRoot.transform);
            Disable();
        }

        public void Enable()
        {
            _cameraRoot.SetActive(true);
        }

        public void Disable()
        {
            _cameraRoot.SetActive(false);
        }

        public void SetLocation(Vector3 position, Quaternion orientation)
        {
            _cameraRoot.transform.position = position;
            _cameraRoot.transform.rotation = orientation;
        }

        public void Translate(Vector2 translation)
        {
           // Debug.Log($"Translate free camera {this.gameObject.name}");

            _cameraRoot.transform.Translate(new Vector3(translation.x, 0, translation.y), Space.Self);
        }

        public void Rotate(Vector2 rotation)
        {
            _cameraRoot.transform.Rotate(Vector3.up, rotation.x, Space.World);
            _cameraRoot.transform.Rotate(Vector3.left, rotation.y, Space.Self);
        }
    }
}
