﻿using Pixsaoul.App.Input;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.Free
{
    [System.Serializable]
    public class IFreeCameraManagerContainer : IUnifiedContainer<IFreeCameraManager> { }


    public interface IFreeCameraManager : ICameraManager
    {
        void Initialize();

        void SetLocation(Vector3 position, Quaternion orientation);

        void Rotate(Vector2 rotation);

        void Translate(Vector2 translation);
    }
}