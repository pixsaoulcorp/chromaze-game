﻿using Pixsaoul.App.Camera;
using Pixsaoul.App.Engine;
using Pixsaoul.App.UserInput;
using Pixsaoul.Fou.Mvcs;
using System;
using UnityEngine;

namespace Pixsaoul.Inf.Camera
{
    public class CameraMediator : Mediator
    {
        [Inject]
        public CameraView View { get; set; } //TODO Set view to real view type

        // camera selection signals
        [Inject] public SelectFirstPersonCameraSignal SelectFirstPersonCameraSignal { get; set; }
        [Inject] public SelectThirdPersonCameraSignal SelectThirdPersonCameraSignal { get; set; }
        [Inject] public SelectFreeCameraSignal SelectFreeCameraSignal { get; set; }
        [Inject] public SelectFixedCameraSignal SelectFixedCameraSignal { get; set; }

        // input signals
        [Inject] public NotifyMainAxisInputSignal NotifyMainAxisInputSignal { get; set; }
        [Inject] public NotifySecondaryAxisInputSignal NotifySecondaryAxisInputSignal { get; set; }

        [Inject] public RequestCameraProviderSignal RequestCameraProviderSignal { get; set; }
        [Inject] public NotifyCameraProviderSignal NotifyCameraProvider { get; set; }

        //used to store current callback from camera
        private Action<Vector2> _rotationCallback;
        private Action<Vector2> _translationCallback;

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();

            SelectFirstPersonCameraSignal.AddListener(SelectFirstPerson);
            SelectThirdPersonCameraSignal.AddListener(SelectThirdPerson);
            SelectFreeCameraSignal.AddListener(SelectFree);
            SelectFixedCameraSignal.AddListener(SelectFixed);

            View.RegisterRotationSignal.AddListener(ListenRotation);
            View.UnregisterAllRotationSignal.AddListener(StopListeningRotation);
            View.RegisterTranslationSignal.AddListener(ListenTranslation);
            View.UnregisterAllTranslationSignal.AddListener(StopListeningTranslation);

            NotifyMainAxisInputSignal.AddListener(TransmitTranslation);
            NotifySecondaryAxisInputSignal.AddListener(TransmitRotation);

            RequestCameraProviderSignal.AddListener(DispatchProvider);
        }

        public override void OnRemove()
        {
            SelectFirstPersonCameraSignal.RemoveListener(SelectFirstPerson);
            SelectThirdPersonCameraSignal.RemoveListener(SelectThirdPerson);
            SelectFreeCameraSignal.RemoveListener(SelectFree);
            SelectFixedCameraSignal.RemoveListener(SelectFixed);
            
            View.RegisterRotationSignal.RemoveListener(ListenRotation);
            View.UnregisterAllRotationSignal.RemoveListener(StopListeningRotation);
            View.RegisterTranslationSignal.RemoveListener(ListenTranslation);
            View.UnregisterAllTranslationSignal.RemoveListener(StopListeningTranslation);

            NotifyMainAxisInputSignal.RemoveListener(TransmitTranslation);
            NotifySecondaryAxisInputSignal.RemoveListener(TransmitRotation);

            RequestCameraProviderSignal.RemoveListener(DispatchProvider);

            base.OnRemove();
        }

        private void SelectFirstPerson(IAttachTarget attachTarget)
        {
            View.EnableFirstPersonCamera(attachTarget);
        }

        private void SelectThirdPerson(IAttachTarget attachTarget)
        {
            View.EnableThirdPersonCamera(attachTarget);
        }

        private void SelectFree(Vector3 position, Quaternion orientation)
        {
            View.EnableFreeCamera(position, orientation);
        }

        private void SelectFixed(Vector3 position, Quaternion orientation)
        {
            View.EnableFixCamera(position, orientation);
        }

        #region input management

        private void ListenRotation(Action<Vector2> callback)
        {
            _rotationCallback = callback;
        }


        private void StopListeningRotation()
        {
            _rotationCallback = null;
        }

        private void ListenTranslation(Action<Vector2> callback)
        {
            _translationCallback = callback;
        }

        private void StopListeningTranslation()
        {
            _translationCallback = null;
        }

        private void TransmitRotation(Vector2 rotation)
        {            
            _rotationCallback?.Invoke(rotation);
        }

        private void TransmitTranslation(Vector2 translation)
        {
            _translationCallback?.Invoke(translation);
        }
        #endregion

        private void DispatchProvider()
        {
            NotifyCameraProvider.Dispatch(View.GetCurrentCamera);
        }
    }
}
