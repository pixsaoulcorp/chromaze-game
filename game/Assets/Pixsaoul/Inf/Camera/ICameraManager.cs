﻿using Pixsaoul.App.Camera;

namespace Pixsaoul.Inf.Camera
{
    public interface ICameraManager
    {
        void Enable();
        void Disable();

        ICamera ManagedCamera { get; }
    }
}
