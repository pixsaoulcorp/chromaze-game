﻿using Pixsaoul.App.Camera;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.Fix
{

    public class FixedCameraManager : BaseBehaviour, IFixedCameraManager
    {
        [SerializeField] private FixedCamera _camera;

        public ICamera ManagedCamera => _camera;

        public void Disable()
        {
            _camera.Disable();
        }

        public void Enable()
        {
            _camera.Enable();
        }

        public void Initialize()
        {
            _camera.Initialize();
        }

        public void SetLocation(Vector3 position, Quaternion orientation)
        {
            _camera.SetLocation(position, orientation);
        }
    }
}
