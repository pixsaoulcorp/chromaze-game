﻿using UnityEngine;

namespace Pixsaoul.Inf.Camera.Fix
{
    [System.Serializable]
    public class IFixedCameraManagerContainer : IUnifiedContainer<IFixedCameraManager> { }

    public interface IFixedCameraManager : ICameraManager
    {
        void Initialize();
        void SetLocation(Vector3 position, Quaternion orientation);

    }
}