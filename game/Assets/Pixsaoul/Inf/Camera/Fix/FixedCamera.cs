﻿using Pixsaoul.App.Camera;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.Fix
{
    public class FixedCamera : BaseBehaviour, ICamera
    {
        [SerializeField] private UnityEngine.Camera _camera; // camera object
        [SerializeField] private GameObject _cameraRoot; // camera holder, camera should ALWAYS be under this

        public UnityEngine.Camera Camera => _camera;

        public void Disable()
        {
            _cameraRoot.SetActive(false);
        }

        public void Enable()
        {
            _cameraRoot.SetActive(true);
        }

        public void Initialize()
        {
            _camera.transform.SetParent(_cameraRoot.transform);
            Disable();
        }

        /// <summary>
        /// Set world location of camera
        /// </summary>
        /// <param name="position"></param>
        /// <param name="orientation"></param>
        public void SetLocation(Vector3 position, Quaternion orientation)
        {
            _cameraRoot.transform.position = position;
            _cameraRoot.transform.rotation = orientation;
        }
    }
}
