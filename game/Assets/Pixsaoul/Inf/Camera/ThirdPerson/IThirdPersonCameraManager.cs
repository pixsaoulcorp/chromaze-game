﻿using Pixsaoul.App;
using Pixsaoul.App.Engine;
using Pixsaoul.App.Input;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.ThirdPerson
{
    [System.Serializable]
    public class IThirdPersonCameraManagerContainer : IUnifiedContainer<IThirdPersonCameraManager> { }

    public interface IThirdPersonCameraManager : ICameraManager
    {
        void Initialize();

        void AttachTo(IAttachTarget target);

        void Rotate(Vector2 rotation);
    }
}