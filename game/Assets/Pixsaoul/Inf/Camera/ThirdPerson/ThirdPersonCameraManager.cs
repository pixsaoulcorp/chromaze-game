﻿using Pixsaoul.App.Camera;
using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.ThirdPerson
{

    public class ThirdPersonCameraManager : BaseBehaviour, IThirdPersonCameraManager
    {
        [SerializeField] private ThirdPersonCamera _camera;

        [Header("Rotation sensitivity")]
        [SerializeField] private float _rotationSensitivity;
        [SerializeField] private AnimationCurve _rotationSmoothness;
        public ICamera ManagedCamera => _camera;

        public void Initialize()
        {
            _camera.Initialize();
        }

        public void Enable()
        {
            _camera.Enable();
        }

        public void Disable()
        {
            _camera.Disable();
            _camera.Detach();
        }

        public void AttachTo(IAttachTarget target)
        {
            _camera.AttachTo(target);
        }

        private void Detach()
        {
            _camera.Detach();
        }

        //TODO TBU handle camera sensitivity here

        public void Rotate(Vector2 rotation)
        {
            float smoothedX = (rotation.x >= 0 ? 1:-1) * _rotationSmoothness.Evaluate(Mathf.Abs(rotation.x));
            float smoothedY = (rotation.y >= 0 ? 1:-1) * _rotationSmoothness.Evaluate(Mathf.Abs(rotation.y));
            _camera.Rotate(_rotationSensitivity*new Vector2(smoothedX,smoothedY));
        }
    }
}
