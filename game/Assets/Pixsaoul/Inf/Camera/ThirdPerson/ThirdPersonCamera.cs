﻿using Pixsaoul.App.Camera;
using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.ThirdPerson
{
    public class ThirdPersonCamera : BaseBehaviour, ICamera, IAttachable
    {
        [SerializeField] private UnityEngine.Camera _camera; // camera object
        [SerializeField] private GameObject _cameraRoot; // camera holder, camera should ALWAYS be under this
        [SerializeField] private IAttachTargetContainer _defaultParent; // should be a IAttachTarget

        public UnityEngine.Camera Camera => _camera;

        public void AttachTo(IAttachTarget target)
        {
            _cameraRoot.transform.SetParent(target.TargetTransform, false);
        }

        public void Detach()
        {
            _cameraRoot.transform.SetParent(_defaultParent.Result.TargetTransform, false);
        }

        public void Disable()
        {
            _cameraRoot.SetActive(false);
        }

        public void Enable()
        {
            _cameraRoot.SetActive(true);
        }

        public void Initialize()
        {
            _camera.transform.SetParent(_cameraRoot.transform);
            _camera.transform.position = -3 * Vector3.forward;
            Detach();
            Disable();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotation"></param>
        public void Rotate(Vector2 rotation)
        {
            //TODO TBU check rotation axis correctness

            _cameraRoot.transform.Rotate(Vector3.up, rotation.x, Space.World);
            _cameraRoot.transform.Rotate(Vector3.left, rotation.y, Space.Self);
           // _cameraRoot.transform.Rotate(-rotation.y, rotation.x, 0, Space.World);
           // _cameraRoot.transform.RotateAround
        }
    }
}
