﻿using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Mvcs;
using Pixsaoul.Inf.Camera.FirstPerson;
using Pixsaoul.Inf.Camera.Fix;
using Pixsaoul.Inf.Camera.Free;
using Pixsaoul.Inf.Camera.ThirdPerson;
using System;
using UnityEngine;

namespace Pixsaoul.Inf.Camera
{
    public enum CameraType
    {
        //First,
        //Third,
        Fixed,
        Free
    }
    public class CameraView : View
    {
        [SerializeField] private CameraType _initialCamera;
        private ICameraManager _currentCameraManager;

        [SerializeField] private IFirstPersonCameraManagerContainer _firstPersonCameraManagerContainer;
        [SerializeField] private IThirdPersonCameraManagerContainer _thirdPersonCameraManagerContainer;
        [SerializeField] private IFixedCameraManagerContainer _fixedCameraManagerContainer;
        [SerializeField] private IFreeCameraManagerContainer _freeCameraManagerContainer;
        
        public Signal<Action<Vector2>> RegisterRotationSignal = new Signal<Action<Vector2>>();
        public Signal UnregisterAllRotationSignal = new Signal();
        public Signal<Action<Vector2>> RegisterTranslationSignal = new Signal<Action<Vector2>>();
        public Signal UnregisterAllTranslationSignal = new Signal();

        public ICameraManager CurrentCameraManager { get => _currentCameraManager; }

        public override void Initialize()
        {
            base.Initialize();
            _firstPersonCameraManagerContainer.Result.Initialize();
            _thirdPersonCameraManagerContainer.Result.Initialize();
            _fixedCameraManagerContainer.Result.Initialize();
            _freeCameraManagerContainer.Result.Initialize();
            EnableInitialCamera();           
        }

        private void EnableInitialCamera()
        {
            if(_initialCamera == CameraType.Fixed)
            {
                EnableFixCamera(new Vector3(-2, 1.2f, -2), Quaternion.Euler(-5, 45, 0));
            }
            else if (_initialCamera == CameraType.Free)
            {
                EnableFreeCamera(new Vector3(-2, 1.2f , -2), Quaternion.Euler(-5,45,0));
            } 
            else
            {
                EnableFixCamera(new Vector3(0,0,0), Quaternion.Euler(0,0,0));
            }
        }

        public void EnableFixCamera(Vector3 position, Quaternion orientation)
        {
            IFixedCameraManager manager = _fixedCameraManagerContainer.Result;
            manager.SetLocation(position, orientation);
            manager.Enable();
            ReplaceCurrentCameraManager(manager);

        }

        public void EnableFirstPersonCamera(IAttachTarget target)
        {
            IFirstPersonCameraManager manager = _firstPersonCameraManagerContainer.Result;
            manager.AttachTo(target);
            manager.Enable();
            ReplaceCurrentCameraManager(manager);
        }

        public void EnableFreeCamera(Vector3 position, Quaternion orientation)
        {
            IFreeCameraManager manager = _freeCameraManagerContainer.Result;
            manager.SetLocation(position, orientation);
            manager.Enable();
            ReplaceCurrentCameraManager(manager);
            RegisterRotationSignal.Dispatch(manager.Rotate);
            RegisterTranslationSignal.Dispatch(manager.Translate);
        }

        public void EnableThirdPersonCamera(IAttachTarget target)
        {
            IThirdPersonCameraManager manager = _thirdPersonCameraManagerContainer.Result;
            manager.AttachTo(target);
            manager.Enable();
            ReplaceCurrentCameraManager(manager);
            RegisterRotationSignal.Dispatch(manager.Rotate);
        }

        private void ReplaceCurrentCameraManager(ICameraManager cameraManager)
        {
            if (CurrentCameraManager != null)
            {
                UnregisterAllRotationSignal.Dispatch();
                UnregisterAllTranslationSignal.Dispatch();
                CurrentCameraManager.Disable(); // TODO TBU find a clean way to handle this case, maybe setup a default manager that is initialy assign before EnableFreeCamera in Initialize.
            }
            _currentCameraManager = cameraManager;
        }
        
        public UnityEngine.Camera GetCurrentCamera()
        {
            return _currentCameraManager?.ManagedCamera?.Camera;
        }

        //TODO TBU Allow access to camera for other people to use its data

        //TODO TBU
        /* Camera expected behaviour :
         * - Allow to switch from perspective to orthographic (internally)
         * - Set a target to follow around
         * - Enable / Disable camera movement
         * - place camera at a fixed point
         * - different modes of camera to handle:
         *      - AroundTarget
         *      - TurnWithTarget
         *      - Free
         *      - Fix viewpoint
         */



        //input behaviour by camera mode
        //Free
        //To be provided : initial position, orientation
        //Input to be listened : rotation : yaw pitch (Secondary) / position : forward / side strafing (Main)
        //Fix
        //setting up fix require a position and orientation
        // maybe a perspective mode
        //Input to be listened : None
        //Tps
        //Should provide a Transform target and initial orientation
        //How to handle distance from object ? intern to camera management ? to specific use ?
        //Input to be listened : rotation, yaw pitch, no roll
        //Fps
        // Requires a Target (position, orientation)
        // in order to display stuff better, an offset might be provided (V3 in the looked direction)
        //Input to be listened : nothing. 
        // evolution, maybe a distance to see further / closer some stuff. NOT a priority, and might not be a good idea overall.

    }
}