﻿using Pixsaoul.App.Engine;
using Pixsaoul.App.Camera;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera
{
    public class FirstPersonCamera : BaseBehaviour, IAttachable, ICamera
    {
        [SerializeField] private UnityEngine.Camera _camera; // camera object
        [SerializeField] private GameObject _cameraRoot; // camera holder, camera should ALWAYS be under this
        [SerializeField] private IAttachTargetContainer _defaultParent; // default parent for camera holder, this is used to detach camera from target, 
        //and put it back in default place.

        private Vector3 _offset;

        public UnityEngine.Camera Camera => _camera;

        public void Initialize()
        {
            _camera.transform.SetParent(_cameraRoot.transform, false);
            _offset = -Vector3.forward;
            Detach();
            Disable();
        }

        public void AttachTo(IAttachTarget target)
        {
            _cameraRoot.transform.SetParent(target.TargetTransform, false);
        }

        public void Detach()
        {
            _cameraRoot.transform.SetParent(_defaultParent.Result.TargetTransform, false);
        }

        public void Enable()
        {
            _cameraRoot.SetActive(true);
        }

        public void Disable()
        {
            _cameraRoot.SetActive(false);
        }

        public void SetOffset(Vector3 offset)
        {
            _offset = offset;
            _camera.transform.position = _offset;
        }
    }
}
