﻿

using Pixsaoul.App.Camera;
using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.FirstPerson
{
    // purpose of this manager ?
    public class FirstPersonCameraManager : BaseBehaviour, IFirstPersonCameraManager
    {
        [SerializeField] private FirstPersonCamera _camera;

        private Vector3 _offset;

        public ICamera ManagedCamera => _camera;

        //TODO TBU implement

        public void Initialize()
        {
            _camera.Initialize();
        }

        public void Disable()
        {
            _camera.Disable();
            _camera.Detach();
        }

        public void Enable()
        {
            _camera.Enable();
        }

        public void AttachTo(IAttachTarget target)
        {
            _camera.AttachTo(target);
        }

        public void Detach()
        {
            _camera.Detach();
        }

        public void SetOffset(Vector3 offset)
        {
            _camera.SetOffset(offset);
        }
    }
}
