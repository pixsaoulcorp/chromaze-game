﻿using Pixsaoul.App;
using Pixsaoul.App.Camera;
using Pixsaoul.App.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Camera.FirstPerson
{
    [System.Serializable]
    public class IFirstPersonCameraManagerContainer : IUnifiedContainer<IFirstPersonCameraManager> { }

    public interface IFirstPersonCameraManager : ICameraManager
    {
        void Initialize();

        void AttachTo(IAttachTarget target);

        void SetOffset(Vector3 offset);

    }
}