﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Pixsaoul.Fou.Mvcs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Pixsaoul.Dom.Data;
using Pixsaoul.App.SceneManagement;
using Chromaze.App.SceneWorkflow;

namespace Pixsaoul.Inf.SceneManagement
{
    public class SceneManagerService : Service, ISceneManagerService
    {
        [SerializeField] private GameObject _additionalSceneRootContainer;
        private GameObject _additionalSceneRoot;

        [Inject] public OpenMenuSignal OpenMenuSignal { get; set; }
        [Inject] public OnSceneChangedSignal OnSceneChangedSignal { get; set; }
              

        private SceneId _initialLevel;

        private List<SceneId> _availableScenes;

        private SceneId _currentLoadedLevel;

        private bool _isLoadingScene = false;

        /// <summary>
        ///
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
            _initialLevel = SceneId.Menu;
            //TODO TBU find a cleaner way to references scene because using string is really uggly.
            // try to access all scenes available in build and remove LBMain ?
            //use an enum value more than string
            _availableScenes = new List<SceneId>()
            {
                SceneId.Create,
                (SceneId)"Main",
                SceneId.Menu,
                SceneId.Play
            };
            _currentLoadedLevel = (SceneId)string.Empty;
        }

        /// <summary>
        ///
        /// </summary>
        public void LoadInitialScene()
        {
            OpenMenuSignal.Dispatch();
        }

        /// <summary>
        /// Changes the level. Load a new level, and once the loading is over, unload the previous loaded level
        /// </summary>
        /// <param name="levelName">Name of the level.</param>
        public void ChangeScene(SceneId levelName)
        {
            if (_isLoadingScene)
                return;
            _isLoadingScene = true;
            bool newLevelFound = _availableScenes.Contains(levelName);
            if (newLevelFound)
            {
                Coroutine loading = StartCoroutine(ChangeLevelCoroutine(levelName));
            } else
            {
                throw new System.Exception($"Scene {levelName} not found");
            }
        }

        /// <summary>
        /// Changes the level coroutine. Previously loaded scene will be unloaded
        /// </summary>
        /// <param name="levelToLoadName">Name of the level.</param>
        /// <returns></returns>
        private IEnumerator ChangeLevelCoroutine(SceneId levelToLoadName)
        {
            
            SceneId sceneToUnload = _currentLoadedLevel;

            Coroutine loading = StartCoroutine(LoadSceneAsyncAdditive(levelToLoadName));
            yield return loading;
            _currentLoadedLevel = levelToLoadName;

            Coroutine unloading = StartCoroutine(UnloadSceneAsync(sceneToUnload));
            yield return unloading;

            OnSceneChangedSignal.Dispatch(levelToLoadName);
        }

        
        /// <summary>
        ///
        /// </summary>
        /// <param name="sceneName"></param>
        /// <returns></returns>
        private IEnumerator LoadSceneAsyncAdditive(SceneId sceneName)
        {
            yield return null;
            bool canBeLoaded = false;
            Scene scene = SceneManager.GetSceneByName(sceneName);
            if(scene != null)
            {
                canBeLoaded = !scene.isLoaded;
            }

            if (canBeLoaded)
            {
                AsyncOperation async = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
                yield return async;
            }
            _isLoadingScene = false;
        }

        /// <summary>
        /// UnloadScene
        /// </summary>
        /// <param name="sceneName"></param>
        private  IEnumerator UnloadSceneAsync(SceneId sceneName)
        {
            yield return null;
            //TODO check if sceneName really needs to be unloaded
            bool needsUnloading = false;
            Scene scene = SceneManager.GetSceneByName(sceneName);
            if(scene != null)
            {
                needsUnloading = scene.isLoaded;
            }

            if (needsUnloading)
            {
                AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(sceneName);
                yield return asyncUnload;
            }
        }
    }
}