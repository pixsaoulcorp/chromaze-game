﻿
    //internal class PixsaoulProfile : AutoMapper.Profile
    //{
    //    public PixsaoulProfile()
    //    {
    //        CreateMap<Dom.Data.Engine.Name, string>().ConvertUsing(r => r.Value);
    //        CreateMap<string, Dom.Data.Engine.Name>().ConvertUsing(f => new Dom.Data.Engine.Name(f));

    //        CreateMap<Angle, float>().ConvertUsing(r => r.Value);
    //        CreateMap<float, Angle>().ConvertUsing(f => new Angle(f));

    //        CreateMap<Length, float>().ConvertUsing(r => r.Value);
    //        CreateMap<float, Length>().ConvertUsing(f => new Length(f));

    //        CreateMap<Offset, float>().ConvertUsing(r => r.Value);
    //        CreateMap<float, Offset>().ConvertUsing(f => new Offset(f));

    //        CreateMap<SnappedLength, int>().ConvertUsing(r => r.Value);
    //        CreateMap<int, SnappedLength>().ConvertUsing(f => new SnappedLength(f));

    //        CreateMap<SnappedOffset, int>().ConvertUsing(r => r.Value);
    //        CreateMap<int, SnappedOffset>().ConvertUsing(f => new SnappedOffset(f));

    //        CreateMap<SnappedAngle, int>().ConvertUsing(r => r.Value);
    //        CreateMap<int, SnappedAngle>().ConvertUsing(f => new SnappedAngle(f));

    //        CreateMap<Id, int>().ConvertUsing(r => r.Value);
    //        CreateMap<int, Dom.Data.Engine.Id>().ConvertUsing(f=> new Id(f));   

    //        CreateMap<SnappedRotation, Persistency.Engine.SnappedRotation>();
    //        CreateMap<Orientation, Persistency.Engine.Orientation>();
    //        CreateMap<Position, Persistency.Engine.Position>();
    //        CreateMap<Rotation, Persistency.Engine.Rotation>(); ;
    //        CreateMap<SnappedDimension, Persistency.Engine.SnappedDimension>();
    //        CreateMap<SnappedPosition, Persistency.Engine.SnappedPosition>();

    //        CreateMap<Persistency.Engine.SnappedRotation, SnappedRotation>();
    //        CreateMap<Persistency.Engine.Orientation, Orientation>();
    //        CreateMap<Persistency.Engine.Position, Position>();
    //        CreateMap<Persistency.Engine.Rotation, Rotation>();
    //        CreateMap<Persistency.Engine.SnappedDimension, SnappedDimension>();
    //        CreateMap<Persistency.Engine.SnappedPosition, SnappedPosition>();

    //        CreateMap<Dom.Text, string>().ConvertUsing(r => r.Value);
    //        CreateMap<string, Dom.Text>().ConvertUsing(f => new Dom.Text(f));

    //        CreateMapShape();
    //    }

    //    #region shape

    //    private void CreateMapShape()
    //    {
    //        CreateMap<Surface, Persistency.Shape.Direction>().ReverseMap();
    //        CreateMap<SurfaceMode, Persistency.Shape.SurfaceMode>().ReverseMap();
    //        CreateMap<Boxel, Persistency.Shape.Boxel>().ReverseMap();

    //        CreateMap<BoxelShape, Persistency.Shape.BoxelShape>()
    //            .ForMember(
    //                dest => dest.SurfaceMode,
    //                opts => opts
    //                .MapFrom(src => src.SurfaceMode))
    //            .ForMember(
    //                dest => dest.Boxels,
    //                opts => opts.MapFrom<List<Persistency.Shape.BoxelLocation>>(
    //                    s => BoxelsToPersist(s.Boxels)));

    //        CreateMap<Persistency.Shape.BoxelShape, BoxelShape>()
    //            .ForMember(
    //                dest => dest.Boxels,
    //                opts => opts.MapFrom<IReadOnlyDictionary<BoxelLocation, Boxel>>(
    //                    s => BoxelsToDom(s.Boxels)));               
    //    }

    //    private List<Persistency.Shape.BoxelLocation> BoxelsToPersist(IReadOnlyDictionary<BoxelLocation, Boxel> domBoxels)
    //    {
    //        var persistboxelsLocations = new List<Persistency.Shape.BoxelLocation>();
    //        foreach (var kvp in domBoxels)
    //        {
    //            persistboxelsLocations.Add(new Persistency.Shape.BoxelLocation(kvp.Key.x, kvp.Key.y, kvp.Key.z, BoxelFromDom(kvp.Value)));
    //        }
    //        return persistboxelsLocations;
    //    }

    //    private IReadOnlyDictionary<BoxelLocation, Boxel> BoxelsToDom(List<Persistency.Shape.BoxelLocation> persistBoxel)
    //    {
    //        var domBoxelLocations = new Dictionary<BoxelLocation, Boxel>();
    //        foreach (var kvp in persistBoxel)
    //        {
    //            domBoxelLocations.Add(new BoxelLocation(kvp.X, kvp.Y, kvp.Z), BoxelToDom(kvp.Boxel)); 
    //        }
    //        return domBoxelLocations;
    //    }

    //    private Boxel BoxelToDom(Persistency.Shape.Boxel boxelPersistency)
    //    {
    //        HashSet<Surface> directions = new HashSet<Surface>();
    //        foreach (Persistency.Shape.Direction direction in boxelPersistency.Directions)
    //        {
    //            directions.Add((Surface)((int)direction));
    //        }
    //        Boxel boxel = new Boxel(directions);
    //        return boxel;
    //    }

    //    private Persistency.Shape.Boxel BoxelFromDom(Boxel boxelDom)
    //    {
    //        Persistency.Shape.Boxel boxel = new Persistency.Shape.Boxel(); 
    //        boxel.Directions = new List<Persistency.Shape.Direction>();
    //        foreach (Surface direction in boxelDom.Directions)
    //        {
    //            boxel.Directions.Add((Persistency.Shape.Direction)((int)direction));
    //        }
    //        return boxel;
    //    }
