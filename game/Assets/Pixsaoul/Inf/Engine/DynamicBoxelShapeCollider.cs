﻿using Pixsaoul.App.BoxelGeneration;
using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Engine;
using System;
using System.Reactive;
using UnityEngine;

namespace Pixsaoul.Inf.Engine
{
    public class DynamicBoxelShapeCollider : BaseBehaviour, IInitializable
    {
        [SerializeField] private MeshFilter _meshFilter;
        [SerializeField] private MeshCollider _collider;
        [SerializeField] private IMeshProviderProviderContainer _meshProviderProvider; // the provider of meshprovider because the meshes can change
        // and the mesh provider too.

        private IObserver<IMeshProvider> _meshProviderObserver;

        public void Initialize()
        {
            _meshProviderObserver = Observer.Create<IMeshProvider>(OnMeshProviderChanged);
            _meshProviderProvider.Result.ObservableMeshProvider.Subscribe(_meshProviderObserver);
        }

        private void OnMeshProviderChanged(IMeshProvider meshProvider)
        {         
            meshProvider.ObservableMesh.Subscribe(OnMeshChanged);
        }

        private void OnMeshChanged(Mesh mesh)
        {
            _meshFilter.mesh = mesh;
            _collider.sharedMesh = mesh;
        }
    }
}