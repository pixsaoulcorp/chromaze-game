﻿using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Engine;
using UnityEngine;

public class AttachTarget : BaseBehaviour, IAttachTarget
{
    [SerializeField] private Transform _target;

    public Transform TargetTransform => _target;
}
