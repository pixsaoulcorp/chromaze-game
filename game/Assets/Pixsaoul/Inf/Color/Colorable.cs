﻿using Pixsaoul.App.Color;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.Color
{
    public class Colorable : BaseBehaviour, IColorable
    {
        [SerializeField] private Renderer _meshRenderer;

        public void Colorize(UnityEngine.Color color)
        {
            MaterialPropertyBlock props = new MaterialPropertyBlock();
            float alpha = _meshRenderer.material.color.a;
            color.a = alpha;
            _meshRenderer.material.color = color;

            //props.SetColor("_Color", color);
            //_meshRenderer.SetPropertyBlock(props);
        }
    }
}
