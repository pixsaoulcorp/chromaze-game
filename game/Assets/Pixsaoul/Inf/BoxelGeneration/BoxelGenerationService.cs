﻿using Pixsaoul.App.BoxelGeneration;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Extensions;
using Pixsaoul.Fou.Mvcs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pixsaoul.Inf.BoxelGeneration
{
    public class BoxelGenerationService : Service, IBoxelGenerationService
    {
        [SerializeField] private BoxelObject _internBoxelObject;
        [SerializeField] private BoxelObject _externBoxelObject;

        [SerializeField] private BoxelShapeObject _rootShapePrefab;

        [SerializeField] private Transform _generationRoot;

        public BoxelShapeObject Create(IBoxelShape shape)
        {
            SurfaceMode surfaceMode = shape.SurfaceMode;
            BoxelShapeObject boxelShapeObject = Instantiate<BoxelShapeObject>(_rootShapePrefab, _generationRoot);
            IReadOnlyDictionary<BoxelLocation, Boxel> boxels = shape.Boxels;
            foreach(var kvp in boxels)
            {
                BoxelObject boxelObject = CreateBoxel(kvp.Key, kvp.Value, surfaceMode, boxelShapeObject.transform );
            }
            StartCoroutine(FilterContainer(boxelShapeObject));
            return boxelShapeObject;
        }

        private BoxelObject CreateBoxel(BoxelLocation pos, Boxel boxel,SurfaceMode surfaceMode, Transform parent)
        {
            BoxelObject boxelPrefab = _externBoxelObject;
            if (surfaceMode == SurfaceMode.Extern)
            {
                boxelPrefab = _externBoxelObject;
            }
            else if (surfaceMode == SurfaceMode.Intern)
            {
                boxelPrefab = _internBoxelObject;
            }

            BoxelObject boxelObject = Instantiate<BoxelObject>(boxelPrefab, parent);
            boxelObject.transform.localPosition = pos.ToVector3();
            boxelObject.Initialize(boxel);
            return boxelObject;
        }

        private IEnumerator<WaitForSeconds> FilterContainer(BoxelShapeObject container)
        {
            yield return new WaitForSeconds(0f);
            MeshFilter[] meshFilters = container.GetComponentsInChildren<MeshFilter>();
            CombineInstance[] combine = new CombineInstance[meshFilters.Length];

            int i = 0;
            while (i < meshFilters.Length)
            {
                combine[i].mesh = meshFilters[i].sharedMesh;
                Vector3 position = container.transform.InverseTransformPoint(meshFilters[i].transform.position) ;
                combine[i].transform = Matrix4x4.Translate(position) * Matrix4x4.Rotate(meshFilters[i].transform.localRotation);
                i++;
            }
            container.transform.GetComponent<MeshFilter>().mesh = new Mesh();
            container.transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine, true);
            container.transform.GetComponent<MeshCollider>().sharedMesh = container.transform.GetComponent<MeshFilter>().mesh;
            container.transform.DeleteChildren();
            container.transform.gameObject.SetActive(true);

            container.OnMeshChanged();
        }
    }
}
