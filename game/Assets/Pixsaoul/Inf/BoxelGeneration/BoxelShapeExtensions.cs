﻿using Pixsaoul.Dom.Shape;
using System.Collections.Generic;

namespace Pixsaoul.Inf.BoxelGeneration
{
    public static class BoxelShapeExtensions
    {
        public static void AddSurface(this BoxelShape shape, int x, int y, int z, Surface surface)
        {
            Boxel boxel;
            if (shape.TryGetBoxel(x, y, z, out boxel))
            {
                boxel.AddSurface(surface);
            }
            else
            {
                boxel = new Boxel(new HashSet<Surface>() { surface });
                shape.SetBoxel(x,y,z,boxel);
            }
        }

        public static BoxelShape CreateSimpleShape(int width, int height, int depth, SurfaceMode mode)
        {
            BoxelShape shape = new BoxelShape(mode);

            //Up down
            for (int x = 0; x < width; x++)
            {
                for (int z = 0; z < depth; z++)
                {
                    // Up
                    shape.AddSurface(x, height - 1, z, Surface.Up);
                    // Down
                    shape.AddSurface(x, 0, z, Surface.Down);
                }
            }

            //Left,
            //Right,
            for (int y = 0; y < height; y++)
            {
                for (int z = 0; z < depth; z++)
                {
                    //Left
                    shape.AddSurface(0, y, z, Surface.Left);
                    //Right
                    shape.AddSurface(width - 1, y, z, Surface.Right);

                }
            }

            //Back,
            //Front
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    //Back
                    shape.AddSurface(x, y, 0, Surface.Back);
                    //Front
                    shape.AddSurface(x, y, depth - 1, Surface.Front);
                }
            }

            return shape;
        }
    }
}