﻿using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.PhysicsEngine
{
    public class HorizontalDrag : BaseBehaviour
    {
        [SerializeField] private Rigidbody _rigidbody;
        [Header("lower than 1")]
        [SerializeField] private float _customDrag;

        private void Start()
        {
            if (_customDrag > 1) _customDrag = 1;
            if (_customDrag < 0) _customDrag = 0;
        }

        private void FixedUpdate()
        {
            ApplyDragForce();
        }

        private void ApplyDragForce()
        { // this is not clean but it allows to have a good control on the player without messing with gravity.
            // TODO TBU investigate a solution with force application instead of velocity manipulation
            // definitely not a priority ;)
            Vector3 customHorizontalVelocity = _rigidbody.velocity;
            customHorizontalVelocity.y = 0;
            customHorizontalVelocity = customHorizontalVelocity * (1 - _customDrag);
            _rigidbody.velocity = new Vector3(customHorizontalVelocity.x, _rigidbody.velocity.y, customHorizontalVelocity.z);
        }
    }
}