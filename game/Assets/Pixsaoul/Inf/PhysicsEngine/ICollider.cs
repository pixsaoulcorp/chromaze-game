﻿using System;

namespace Pixsaoul.Inf.PhysicsEngine
{
    [System.Serializable]
    public class IColliderContainer : IUnifiedContainer<ICollider> { }

    public interface ICollider
    {
        bool IsColliding();
    }
}
