﻿
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Inf.PhysicsEngine
{
    public class Collidable : BaseBehaviour, ICollider
    {
        [SerializeField] private SphereCollider _collider;
        [SerializeField] private LayerMask _envMask;

        public bool IsColliding()
        {
           Collider[] colliders = Physics.OverlapSphere(this.transform.position, _collider.radius, _envMask);
            return colliders.Length > 0;
        }
    }
}
