﻿namespace Pixsaoul.App.Focus
{
    interface IFocusable
    {
        void Focus();
    }
}
