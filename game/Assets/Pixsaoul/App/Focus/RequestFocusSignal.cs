﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.Focus
{
    public class RequestFocusSignal : Signal { }
}
