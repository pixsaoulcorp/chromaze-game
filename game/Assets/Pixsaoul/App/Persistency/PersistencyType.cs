﻿namespace Pixsaoul.App.Persistency
{
    public enum PersistencyType
    {
        Internal,
        Local,
        Remote
    }
}
