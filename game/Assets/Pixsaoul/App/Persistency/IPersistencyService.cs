﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.Persistency
{

    [System.Serializable]
    public class IPersistencyServiceContainer : IUnifiedContainer<IPersistencyService> { }

    /// <summary>
    /// IO service interface
    /// </summary>
    public interface IPersistencyService : IService
    {
        /// <summary>
        /// Write file
        /// </summary>
        /// <param name="data"></param>
        /// <returns>saved file path</returns>
        void Write(object data, string filePath);


        /// <summary>
        /// Read file at path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        T Read<T>(string path) where T : class, new();
    }
}