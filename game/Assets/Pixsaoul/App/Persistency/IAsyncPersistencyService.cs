﻿using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;
using System;

namespace Pixsaoul.App.Persistency
{
    [System.Serializable]
    public class IAsyncPersistencyServiceContainer : IUnifiedContainer<IAsyncPersistencyService> { }

    public interface IAsyncPersistencyService : IService
    {
        void RequestWrite(object data, EndPoint endPoint);

        void RequestRead<T>(EndPoint endPoint, Action<T> callback) where T : class, new();
    }
}
