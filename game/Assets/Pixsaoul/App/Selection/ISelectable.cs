﻿using System;
using UnityEngine;

namespace Pixsaoul.App.Selection
{
    [Serializable]
    public class ISelectableContainer : IUnifiedContainer<ISelectable> { }

    public interface ISelectable 
    {
        void SetSelectionCallback(Action selectCallback);

        void Select();

        Transform SelectionLocation { get; }
    }
}
