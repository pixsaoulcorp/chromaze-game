﻿using System;
using System.Reactive.Subjects;
using UnityEngine;

namespace Pixsaoul.App.Selection
{
    [Serializable]
    public class ISelectorContainer : IUnifiedContainer<ISelector> { }

    public interface ISelector
    {
        void Initialize();
        bool TryApplySelection();
    }
}
