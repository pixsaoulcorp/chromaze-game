﻿using System;
using UnityEngine;

namespace Pixsaoul.App.Selection
{
    [Serializable]
    public class IObservableSelectorContainer : IUnifiedContainer<IObservableSelector> { }

    public interface IObservableSelector
    {
        void SubscribeTarget(IObserver<Transform> selectionObserver);
    }
}
