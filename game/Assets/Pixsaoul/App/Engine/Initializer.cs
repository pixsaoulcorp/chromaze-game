﻿using Pixsaoul.Fou.Engine;
using System.Collections.Generic;
using UnityEngine;

public class Initializer : BaseBehaviour, IInitializable
{
    [SerializeField] private List<IInitializableContainer> _subParts;

    public void Initialize()
    {
        foreach (IInitializableContainer initializableContainer in _subParts)
        {
            initializableContainer.Result.Initialize();
        }
    }
}
