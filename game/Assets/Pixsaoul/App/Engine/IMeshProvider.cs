﻿using System;
using UnityEngine;

namespace Pixsaoul.App.Engine
{
    /// <summary>
    /// Interface that provides an observable Mesh to be used.
    /// </summary>
    [Serializable]
    public class IMeshProviderContainer : IUnifiedContainer<IMeshProvider> { }

    public interface IMeshProvider
    {
        IObservable<Mesh> ObservableMesh { get; }
    }
}
