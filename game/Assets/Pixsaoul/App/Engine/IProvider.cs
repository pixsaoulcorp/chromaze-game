﻿using System;

namespace Pixsaoul.App.Engine
{
    public interface IProvider<T>
    {
        IObservable<T> ObservableData { get; }
    }
}
