﻿
using Pixsaoul.Dom.Data.Engine;
using System;

namespace Pixsaoul.App.Engine
{
    [Serializable]
    public class ISizeableContainer : IUnifiedContainer<ISizeable> { }

    public interface ISizeable
    {
        void Resize(SnappedDimension dimensions);
    }
}