﻿using Pixsaoul.Dom.Data.Engine;
using System;
using UnityEngine;

namespace Assets.Pixsaoul.App.Engine
{
    [Serializable]
    public class IOrientableContainer : IUnifiedContainer<IOrientable> { }

    public interface IOrientable
    {
        Orientation GetDirection();
        void SetDirection(Orientation direction);
    }
}
