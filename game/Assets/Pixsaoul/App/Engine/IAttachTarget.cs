﻿using System;
using UnityEngine;

namespace Pixsaoul.App.Engine
{
    [Serializable]
    public class IAttachTargetContainer : IUnifiedContainer<IAttachTarget> { }

    public interface IAttachTarget
    {
        Transform TargetTransform { get; }
    }
}