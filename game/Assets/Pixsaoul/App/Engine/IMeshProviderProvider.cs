﻿using System;

namespace Pixsaoul.App.Engine
{
    /// <summary>
    /// Interface that must provide an observable MeshProvider, to listen to items that have mesh providers which may change.
    /// </summary>
    [Serializable]
    public class IMeshProviderProviderContainer : IUnifiedContainer<IMeshProviderProvider> { }

    public interface IMeshProviderProvider
    {
        IObservable<IMeshProvider> ObservableMeshProvider { get; }
    }
}
