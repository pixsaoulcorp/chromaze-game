﻿using System;

namespace Pixsaoul.App.Engine
{
    public class IOpenableContainer : IUnifiedContainer<IOpenable> { }

    public interface IOpenable
    {
        void Open();
        void Close();
    }
}
