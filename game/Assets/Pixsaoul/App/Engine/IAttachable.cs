﻿namespace Pixsaoul.App.Engine
{
    public interface IAttachable
    {
        void AttachTo(IAttachTarget target);

        void Detach();
    }
}
