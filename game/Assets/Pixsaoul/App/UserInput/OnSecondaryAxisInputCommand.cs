﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.App.UserInput
{
    public class OnSecondaryAxisInputCommand : Command
    {
        [Inject] public NotifySecondaryAxisInputSignal NotifySecondaryAxisInputSignal { get; set; }
        [Inject] public Vector2 AxisInput { get; set; }

        public override void Execute()
        {
            NotifySecondaryAxisInputSignal.Dispatch(AxisInput);
        }
    }
}
