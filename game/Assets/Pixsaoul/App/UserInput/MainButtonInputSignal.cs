﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.UserInput
{
    public class MainButtonInputSignal : Signal{ }
    public class NotifyMainButtonInputSignal : Signal{ }
}
