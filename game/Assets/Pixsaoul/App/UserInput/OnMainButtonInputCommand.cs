﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.UserInput
{
    public class OnMainButtonInputCommand : Command
    {
        [Inject] public NotifyMainButtonInputSignal NotifyMainButtonInputSignal { get; set; }

        public override void Execute()
        {
            NotifyMainButtonInputSignal.Dispatch();
        }
    }
}
