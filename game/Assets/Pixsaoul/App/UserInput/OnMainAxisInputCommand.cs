﻿using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.App.UserInput
{
    public class OnMainAxisInputCommand : Command
    {
        [Inject] public NotifyMainAxisInputSignal NotifyMainAxisInputSignal { get; set; }


        [Inject] public Vector2 AxisInput { get; set; }

        public override void Execute()
        {
            NotifyMainAxisInputSignal.Dispatch(AxisInput);

        }
    }
}
