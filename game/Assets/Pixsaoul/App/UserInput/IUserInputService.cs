﻿using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using Pixsaoul.Fou.Mvcs;
using System;
using UnityEngine;

namespace Pixsaoul.App.Input
{

    [System.Serializable]
    public class IUserInputServiceContainer : IUnifiedContainer<IUserInputService> { }

    public interface IUserInputService : IService, IEnablable
    {

    }
}
