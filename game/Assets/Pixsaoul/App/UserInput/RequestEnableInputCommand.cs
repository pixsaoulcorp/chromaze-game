﻿using Pixsaoul.App.Input;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.App.UserInput
{
    public class RequestEnableInputCommand : Command
    {
        [Inject] public IUserInputService UserInputService { get; set; }

        [Inject] public bool Enable { get; set; }

        public override void Execute()
        {
            if (Enable)
            {
                UserInputService.Enable();
            }
            else
            {
                UserInputService.Disable();
            }
        }
    }
}
