﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.UserInput
{
    public class SecondaryButtonInputSignal : Signal { }
    public class NotifySecondaryButtonInputSignal : Signal { }
}
