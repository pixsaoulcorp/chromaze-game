﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.App.UserInput
{
    public class SecondaryAxisInputSignal : Signal<Vector2> { }
    public class NotifySecondaryAxisInputSignal : Signal<Vector2> { }
}
