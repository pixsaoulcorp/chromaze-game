﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.App.UserInput
{
    public class MainAxisInputSignal : Signal<Vector2> { }
    public class NotifyMainAxisInputSignal : Signal<Vector2> { }
}
