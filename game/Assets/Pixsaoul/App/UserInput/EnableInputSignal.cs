﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.UserInput
{
    public class EnableInputSignal : Signal<bool> { }
}
