﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.UserInput
{
    public class OnSecondaryButtonInputCommand : Command
    {
        [Inject] public NotifySecondaryButtonInputSignal NotifySecondaryButtonInputSignal { get; set; }

        public override void Execute()
        {
            NotifySecondaryButtonInputSignal.Dispatch();
        }
    }
}
