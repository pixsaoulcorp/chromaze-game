﻿using Pixsaoul.App.SceneTransition;
using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.SceneManagement
{
    public class RequestLoadSceneCommand : Command
    {
        [Inject]
        public ISceneManagerService sceneManagerService { get; set; }

        [Inject] public SceneId id { get; set; }

        /// <summary>
        /// Execute method to override when creating a command
        /// call base.Execute() after your specific behaviour
        /// </summary>
        public override void Execute()
        {
            sceneManagerService.ChangeScene(id);
        }

    }
}