﻿using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.SceneManagement
{
    public class RequestChangeSceneSignal : Signal<SceneId> { }
}
