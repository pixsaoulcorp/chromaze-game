﻿namespace Pixsaoul.App.Network
{
    enum ErrorCode
    {
        Nothing = 0,
        Continue = 100,
        SwitchingProtocol = 101,
        Processing = 102,
        EarlyHints = 103,

        Ok = 200,
        Created = 201,
        Accepted = 202,

        NoContent = 204,

        BadRequest = 400,
        Unauthorized = 401,



    }
}
