﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.App.Camera
{
    public class SelectFixedCameraSignal : Signal<Vector3, Quaternion> { }
}
