﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.App.Camera
{
    public class SelectFreeCameraSignal : Signal<Vector3, Quaternion> { }
}
