﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.Camera
{
    public class NotifyCameraProviderSignal : Signal<Pixsaoul.App.Camera.CameraDelegate.GetCameraDelegate> { }
}
