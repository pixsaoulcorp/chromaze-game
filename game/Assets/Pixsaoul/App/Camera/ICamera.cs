﻿namespace Pixsaoul.App.Camera
{
    public interface ICamera
    {
        void Initialize();
        void Enable();
        void Disable();

        //This part is not to be used lightly, No found way to access viewport informations without passing the
        // unity camera through the interface, otherwise, have to recode the whole camera implementation
        // into this interface.
        UnityEngine.Camera Camera { get; }
    }
}
