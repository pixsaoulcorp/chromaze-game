﻿using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.Camera
{
    public class SelectThirdPersonCameraSignal : Signal<IAttachTarget> { }
}
