﻿using Pixsaoul.Fou.Mvcs;


namespace Pixsaoul.App.Serialization
{
    [System.Serializable]
    public class ISerializerServiceContainer : IUnifiedContainer<ISerializerService> { }


    public interface ISerializerService : IService
    {
        string Serialize(object obj);

        T Deserialize<T>(string serializedObject) where T : class, new();
    }
}
