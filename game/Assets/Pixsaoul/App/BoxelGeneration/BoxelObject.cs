﻿using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.App.BoxelGeneration
{
    public class BoxelObject : BaseBehaviour
    {
        // desiredRoot = this.transform (useless)
        [SerializeField] private GameObject _upSurface;
        [SerializeField] private GameObject _downSurface;
        [SerializeField] private GameObject _leftSurface;
        [SerializeField] private GameObject _rightSurface;
        [SerializeField] private GameObject _backSurface;
        [SerializeField] private GameObject _frontSurface;        

        /// <summary>
        /// Can only be called once on a boxel. should not be an issue anyway
        /// </summary>
        /// <param name="boxel"></param>
        public void Initialize(Boxel boxel)
        {
            
            // enable present surfaces
            foreach (Surface direction in boxel.Directions)
            {
                switch (direction)
                {
                    case (Surface.Up):
                        Instantiate(_upSurface, this.transform);
                        break;
                    case (Surface.Down):
                        Instantiate(_downSurface, this.transform);
                        break;
                    case (Surface.Left):
                        Instantiate(_leftSurface, this.transform);
                        break;
                    case (Surface.Right):
                        Instantiate(_rightSurface, this.transform);
                        break;
                    case (Surface.Back):
                        Instantiate(_backSurface, this.transform);
                        break;
                    case (Surface.Front):
                        Instantiate(_frontSurface, this.transform);
                        break;
                }
            }
        }
    }
}
