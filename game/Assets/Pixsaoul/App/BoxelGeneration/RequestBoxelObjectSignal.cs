﻿using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Mvcs;
using System;
using UnityEngine;

namespace Pixsaoul.App.BoxelGeneration
{
    public class RequestBoxelObjectSignal : Signal<IBoxelShape, Action<BoxelShapeObject>> { }
}
