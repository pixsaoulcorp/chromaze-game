﻿using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Engine;
using System;
using System.Reactive.Subjects;
using UnityEngine;

namespace Pixsaoul.App.BoxelGeneration
{
    public class BoxelShapeObject : BaseBehaviour, IMeshProvider
    {
        [SerializeField] private MeshFilter _meshFilter;
        [SerializeField] private MeshRenderer _meshRenderer;

        private BehaviorSubject<Mesh> _meshSubject; // DO NOT USE THIS 

        private BehaviorSubject<Mesh> MeshSubject // USE THIS instead to ensure that the subject is created
        {
            get
            {
                if (_meshSubject == null)
                {
                    _meshSubject = new BehaviorSubject<Mesh>(GetMesh());
                }
                return _meshSubject;
            }
        }
        public IObservable<Mesh> ObservableMesh { get => MeshSubject; }

        private Mesh GetMesh()
        {              
            return _meshFilter.mesh;
        }

        /// <summary>
        /// To call every time the mesh data is changed.
        /// </summary>
        public void OnMeshChanged()
        {
            MeshSubject.OnNext(GetMesh());
        }

        public void SetMaterial(Material material)
        {
            _meshRenderer.material = material;
        }
    }
}
