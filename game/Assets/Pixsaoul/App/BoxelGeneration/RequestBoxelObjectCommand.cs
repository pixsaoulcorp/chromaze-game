﻿using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Mvcs;
using System;
using UnityEngine;

namespace Pixsaoul.App.BoxelGeneration
{
    public class RequestBoxelObjectCommand : Command
    {
        [Inject] public IBoxelGenerationService BoxelGenerationService { get; set; }
        [Inject] public IBoxelShape Shape { get; set; }
        [Inject] public Action<BoxelShapeObject> Callback { get; set; }

        public override void Execute()
        {
           BoxelShapeObject boxelObject = BoxelGenerationService.Create(Shape);
           Callback.Invoke(boxelObject);
        }
    }
}
