﻿
using Pixsaoul.Dom.Shape;
using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.BoxelGeneration
{
    [System.Serializable]
    public class IBoxelGenerationServiceContainer : IUnifiedContainer<IBoxelGenerationService> { }

    public interface IBoxelGenerationService : IService
    {
         BoxelShapeObject Create(IBoxelShape shape); //TODO add abstraction to not transmit game object
    }
}