﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.SceneTransition
{
    public class RequestFadeOutSignal : Signal { }
}
