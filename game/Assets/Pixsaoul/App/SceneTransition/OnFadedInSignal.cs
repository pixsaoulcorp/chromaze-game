﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.SceneTransition
{
    public class OnFadedInSignal : Signal { }
}
