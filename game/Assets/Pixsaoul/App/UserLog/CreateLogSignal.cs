﻿using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.App.UserLog
{
    public class CreateLogSignal : Signal<Log> { }
}