﻿using Pixsaoul.Fou.Markers;
using System;

namespace Pixsaoul.App.UserLog
{
    public class Log : IEquatable<Log>, IValueObject
    {
        #region DATA

        private readonly string _title;
        private readonly string _content;

        #endregion

        #region CTORS

        public Log(string title, string content)
        {
            _title = title;
            _content = content;
        }

        #endregion

        public string Title => _title;
        public string Content => _content;

        public bool SameValueAs(Log other)
        {
            return Title.Equals(other.Title) && Content.Equals(other.Content);
        }

        public bool Equals(Log other)
        {
            return Content == other.Content;
        }

        public override int GetHashCode()
        {
            return _content.GetHashCode();
        }
    }
}
