﻿using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.App
{
    /// <summary>
    /// Request exit command.
    /// </summary>
    public class RequestExitCommand : Command
    {       
        public override void Execute()
        {         
            //TODO TBU implement exit of application
            //if (Debug.isDebugBuild)
            //{
                Debug.Log("Exiting application");
                Application.Quit();
#if UNITY_EDITOR
                // Application.Quit() does not work in the editor so
                // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
                UnityEditor.EditorApplication.isPlaying = false;
#endif
            //}
        }
    }
}