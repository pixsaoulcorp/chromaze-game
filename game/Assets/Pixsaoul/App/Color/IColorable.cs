﻿using System;

namespace Pixsaoul.App.Color
{
    [Serializable]
    public class IColorableContainer : IUnifiedContainer<IColorable> { }

    public interface IColorable
    {
        void Colorize(UnityEngine.Color color);
    }
}
