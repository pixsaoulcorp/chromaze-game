﻿using NUnit.Framework;
using System.Collections;
using UnityEngine;
using DomEngine = Pixsaoul.Dom.Data.Engine;

namespace Pixsaoul.Tests.Dom.Data.Engine
{
    [TestFixture]
    public class IdUT : MonoBehaviour
    {
        [Test]
        [TestCaseSource(typeof(IdCaseFactory), "EqualCases")]
        public void CheckEquality(DomEngine.Id a, DomEngine.Id b)
        {
            Assert.IsTrue(a.Equals(b));
            Assert.IsTrue(b.Equals(a));
        }
    }

    public class IdCaseFactory
    {
        public static IEnumerable EqualCases
        {
            get
            {
                for (int i = 0; i < 5; i++)
                {
                    DomEngine.Id id = Random.Range(0,10000);
                    yield return new TestCaseData(id, id);
                }
            }
        }
    }
}