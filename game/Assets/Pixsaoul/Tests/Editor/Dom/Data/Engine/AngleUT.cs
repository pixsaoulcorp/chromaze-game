using NUnit.Framework;
using DomEngine = Pixsaoul.Dom.Data.Engine;
using Persist = Pixsaoul.Inf.Persistency.Engine;
using System.Collections;
using Pixsaoul.Inf.Serialization;

namespace Pixsaoul.Tests.Dom.Data.Engine
{
    [TestFixture]
    public class AngleUT
    {

        [Test]
        [TestCaseSource(typeof(AngleCaseFactory), "EqualCases")]
        public void CheckEquality(DomEngine.Angle a, DomEngine.Angle b)
        {
            Assert.IsTrue(a.Equals(b));
            Assert.IsTrue(b.Equals(a));
        }

        [Test]
        [TestCaseSource(typeof(AngleCaseFactory), "DifferentCases")]
        public void CheckDifference(DomEngine.Angle a, DomEngine.Angle b)
        {
            Assert.IsFalse(a.Equals(b));
            Assert.IsFalse(b.Equals(a));
        }

        [Test]
        [TestCaseSource(typeof(AngleCaseFactory), "AngleCases")]
        public void SerializationUT(DomEngine.Angle value)
        {           
            float persist = (float)value;
            DomEngine.Angle outDom = (DomEngine.Angle)persist;
            Assert.AreEqual(outDom, value);
        }

        [Test]
        [TestCaseSource(typeof(AngleCaseFactory), "PeriodicityCases")]
        public void AnglePeriodicityUT(DomEngine.Angle value, int turnQuantity)
        {
            DomEngine.Angle nextAngle = new DomEngine.Angle(value + turnQuantity*360);
            Assert.AreEqual((DomEngine.Angle)value, nextAngle);
        }
    }

    public class AngleCaseFactory
    {
        public static IEnumerable EqualCases
        {
            get
            {
                yield return new TestCaseData((DomEngine.Angle)12, (DomEngine.Angle)12);
                yield return new TestCaseData((DomEngine.Angle)0, (DomEngine.Angle)0);
                yield return new TestCaseData((DomEngine.Angle) (-3.5), (DomEngine.Angle) (- 3.5));
            }
        }

        public static IEnumerable DifferentCases
        {
            get
            {
                yield return new TestCaseData((DomEngine.Angle)0, (DomEngine.Angle)3);
                yield return new TestCaseData((DomEngine.Angle) (-894), (DomEngine.Angle)6.89);
                yield return new TestCaseData((DomEngine.Angle) (-0), (DomEngine.Angle)789456123);
                yield return new TestCaseData((DomEngine.Angle)1, (DomEngine.Angle)2);
            }
        }

        public static IEnumerable AngleCases
        {
            get
            {
                yield return new TestCaseData((DomEngine.Angle)0);
                yield return new TestCaseData((DomEngine.Angle)1.2);
                yield return new TestCaseData((DomEngine.Angle)1);
                yield return new TestCaseData((DomEngine.Angle)95);
                yield return new TestCaseData((DomEngine.Angle)(-8956));
                yield return new TestCaseData((DomEngine.Angle)(-0));
                yield return new TestCaseData((DomEngine.Angle)546645354);
            }
        }

        public static IEnumerable PeriodicityCases
        {
            get
            {
                yield return new TestCaseData((DomEngine.Angle)0, 3);
                yield return new TestCaseData((DomEngine.Angle)1.5, 1);
                yield return new TestCaseData((DomEngine.Angle)1,89);
                yield return new TestCaseData((DomEngine.Angle)95, 1000);
                yield return new TestCaseData((DomEngine.Angle)(-8956), 0);
                yield return new TestCaseData((DomEngine.Angle)(-0), 0);
                yield return new TestCaseData((DomEngine.Angle)546645354, 999);
            }
        }
    }
}
