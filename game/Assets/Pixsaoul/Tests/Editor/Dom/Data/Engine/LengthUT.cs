﻿using NUnit.Framework;
using DomEngine = Pixsaoul.Dom.Data.Engine;

namespace Pixsaoul.Tests.Dom.Data.Engine
{
    public class LengthUT
    {
        [Test]
        [TestCase(1.5f)]
        [TestCase(30f)]
        public void SerializationUT(float value)
        {
           
            DomEngine.Length dom = (DomEngine.Length)(value);
            float persist = (float)dom;
            DomEngine.Length outDom = (DomEngine.Length)persist;
            Assert.AreEqual(outDom, dom);
        }

        [Test]
        [TestCase(10,26)]
        public void EqualityComparison(float a, float b)
        {
            DomEngine.Length dom = new DomEngine.Length(a);
            DomEngine.Length dom2 = new DomEngine.Length(a);
            DomEngine.Length dom3 = new DomEngine.Length(b);
            Assert.AreEqual(dom, dom2);
            Assert.AreNotEqual(dom, dom3);
        }
    }

}