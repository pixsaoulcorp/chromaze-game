﻿using NUnit.Framework;
using System.Collections;

namespace Pixsaoul.Tests.Dom.Data.Engine
{
    [TestFixture]
    public class IntensityUT
    {

        [Test]
        [TestCaseSource(typeof(IntensityCaseFactory), "EqualCases")]
        public void CheckEquality(Intensity a, Intensity b)
        {
            Assert.IsTrue(a.Equals(b));
            Assert.IsTrue(b.Equals(a));
        }

        [Test]
        [TestCaseSource(typeof(IntensityCaseFactory), "DifferentCases")]
        public void CheckDifference(Intensity a, Intensity b)
        {
            Assert.IsFalse(a.Equals(b));
            Assert.IsFalse(b.Equals(a));
        }

        [Test]
        [TestCaseSource(typeof(IntensityCaseFactory), "IntensityCases")]
        public void SerializationUT(Intensity value)
        {
            float persist = (float)value;
            Intensity outDom = (Intensity)value;
            Assert.AreEqual(outDom, value);
        }

    }

    public class IntensityCaseFactory
    {
        public static IEnumerable EqualCases
        {
            get
            {
                yield return new TestCaseData((Intensity)12, (Intensity)12);
                yield return new TestCaseData((Intensity)0, (Intensity)0);
                yield return new TestCaseData((Intensity)(-3.5), (Intensity)(-3.5));
                yield return new TestCaseData((Intensity)(-3.5), (Intensity)(-50));
                yield return new TestCaseData((Intensity)(200), (Intensity)(100));
            }
        }

        public static IEnumerable DifferentCases
        {
            get
            {
                yield return new TestCaseData((Intensity)0, (Intensity)3);
                yield return new TestCaseData((Intensity)(-894), (Intensity)6.89);
                yield return new TestCaseData((Intensity)(-0), (Intensity)789456123);
                yield return new TestCaseData((Intensity)1, (Intensity)2);
            }
        }

        public static IEnumerable IntensityCases
        {
            get
            {
                yield return new TestCaseData((Intensity)0);
                yield return new TestCaseData((Intensity)1.2);
                yield return new TestCaseData((Intensity)1);
                yield return new TestCaseData((Intensity)95);
                yield return new TestCaseData((Intensity)(-8956));
                yield return new TestCaseData((Intensity)(-0));
                yield return new TestCaseData((Intensity)546645354);
            }
        }
    }
}
