﻿using NUnit.Framework;
using Pixsaoul.Dom.Data.Engine;
using DomEngine = Pixsaoul.Dom.Data.Engine;
using Persist = Pixsaoul.Inf.Persistency.Engine;

namespace Pixsaoul.Tests.Editor.Dom.Data.Engine
{
    public class OrientationUT 
    {
        [Test]
        public void SerializationUT()
        {
            Orientation dom = new Orientation((Angle)90, (Angle)0);
            Persist.Orientation persist = new Persist.Orientation(dom);
            //TODO TBU find a way to test actual string convertion from IOService
            Orientation outDom = persist.ToDom();
            Assert.AreEqual(outDom, dom);
        }

        [Test]
        [TestCase(10, 26)]
        public void EqualityComparison(float a, float b)
        {
            DomEngine.Length dom = new DomEngine.Length(a);
            DomEngine.Length dom2 = new DomEngine.Length(a);
            DomEngine.Length dom3 = new DomEngine.Length(b);
            Assert.AreEqual(dom, dom2);
            Assert.AreNotEqual(dom, dom3);
        }

    }
}
