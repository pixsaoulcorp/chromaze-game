﻿using NUnit.Framework;
using Pixsaoul.Dom.Shape;
using System.Collections;
using System.Collections.Generic;

namespace Pixsaoul.Tests.Dom.Shape
{
    [TestFixture]
    public class BoxelUT
    {
        [Test]
        [TestCaseSource(typeof(BoxelCases), "AddSuccess")]
        public void AddSurface(Boxel boxel, Surface direction)
        {
            boxel.AddSurface(direction);
            IReadOnlyCollection<Surface> directions = boxel.Directions;
            bool found = false;
            foreach (Surface dir in directions)
            {
                if (dir == direction)
                {
                    found = true;
                }
            }
            Assert.IsTrue(found);
        }

        [Test]
        [TestCaseSource(typeof(BoxelCases), "RemoveSuccess")]
        public void RemoveSurface(Boxel boxel, Surface direction)
        {
            boxel.RemoveDirection(direction);
            IReadOnlyCollection<Surface> directions = boxel.Directions;
            bool found = false;
            foreach (Surface dir in directions)
            {
                if (dir == direction)
                {
                    found = true;
                }
            }
            Assert.IsFalse(found);
        }

        public class BoxelCases
        {
            public static IEnumerable AddSuccess
            {
                get
                {
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>()), Surface.Front);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>()), Surface.Back);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>()), Surface.Down);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>()), Surface.Up);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>()), Surface.Left);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>()), Surface.Right);
                }
            }

            public static IEnumerable RemoveSuccess
            {
                get
                {
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>() {Surface.Back, Surface.Front}), Surface.Front);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>() { Surface.Back}), Surface.Back);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>(){}), Surface.Down);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>(){}), Surface.Up);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>(){}), Surface.Left);
                    yield return new TestCaseData(new Boxel(new HashSet<Surface>() {}), Surface.Right);
                }
            }


        }
    }
}