﻿using NUnit.Framework;
using System.Collections;
using UnityEngine;
using DomEngine = Pixsaoul.Dom.Data.Engine;


namespace Pixsaoul.Tests.Editor.Inf.Persistency.Data.Engine
{
    [TestFixture]
    public class IdUT
    {
        [Test]
        [TestCaseSource(typeof(IdCases), "ToExportCases")]
        public void PersistBackAndForth(DomEngine.Id domId)
        {
            int outputPersistId = (int)domId;
            Assert.IsTrue(domId.Equals((DomEngine.Id)outputPersistId));
        }
    }

    public class IdCases
    {
        public static IEnumerable ToExportCases
        {
            get
            {
                int RandomGenerationCount = 10;
                for (int i = 0; i < RandomGenerationCount; i++)
                {
                    yield return new TestCaseData(Random.Range(0, 156126));
                }
            }
        }

    }
}
