﻿using NUnit.Framework;
using Pixsaoul.Inf.Persistency.Shape;
using DomData = Pixsaoul.Dom.Shape;
using System.Collections;
using System.Collections.Generic;
using Pixsaoul.Inf.Serialization;

namespace Pixsaoul.Tests.Editor.Inf.Persistency.BoxelGeneration.Surface
{
    [TestFixture]
    public class BoxelUT
    {

        [Test]
        [TestCaseSource(typeof(BoxelCases), "AddSuccess")]
        public void PersistBackAndForth(DomData.Boxel boxel)
        {
           
            Boxel persistBoxel = new Boxel(boxel);
            DomData.Boxel outBoxel = persistBoxel.ToDom();
            Assert.AreEqual(outBoxel.Directions.Count, boxel.Directions.Count);
            foreach (DomData.Surface direction in outBoxel.Directions)
            {
                Assert.IsTrue(boxel.Contains(direction)); // checking that everything is still here
            }
        }
        
        [Test]
        [TestCaseSource(typeof(BoxelCases), "EqualsChecker")]
        public void EqualityCheck(DomData.Boxel firstDom, DomData.Boxel secondDom)
        {
            Boxel first = new Boxel(firstDom);
            Boxel second = new Boxel(secondDom);
            Assert.IsTrue(first.Equals(second));
        }
    }

    public class BoxelCases
    {
        public static IEnumerable AddSuccess
        {
            get
            {
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>()));
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>() {DomData.Surface.Back, DomData.Surface.Front }));
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>() {DomData.Surface.Back, DomData.Surface.Back }));
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>() {DomData.Surface.Front, DomData.Surface.Back }));
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>() {DomData.Surface.Left, DomData.Surface.Right, DomData.Surface.Up, DomData.Surface.Down}));
            }
        }


        public static IEnumerable EqualsChecker
        {
            get
            {
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>()), new DomData.Boxel(new HashSet<DomData.Surface>()));
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>() { DomData.Surface.Back, DomData.Surface.Front }), new DomData.Boxel(new HashSet<DomData.Surface>() { DomData.Surface.Back, DomData.Surface.Front }));
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>() { DomData.Surface.Back, DomData.Surface.Back }), new DomData.Boxel(new HashSet<DomData.Surface>() { DomData.Surface.Back }));
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>() { DomData.Surface.Front, DomData.Surface.Back }), new DomData.Boxel(new HashSet<DomData.Surface>() { DomData.Surface.Back , DomData.Surface.Front}));
                yield return new TestCaseData(new DomData.Boxel(new HashSet<DomData.Surface>() { DomData.Surface.Left, DomData.Surface.Right, DomData.Surface.Up, DomData.Surface.Down }), new DomData.Boxel(new HashSet<DomData.Surface>() { DomData.Surface.Left, DomData.Surface.Right, DomData.Surface.Up, DomData.Surface.Down }));
            }
        }

    }
}
