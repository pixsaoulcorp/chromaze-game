﻿using NUnit.Framework;
using Pixsaoul.Inf.Persistency.Shape;
using DomData = Pixsaoul.Dom.Shape;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixsaoul.Inf.Serialization;

namespace Pixsaoul.Tests.Editor.Inf.Persistency.BoxelGeneration.Surface
{
    [TestFixture]
    public class ShapeUT
    {
        [Test]
        [TestCaseSource(typeof(ShapeCases), "ToExportCases")]
        public void PersistBackAndForth(DomData.BoxelShape shape)
        {           
            BoxelShape persistBoxel =new BoxelShape(shape);
            JsonSerializerService serializer = new JsonSerializerService();
            string seri = serializer.Serialize(persistBoxel);
            BoxelShape outputPersistBoxel = serializer.Deserialize<BoxelShape>(seri);            
            foreach(BoxelLocation boxelLocation in persistBoxel.Boxels)
            {
                bool found = false;
                for(int i = 0; i < outputPersistBoxel.Boxels.Count; i++ )
                {
                    if (boxelLocation.Equals(outputPersistBoxel.Boxels[i]))
                    {
                        found = true;
                    }
                }
                Assert.IsTrue(found);
            }
        }
    }

    public class ShapeCases
    {
        public static IEnumerable ToExportCases
        {
            get
            {
                int RandomGenerationCount = 10;
                for(int i = 0; i < RandomGenerationCount; i++)
                {
                    yield return new TestCaseData(CreateRandomShape());
                }
            }
        }

        private static DomData.BoxelShape CreateRandomShape()
        {
            int size = Random.Range(0, 100);
            DomData.BoxelShape shape = new DomData.BoxelShape();
            for(int i = 0; i < size; i++)
            {
                shape.SetBoxel(Random.Range(-1000, 1000), Random.Range(-1000, 1000), Random.Range(-1000, 1000), CreateRandomBoxel());
            }
            return shape;
        }

        private static DomData.Boxel CreateRandomBoxel()
        {
            HashSet<DomData.Surface> directions = new HashSet<DomData.Surface>();
            int size = Random.Range(0, 7);
            for(int i = 0; i < size; i++)
            {
                directions.Add((DomData.Surface)Random.Range(0, 7));
            }
            DomData.Boxel randomBoxel = new DomData.Boxel(directions);
            return randomBoxel;
        }

    }
}
