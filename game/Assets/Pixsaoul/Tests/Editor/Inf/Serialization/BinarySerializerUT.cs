﻿
using NUnit.Framework;
using Pixsaoul.Inf.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Pixsaoul.Tests.Editor.Inf.Serialization
{
    [TestFixture]
    public class BinarySerializerUT
    {
        [Test]
        [TestCaseSource(typeof(BinaryCases), "ToPersist")]
        public void PersistBackAndForth(SimpleData data)
        {
            BinarySerializer serializer = new BinarySerializer();
            serializer.Initialize();
            string serializedData = serializer.Serialize(data);
            SimpleData output = serializer.Deserialize<SimpleData>(serializedData);

            Assert.IsTrue(data.Toto == output.Toto);
            for(int i = 0; i < data.Names.Count; i++)
            {
                Assert.IsTrue(data.Names[i] == output.Names[i]);
            }
        }
    }

    [Serializable]
    public class SimpleData
    {
        public int Toto { get; set; }
        public List<string> Names { get; set; }

        public SimpleData()
        {

        }

        public SimpleData(int toto, List<string> names)
        {
            Toto = toto;
            Names = names;
        }

     
    }

    public class BinaryCases
    {
        public static IEnumerable ToPersist
        {
            get
            {
                yield return new TestCaseData(new SimpleData(3, new List<string>() {"treio", "dsq3" }));
                yield return new TestCaseData(new SimpleData(0, new List<string>() {"dsq3", "dsq3"," dskqlmqdsmldmsq" }));
                yield return new TestCaseData(new SimpleData(16232, new List<string>()));
                yield return new TestCaseData(new SimpleData(-5623, new List<string>() {"treio"}));
            }
        }



    }


}
