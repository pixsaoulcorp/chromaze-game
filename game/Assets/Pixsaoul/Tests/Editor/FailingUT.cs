﻿using NUnit.Framework;

namespace Pixsaoul.Tests.Dom
{
    [TestFixture]
    public class FailingUT
    {
        //Used to volontarily have a test that fails to test CI
        [Test]      
        public void FailingTest()
        {
            //Assert.Fail();
            Assert.IsTrue(true);
        }
    }
}