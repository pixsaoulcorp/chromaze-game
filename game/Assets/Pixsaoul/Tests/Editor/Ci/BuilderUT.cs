﻿using NUnit.Framework;
using Pixsaoul.Ci;
using System.Collections;

public class BuilderUT
{
    [Test]
    [TestCaseSource(typeof(BuilderCaseFactory), "NoExecutionMethodCases")]
    public void TestNoExecutionMethod(string[] parameters, int expectedArgumentCount)
    {        
        Assert.Throws<System.Exception>(()=>Builder.GetExecutionMethodParameters(parameters, expectedArgumentCount));
    }

    [TestCaseSource(typeof(BuilderCaseFactory), "CorectCommandLineArgs")]
    public void TestCorectArgumentsMethod(string[] parameters, int expectedArgumentCount)
    {
        Assert.IsTrue(Builder.GetExecutionMethodParameters(parameters, expectedArgumentCount).Length == expectedArgumentCount);
    }
}

public class BuilderCaseFactory
{
    public static IEnumerable NoExecutionMethodCases
    {
        get
        {
            yield return new TestCaseData(new string[] { "-batchmode", "-noHomo", "2019.3.2", "-", "executeMethod" }, 3);
            yield return new TestCaseData(new string[] { "execute", "Method", "Nothin", "98746", "Test" }, 3);
            yield return new TestCaseData(new string[] { "nothing", "to", "do", "here", "Test" }, 1);
            yield return new TestCaseData(new string[] { "nothing", "to", "do", "here", "Test" }, -6);
            yield return new TestCaseData(new string[] { "./test.xml", "abcd-executeMethod", "toto" }, 9);
            yield return new TestCaseData(new string[] { "./test.xml", "-executeMethod", "toto" }, 3);
            yield return new TestCaseData(new string[] { "./test.xml", "-executeMethod", "ComputeStuffMethod", "tat", "-newOption" }, 2);
            yield return new TestCaseData(new string[] { "./test.xml", "-executeMethod", "Argument", "-newOption", "nothing" }, 3);
            yield return new TestCaseData(new string[] { "./test.xml", "-executeMetDSKLMhod", "toto", "-executeMethod" }, 1);
            yield return new TestCaseData(new string[] { "-batchmode", "-noHomo", "2019.3.2", "-executeMethod" }, 0);
            //yield return new TestCaseData(new string[] { "./test.xml", "-executeMethod", "toto" }, 1);
        }
    }

    public static IEnumerable CorectCommandLineArgs
    {
        get
        {
            yield return new TestCaseData(new string[] { "./test.xml", "-executeMetDSKLMhod", "toto", "-executeMethod" ," too"}, 0);
            yield return new TestCaseData(new string[] { "./test.xml", "-executeMethod", "MethodName","toto" }, 1);
            yield return new TestCaseData(new string[] { "./test.xml", "-executeMethod", "Pixsaoul.BuildAndroid","toto", "tat", "-newOption" }, 2);
            //yield return new TestCaseData(new string[] { "./test.xml", "-executeMethod", "toto" }, 1);
        }
    }
}
