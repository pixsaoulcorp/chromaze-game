﻿using NUnit.Framework;
using Pixsaoul.Fou.Maths;
using System.Collections;

namespace Pixsaoul.Tests.Fou.Maths
{
    [TestFixture]
    public class OperatorUT
    {

        [Test]
        [TestCaseSource(typeof(ModCaseFactory), "FloatModCases")]
        public float FloatMod(float input, float mod)
        {           
            return Operator.Mod(input, mod);
        }
    }

    public class ModCaseFactory
    {
        public static IEnumerable FloatModCases
        {
            get
            {
                //yield return new TestCaseData(-598, 300).Returns(2); mod does not handle negative values as a real modulo, see differece % mod
                yield return new TestCaseData(0, 5).Returns(0);
                yield return new TestCaseData(100, 100).Returns(0);
            }
        }

    }
}
