﻿using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Engine;
using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using TMPro;
using UnityEngine;

namespace Pixsaoul.Pre.UserLog
{
    public class LogPanel : BaseBehaviour, ILogConsumer
    {
        [SerializeField] private TMP_Text _titleField;
        [SerializeField] private TMP_Text _contentField;

        [SerializeField] private CanvasGroup _rootGroup;

        [Header("Animation")]
        [SerializeField] private AnimationCurve _fadeCurve;
        [SerializeField] private float _fullFadeDuration;
        [SerializeField] private float _displayDuration;

        private WaitForEndOfFrame _endOfFrame;

        public IObservable<bool> CanConsumeObservable => _canConsume;
        public bool CanConsume => _canConsume.Value;
        private BehaviorSubject<bool> _canConsume;
        
        private Coroutine _fadeInRoutine;
        private Coroutine _displayRoutine;
        private Coroutine _fadeOutRoutine;
        public void Initialize()
        {
            _endOfFrame = new WaitForEndOfFrame();
            _canConsume = new BehaviorSubject<bool>(false);
        }

        public bool TrySetLog(Log log)
        {
            if (_canConsume.Value)
            {
                _canConsume.OnNext(false);
                StartCoroutine(FadeIn(log));
                return true;
            } 
            else
            {
                return false;
            }
        }

        private IEnumerator<WaitForEndOfFrame> FadeIn(Log log)
        {
            _titleField.text = log.Title;
            _contentField.text = log.Content;

            _rootGroup.gameObject.SetActive(true);
            _rootGroup.alpha = 0f;
            float startTime = Time.time;
            while (Time.time <= startTime + _fullFadeDuration)
            {
                float progress = (Time.time - startTime) / _fullFadeDuration;
                _rootGroup.alpha = _fadeCurve.Evaluate(progress);
                yield return _endOfFrame;
            }
            _displayRoutine = StartCoroutine(Display());
        }

        private IEnumerator<WaitForSeconds> Display()
        {
            yield return new WaitForSeconds(_displayDuration);
            _fadeOutRoutine = StartCoroutine(FadeOut());
        }

        private IEnumerator<WaitForEndOfFrame> FadeOut()
        {
            float startTime = Time.time;
            float startAlpha = _rootGroup.alpha; // we can start to fade out from ANY value
            // because it could be interrupted. Not implemented atm
            if(startAlpha > 0)
            {
                float fadeDuration = _fullFadeDuration / _rootGroup.alpha;
                while (Time.time <= startTime + fadeDuration)
                {
                    float progress = (Time.time - startTime) / fadeDuration;
                    _rootGroup.alpha = startAlpha * _fadeCurve.Evaluate(1 - progress);

                    yield return _endOfFrame;
                }
            }
            CloseDiplay();
        }

        public void CloseDiplay()
        {
            StopAllCoroutines();
            _rootGroup.alpha = 0f;
            _rootGroup.gameObject.SetActive(false);
            _canConsume.OnNext(true);
        }       
    }
}