﻿

using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs;

namespace Pixsaoul.Pre.UserLog
{
    public class UserLogMediator : Mediator
    {
        [Inject]
        public UserLogView View { get; set; }

        [Inject] public CreateLogSignal LogSignal { get; set; }

        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            base.OnRegister();
            LogSignal.AddListener(AddLog);
        }

        
        public override void OnRemove()
        {
            base.OnRemove();
            LogSignal.RemoveListener(AddLog);

        }

        private void AddLog(Log log)
        {
            View.AddLog(log);
        }
    }
}