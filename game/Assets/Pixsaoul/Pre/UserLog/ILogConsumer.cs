﻿using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Engine;
using System;

namespace Pixsaoul.Pre.UserLog
{
    [Serializable]
    public class ILogConsumerContainer : IUnifiedContainer<ILogConsumer> { }

    public interface ILogConsumer: IInitializable
    {
        IObservable<bool> CanConsumeObservable { get; }

        bool CanConsume { get; }

        /// Only works if canConsume
        bool TrySetLog(Log log);

        void CloseDiplay();
    }
}
