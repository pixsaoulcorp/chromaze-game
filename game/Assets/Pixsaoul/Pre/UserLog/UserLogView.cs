﻿using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs;
using System.Collections.Generic;
using System.Reactive;
using UnityEngine;
using UnityEngine.UI;

namespace Pixsaoul.Pre.UserLog
{
    public class UserLogView : View
    {
        [SerializeField] private ILogConsumerContainer _panel;
        [SerializeField] private Button _closeLogButton;

        // runtime internal values
        private List<Log> _buffer;

        public override void Initialize()
        {
            base.Initialize();
            _panel.Result.Initialize();
            _panel.Result.CanConsumeObservable.Subscribe(Observer.Create<bool>(OnPanelAvailable));            
            _closeLogButton.onClick.AddListener(CloseCurrentLog);
            _buffer = new List<Log>();
            _panel.Result.CloseDiplay();
        }

        private void OnPanelAvailable(bool available)
        {
            if (available)
            {
                TryConsumeNextLog();
            }
        }

        public void AddLog(Log log)
        {
            _buffer.Add(log);
            TryConsumeNextLog();
        }

        public void CloseCurrentLog()
        {
            _panel.Result.CloseDiplay();
        }

        private void TryConsumeNextLog()
        {            
            if (_buffer.Count > 0)
            {
                if (_panel.Result.CanConsume)
                {
                    Log log = _buffer[0];
                    if (_panel.Result.TrySetLog(_buffer[0]))
                    {
                        _buffer.RemoveAt(0);
                    }
                }                
            }
        }
    }
}
