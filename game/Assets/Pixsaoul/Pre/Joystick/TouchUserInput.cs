﻿using Pixsaoul.Fou.Engine;
using Pixsaoul.Inf.UserInput;
using System;
using System.Reactive.Subjects;
using UnityEngine;

namespace Pixsaoul.Pre.Joystick
{
    public class TouchUserInput : BaseBehaviour, IInputProvider
    {
        [SerializeField] private GameObject _root;
        [SerializeField] private TouchStick _mainAxisSitck;
        [SerializeField] private TouchStick _secondaryAxisStick;

        private BehaviorSubject<Vector2> _mainAxisSubject;
        private BehaviorSubject<Vector2> _secondaryAxisSubject;        
        private BehaviorSubject<int> _mainActionSubject;
        private BehaviorSubject<int> _secondaryActionSubject;

        public IObservable<Vector2> MainAxis => _mainAxisSubject;
        public IObservable<Vector2> SecondaryAxis => _secondaryAxisSubject;
        public IObservable<int> MainAction => _mainActionSubject;
        public IObservable<int> SecondaryAction => _secondaryActionSubject;

        public void Disable()
        {
            _root.GetComponent<CanvasGroup>().alpha = 0;
            _root.GetComponent<CanvasGroup>().interactable = false;
        }

        public void Enable()
        {
            _root.GetComponent<CanvasGroup>().alpha = 1;
            _root.GetComponent<CanvasGroup>().interactable = true;
        }

        public void Initialize()
        {
            _root.SetActive(true);
            _mainAxisSubject = new BehaviorSubject<Vector2>(Vector2.zero);
            _secondaryAxisSubject = new BehaviorSubject<Vector2>(Vector2.zero);

            _mainActionSubject = new BehaviorSubject<int>(-1);
            _secondaryActionSubject = new BehaviorSubject<int>(-1);

            _mainAxisSitck.Initialize((Vector2 v)=>_mainAxisSubject.OnNext(v), ()=>_mainActionSubject.OnNext(0));
            _secondaryAxisStick.Initialize((Vector2 v) => _secondaryAxisSubject.OnNext(v), () => _secondaryActionSubject.OnNext(0));
        }
    }
}