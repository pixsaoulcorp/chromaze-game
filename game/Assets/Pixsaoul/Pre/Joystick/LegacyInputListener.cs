﻿using Pixsaoul.Fou.Engine;
using Pixsaoul.Inf.UserInput;
using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using UnityEngine;
//using UnityEngine.InputSystem;
//using static Pixsaoul.Inf.UserInput.MinimalistMapAsset;

namespace Pixsaoul.Pre.UserInput
{
    [Obsolete]
    public class LegacyInputListener : BaseBehaviour, IInputProvider//, IMinimalistMapActions
    {
        //private MinimalistMapAsset _minimalistMap; //TODO TBU user new input system
        private Coroutine _axisCoroutine;
        private Coroutine _buttonCoroutine;

        [SerializeField] private float _mainScale;
        [SerializeField] private float _secondaryScale;

        private BehaviorSubject<Vector2> _mainAxisSubject;
        private BehaviorSubject<Vector2> _secondaryAxisSubject;
        private BehaviorSubject<int> _mainActionSubject;
        private BehaviorSubject<int> _secondaryActionSubject;

        public IObservable<Vector2> MainAxis => _mainAxisSubject;
        public IObservable<Vector2> SecondaryAxis => _secondaryAxisSubject;
        public IObservable<int> MainAction => _mainActionSubject;
        public IObservable<int> SecondaryAction => _secondaryActionSubject;

        private bool _enabled = false;

        public void Initialize()
        {
            _mainAxisSubject = new BehaviorSubject<Vector2>(Vector2.zero);
            _secondaryAxisSubject = new BehaviorSubject<Vector2>(Vector2.zero);

            _mainActionSubject = new BehaviorSubject<int>(-1);
            _secondaryActionSubject = new BehaviorSubject<int>(-1);

            //_minimalistMap = new MinimalistMapAsset();
            //_minimalistMap.MinimalistMap.SetCallbacks(this);
            _axisCoroutine = StartCoroutine(ListenAxisInputsC());
            _buttonCoroutine = StartCoroutine(ListenButtonsInputC());
        }

        public void Enable()
        {
            _enabled = true;
        }

        public void Disable()
        {
            _enabled = false;
        }

        private IEnumerator<WaitForFixedUpdate> ListenAxisInputsC()
        {
            while (true)
            {
                if (_enabled)
                {
                    ListenAxisInput();
                }
                yield return new WaitForFixedUpdate();
            }
        }

        private IEnumerator<float> ListenButtonsInputC()
        {
            while (true)
            {
                if (_enabled)
                {
                    ListenButtonInput();
                }
                yield return 0;
            }
        }

        private void ListenAxisInput()
        {
            // Internal inputs for moving character
            float forwardInput = Input.GetAxis("Vertical");
            float lateralInput = Input.GetAxis("Horizontal");

            //TODO dispatch input
            if (forwardInput != 0 || lateralInput != 0)
            { // TODO change this test to ignore dead zone from input
                Vector2 input = _mainScale * new Vector2(lateralInput, forwardInput);
                _mainAxisSubject.OnNext(input);
            }

            if (Input.GetMouseButton(1))
            {
                float forwardInputSecondary = Input.GetAxis("Mouse Y");
                float lateralInputSecondary = Input.GetAxis("Mouse X");

                //TODO dispatch input
                if (forwardInputSecondary != 0 || lateralInputSecondary != 0)
                { // TODO change this test to ignore dead zone from input
                    Vector2 input = _secondaryScale * new Vector2(lateralInputSecondary, forwardInputSecondary);
                    _secondaryAxisSubject.OnNext(input);
                }
            }
        }

        private void ListenButtonInput()
        {
            if (Input.GetButtonUp("MainButton"))
            {
                _mainActionSubject.OnNext(0);
            }
            if (Input.GetButtonUp("SecondaryButton"))
            {
                _secondaryActionSubject.OnNext(0);
            }
        }


    }
}