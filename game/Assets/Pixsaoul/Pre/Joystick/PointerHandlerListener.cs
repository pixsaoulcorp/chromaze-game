﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Pixsaoul.Pre.Joystick {

    /// <summary>
    /// Allows to call drag up and down method on other handler on top of this one.
    /// Usefull for having 2 behaviour when interacting with a panel.
    /// </summary>
    public class PointerHandlerListener : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
    {
        IDragHandler _dragHandler;
        IPointerUpHandler _upHandler;
        IPointerDownHandler _downHandler;

        public void Initialize(IDragHandler dragHandler, IPointerUpHandler upHandler, IPointerDownHandler downHandler)
        {
            _dragHandler = dragHandler;
            _downHandler = downHandler;
            _upHandler = upHandler;
        }

        public void OnDrag(PointerEventData eventData)
        {
            _dragHandler.OnDrag(eventData);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _downHandler.OnPointerDown(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _upHandler.OnPointerUp(eventData);
        }
    }
}
