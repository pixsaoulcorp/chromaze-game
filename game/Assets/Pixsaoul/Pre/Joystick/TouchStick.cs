﻿
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Pixsaoul.Pre.Joystick
{
    public class TouchStick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
    {

        /* Hypothesis : the joystick area must be streched (user a parent container)
         * 
         * 
         */
        [SerializeField] private Image _joystickArea;
        [SerializeField] private Image _stick;

        [SerializeField] private PointerHandlerListener _middleButtonPointer;
        [SerializeField] private Button _middleButton;


        private Vector2 _currentInputValue;
        private Coroutine _handle;

        private Action<Vector2> _inputCallback;
        private Action _buttonCallback;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputCallback"></param>
        public void Initialize(Action<Vector2> inputCallback, Action buttonCallback)
        {
            _middleButtonPointer.Initialize(this, this, this);
            _buttonCallback = buttonCallback;
            _inputCallback = inputCallback;
            StartCoroutine(GenerateInputRoutine());
        }

        private IEnumerator<WaitForEndOfFrame> GenerateInputRoutine()
        {
            while (true)
            {
                if (_currentInputValue != Vector2.zero)
                {
                    if (_inputCallback != null)
                    {
                        _inputCallback.Invoke(_currentInputValue);
                    }
                }
                yield return new WaitForEndOfFrame();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(this.gameObject, eventData);
            Vector2 touchPos = Vector2.zero;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_joystickArea.rectTransform,
                eventData.position,
                eventData.pressEventCamera,
                out touchPos))
            {

                ProcessCursorPosition(touchPos);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
           // OnDrag(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            //TODO back to center in animation routine
            Vector2 output = Vector2.zero;
            ProcessCursorPosition(output);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="touchPos"></param>
        private void ProcessCursorPosition(Vector2 touchPos)
        {
            Vector2 output = Vector2.zero;

            output.x = touchPos.x * 2f / _joystickArea.rectTransform.rect.width; // size delta is the full width of the rectangle, we want half of it
            output.y = touchPos.y * 2f / _joystickArea.rectTransform.rect.height;

            if (output.magnitude > 1)
            {
                output.Normalize();
            }
            _stick.rectTransform.anchoredPosition = new Vector2(output.x * _joystickArea.rectTransform.rect.width / 2f, output.y * _joystickArea.rectTransform.rect.height / 2f);
            _currentInputValue = output;
        }

        public void OnMiddleButtonClicked()
        {
            _buttonCallback?.Invoke();
        }
    }
}
