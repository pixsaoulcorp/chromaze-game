using Pixsaoul.Fou.Mvcs;
using System.Collections;
using UnityEngine;

namespace Pixsaoul.Pre.SceneTransition
{
    public class SceneTransitionView : View
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private AnimationCurve _fadeInCurve;
        [SerializeField] private float _fadeDuration;

        private float _lastKnownFadeProgress;

        public Signal FadedInDone = new Signal();
        public Signal FadedOutDone = new Signal();


        public override void Initialize()
        {
            base.Initialize();
            ForceFadeState(false); // TODO TBU change this to true at beginning
        }

        public void FadeIn()
        {
            StopAllCoroutines();
            StartCoroutine(FadeInC(true));
        }

        public void FadeOut()
        {
            StopAllCoroutines();
            StartCoroutine(FadeInC(false));

        }

        private IEnumerator FadeInC(bool fadeIn)
        {
            _canvasGroup.interactable = false;
            float startTime = Time.time;
            float initialFade = _lastKnownFadeProgress;
            while (fadeIn?(_lastKnownFadeProgress <= 1f):(_lastKnownFadeProgress >= 0f))
            {                
                SetAlpha(initialFade + (fadeIn ? 1 : -1) * (Time.time - startTime)/_fadeDuration);
                yield return null;
            }
            FinalizeFade(fadeIn);
        }

        private void ForceFadeState(bool visible)
        {
            SetAlpha(visible ? 1 : 0);
            FinalizeFade(visible);
        }

        private void SetAlpha(float value)
        {
            _canvasGroup.alpha = _fadeInCurve.Evaluate(value);
            _lastKnownFadeProgress = value;
           
        }

        private void FinalizeFade(bool fadeIn)
        {
            if (!fadeIn)
            {
                _canvasGroup.interactable = true;
            }
            else
            {
                _canvasGroup.interactable = false;
            }

            if (fadeIn)
            {
                FadedInDone.Dispatch();
            }
            else
            {
                FadedOutDone.Dispatch();
            }
        }
    }
}