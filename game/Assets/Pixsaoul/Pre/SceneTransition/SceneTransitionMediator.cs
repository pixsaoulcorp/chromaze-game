using Pixsaoul.App.SceneManagement;
using Pixsaoul.App.SceneTransition;
using Pixsaoul.Dom.Data;
using Pixsaoul.Fou.Mvcs;
using UnityEngine;

namespace Pixsaoul.Pre.SceneTransition
{
    public class SceneTransitionMediator : Mediator
    {
        [Inject]
        public SceneTransitionView View { get; set; }

        //TODO get real signals from the scenemanagement service

        [Inject] public RequestFadeInSignal FadeinSignal{ get; set; }
        [Inject] public RequestFadeOutSignal FadeoutSignal { get; set; }

        [Inject] public OnFadedOutSignal OnFadedOutSignal { get; set; }
        [Inject] public OnFadedInSignal OnFadedInSignal { get; set; }

        //[Inject] public OnSceneChangedSignal OnSceneChangedSignal { get; set; }
        
        protected override View GetView()
        {
            return View;
        }

        public override void OnRegister()
        {
            FadeinSignal.AddListener(FadeIn);
            FadeoutSignal.AddListener(FadeOut);
            //OnSceneChangedSignal.AddListener((SceneId id) => FadeOut());
            View.FadedInDone.AddListener(OnFadedIn);
            View.FadedOutDone.AddListener(OnFadedOut);
            base.OnRegister();
        }

        public override void OnRemove()
        {
            base.OnRemove();
            FadeinSignal.RemoveListener(FadeIn);
            FadeoutSignal.RemoveListener(FadeOut);
            //OnSceneChangedSignal.RemoveListener((SceneId id)=>FadeOut());
            View.FadedInDone.RemoveListener(OnFadedIn);
            View.FadedOutDone.RemoveListener(OnFadedOut);
        }


        private void FadeIn()
        {
            View.FadeIn();
        }

        private void FadeOut()
        {
            View.FadeOut();
        }

        private void OnFadedIn()
        {
            OnFadedInSignal.Dispatch();
            Debug.Log("FadedIn finished");
        }

        private void OnFadedOut()
        {
            OnFadedOutSignal.Dispatch();
            Debug.Log("FadedOut finished");
        }

        private void Update()
        {
            // TEST
            //if (Input.GetKeyDown(KeyCode.I))
            //{
            //    FadeIn();
            //}
            //if (Input.GetKeyDown(KeyCode.O))
            //{
            //    FadeOut();
            //}
        }
    }
}