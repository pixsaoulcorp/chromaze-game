﻿using System;


namespace Pixsaoul.Pre.Menu
{
    public class NamedButtonList : DataElementList<Pixsaoul.Dom.Data.Engine.Name, NamedButton>
    {
        private Action<Pixsaoul.Dom.Data.Engine.Name> _callback;

        public void SetButtonCallback(Action<Dom.Data.Engine.Name> callback)
        {
            _callback = callback;
        }

        protected override NamedButton CreateElement(Dom.Data.Engine.Name data)
        {
            NamedButton button = base.CreateElement(data);
            button.Initialize(data, _callback);
            return button;
        }
    }
}
