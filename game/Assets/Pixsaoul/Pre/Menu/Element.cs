﻿using Pixsaoul.Fou.Engine;

namespace Pixsaoul.Pre.Menu
{
    public class Element<T> : BaseBehaviour
    {
        private T _data;
        protected T Data { get => _data; }

        public virtual void Initialize(T data)
        {
            _data = data;
        }
    }
}
