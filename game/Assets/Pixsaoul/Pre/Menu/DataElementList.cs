﻿using Pixsaoul.Fou.Engine;
using Pixsaoul.Fou.Extensions;
using System.Collections.Generic;
using UnityEngine;

namespace Pixsaoul.Pre.Menu
{
    public class DataElementList<T,U> : BaseBehaviour where U:Element<T>
    {
        // T is the type of data in input, U the type of element associated
        private IDictionary<T, U> _managedElements;
        [SerializeField] private Transform _elementContainer;
        [SerializeField] private U _elementPrefab;

        public virtual void Initialize()
        {
            _managedElements = new Dictionary<T, U>();
        }

        public virtual void UpdateElements(ICollection<T> datas)
        {
            // first implementation, no optimization
            _managedElements.Clear();
            _elementContainer.DeleteChildren();

            foreach(T data in datas)
            {
                CreateElement(data);
            }
        }

        protected virtual U CreateElement(T data)
        {
            U element = Instantiate<U>(_elementPrefab);
            element.transform.SetParent(_elementContainer, false);
            element.Initialize(data);
            _managedElements.Add(data, element);
            return element;
        }        
    }
}
