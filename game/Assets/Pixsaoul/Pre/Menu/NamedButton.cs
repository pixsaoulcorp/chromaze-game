﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace Pixsaoul.Pre.Menu
{
    public class NamedButton : Element<Pixsaoul.Dom.Data.Engine.Name>
    {
        [SerializeField] private UnityEngine.UI.Button _button;
        [SerializeField] private TMP_Text _nameField;

        private Action<Dom.Data.Engine.Name> _selectionCallback;

        public void Initialize(Dom.Data.Engine.Name name, Action<Dom.Data.Engine.Name> selectCallback)
        {
            base.Initialize(name);
            _nameField.text = name;
            _selectionCallback = selectCallback;
            _button.onClick.AddListener(Select);
        }

        public void Select()
        {
            _selectionCallback.Invoke(Data);
        }

        public void SetSelectionCallback(Action<Dom.Data.Engine.Name> selectCallback)
        {
            _selectionCallback = selectCallback;
        }
    }
}