﻿using Pixsaoul.Fou.Engine;
using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using TMPro;
using UnityEngine;

namespace Pixsaoul.Pre.DataEditor
{
    public class ColorEditor : BaseBehaviour, IDataEditorT<Chromaze.Dom.Data.Color>
    {
        [SerializeField] private TMP_Dropdown _dropdownSelector;

        private Dictionary<int, Chromaze.Dom.Data.Color> _colorMapping = new Dictionary<int, Chromaze.Dom.Data.Color>()
        {
            {0,Chromaze.Dom.Data.Color.BlackC },
            {1,Chromaze.Dom.Data.Color.RedC },
            {2,Chromaze.Dom.Data.Color.GreenC },
            {3,Chromaze.Dom.Data.Color.BlueC },
            {4,Chromaze.Dom.Data.Color.YellowC },
            {5,Chromaze.Dom.Data.Color.MagentaC },
            {6,Chromaze.Dom.Data.Color.CyanC },
            {7,Chromaze.Dom.Data.Color.WhiteC }
        };

        private BehaviorSubject<Chromaze.Dom.Data.Color> _dataSubject; // Don't use field
        private BehaviorSubject<Chromaze.Dom.Data.Color> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Chromaze.Dom.Data.Color>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Chromaze.Dom.Data.Color> ObservableData => DataSubject;

        public void Initialize()
        {
            _dropdownSelector.onValueChanged.AddListener((int value) => OnChanged());
            SetData(new Chromaze.Dom.Data.Color());
        }

        public Chromaze.Dom.Data.Color GetData()
        {
            int colorId = _dropdownSelector.value;
            Chromaze.Dom.Data.Color color;

            if(_colorMapping.TryGetValue(colorId, out color))
            {
                return color;
            } else
            {
                throw new NotImplementedException();
            }

        }

        public void SetData(Chromaze.Dom.Data.Color color)
        {
            _dropdownSelector.value = GetColorId(color);
        }
        
        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        private int GetColorId(Chromaze.Dom.Data.Color color)
        {
            foreach (KeyValuePair<int, Chromaze.Dom.Data.Color> kvp in _colorMapping)
            {
                if (kvp.Value.Equals(color))
                {
                    return kvp.Key;
                }
            }
            throw new InvalidCastException("Could not get id of color.");
        }
    }
}
