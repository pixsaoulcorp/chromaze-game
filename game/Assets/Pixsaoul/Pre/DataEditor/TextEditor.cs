﻿using System;
using System.Reactive.Subjects;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pixsaoul.Pre.DataEditor
{
    public class TextEditor : BaseBehaviour, IDataEditorT<Pixsaoul.Dom.Text>
    {
        [SerializeField] private TMP_InputField _field;

        private BehaviorSubject<Pixsaoul.Dom.Text> _dataSubject; // Don't use field
        private BehaviorSubject<Pixsaoul.Dom.Text> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Pixsaoul.Dom.Text>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Pixsaoul.Dom.Text> ObservableData => DataSubject;

        public void Initialize()
        {
            _field.onEndEdit.AddListener((string value) => OnChanged()); // register everything that should update on change
            SetData(Pixsaoul.Dom.Text.Empty);
        }

        public Pixsaoul.Dom.Text GetData()
        {
            return (Pixsaoul.Dom.Text)_field.text;
        }

        public void SetData(Pixsaoul.Dom.Text data)
        {
            _field.text = data.Value.ToString();
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

    }
}
