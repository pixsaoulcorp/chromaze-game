﻿using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using System;
using System.Reactive.Subjects;
using UnityEngine;

namespace Pixsaoul.Pre.DataEditor
{
    public class OrientationEditor : BaseBehaviour, IDataEditorT<Orientation>
    {
        //TODO TBU create prefab for it
        [SerializeField] private AngleEditor _yawField;
        [SerializeField] private AngleEditor _pitchField;

            private BehaviorSubject<Orientation> _dataSubject; // Don't use field
            private BehaviorSubject<Orientation> DataSubject
            {
                get
                {
                    if (_dataSubject == null)
                    {
                        _dataSubject = new BehaviorSubject<Orientation>(GetData());
                    }
                    return _dataSubject;
                }
            }
            public IObservable<Orientation> ObservableData => DataSubject;

            public void Initialize()
        {
            _yawField.Initialize();
            _pitchField.Initialize();

            _yawField.ObservableData.Subscribe((Angle o) => OnChanged());
            _pitchField.ObservableData.Subscribe((Angle o) => OnChanged());
            SetData(new Orientation());
        }

        public Dom.Data.Engine.Orientation GetData()
        {
            Angle yaw = _yawField.GetData();
            Angle pitch = _pitchField.GetData();
            return new Orientation(yaw, pitch);
        }

        public void SetData(Orientation rotation)
        {
            _yawField.SetData(rotation.yaw);
            _pitchField.SetData(rotation.pitch);
        }
        
        private void OnChanged()
        {
                DataSubject.OnNext(GetData());
        }
    }
}
