﻿using System;
using System.Reactive.Subjects;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using UnityEngine;
using UnityEngine.UI;

namespace Pixsaoul.Pre.DataEditor
{
    public class SnappedDimensionEditor : BaseBehaviour,IDataEditorT<SnappedDimension>
    {
        [SerializeField] private SnappedLengthEditor _widthField;
        [SerializeField] private SnappedLengthEditor _heightField;
        [SerializeField] private SnappedLengthEditor _depthField;

        private BehaviorSubject<SnappedDimension> _dataSubject; // Don't use field
        private BehaviorSubject<SnappedDimension> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<SnappedDimension>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<SnappedDimension> ObservableData => DataSubject;

        public void Initialize()
        {
            _widthField.Initialize();
            _heightField.Initialize();
            _depthField.Initialize();

            _widthField.ObservableData.Subscribe((SnappedLength o) => OnChanged());
            _heightField.ObservableData.Subscribe((SnappedLength o) => OnChanged());
            _depthField.ObservableData.Subscribe((SnappedLength o) => OnChanged());
        }

        public void SetData(SnappedDimension data)
        {
            _widthField.SetData(data.Width);
            _heightField.SetData(data.Height);
            _depthField.SetData(data.Depth);
        }

        public Dom.Data.Engine.SnappedDimension GetData()
        {
            SnappedLength width = _widthField.GetData();
            SnappedLength height = _heightField.GetData();
            SnappedLength depth = _depthField.GetData();
            return new SnappedDimension(width, height, depth);
        }
        
        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }
    }
}