﻿using System;
using System.Reactive.Subjects;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pixsaoul.Pre.DataEditor
{
    public class SnappedAngleEditor : BaseBehaviour, IDataEditorT<Dom.Data.Engine.SnappedAngle>
    {
        [SerializeField] private TMP_InputField _field;
        [SerializeField] private Button _incrementButton;
        [SerializeField] private Button _decrementButton;

        private BehaviorSubject<SnappedAngle> _dataSubject; // Don't use field
        private BehaviorSubject<SnappedAngle> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<SnappedAngle>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<SnappedAngle> ObservableData => DataSubject;

        public void Initialize()
        {
            _field.onEndEdit.AddListener((string value) => OnChanged());
            _incrementButton.onClick.AddListener(Increment);
            _decrementButton.onClick.AddListener(Decrement);
            SetData(new SnappedAngle());
        }

        public SnappedAngle GetData()
        {
            return (SnappedAngle)int.Parse(_field.text);
        }

        public void SetData(SnappedAngle data)
        {
            _field.text = data.Value.ToString();
        }
        
        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        private void Increment()
        {
            SetData(new SnappedAngle(GetData().Value + 1));
            OnChanged();
        }

        private void Decrement()
        {
            SetData(new SnappedAngle(GetData().Value - 1));
            OnChanged();
        }
    }
}
