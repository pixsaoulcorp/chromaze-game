﻿using System;
using System.Reactive.Subjects;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pixsaoul.Pre.DataEditor
{
    public class IntensityEditor : BaseBehaviour, IDataEditorT<Intensity>
    {
        [SerializeField] private TMP_InputField _field;
        [SerializeField] private Button _incrementButton;
        [SerializeField] private Button _decrementButton;

        private BehaviorSubject<Intensity> _dataSubject; // Don't use field
        private BehaviorSubject<Intensity> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Intensity>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Intensity> ObservableData => DataSubject;

        public void Initialize()
        {
            _field.onEndEdit.AddListener((string value) => OnChanged()); // TODO TBU maybe change this ?
            _incrementButton.onClick.AddListener(Increment);
            _decrementButton.onClick.AddListener(Decrement);
            SetData(new Intensity());
        }

        public Intensity GetData()
        {
            return (Intensity)float.Parse(_field.text);
        }

        public void SetData(Intensity data)
        {
            _field.text = data.Value.ToString();
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        private void Increment()
        {
            SetData(new Intensity(GetData().Value + 1));
            OnChanged();
        }

        private void Decrement()
        {
            SetData(new Intensity(GetData().Value - 1));
            OnChanged();
        }
    }
}
