﻿using Assets.Pixsaoul.App.Engine;

namespace Pixsaoul.Pre.DataEditor
{
    public class IDataEditorContainer : IUnifiedContainer<IDataEditor> { }

    public interface IDataEditor
    { 
    }
}
