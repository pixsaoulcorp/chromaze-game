﻿using System;
using System.Reactive.Subjects;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pixsaoul.Pre.DataEditor
{
    public class SnappedLengthEditor : BaseBehaviour, IDataEditorT<Dom.Data.Engine.SnappedLength>
    {
        [SerializeField] private TMP_InputField _field;
        [SerializeField] private Button _incrementButton;
        [SerializeField] private Button _decrementButton;

        private BehaviorSubject<SnappedLength> _dataSubject; // Don't use field
        private BehaviorSubject<SnappedLength> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<SnappedLength>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<SnappedLength> ObservableData => DataSubject;

        public SnappedLength GetData()
        {
            return (SnappedLength)int.Parse(_field.text);
        }

        public void SetData(SnappedLength length)
        {
            _field.text = length.Value.ToString();
        }

        public void Initialize()
        {
            _field.onEndEdit.AddListener((string value)=>OnChanged());
            _incrementButton.onClick.AddListener(Increment);
            _decrementButton.onClick.AddListener(Decrement);
            SetData(new SnappedLength());
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        private void Increment()
        {
            SetData(new SnappedLength(GetData().Value + 1));
            OnChanged();
        }

        private void Decrement()
        {
            SetData(new SnappedLength(GetData().Value - 1));
            OnChanged();
        }
    }
}
