﻿using System;
using System.Reactive.Subjects;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pixsaoul.Pre.DataEditor
{
    public class AngleEditor : BaseBehaviour, IDataEditorT<Dom.Data.Engine.Angle>
    {
        [SerializeField] private TMP_InputField _field;
        [SerializeField] private Button _incrementButton;
        [SerializeField] private Button _decrementButton;        

        private BehaviorSubject<Angle> _dataSubject; // Don't use field
        private BehaviorSubject<Angle> DataSubject
        {
            get
            {
                if(_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<Angle>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<Angle> ObservableData => DataSubject;

        public void Initialize()
        {
            _field.onEndEdit.AddListener((string value) => OnChanged()); // TODO TBU maybe change this ?
            _incrementButton.onClick.AddListener(Increment);
            _decrementButton.onClick.AddListener(Decrement);
            SetData(new Angle());
        }

        public Angle GetData()
        {
            return (Angle)float.Parse(_field.text);
        }

        public void SetData(Angle data)
        {
            _field.text = data.Value.ToString();
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        private void Increment()
        {
            SetData(new Angle(GetData().Value + 1));
            OnChanged();
        }

        private void Decrement()
        {
            SetData(new Angle(GetData().Value - 1));
            OnChanged();
        }
    }
}
