﻿using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using System;
using System.Reactive.Subjects;
using UnityEngine;

namespace Pixsaoul.Pre.DataEditor {
    public class SnappedRotationEditor : BaseBehaviour, IDataEditorT<SnappedRotation>
    {
        //TODO TBU create prefab for it
        [SerializeField] private SnappedAngleEditor _yawField;
        [SerializeField] private SnappedAngleEditor _pitchField;
        [SerializeField] private SnappedAngleEditor _rollField;

        private BehaviorSubject<SnappedRotation> _dataSubject; // Don't use field
        private BehaviorSubject<SnappedRotation> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<SnappedRotation>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<SnappedRotation> ObservableData => DataSubject;

        public void Initialize()
        {
            _yawField.Initialize();
            _pitchField.Initialize();
            _rollField.Initialize();

            _yawField.ObservableData.Subscribe((SnappedAngle o) => OnChanged());
            _pitchField.ObservableData.Subscribe((SnappedAngle o) => OnChanged());
            _rollField.ObservableData.Subscribe((SnappedAngle o) => OnChanged());
            SetData(new SnappedRotation());
        }

        public Dom.Data.Engine.SnappedRotation GetData()
        {
            SnappedAngle yaw = _yawField.GetData();
            SnappedAngle pitch = _pitchField.GetData();
            SnappedAngle roll = _rollField.GetData();
            return new SnappedRotation(yaw, pitch, roll);
        }

        public void SetData(SnappedRotation rotation)
        {
            _yawField.SetData(rotation.yaw);
            _pitchField.SetData(rotation.pitch);
            _rollField.SetData(rotation.roll);
        }
        
        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }
    }
}
