﻿using System;
using System.Reactive.Subjects;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pixsaoul.Pre.DataEditor
{
    public class SnappedOffsetEditor : BaseBehaviour, IDataEditorT<Dom.Data.Engine.SnappedOffset>
    {
        [SerializeField] private TMP_InputField _field;
        [SerializeField] private Button _incrementButton;
        [SerializeField] private Button _decrementButton;

        private BehaviorSubject<SnappedOffset> _dataSubject; // Don't use field
        private BehaviorSubject<SnappedOffset> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<SnappedOffset>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<SnappedOffset> ObservableData => DataSubject;

        public void Initialize()
        {
            _field.onEndEdit.AddListener((string value) => OnChanged()); // register everything that should update on change
            _incrementButton.onClick.AddListener(Increment);
            _decrementButton.onClick.AddListener(Decrement);
            SetData(new SnappedOffset());
        }

        public SnappedOffset GetData()
        {
            return (SnappedOffset)int.Parse(_field.text);
        }

        public void SetData(SnappedOffset data)
        {
            _field.text = data.Value.ToString();
        }
        
        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }

        private void Increment()
        {
            SetData(new SnappedOffset(GetData().Value + 1));
            OnChanged();
        }

        private void Decrement()
        {
            SetData(new SnappedOffset(GetData().Value - 1));
            OnChanged();
        }
    }
}
