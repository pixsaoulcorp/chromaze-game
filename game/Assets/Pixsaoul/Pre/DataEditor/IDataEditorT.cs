﻿
using Pixsaoul.App.Engine;
using Pixsaoul.Fou.Engine;

namespace Pixsaoul.Pre.DataEditor
{
    public interface IDataEditorT<T> : IDataEditor, IProvider<T>, IInitializable
    {
        T GetData();
        void SetData(T data);
    }
}