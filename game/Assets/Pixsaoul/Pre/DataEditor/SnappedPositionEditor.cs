﻿using System;
using System.Reactive.Subjects;
using Pixsaoul.Dom.Data.Engine;
using Pixsaoul.Fou.Engine;
using UnityEngine;

namespace Pixsaoul.Pre.DataEditor
{
    public class SnappedPositionEditor : BaseBehaviour, IDataEditorT<SnappedPosition>
    {
        [SerializeField] private SnappedOffsetEditor _xField;
        [SerializeField] private SnappedOffsetEditor _yField;
        [SerializeField] private SnappedOffsetEditor _zField;

        private BehaviorSubject<SnappedPosition> _dataSubject; // Don't use field
        private BehaviorSubject<SnappedPosition> DataSubject
        {
            get
            {
                if (_dataSubject == null)
                {
                    _dataSubject = new BehaviorSubject<SnappedPosition>(GetData());
                }
                return _dataSubject;
            }
        }
        public IObservable<SnappedPosition> ObservableData => DataSubject;

        public void Initialize()
        {
            _xField.Initialize();
            _yField.Initialize();
            _zField.Initialize();

            _xField.ObservableData.Subscribe((SnappedOffset o) => OnChanged());
            _yField.ObservableData.Subscribe((SnappedOffset o) => OnChanged());
            _zField.ObservableData.Subscribe((SnappedOffset o) => OnChanged());
            SetData(new SnappedPosition());
        }

        public Dom.Data.Engine.SnappedPosition GetData()
        {
            SnappedOffset x = _xField.GetData();
            SnappedOffset y = _yField.GetData();
            SnappedOffset z = _zField.GetData();
            return new SnappedPosition(x, y, z);
        }

        public void SetData(SnappedPosition position)
        {
            _xField.SetData(position.X);
            _yField.SetData(position.Y);
            _zField.SetData(position.Z);
        }

        private void OnChanged()
        {
            DataSubject.OnNext(GetData());
        }
    }
}