﻿namespace Pixsaoul.Fou.Mvcs
{
    public class Signal : strange.extensions.signal.impl.Signal
    {
    }

    public class Signal<T> : strange.extensions.signal.impl.Signal<T>
    {
    }
    public class Signal<T,U> : strange.extensions.signal.impl.Signal<T,U>
    {
    }

    public class Signal<T, U, V> : strange.extensions.signal.impl.Signal<T, U, V>
    {
    }

    public class Signal<T, U, V, W> : strange.extensions.signal.impl.Signal<T, U, V, W>
    {
    }
}
