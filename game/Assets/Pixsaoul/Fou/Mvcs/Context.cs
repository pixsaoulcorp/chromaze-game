﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.

using Chromaze.App.SceneWorkflow;
using Chromaze.Inf.Level.Use;
using Pixsaoul.App.Advertising;
using Pixsaoul.App.Persistency;
using Pixsaoul.App.Serialization;
using Pixsaoul.App.UserLog;
using Pixsaoul.Fou.Mvcs.Signals;
using Pixsaoul.Inf.Advertising;
using Pixsaoul.Inf.Persistency;
using Pixsaoul.Inf.Persistency.Remote;
using Pixsaoul.Inf.Serialization;
using Pixsaoul.Pre.UserLog;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using System;
using UnityEngine;

namespace Pixsaoul.Fou.Mvcs
{
    public abstract class Context : MVCSContext
    {
        public Context(MonoBehaviour view) : base(view)
        {
        }

        public Context(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }
        protected override void addCoreComponents()
        {
            base.addCoreComponents();
            injectionBinder.Unbind<ICommandBinder>();
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
        }

        protected override void mapBindings()
        {
            // Services injection
            BindServices();

            // Models injection
            BindModels();

            // View / Mediator bindings : bind views to their dedicated mediators
            BindMediations();

            // Command binding : bind events to controllers command
            BindCommands();

            // Bind starting point
            BindApplicationInitializationCycle();
        }

        protected virtual void BindServices()
        {
            GameObject contextViewObject = (GameObject)contextView;
            ServiceReferencer referencer = contextViewObject.GetComponent<ServiceReferencer>();

            if (referencer != null)
            {
                referencer.ApplyBindingCallback(BindService);
            }
            else
            {
                Debug.LogError("Injection binding impossible, ContextView might be missing a Referencer component.");
            }

            injectionBinder.Bind<ILevelUserService>().To<LevelUserService>().ToSingleton();
            injectionBinder.Bind<IPersistencyService>().To<InternalPersistencyService>().ToSingleton().ToName(PersistencyType.Internal);
            injectionBinder.Bind<IFilePersistencyService>().To<LocalPersistencyService>().ToSingleton().ToName(PersistencyType.Local);

            injectionBinder.Bind<ISerializerService>().To<JsonSerializerService>().ToSingleton();
            injectionBinder.Bind<ISerializerService>().To<BinarySerializer>().ToSingleton().ToName(SerializationType.Binary);

            injectionBinder.Bind<IAdvertisingService>().To<SimpleAdsService>().ToSingleton();
        }

        private void BindService(Type interfaceType, IService implementation)
        {
            bool implements = interfaceType.IsAssignableFrom(implementation.GetType());
            if (implements)
            {
                injectionBinder.Bind(interfaceType).ToValue(implementation).ToSingleton().CrossContext();
            }
            else
            {
                Debug.Log("Error in Service binding : " + implementation.GetType().ToString() + " does not implements " + interfaceType.ToString());
            }
        }


        protected virtual void BindModels()
        {
            //injectionBinder.Bind<IModel> ().To<Model> ().ToSingleton ();

        }

        protected virtual void BindMediations()
        {
            // Game Views
            //mediationBinder.Bind<SampleView>().To<SampleMediator>();
            mediationBinder.Bind<UserLogView>().To<UserLogMediator>();
        }


        protected virtual void BindCommands()
        {
            // commandBinder.Bind(LevelManagementEvent.REQUEST_LEVEL_SELECTION).To<RequestLevelSelectionCommand>();
            injectionBinder.Bind<CreateLogSignal>().ToSingleton();

        }

        private void BindApplicationInitializationCycle()
        {
           // commandBinder.Bind<SomeSignal>().To<SomeCommand>();
            commandBinder.Bind<StartSignal>().To<OnBindingEndedCommand>().Once();
            commandBinder.Bind<InitializeServiceSignal>().To(GetInitializeServiceCommand()).Once();
            commandBinder.Bind<InitializeModelSignal>().To(GetInitializeModelCommand()).Once();
            commandBinder.Bind<OnContextInitializeSignal>().To(GetOnInitializedCommand()).Once();
     }

        #region Initialize comand process binding
        override public void Launch()
        {
            base.Launch();
            //Make sure you've mapped this to a StartCommand!
            StartSignal startSignal = (StartSignal)injectionBinder.GetInstance<StartSignal>();
            startSignal.Dispatch();
        }

        protected virtual Type GetInitializeServiceCommand()
        {
            return typeof(InitializeServiceCommand);
        }

        protected virtual Type GetInitializeModelCommand()
        {
            return typeof(InitializeModelCommand);
        }

        protected virtual Type GetOnInitializedCommand()
        {
            return typeof(OnInitializedCommand);
        }

        #endregion Initialize comand process binding
    }
}