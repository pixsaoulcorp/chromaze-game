﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.

namespace Pixsaoul.Fou.Mvcs
{
    public abstract class Mediator : strange.extensions.mediation.impl.Mediator
    {
        /// <summary>
        /// Gets the procedure view.
        /// </summary>
        /// <returns></returns>
        protected abstract View GetView();

        /// <summary>
        /// Add here listener for signal you want. (you need to inject them first).
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            GetView().InitializeSignal.AddListener(Initialize);
        }

        /// <summary>
        /// destructor for Mediator. Should Remove listener for all signal, not only those from register.
        /// </summary>
        public override void OnRemove()
        {
            GetView().InitializeSignal.RemoveListener(Initialize);
            base.OnRemove();
        }

        // only start communicating with the rest of the app after this initialize.
        public virtual void Initialize()
        {
        }
        
    }
}