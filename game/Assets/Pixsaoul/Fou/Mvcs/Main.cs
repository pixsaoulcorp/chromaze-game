﻿// * Copyright [2016] [thibaud BOURDEAU]
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
using strange.extensions.context.impl;
using System;
using UnityEngine;

namespace Pixsaoul.Fou.Mvcs
{
    [RequireComponent(typeof(ServiceReferencer))]
    /// <summary>
    /// Inherit from this class if you want to instantiate a context
    /// </summary>
    public abstract class Main<T> : ContextView where T : Context
    {
        /// <summary>
        ///
        /// </summary>
        private void Awake()
        {
            context = CreateContext();
        }

        private T InstantiateObject(params object[] args)
        {
            return (T)Activator.CreateInstance(typeof(T), args);
        }

        private Context CreateContext()
        {
            return InstantiateObject(this);
        }
    }
}