﻿using Pixsaoul.Fou.Engine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using System;
using UnityEngine;

namespace Pixsaoul.Fou.Mvcs
{
    public class ContextViewProxy : BaseBehaviour, IContextView
    {
        private IContextView _proxiedContextView;

        private IContextView TryGetProxy()
        {
            if(_proxiedContextView == null)
            {
                _proxiedContextView = GameObject.FindObjectOfType<ContextView>();
            }
            return _proxiedContextView;
        }

        public IContext context
        {
            get => TryGetProxy().context;
            set => TryGetProxy().context = value;
        }
        public bool requiresContext
        {
            get => TryGetProxy().requiresContext;
            set => TryGetProxy().requiresContext = value;
        }

        public bool registeredWithContext
        {
            get => TryGetProxy().registeredWithContext;
            set => TryGetProxy().registeredWithContext = value;
        }

        public bool autoRegisterWithContext => TryGetProxy().autoRegisterWithContext;

        public new bool enabled => TryGetProxy().enabled;

        public bool shouldRegister => TryGetProxy().shouldRegister;
    }
}
