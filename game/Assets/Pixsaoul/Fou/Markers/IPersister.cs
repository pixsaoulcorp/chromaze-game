﻿namespace Pixsaoul.Fou.Markers
{
    public interface IPersister<T>
    {
        public T ToDom();
    }
}
