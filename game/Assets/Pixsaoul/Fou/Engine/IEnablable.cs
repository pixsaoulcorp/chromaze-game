﻿namespace Pixsaoul.Fou.Engine
{
    public interface IEnablable
    {
        void Enable();
        void Disable();
    }
}
