﻿using UnityEngine;

namespace Pixsaoul.Fou.Engine
{
    /// <summary>
    /// Base Behaviour
    /// </summary>
    /// <seealso cref="UnityEngine.MonoBehaviour" />
    public class BaseBehaviour : MonoBehaviour
    {
    }
}