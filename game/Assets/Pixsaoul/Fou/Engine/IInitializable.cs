﻿
namespace Pixsaoul.Fou.Engine
{
    [System.Serializable]
    public class IInitializableContainer : IUnifiedContainer<IInitializable> { }

    public interface IInitializable
    {
        void Initialize();
    }

}
