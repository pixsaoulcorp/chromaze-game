﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pixsaoul.Dom.Data
{
    public class EndPoint : IEquatable<EndPoint>
    {
        private List<string> _points;

        public EndPoint()
        {
            _points = new List<string>();
        }

        public EndPoint(List<string> points)
        {
            _points = points;
        }

        public EndPoint(string point)
        {
            _points = new List<string>() { point };
        }

        public bool Equals(EndPoint other)
        {
            return other.ToString().Equals(ToString()); // TODO TBU not use to string for equality
        }

        public static EndPoint Concat(EndPoint left, EndPoint right)
        {
            EndPoint newEndpoint = new EndPoint();
            for(int i = 0; i < left._points.Count; i++)
            {
                newEndpoint._points.Add(left._points[i]);
            }
            for (int i = 0; i < right._points.Count; i++)
            {
                newEndpoint._points.Add(right._points[i]);
            }
            return newEndpoint;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < _points.Count; i++)
            {
                if(i == 0)
                {
                    builder.Append(_points[0]);
                } else
                {
                    builder.Append("/" + _points[i]);
                }
            }
            return builder.ToString();
        }
    }
}
