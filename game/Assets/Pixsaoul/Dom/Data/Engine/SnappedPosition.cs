﻿using System;
using UnityEngine;

namespace Pixsaoul.Dom.Data.Engine
{
    public class SnappedPosition : IEquatable<SnappedPosition>
    {
        public SnappedPosition()
        {
            X=0;
            Y=0;
            Z=0;
        }
        public SnappedPosition(SnappedOffset x, SnappedOffset y, SnappedOffset z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public SnappedOffset X { get; set; }
        public SnappedOffset Y { get; set; }
        public SnappedOffset Z { get; set; }

        public static implicit operator Vector3(SnappedPosition pos)
        {
            return new Vector3(pos.X, pos.Y, pos.Z);
        }

        public static SnappedPosition SnapPosition(Position pos)
        {
            return new SnappedPosition(SnappedOffset.Snap(pos.x), SnappedOffset.Snap(pos.y), SnappedOffset.Snap(pos.z));
        }

        public override string ToString()
        {
            return $@"x: {X.ToString()} /y: {Y.ToString()} /z: {Z.ToString()}";
        }

        public bool Equals(SnappedPosition other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y) && Z.Equals(other.Z);
        }
    }
}
