﻿
using System;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct SnappedAngle : IEquatable<SnappedAngle>
    {
        private static Angle StepAngle = new Angle(90);

        private int _value;

        public SnappedAngle(int stepCount = 0)
        {
            _value = stepCount;
        }

        public SnappedAngle(Angle angle)
        {
            _value = (int)(angle / StepAngle);
        }

        public static implicit operator SnappedAngle(int value)
        {
            return new SnappedAngle(value%(360/(int)StepAngle)); //just to be sure we are between 0 and acceptable angle and without duplicate
        }

        public static implicit operator int(SnappedAngle value)
        {
            return value._value;
        }

        public int Value { get => _value; set => _value = value; }

        public Angle ToAngle()
        {
            return new Angle((float)StepAngle * _value);
        }

        public override string ToString()
        {
            return _value.ToString();
        }

        public bool Equals(SnappedAngle other)
        {
            return Value.Equals(other.Value);
        }
    }
}
