﻿
using System;
using Pixsaoul.Fou.Markers;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct Length : IValueObject, IEquatable<Length>
    {
        #region DATA 
        private float _value;

        #endregion

        #region CTORS

        public Length(float value)
        {
            _value = value;
        }

        #endregion

        public float Value => _value;

        public bool SameValueAs(Length other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator float(Length type)
        {
            return type.Value;
        }

        public static implicit operator Length(float value)
        {
            return new Length(value);
        }

        public static bool operator ==(Length left, Length right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Length left, Length right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(Length other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is Length other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        #endregion

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}

