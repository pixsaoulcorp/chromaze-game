﻿using Pixsaoul.Fou.Markers;
using System;
using Pixsaoul.Fou.Maths;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct Angle : IValueObject, IEquatable<Angle>
    {
        #region DATA

        private float _value;

        #endregion

        #region CTORS

        public Angle(float value)
        {
            _value = Operator.Mod(value,360);
        }        

        #endregion

        public float Value => _value;

        public bool SameValueAs(Angle other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator float(Angle type)
        {
            return type.Value;
        }

        public static implicit operator Angle(float value)
        {
            return new Angle(value);
        }

        public static bool operator ==(Angle left, Angle right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Angle left, Angle right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(Angle other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is Angle other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
        #endregion

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}
