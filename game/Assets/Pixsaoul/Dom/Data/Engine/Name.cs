﻿
using System;
using Pixsaoul.Fou.Markers;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct Name : IValueObject, IEquatable<Name>
    {

        #region CTORS

        public Name(string value)
        {
            Value = value;
        }

        #endregion

        public string Value { get; set; }

        public bool SameValueAs(Name other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator string(Name type)
        {
            return type.Value;
        }

        public static implicit operator Name(string value)
        {
            return new Name(value);
        }

        public static bool operator ==(Name left, Name right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Name left, Name right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(Name other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is Name other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        #endregion

        public static Name Empty = new Name("");

        public bool IsEmpty()
        {
            return this.Equals(Empty);
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}

