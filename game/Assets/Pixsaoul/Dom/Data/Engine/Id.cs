﻿
using System;
using Pixsaoul.Fou.Markers;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct Id
        : IValueObject, IEquatable<Id>
    {
        #region DATA

        private int _value;

        #endregion

        #region CTORS

        public Id(int value)
        {
            _value = value;
        }
        #endregion

        public int Value => _value;

        public bool SameValueAs(Id other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator int(Id type)
        {
            return type.Value;
        }

        public static implicit operator Id(int value)
        {
            return new Id(value);
        }

        public static bool operator ==(Id left, Id right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Id left, Id right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(Id other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is Id other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        #endregion

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}

