﻿
using System;
using Pixsaoul.Fou.Markers;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct SnappedLength : IValueObject, IEquatable<SnappedLength>
    {

        #region CTORS

        public SnappedLength(int value)
        {
            Value = value;
        }

        #endregion

        public int Value { get; set; }

        public bool SameValueAs(SnappedLength other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator int(SnappedLength type)
        {
            return type.Value;
        }

        public static implicit operator SnappedLength(int value)
        {
            return new SnappedLength(value);
        }

        public static bool operator ==(SnappedLength left, SnappedLength right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(SnappedLength left, SnappedLength right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(SnappedLength other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is SnappedLength other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        #endregion

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}

