using Pixsaoul.Fou.Markers;
using System;
using UnityEngine;

public struct Intensity : IValueObject, IEquatable<Intensity>
{
    #region DATA

    private float _value;

    #endregion

    #region CTORS

    public Intensity(float value)
    {
        _value = Mathf.Clamp(value,0,100);
    }

    #endregion

    public float Value => _value;

    public bool SameValueAs(Intensity other)
    {
        return Equals(other);
    }

    #region OPERATORS

    public static implicit operator float(Intensity type)
    {
        return type.Value;
    }

    public static implicit operator Intensity(float value)
    {
        return new Intensity(value);
    }

    public static bool operator ==(Intensity left, Intensity right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(Intensity left, Intensity right)
    {
        return !left.Equals(right);
    }

    #endregion OPERATORS

    #region Equality members

    public bool Equals(Intensity other)
    {
        return Value == other.Value;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }

        return obj is Intensity other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _value.GetHashCode();
    }
    #endregion

    public override string ToString()
    {
        return _value.ToString();
    }
}
