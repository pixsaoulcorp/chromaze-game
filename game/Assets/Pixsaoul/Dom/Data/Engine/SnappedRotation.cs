﻿using System;
using UnityEngine;

namespace Pixsaoul.Dom.Data.Engine
{
    public class SnappedRotation : IEquatable<SnappedRotation>
    {
        private SnappedAngle _yaw;
        private SnappedAngle _pitch;
        private SnappedAngle _roll;

        public SnappedRotation()
        {
            _yaw = new SnappedAngle();
            _pitch = new SnappedAngle();
            _roll = new SnappedAngle();
        }

        public SnappedRotation(SnappedAngle yaw, SnappedAngle pitch, SnappedAngle roll)
        {
            _yaw = yaw;
            _pitch = pitch;
            _roll = roll;
        }

        public Vector3 ToEulerAngle()
        {
            return new Vector3(pitch.ToAngle(), yaw.ToAngle(), roll.ToAngle());
        }

        public SnappedAngle yaw { get => _yaw; set => _yaw = value; }
        public SnappedAngle pitch { get => _pitch; set => _pitch = value; }
        public SnappedAngle roll { get => _roll; set => _roll = value; }

        public override string ToString()
        {
            return $@"yaw: {_yaw.ToString()} / pitch: {_pitch.ToString()} / roll: {_roll.ToString()}";
        }

        public bool Equals(SnappedRotation other)
        {
            return yaw.Equals(other.yaw) && pitch.Equals(other.pitch) && roll.Equals(other.roll);
        }
    }
}
