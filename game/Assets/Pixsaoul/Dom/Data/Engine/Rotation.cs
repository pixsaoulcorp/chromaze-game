﻿using System;
using UnityEngine;

namespace Pixsaoul.Dom.Data.Engine
{
    public class Rotation : IEquatable<Rotation>
    {
        private Angle _yaw;
        private Angle _pitch;
        private Angle _roll;

        public Rotation()
        {
            _yaw = new Angle();
            _pitch = new Angle();
            _roll = new Angle();
        }

        public Rotation(Angle yaw, Angle pitch, Angle roll)
        {
            _yaw = yaw;
            _pitch = pitch;
            _roll = roll;
        }

        public Rotation(Vector3 euler)
        {
            _yaw = (Angle) euler.x;
            _pitch = (Angle) euler.y;
            _roll = (Angle) euler.z;
        }

        public Angle yaw { get => _yaw; set => _yaw = value; }
        public Angle pitch { get => _pitch; set => _pitch = value; }
        public Angle roll { get => _roll; set => _roll = value; }


        public Vector3 ToEuler()
        { // TODO TBU investigate real use of this method. maybe should not be in dom.
            return new Vector3(_yaw, _pitch, _roll);
        }

        public override string ToString()
        {
            return $@"yaw: {_yaw.ToString()} / pitch: {_pitch.ToString()} / roll: {_roll.ToString()}";
        }

        public bool Equals(Rotation other)
        {
            return yaw.Equals(other.yaw) && pitch.Equals(other.pitch) && roll.Equals(other.roll);
        }
    }
}
