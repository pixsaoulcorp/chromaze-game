﻿

using System;
using Pixsaoul.Fou.Markers;
using UnityEngine;

namespace Pixsaoul.Dom.Data.Engine
{
    public class SnappedDimension : IEquatable<SnappedDimension>, IValueObject
    {

        public SnappedDimension()
        {
            Width = 0;
            Height = 0;
            Depth  = 0;
        }

        #region CTORS
        public SnappedDimension(SnappedLength width, SnappedLength height, SnappedLength depth)
        {
            Width = width;
            Height = height;
            Depth = depth;
        }

        #endregion

        public SnappedLength Width { get; set; }
        public SnappedLength Height { get; set; }
        public SnappedLength Depth { get; set; }

        public bool SameValueAs(SnappedDimension other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator SnappedDimension(Vector3 size)
        {
            return new SnappedDimension((SnappedLength)size.x, (SnappedLength)size.y, (SnappedLength)size.z);
        }

        public static implicit operator Vector3(SnappedDimension size)
        {
            return new Vector3(size.Width, size.Height, size.Depth);
        }
        #endregion OPERATORS

        public override string ToString()
        {
            return $@"width: {Width.ToString()} /height: {Height.ToString()} /depth: {Depth.ToString()}";
        }

        public bool Equals(SnappedDimension other)
        {
            return Width.Equals(other.Width) && Height.Equals(other.Height) && Depth.Equals(other.Depth);
        }
    }
}

