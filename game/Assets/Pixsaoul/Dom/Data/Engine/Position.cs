﻿using System;
using UnityEngine;

namespace Pixsaoul.Dom.Data.Engine
{
    public class Position : IEquatable<Position>
    {
        private Offset _x;
        private Offset _y;
        private Offset _z;

        public Offset x { get => _x; set => _x = value; }
        public Offset y { get => _y; set => _y = value; }
        public Offset z { get => _z; set => _z = value; }

        public Position()
        {
            _x = 0;
            _y = 0;
            _z = 0;
        }

        public Position(Offset x, Offset y, Offset z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        public static implicit operator Position(Vector3 value)
        {
            return new Position((Offset)value.x, (Offset)value.y, (Offset)value.z);
        }

        public static implicit operator Vector3(Position data)
        {
            return new Vector3(data._x, data._y, data._z) ;
        }

        public static Position operator +(Position a, Position b)
       => new Position((Offset)(a.x + b.x), (Offset)(a.y + b.y), (Offset)(a.z + b.z));
        public static Position operator *(Position a, Position b)
       => new Position((Offset)(a.x * b.x), (Offset)(a.y * b.y), (Offset)(a.z * b.z));
        public static Position operator *(Position a, float b)
       => new Position((Offset)(a.x * b), (Offset)(a.y * b), (Offset)(a.z * b));

        public override string ToString()
        {
            return $@"x: {_x.ToString()} /y: {_y.ToString()} /z: {_z.ToString()}";
        }

        public bool Equals(Position other)
        {
            return x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z);
        }
    }
}
