﻿using Pixsaoul.Fou.Markers;
using System;
using UnityEngine;

namespace Pixsaoul.Dom.Data.Engine
{
    public class Orientation :  IEquatable<Orientation>, IValueObject
    {
        #region DATA

        private Angle _yaw;
        private Angle _pitch;

        #endregion

        #region CTORS
        public Orientation()
        {
            _yaw = new Angle();
            _pitch = new Angle();
        }

        public Orientation(Vector3 euler)
        {
            _yaw = (Angle)euler.x;
            _pitch = (Angle)euler.y;
        }

        public Orientation(Angle yaw, Angle pitch)
        {
            _yaw = yaw;
            _pitch = pitch;
        }

        #endregion

        public Angle yaw => _yaw;
        public Angle pitch => _pitch;

        public Vector3 ToEuler()
        {
            return new Vector3(yaw, pitch, 0);
        }

        public bool SameValueAs(Orientation other)
        {
            return Equals(other);
        }

        public override string ToString()
        {
            return $@"yaw: {_yaw.ToString()} / pitch: {_pitch.ToString()}";
        }

        public bool Equals(Orientation other)
        {
            return yaw.Equals(other.yaw) && pitch.Equals(other.pitch);
        }
    }
}

