﻿
using System;
using Pixsaoul.Fou.Markers;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct Offset : IValueObject, IEquatable<Offset>
    {
        #region DATA

        private float _value;

        #endregion

        #region CTORS

        public Offset(float value)
        {
            _value = value;
        }

        #endregion

        public float Value => _value;

        public bool SameValueAs(Offset other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator float(Offset type)
        {
            return type.Value;
        }

        public static implicit operator Offset(float value)
        {
            return new Offset(value);
        }

        public static bool operator ==(Offset left, Offset right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Offset left, Offset right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(Offset other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is Offset other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        #endregion

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}

