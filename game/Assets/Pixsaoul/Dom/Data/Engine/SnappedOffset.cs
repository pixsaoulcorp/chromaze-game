﻿
using System;
using Pixsaoul.Fou.Markers;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct SnappedOffset : IValueObject, IEquatable<SnappedOffset>
    {
        #region DATA

        private int _value;

        #endregion

        #region CTORS

        public SnappedOffset(int value)
        {
            _value = value;
        }

        #endregion

        public int Value => _value;

        public bool SameValueAs(SnappedOffset other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator int(SnappedOffset type)
        {
            return type.Value;
        }

        public static implicit operator SnappedOffset(int value)
        {
            return new SnappedOffset(value);
        }

        public static bool operator ==(SnappedOffset left, SnappedOffset right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(SnappedOffset left, SnappedOffset right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(SnappedOffset other)
        {
            return Value.Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is SnappedOffset other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        #endregion

        public static SnappedOffset Snap(Offset offset)
        {
            return new SnappedOffset((int)offset);
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}

