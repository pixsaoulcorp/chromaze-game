﻿
using System;
using Pixsaoul.Fou.Markers;

namespace Pixsaoul.Dom.Data
{
    public struct SceneId : IValueObject, IEquatable<SceneId>
    {
        #region DATA

        private string _value;

        #endregion

        #region CTORS

        public SceneId(string value)
        {
            _value = value;
        }

        public static SceneId Play = new SceneId("Play");
        public static SceneId Menu = new SceneId("Menu");
        public static SceneId Create = new SceneId("Creator");

        #endregion

        public string Value => _value;

        public bool SameValueAs(SceneId other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator string(SceneId type)
        {
            return type.Value;
        }

        public static implicit operator SceneId(string value)
        {
            return new SceneId(value);
        }

        public static bool operator ==(SceneId left, SceneId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(SceneId left, SceneId right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(SceneId other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is SceneId other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        #endregion
    }
}

