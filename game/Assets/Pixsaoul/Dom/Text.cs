﻿
using System;
using Pixsaoul.Fou.Markers;

namespace Pixsaoul.Dom
{
    public struct Text : IValueObject, IEquatable<Text>
    {
        #region DATA

        private string _value;

        #endregion

        #region CTORS

        public Text(string value = "")
        {
            _value = value;
        }

        #endregion

        public string Value => _value;

        public bool SameValueAs(Text other)
        {
            return Equals(other);
        }

        #region OPERATORS

        public static implicit operator string(Text type)
        {
            return type.Value;
        }

        public static implicit operator Text(string value)
        {
            return new Text(value);
        }

        public static bool operator ==(Text left, Text right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Text left, Text right)
        {
            return !left.Equals(right);
        }

        #endregion OPERATORS

        #region Equality members

        public bool Equals(Text other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is Text other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        #endregion

        public static Text Empty = new Text("");

        public bool IsEmpty()
        {
            return this.Equals(Empty);
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}

