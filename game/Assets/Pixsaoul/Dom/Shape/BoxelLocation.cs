﻿using System;
using UnityEngine;

namespace Pixsaoul.Dom.Data.Engine
{
    public struct BoxelLocation : IEquatable<BoxelLocation>
    {
        private int _x;
        private int _y;
        private int _z;

        public BoxelLocation(int x, int y, int z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        public int x { get => _x; set => _x = value; }
        public int y { get => _y; set => _y = value; }
        public int z { get => _z; set => _z = value; }

        public override string ToString()
        {
            return $@"x: {_x.ToString()} /y: {_y.ToString()} /z: {_z.ToString()}";
        }

        public bool Equals(BoxelLocation other)
        {
            return x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z);
        }

        public Vector3 ToVector3()
        {
            return new Vector3(x, y, z);
        }

        public override int GetHashCode() => HashCode.Combine(x, y,z);
    }
}
