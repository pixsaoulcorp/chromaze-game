﻿using System;
using System.Collections.Generic;

namespace Pixsaoul.Dom.Shape
{
    /// <summary>
    /// In this implementation, a boxel is defined only as some surfaces. Location of a boxel is 
    /// </summary>
    public class Boxel : IEquatable<Boxel>
    {
        private HashSet<Surface> _directions;

        public Boxel()
        {
            _directions = new HashSet<Surface>();
        }

        public Boxel(HashSet<Surface> surfaces)
        {
            _directions = surfaces;
        }

        public HashSet<Surface> Directions { get => _directions; }

        public void AddSurface(Surface direction)
        {
            _directions.Add(direction);
        }

        public void RemoveDirection(Surface direction)
        {
            _directions.Remove(direction);
        }

        public bool Contains(Surface direction)
        {
            return _directions.Contains(direction);
        }

        public bool Equals(Boxel other)
        {
            bool equals = Directions.Count == other.Directions.Count;
            IEnumerator<Surface> enumerator = Directions.GetEnumerator();
            while(enumerator.MoveNext())
            {
                Surface surface = enumerator.Current;
                equals &= other.Directions.Contains(surface);
            }
            return equals;
        }
    }
}
