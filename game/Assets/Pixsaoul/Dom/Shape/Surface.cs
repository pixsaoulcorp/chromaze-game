﻿
namespace Pixsaoul.Dom.Shape
{
    public enum Surface
    {
        /*
         *       up
         * left      right
         *      down
         *      
         *      top view:
         *      
         *       Front
         * Left         Right
         *       Back
         */

        Up = 0,
        Down,
        Left,
        Right,
        Back,
        Front
    }
}
