﻿using Pixsaoul.Dom.Data.Engine;
using System;
using System.Collections.Generic;

namespace Pixsaoul.Dom.Shape
{
    public class BoxelShape : IBoxelShape, IEquatable<BoxelShape>
    {
        private Dictionary<BoxelLocation, Boxel> _boxels;
        private SurfaceMode _surfaceMode;

        public BoxelShape(SurfaceMode surfaceMode = SurfaceMode.Extern)
        {
            _boxels = new Dictionary<BoxelLocation, Boxel>();
            _surfaceMode = surfaceMode;
        }

        public Dictionary<BoxelLocation,Boxel> Boxels { get => _boxels; }
        public SurfaceMode SurfaceMode { get => _surfaceMode; }

        public bool TryGetBoxel(int x, int y, int z, out Boxel boxel)
        {
            BoxelLocation pos = new BoxelLocation(x, y, z);
            return _boxels.TryGetValue(pos, out boxel);           
        }

        public void SetBoxel(int x, int y, int z, Boxel boxel)
        {
            if(_boxels.ContainsKey(new BoxelLocation(x, y, z)))
            {
                _boxels[new BoxelLocation(x, y, z)] = boxel;
            } 
            else
            {
                _boxels.Add(new BoxelLocation(x, y, z), boxel);
            }
           
        }

        public bool Equals(BoxelShape other)
        {
            bool equals = SurfaceMode.Equals(other.SurfaceMode);
            equals &= Boxels.Count == other.Boxels.Count;
            foreach(var kvp in Boxels)
            {
                Boxel otherBoxel;
                equals &= other.Boxels.TryGetValue(kvp.Key, out otherBoxel);
                if(otherBoxel != null)
                {
                    equals &= otherBoxel.Equals(kvp.Value);
                }
            }
            return equals;
        }
    }
}
