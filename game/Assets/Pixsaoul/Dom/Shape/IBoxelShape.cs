﻿using Pixsaoul.Dom.Data.Engine;
using System.Collections.Generic;

namespace Pixsaoul.Dom.Shape
{
    public interface IBoxelShape {

        Dictionary<BoxelLocation, Boxel> Boxels { get; }

        SurfaceMode SurfaceMode { get; }

        bool TryGetBoxel(int x, int y, int z, out Boxel boxel);

        void SetBoxel(int x, int y, int z, Boxel boxel);
    }
}
