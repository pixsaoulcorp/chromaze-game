# Chromaze.game

Latest build available in https://drive.google.com/drive/folders/1_t8cReUfPfu94hG9Z3QfsWS3VxXuESpO?usp=sharing
Discord of the project : https://discord.gg/RmYyWn Don't hesitate to come here for help about level creation, trying out some home made levels and share your own creations !

(temp solution for storing builds)
available in Android and windows standalone.

# Status
![TestCI](https://github.com/pixsaoul/Chromaze.game2/workflows/TestCI/badge.svg)

![BuildCI](https://github.com/pixsaoul/Chromaze.game2/workflows/BuildCI/badge.svg)

![PackageCI](https://github.com/pixsaoul/Chromaze.game/workflows/PackageCI/badge.svg)
 
 

# How to install

## Windows
copy the build your want to try, for example 0.1.110564433 and paste it wherever you want. You can then start the game by launching Chromaze.exe.
There is currently no complete installer.

## Android
Available on Google Play Store.

# How to share levels
You can use the in game Online panel to look for a level (search button). Currently levels are only stored for a small duration online. Use the online to send a level to a friend nothing else. 
To upload a level, you can use the Creator menu, select your level and "Upload".

Later on, there will be a real persistent way to upload level so that you can challenge anyone with your best creations.

# issues

the best way to contact me is by creating an issue here. If somehow this is not really related issue material, contact
me at Pixsaoul.Chromaze@gmail.com

# contribution

Right now this project is only used for research purpose and training purpose, so I don't really need / want any particuliar help on how to solve some issues. However I would love some feedback on the current state of the game, issues with it and ideas you may have, so that I can focus on useful research ;)
